<?php

namespace app\Http\Controllers\Barang;

use app\Models\mBarang;
use app\Models\mBarangHargaJualHistory;
use app\Models\mBarangHistory;
use app\Models\mJenisBarang;
use app\Models\mSatuan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Barang extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['master_barang'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }

        $filter_component = Main::date_filter($request, ['kode_barang', 'nama_barang', 'download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $kode_barang = $filter_component['kode_barang'];
        $nama_barang = $filter_component['nama_barang'];


        $data = Main::data($this->breadcrumb);
        $data_list = mBarang
            ::with([
                'jenis_barang'
            ])
            ->orderBy('brg_kode', 'ASC')
            ->get();
        $jenis_barang = mJenisBarang::orderBy('jbr_nama', 'ASC')->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $datatable_column = [
            ["data" => "no"],
            ["data" => "brg_kode"],
            ["data" => "brg_nama"],
            ["data" => "jbr_nama"],
            ["data" => "stn_nama"],
            ["data" => "brg_harga_beli"],
            ["data" => "brg_harga_jual"],
//            ["data" => "brg_golongan"],
            ["data" => "brg_minimal_stok"],
            ["data" => "total_stok"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'jenis_barang' => $jenis_barang,
            'satuan' => $satuan,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'kode_barang' => $kode_barang,
                'nama_barang' => $nama_barang,
            ),
        ]);

        return view('barang/barang/barangList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $kode_barang = $data_post['kode_barang'];
        $nama_barang = $data_post['nama_barang'];

        $total_data = mBarang
            ::whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'brg_kode'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mBarang
            ::with([
                'jenis_barang',
                'satuan'
            ])
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang = Main::encrypt($row->id_barang);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['brg_kode'] = $row->brg_kode;
            $nestedData['brg_nama'] = $row->brg_nama;
            $nestedData['jbr_nama'] = $row->jenis_barang['jbr_nama'];
            $nestedData['stn_nama'] = $row->satuan['stn_nama'];
            $nestedData['row'] = $row;
            $nestedData['brg_harga_beli'] = Main::format_number($row->brg_harga_beli);
            $nestedData['brg_harga_jual'] = Main::format_number($row->brg_harga_jual);
//            $nestedData['brg_golongan'] = Main::barang_golongan_label($row->brg_golongan);
            $nestedData['brg_minimal_stok'] = Main::format_number($row->brg_minimal_stok);
            $nestedData['total_stok'] = Main::format_number($row->total_stok);
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                         <span class="m--hide">Stok barang di hide, karena tidak ada perbedaan penjualan barang, semuanya sama</span>
                        <a class="akses-action_wait_done dropdown-item"
                           href="' . route('stokBarangList', ['id_barang' => $id_barang]) . '">
                            <i class="la la-list"></i>
                            Daftar Stok Barang
                        </a>
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('barangEditModal', ['id_barang' => $id_barang]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus text-danger"
                           href="#"
                            data-route="' . route('barangDelete', ['id_barang' => $id_barang]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_jenis_barang' => 'required',
            'id_satuan' => 'required',
            'brg_kode' => 'required',
            'brg_nama' => 'required',
            'brg_harga_beli' => 'required',
            'brg_harga_jual' => 'required',
//            'brg_golongan' => 'required',
            'brg_minimal_stok' => 'required',
            'brg_keterangan' => 'required',
        ]);

        $data = $request->except('_token');
        $data['brg_harga_beli'] = Main::format_number_db($data['brg_harga_beli']);
        $data['brg_harga_jual'] = Main::format_number_db($data['brg_harga_jual']);
        $id_barang = mBarang::create($data)->id_barang;
        $user = Session::get('user');
        $id_user = $user['id'];

        $data_harga_jual_history = [
            'id_barang' => $id_barang,
            'brg_harga_jual' => $data['brg_harga_jual'],
            'id_user' => $id_user
        ];
        mBarangHargaJualHistory::create($data_harga_jual_history);
    }

    function edit_modal($id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        $edit = mBarang::where('id_barang', $id_barang)->first();
        $jenis_barang = mJenisBarang::orderBy('jbr_nama', 'ASC')->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $data = [
            'edit' => $edit,
            'jenis_barang' => $jenis_barang,
            'satuan' => $satuan
        ];

        return view('barang/barang/barangEditModal', $data);
    }

    function delete($id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        mBarang::where('id_barang', $id_barang)->delete();
        mBarangHargaJualHistory::where('id_barang', $id_barang)->delete();
    }

    function update(Request $request, $id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        $request->validate([
            'id_jenis_barang' => 'required',
            'brg_kode' => 'required',
            'brg_nama' => 'required',
            'brg_harga_beli' => 'required',
            'brg_harga_jual' => 'required',
//            'brg_golongan' => 'required',
            'brg_minimal_stok' => 'required',
            'brg_keterangan' => 'required',
        ]);
        $data = $request->except("_token");
        $data['brg_harga_beli'] = Main::format_number_db($data['brg_harga_beli']);
        $data['brg_harga_jual'] = Main::format_number_db($data['brg_harga_jual']);
        $user = Session::get('user');
        $id_user = $user['id'];

        mBarang::where(['id_barang' => $id_barang])->update($data);

        $data_harga_jual_history = [
            'id_barang' => $id_barang,
            'brg_harga_jual' => $data['brg_harga_jual'],
            'id_user' => $id_user
        ];
        mBarangHargaJualHistory::create($data_harga_jual_history);
    }

    function download($request)
    {
        $kode_barang = $request->kode_barang;
        $nama_barang = $request->nama_barang;

        $data_list = mBarang
            ::with('jenis_barang')
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->orderBy('brg_kode', 'ASC')
            ->orderBy('brg_nama', 'ASC')
            ->get();

        $data = [
            'data' => $data_list
        ];

        if ($request->filter_type == 'download_pdf') {
            return view('barang.barang.barangPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('barang.barang.barangPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Barang');
        }

        if ($request->filter_type == 'download_excel') {
            return view('barang.barang.barangExcel', $data);
        }
    }
}
