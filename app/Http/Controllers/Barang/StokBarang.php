<?php

namespace app\Http\Controllers\Barang;

use app\Models\mBarang;
use app\Models\mJenisBarang;
use app\Models\mPembelianDetail;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class StokBarang extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['master_barang'];
        $this->breadcrumb = [
            [
                'label' => $cons['master_barang'],
                'route' => route('barangList')
            ]
        ];
    }

    function list($id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        $barang = mBarang::where('id_barang', $id_barang)->first(['brg_kode', 'brg_nama']);
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $breadcrumb = array_merge(
            $this->breadcrumb,
            array(array(
                'label' => $barang->brg_kode . ' ' . $barang->brg_nama,
                'route' => ''
            ))
        );
        $data = Main::data($breadcrumb, $this->menuActive);

        $data_list = mBarang
            ::with([
                'jenis_barang'
            ])
            ->orderBy('brg_kode', 'ASC')
            ->get();
        $datatable_column = [
            ["data" => "no"],
            ["data" => "spl_nama"],
//            ["data" => "sbr_kode_batch"],
//            ["data" => "sbr_expired"],
            ["data" => "sbr_qty"],
            ["data" => "sbr_harga_beli"],
            ["data" => "sbr_harga_jual"],
//            ["data" => "sbr_konsinyasi_status"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'satuan' => $satuan,
            'datatable_column' => $datatable_column,
            'id_barang' => $id_barang
        ]);

        return view('barang/stokBarang/stokBarangList', $data);
    }


    function data_table(Request $request, $id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        $total_data = mStokBarang
            ::where([
                'id_barang' => $id_barang
            ])
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'sbr_kode_batch';
        $order_type = $request->input('order.0.dir');

        $data_list = mStokBarang
            ::with([
                'supplier:id_supplier,spl_kode,spl_nama'
            ])
            ->where([
                'id_barang' => $id_barang
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_stok_barang = Main::encrypt($row->id_stok_barang);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['spl_nama'] = $row['supplier']['spl_kode'] . '/' . $row['supplier']['spl_nama'];
//            $nestedData['sbr_kode_batch'] = $row->sbr_kode_batch;
//            $nestedData['sbr_expired'] = Main::format_date_label($row->sbr_expired);
            $nestedData['sbr_qty'] = $row->sbr_qty;
            $nestedData['sbr_harga_beli'] = Main::format_money($row->sbr_harga_beli);
            $nestedData['sbr_harga_jual'] = Main::format_money($row->sbr_harga_jual);
//            $nestedData['sbr_konsinyasi_status'] = Main::status($row->sbr_konsinyasi_status);
            $nestedData['options'] = '
                <div class="dropdown m--hide">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('stokBarangEditModal', ['id_barang' => $id_barang, 'id_stok_barang' => $id_stok_barang]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >
                    </div>
                </div>
            ';

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }


    function edit_modal($id_barang, $id_stok_barang)
    {
        $id_stok_barang = Main::decrypt($id_stok_barang);
        $edit = mStokBarang::where('id_stok_barang', $id_stok_barang)->first();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $data = [
            'edit' => $edit,
            'id_barang' => $id_barang,
            'satuan' => $satuan
        ];

        return view('barang/stokBarang/stokBarangEditModal', $data);
    }

    function update(Request $request, $id_barang, $id_stok_barang)
    {
        $id_stok_barang = Main::decrypt($id_stok_barang);
        $id_satuan = $request->input('id_satuan');
        $sbr_kode_batch = $request->input('sbr_kode_batch');
        $sbr_expired = Main::format_date_db($request->input('sbr_expired'));
        $sbr_harga_jual = Main::format_number_db($request->input('sbr_harga_jual'));
        $sbr_konsinyasi = $request->input('sbr_konsinyasi');
        $id_pembelian_detail = mStokBarang::where('id_stok_barang', $id_stok_barang)->value('id_pembelian_detail');

        $request->validate([
            'id_satuan' => 'required',
            'sbr_kode_batch' => 'required',
            'sbr_expired' => 'required',
            'sbr_harga_jual' => 'required',
            'sbr_konsinyasi' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $stok_barang_data = [
                'id_satuan' => $id_satuan,
                'sbr_kode_batch' => $sbr_kode_batch,
                'sbr_expired' => $sbr_expired,
                'sbr_harga_jual' => $sbr_harga_jual,
                'sbr_konsinyasi_status' => $sbr_konsinyasi
            ];
            mStokBarang::where(['id_stok_barang' => $id_stok_barang])->update($stok_barang_data);

            $pembelian_detail_data = [
                'pbd_kode_batch' => $sbr_kode_batch,
                'pbd_harga_jual' => $sbr_harga_jual,
                'pbd_expired' => $sbr_expired,
                'pbd_konsinyasi_status' => $sbr_konsinyasi
            ];

            mPembelianDetail::where('id_pembelian_detail', $id_pembelian_detail)->update($pembelian_detail_data);


            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }
}
