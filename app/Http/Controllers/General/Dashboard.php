<?php

namespace app\Http\Controllers\General;

use app\Models\mBarang;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPenjualan;
use app\Models\mPiutangPelanggan;
use app\Models\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }

        $filter_component = Main::date_filter($request, ['date','download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];


        $data = Main::data($this->breadcrumb);
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_barang = mBarang::count();
        $total_pelanggan = mPelanggan
            ::where('id_pelanggan', '!=', '1')
            ->count();
        $total_supplier = mSupplier
            ::count();
        $total_pembelian = mPembelian
            ::whereBetween('pbl_tanggal_order', $where_date)
            ->sum('pbl_grand_total');
        $total_penjualan = mPenjualan
            ::whereBetween('pjl_tanggal_penjualan', $where_date)
            ->sum('pjl_grand_total');
        $total_stok_alert = Main::totalAlertBarang();
        $total_hutang_supplier = mHutangSupplier
            ::whereBetween('hsp_tanggal', $where_date)
            ->sum('hsp_sisa');
        $total_piutang_pelanggan = mPiutangPelanggan
            ::whereBetween('ppl_tanggal', $where_date)
            ->sum('ppl_sisa');
        $total_profit = $total_penjualan - $total_pembelian;

        $start = new \DateTime($date_from_db);
        $end = new \DateTime($date_to_db);
        $end = $end->modify('+1 day');
        $interval = new \DateInterval('P1D');

        $label = [];
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $key => $value) {
            $label[] = $value->format('Y-m-d');
        }


        $cart_patient = [
            'label' => $label
        ];

        foreach ($period as $key => $value) {
            $date = $value->format('Y-m-d');

            $cart_patient['data']['appointment'][$date] = 0;

            $cart_patient['data']['consult'][$date] = 0;

            $cart_patient['data']['action'][$date] = 0;

            $cart_patient['data']['control'][$date] = 0;

            $cart_patient['data']['patient'][$date] = 0;

            $cart_patient['data']['payment'][$date] = 0;
        }


        $data = array_merge($data, array(
            'total_barang' => Main::format_number($total_barang),
            'total_pelanggan' => Main::format_number($total_pelanggan),
            'total_supplier' => Main::format_number($total_supplier),
            'total_pembelian' => Main::format_money($total_pembelian),
            'total_penjualan' => Main::format_money($total_penjualan),
            'total_stok_alert' => Main::format_number($total_stok_alert),
            'total_hutang_supplier' => Main::format_money($total_hutang_supplier),
            'total_piutang_pelanggan' => Main::format_money($total_piutang_pelanggan),
            'total_profit' => Main::format_money($total_profit),
            'cart_patient' => $cart_patient,
            'date_filter' => $date_filter
        ));

        return view('dashboard/dashboard', $data);

    }

    function download($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_barang = mBarang::count();
        $total_pelanggan = mPelanggan
            ::where('id_pelanggan', '!=', '1')
            ->count();
        $total_supplier = mSupplier
            ::count();
        $total_pembelian = mPembelian
            ::whereBetween('pbl_tanggal_order', $where_date)
            ->sum('pbl_grand_total');
        $total_penjualan = mPenjualan
            ::whereBetween('pjl_tanggal_penjualan', $where_date)
            ->sum('pjl_grand_total');
        $total_stok_alert = Main::totalAlertBarang();
        $total_hutang_supplier = mHutangSupplier
            ::whereBetween('hsp_tanggal', $where_date)
            ->sum('hsp_sisa');
        $total_piutang_pelanggan = mPiutangPelanggan
            ::whereBetween('ppl_tanggal', $where_date)
            ->sum('ppl_sisa');
        $total_profit = $total_penjualan - $total_pembelian;

        $data = [
            'total_barang' => $total_barang,
            'total_pelanggan' => $total_pelanggan,
            'total_supplier' => $total_supplier,
            'total_pembelian' => $total_pembelian,
            'total_penjualan' => $total_penjualan,
            'total_stok_alert' => $total_stok_alert,
            'total_hutang_supplier' => $total_hutang_supplier,
            'total_piutang_pelanggan' => $total_piutang_pelanggan,
            'total_profit' => $total_profit,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];

        if ($request->filter_type == 'download_excel') {
            return view('dashboard.dashboardExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('dashboard.dashboardPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('dashboard.dashboardPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Dashboard Sistem ' . $date_from . ' - ' . $date_to);
        }

    }


}
