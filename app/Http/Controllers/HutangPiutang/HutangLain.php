<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHistoryPenyesuaianStok;
use app\Models\mHutangLain;
use app\Models\mHutangLainPembayaran;
use app\Models\mHutangSupplier;
use app\Models\mHutangSupplierPembayaran;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HutangLain extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['hutang_lain_lain'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['hutang_lain_lain'],
                'route' => route('hutangLainList')
            ]
        ];
    }

    function index(Request $request)
    {

        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download_belum_lunas($request);
        }

        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request, ['date', 'no_faktur_hutang', 'keterangan', 'download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $no_faktur_hutang = $filter_component['no_faktur_hutang'];
        $keterangan = $filter_component['keterangan'];
        $hul_no_faktur = Main::fakturHutangLain('GST');

        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur_hutang"],
            ["data" => "tanggal_hutang"],
            ["data" => "jatuh_tempo"],
            ["data" => "total_hutang"],
            ["data" => "sisa"],
            ["data" => "keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'hul_no_faktur' => $hul_no_faktur,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'no_faktur_hutang' => $no_faktur_hutang,
                'keterangan' => $keterangan,
            ),
        ]);

        return view('hutangPiutang/hutangLain/hutangLainList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $no_faktur_hutang = $data_post['no_faktur_hutang'];
        $keterangan = $data_post['keterangan'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mHutangLain
            ::where([
                'hul_status' => 'belum_lunas'
            ])
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('hul_tanggal', $where_date)
                    ->orWhereBetween('hul_tanggal_jatuh_tempo', $where_date);
            })
            ->whereLike('hul_no_faktur', $no_faktur_hutang)
            ->whereLike('hul_keterangan', $keterangan)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_hutang_lain'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mHutangLain
            ::where('hul_status', 'belum_lunas')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('hul_tanggal', $where_date)
                    ->orWhereBetween('hul_tanggal_jatuh_tempo', $where_date);
            })
            ->whereLike('hul_no_faktur', $no_faktur_hutang)
            ->whereLike('hul_keterangan', $keterangan)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_hutang_lain = Main::encrypt($row->id_hutang_lain);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur_hutang'] = $row->hul_no_faktur;
            $nestedData['tanggal_hutang'] = Main::format_date_label($row->hul_tanggal);
            $nestedData['jatuh_tempo'] = Main::format_date_label($row->hul_tanggal_jatuh_tempo);
            $nestedData['total_hutang'] = Main::format_number($row->hul_total);
            $nestedData['sisa'] = Main::format_number($row->hul_sisa);
            $nestedData['keterangan'] = $row->hul_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('hutangLainPembayaranModal', ['id_hutang_lain' => $id_hutang_lain]) . '">
                            <i class="la la-list"></i>
                            Lakukan Pembayaran
                        </a>
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('hutangLainPembayaranHistoryModal', ['id_hutang_lain' => $id_hutang_lain]) . '">
                            <i class="la la-list"></i>
                            History Pembayaran
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item btn-hapus akses-delete m--font-danger" 
                           data-route="' . route('hutangLainDelete', ['id_hutang_lain' => $id_hutang_lain]) . '" href="#">
                            <i class="la la-remove m--font-danger"></i> Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function index_lunas(Request $request)
    {

        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download_lunas($request);
        }

        $data = Main::data($this->breadcrumb, $this->menuActive);

        $filter_component = Main::date_filter($request, ['date', 'no_faktur_hutang', 'keterangan', 'download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $no_faktur_hutang = $filter_component['no_faktur_hutang'];
        $keterangan = $filter_component['keterangan'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur_hutang"],
            ["data" => "tanggal_hutang"],
            ["data" => "jatuh_tempo"],
            ["data" => "waktu_lunas"],
            ["data" => "total_hutang"],
            ["data" => "keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'no_faktur_hutang' => $no_faktur_hutang,
                'keterangan' => $keterangan,
            ),
        ]);

        return view('hutangPiutang/hutangLain/hutangLainLunasList', $data);
    }

    function data_table_lunas(Request $request)
    {
        $data_post = $request->input('data');
        $no_faktur_hutang = $data_post['no_faktur_hutang'];
        $keterangan = $data_post['keterangan'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mHutangLain
            ::where([
                'hul_status' => 'lunas'
            ])
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('hul_tanggal', $where_date)
                    ->orWhereBetween('hul_tanggal_jatuh_tempo', $where_date);
            })
            ->whereLike('hul_no_faktur', $no_faktur_hutang)
            ->whereLike('hul_keterangan', $keterangan)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_hutang_lain'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mHutangLain
            ::where('hul_status', 'lunas')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('hul_tanggal', $where_date)
                    ->orWhereBetween('hul_tanggal_jatuh_tempo', $where_date);
            })
            ->whereLike('hul_no_faktur', $no_faktur_hutang)
            ->whereLike('hul_keterangan', $keterangan)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_hutang_lain = Main::encrypt($row->id_hutang_lain);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur_hutang'] = $row->hul_no_faktur;
            $nestedData['tanggal_hutang'] = Main::format_date_label($row->hul_tanggal);
            $nestedData['jatuh_tempo'] = Main::format_date_label($row->hul_tanggal_jatuh_tempo);
            $nestedData['waktu_lunas'] = Main::format_datetime($row->updated_at);
            $nestedData['total_hutang'] = Main::format_number($row->hul_total);
            $nestedData['sisa'] = Main::format_number($row->hul_sisa);
            $nestedData['keterangan'] = $row->hul_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('hutangLainPembayaranHistoryModal', ['id_hutang_lain' => $id_hutang_lain]) . '">
                            <i class="la la-list"></i>
                            History Pembayaran
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'data_list' => $data_list
        );

        return $json_data;
    }

    function insert(Request $request)
    {

        $request->validate([
            'hul_keterangan' => 'required',
            'hul_total' => 'required',
            'hul_tanggal' => 'required',
            'hul_tanggal_jatuh_tempo' => 'required'
        ]);

        $data = $request->except('_token');
        $data['hul_total'] = Main::format_number_db($data['hul_total']);
        $data['hul_sisa'] = $data['hul_total'];
        $data['hul_tanggal'] = Main::format_date_db($data['hul_tanggal']);
        $data['hul_tanggal_jatuh_tempo'] = Main::format_date_db($data['hul_tanggal_jatuh_tempo']);
        $data['hul_status'] = 'belum_lunas';
        mHutangLain::create($data);
    }

    function delete($id_hutang_lain)
    {
        $id_hutang_lain = Main::decrypt($id_hutang_lain);
        $hutang_lain = mHutangLain
            ::where('id_hutang_lain', $id_hutang_lain)
            ->first(['hul_total', 'hul_sisa']);

        if ($hutang_lain->hul_total == $hutang_lain->hul_sisa) {
            mHutangLain::where('id_hutang_lain', $id_hutang_lain)->delete();
        } else {
            return response([
                'title' => 'Perhatian',
                'message' => 'Hutang sudah terbayar sebagian<br />Sehingga Hutang ini belum bisa Dihapus.'
            ], 422);
        }


    }

    function pembayaran_modal($id_hutang_lain)
    {
        $id_hutang_lain = Main::decrypt($id_hutang_lain);
        $row = mHutangLain
            ::where('id_hutang_lain', $id_hutang_lain)
            ->first();

        $data = [
            'row' => $row,
        ];

        return view('hutangPiutang.hutangLain.hutangLainPembayaranModal', $data);
    }

    function pembayaran_insert(Request $request)
    {
        $request->validate([
            'hlp_tanggal_bayar' => 'required',
            'hlp_keterangan' => 'required',
            'hlp_jumlah_bayar' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $user = Session::get('user');
            $id_user = $user['id'];

            $id_hutang_lain = $request->input('id_hutang_lain');
            $hlp_total_hutang = $request->input('hlp_total_hutang');
            $hlp_sisa_pembayaran = $request->input('hlp_sisa_pembayaran');
            $hlp_tanggal_bayar = $request->input('hlp_tanggal_bayar');
            $hlp_keterangan = $request->input('hlp_keterangan');
            $hlp_jumlah_bayar = Main::format_number_db($request->input('hlp_jumlah_bayar'));

            $hutang_lain_pembayaran = [
                'id_user' => $id_user,
                'id_hutang_lain' => $id_hutang_lain,
                'hlp_total_hutang' => $hlp_total_hutang,
                'hlp_jumlah_bayar' => $hlp_jumlah_bayar,
                'hlp_sisa_bayar' => $hlp_sisa_pembayaran,
                'hlp_tanggal_bayar' => Main::format_date_db($hlp_tanggal_bayar),
                'hlp_keterangan' => $hlp_keterangan
            ];

            mHutangLainPembayaran::create($hutang_lain_pembayaran);

            $hutang_lain_data = [
                'hul_sisa' => $hlp_sisa_pembayaran
            ];

            if ($hlp_sisa_pembayaran == 0) {
                $hutang_lain_data['hul_status'] = 'lunas';
            }

            mHutangLain::where('id_hutang_lain', $id_hutang_lain)->update($hutang_lain_data);


            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }


    }

    function pembayaran_history_modal($id_hutang_lain)
    {
        $id_hutang_lain = Main::decrypt($id_hutang_lain);
        $data = mHutangLainPembayaran
            ::with([
                'user',
            ])
            ->where('id_hutang_lain', $id_hutang_lain)
            ->get();
        $hutang_lain = mHutangLain
            ::where('id_hutang_lain', $id_hutang_lain)
            ->first();

        $data = [
            'data' => $data,
            'hutang_lain' => $hutang_lain
        ];

        return view('hutangPiutang.hutangLain.hutangLainPembayaranHistoryModal', $data);
    }

    function download_belum_lunas($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $no_faktur_hutang = $request->no_faktur_hutang ? $request->no_faktur_hutang : '';
        $keterangan = $request->keterangan ? $request->keterangan : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mHutangLain
            ::where('hul_status', 'belum_lunas')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('hul_tanggal', $where_date)
                    ->orWhereBetween('hul_tanggal_jatuh_tempo', $where_date);
            })
            ->whereLike('hul_no_faktur', $no_faktur_hutang)
            ->whereLike('hul_keterangan', $keterangan)
            ->orderBy('id_hutang_lain', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'no' => 1
        ];


        if ($request->filter_type == 'download_excel') {
            return view('hutangPiutang.hutangLain.hutangLainExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('hutangPiutang.hutangLain.hutangLainPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('hutangPiutang.hutangLain.hutangLainPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Hutang Lain Belum Lunas ' . $date_from . ' - ' . $date_to);
        }

    }

    function download_lunas($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $no_faktur_hutang = $request->no_faktur_hutang ? $request->no_faktur_hutang : '';
        $keterangan = $request->keterangan ? $request->keterangan : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mHutangLain
            ::where('hul_status', 'lunas')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('hul_tanggal', $where_date)
                    ->orWhereBetween('hul_tanggal_jatuh_tempo', $where_date);
            })
            ->whereLike('hul_no_faktur', $no_faktur_hutang)
            ->whereLike('hul_keterangan', $keterangan)
            ->orderBy('id_hutang_lain', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'no' => 1
        ];


        if ($request->filter_type == 'download_excel') {
            return view('hutangPiutang.hutangLain.hutangLainLunasExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('hutangPiutang.hutangLain.hutangLainLunasPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('hutangPiutang.hutangLain.hutangLainLunasPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Hutang Lain Lunas ' . $date_from . ' - ' . $date_to);
        }

    }

}
