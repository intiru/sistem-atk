<?php

namespace app\Http\Controllers\HutangPiutang;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHistoryPenyesuaianStok;
use app\Models\mHutangSupplier;
use app\Models\mHutangSupplierPembayaran;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangLain;
use app\Models\mPiutangPelanggan;
use app\Models\mPiutangPelangganPembayaran;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PiutangPelanggan extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['piutang_pelanggan'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['piutang_pelanggan'],
                'route' => route('piutangPelangganList')
            ]
        ];
    }

    function index(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download_belum_lunas($request);
        }

        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request, ['date', 'no_faktur_piutang', 'no_faktur_penjualan', 'nama_pelanggan','alamat_pelanggan','download']);
        $no_faktur_piutang = $filter_component['no_faktur_piutang'];
        $no_faktur_penjualan = $filter_component['no_faktur_penjualan'];
        $keterangan = $filter_component['keterangan'];
        $nama_pelanggan = $filter_component['nama_pelanggan'];
        $alamat_pelanggan = $filter_component['alamat_pelanggan'];
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur_piutang"],
            ["data" => "faktur_penjualan"],
            ["data" => "nama_pelanggan"],
            ["data" => "alamat_pelanggan"],
            ["data" => "tanggal_piutang"],
            ["data" => "jatuh_tempo"],
            ["data" => "total_piutang"],
            ["data" => "sisa"],
            ["data" => "keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'no_faktur_piutang' => $no_faktur_piutang,
                'no_faktur_penjualan' => $no_faktur_penjualan,
                'keterangan' => $keterangan,
                'nama_pelanggan' => $nama_pelanggan,
                'alamat_pelanggan' => $alamat_pelanggan,
            ),
        ]);

        return view('hutangPiutang/piutangPelanggan/piutangPelangganList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $no_faktur_piutang = $data_post['no_faktur_piutang'];
        $no_faktur_penjualan = $data_post['no_faktur_penjualan'];
        $keterangan = $data_post['keterangan'];
        $nama_pelanggan = $data_post['nama_pelanggan'];
        $alamat_pelanggan = $data_post['alamat_pelanggan'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPiutangPelanggan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'piutang_pelanggan.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'piutang_pelanggan.id_penjualan')
            ->where([
                'ppl_status' => 'belum_lunas'
            ])
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('ppl_tanggal', $where_date);
            })
            ->whereLike('ppl_no_faktur', $no_faktur_piutang)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('ppl_keterangan', $keterangan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->whereLike('plg_alamat', $alamat_pelanggan)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_piutang_pelanggan'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mPiutangPelanggan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'piutang_pelanggan.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'piutang_pelanggan.id_penjualan')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('ppl_tanggal', $where_date)
                    ->orWhereBetween('ppl_jatuh_tempo', $where_date);
            })
            ->whereLike('ppl_no_faktur', $no_faktur_piutang)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('ppl_keterangan', $keterangan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->whereLike('plg_alamat', $alamat_pelanggan)
            ->where('ppl_status', 'belum_lunas')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_piutang_pelanggan = Main::encrypt($row->id_piutang_pelanggan);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur_piutang'] = $row->ppl_no_faktur;
            $nestedData['nama_pelanggan'] = $row->plg_nama;
            $nestedData['alamat_pelanggan'] = $row->plg_alamat;
            $nestedData['faktur_penjualan'] = $row->pjl_no_faktur;
            $nestedData['tanggal_piutang'] = Main::format_date_label($row->ppl_tanggal);
            $nestedData['jatuh_tempo'] = Main::format_date_label($row->ppl_jatuh_tempo);
            $nestedData['total_piutang'] = Main::format_number($row->ppl_total);
            $nestedData['sisa'] = Main::format_number($row->ppl_sisa);
            $nestedData['keterangan'] = $row->ppl_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('piutangPelangganPembayaranModal', ['id_piutang_pelanggan' => $id_piutang_pelanggan]) . '">
                            <i class="la la-list"></i>
                            Lakukan Pembayaran
                        </a>
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('piutangPelangganPembayaranHistoryModal', ['id_piutang_pelanggan' => $id_piutang_pelanggan]) . '">
                            <i class="la la-list"></i>
                            History Pembayaran
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function index_lunas(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download_lunas($request);
        }

        $data = Main::data($this->breadcrumb, $this->menuActive);

        $filter_component = Main::date_filter($request, ['date', 'no_faktur_piutang', 'no_faktur_penjualan', 'nama_pelanggan', 'alamat_pelanggan','download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $no_faktur_piutang = $filter_component['no_faktur_piutang'];
        $no_faktur_penjualan = $filter_component['no_faktur_penjualan'];
        $keterangan = $filter_component['keterangan'];
        $nama_pelanggan = $filter_component['nama_pelanggan'];
        $alamat_pelanggan = $filter_component['alamat_pelanggan'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur_piutang"],
            ["data" => "faktur_penjualan"],
            ["data" => "nama_pelanggan"],
            ["data" => "alamat_pelanggan"],
            ["data" => "tanggal_piutang"],
            ["data" => "jatuh_tempo"],
            ["data" => "waktu_lunas"],
            ["data" => "total_piutang"],
            ["data" => "keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'no_faktur_piutang' => $no_faktur_piutang,
                'no_faktur_penjualan' => $no_faktur_penjualan,
                'keterangan' => $keterangan,
                'nama_pelanggan' => $nama_pelanggan,
                'alamat_pelanggan' => $alamat_pelanggan,
            ),
        ]);

        return view('hutangPiutang/piutangPelanggan/piutangPelangganLunasList', $data);
    }

    function data_table_lunas(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $no_faktur_piutang = $data_post['no_faktur_piutang'];
        $no_faktur_penjualan = $data_post['no_faktur_penjualan'];
        $keterangan = $data_post['keterangan'];
        $nama_pelanggan = $data_post['nama_pelanggan'];
        $alamat_pelanggan = $data_post['alamat_pelanggan'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPiutangPelanggan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'piutang_pelanggan.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'piutang_pelanggan.id_penjualan')
            ->whereIn('ppl_status', ['lunas', 'lunas_karena_retur','lunas_karena_penjualan_edit'])
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('ppl_tanggal', $where_date)
                    ->orWhereBetween('ppl_jatuh_tempo', $where_date);
            })
            ->whereLike('ppl_no_faktur', $no_faktur_piutang)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('ppl_keterangan', $keterangan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->whereLike('plg_alamat', $nama_pelanggan)
            ->count();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_piutang_pelanggan';
        $order_type = $request->input('order.0.dir');

        $data_list = mPiutangPelanggan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'piutang_pelanggan.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'piutang_pelanggan.id_penjualan')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('ppl_tanggal', $where_date)
                    ->orWhereBetween('ppl_jatuh_tempo', $where_date);
            })
            ->whereLike('ppl_no_faktur', $no_faktur_piutang)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('ppl_keterangan', $keterangan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->whereLike('plg_alamat', $alamat_pelanggan)
            ->whereIn('ppl_status', ['lunas', 'lunas_karena_retur','lunas_karena_penjualan_edit'])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_piutang_pelanggan = Main::encrypt($row->id_piutang_pelanggan);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur_piutang'] = $row->ppl_no_faktur;
            $nestedData['faktur_penjualan'] = $row->pjl_no_faktur;
            $nestedData['nama_pelanggan'] = $row->plg_nama;
            $nestedData['alamat_pelanggan'] = $row->plg_alamat;
            $nestedData['tanggal_piutang'] = Main::format_date_label($row->ppl_tanggal);
            $nestedData['jatuh_tempo'] = Main::format_date_label($row->ppl_jatuh_tempo);
            $nestedData['waktu_lunas'] = Main::format_datetime($row->updated_at);
            $nestedData['total_piutang'] = Main::format_number($row->ppl_total);
            $nestedData['keterangan'] = $row->ppl_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('piutangPelangganPembayaranHistoryModal', ['id_piutang_pelanggan' => $id_piutang_pelanggan]) . '">
                            <i class="la la-list"></i>
                            History Pembayaran
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function pembayaran_modal($id_piutang_pelanggan)
    {
        $id_piutang_pelanggan = Main::decrypt($id_piutang_pelanggan);
        $row = mPiutangPelanggan
            ::with([
                'pelanggan',
                'penjualan'
            ])
            ->where('id_piutang_pelanggan', $id_piutang_pelanggan)
            ->first();

        $data = [
            'row' => $row,
        ];

        return view('hutangPiutang.piutangPelanggan.piutangPelangganPembayaranModal', $data);
    }

    function pembayaran_insert(Request $request)
    {
        $request->validate([
            'ppp_tanggal_bayar' => 'required',
            'ppp_keterangan' => 'required',
            'ppp_jumlah_bayar' => 'required'
        ]);

        DB::beginTransaction();
        try {

            $user = Session::get('user');
            $id_user = $user['id'];

            $id_piutang_pelanggan = $request->input('id_piutang_pelanggan');
            $ppp_total_piutang = $request->input('ppp_total_piutang');
            $ppp_sisa_pembayaran = $request->input('ppp_sisa_pembayaran');
            $ppp_tanggal_bayar = $request->input('ppp_tanggal_bayar');
            $ppp_keterangan = $request->input('ppp_keterangan');
            $ppp_jumlah_bayar = Main::format_number_db($request->input('ppp_jumlah_bayar'));

            $piutang_pelanggan_pembayaran = [
                'id_user' => $id_user,
                'id_piutang_pelanggan' => $id_piutang_pelanggan,
                'ppp_total_piutang' => $ppp_total_piutang,
                'ppp_jumlah_bayar' => $ppp_jumlah_bayar,
                'ppp_sisa_bayar' => $ppp_sisa_pembayaran,
                'ppp_tanggal_bayar' => Main::format_date_db($ppp_tanggal_bayar),
                'ppp_keterangan' => $ppp_keterangan
            ];

            mPiutangPelangganPembayaran::create($piutang_pelanggan_pembayaran);

            $piutang_pelanggan_data = [
                'ppl_sisa' => $ppp_sisa_pembayaran
            ];

            if ($ppp_sisa_pembayaran == 0) {
                $piutang_pelanggan_data['ppl_status'] = 'lunas';
            }

            mPiutangPelanggan::where('id_piutang_pelanggan', $id_piutang_pelanggan)->update($piutang_pelanggan_data);


            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }


    }

    function pembayaran_history_modal($id_piutang_pelanggan)
    {
        $id_piutang_pelanggan = Main::decrypt($id_piutang_pelanggan);
        $data = mPiutangPelangganPembayaran
            ::with([
                'user',
            ])
            ->where('id_piutang_pelanggan', $id_piutang_pelanggan)
            ->orderBy('id_piutang_pelanggan_pembayaran', 'ASC')
            ->get();
        $piutang_pelanggan = mPiutangPelanggan
            ::with([
                'penjualan',
                'pelanggan'
            ])
            ->where('id_piutang_pelanggan', $id_piutang_pelanggan)
            ->first();

        $data = [
            'data' => $data,
            'piutang_pelanggan' => $piutang_pelanggan
        ];

        return view('hutangPiutang.piutangPelanggan.piutangPelangganPembayaranHistoryModal', $data);
    }

    function download_belum_lunas($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $no_faktur_piutang = $request->no_faktur_piutang ? $request->no_faktur_piutang : '';
        $no_faktur_penjualan = $request->no_faktur_penjualan ? $request->no_faktur_penjualan : '';
        $keterangan = $request->keterangan ? $request->keterangan : '';
        $nama_pelanggan = $request->nama_pelanggan ? $request->nama_pelanggan : '';
        $alamat_pelanggan = $request->alamat_pelanggan ? $request->alamat_pelanggan : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mPiutangPelanggan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'piutang_pelanggan.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'piutang_pelanggan.id_penjualan')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('ppl_tanggal', $where_date)
                    ->orWhereBetween('ppl_jatuh_tempo', $where_date);
            })
            ->whereLike('ppl_no_faktur', $no_faktur_piutang)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('ppl_keterangan', $keterangan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->whereLike('plg_alamat', $alamat_pelanggan)
            ->where('ppl_status', 'belum_lunas')
            ->orderBy('id_piutang_pelanggan', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'no' => 1
        ];


        if ($request->filter_type == 'download_excel') {
            return view('hutangPiutang.piutangPelanggan.piutangPelangganExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('hutangPiutang.piutangPelanggan.piutangPelangganPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('hutangPiutang.piutangPelanggan.piutangPelangganPdf', $data);

            return $pdf
                ->setPaper('A4', 'landscape')
                ->stream('Piutang Pelanggan Belum Lunas ' . $date_from . ' - ' . $date_to);
        }

    }

    function download_lunas($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $no_faktur_piutang = $request->no_faktur_piutang ? $request->no_faktur_piutang : '';
        $no_faktur_penjualan = $request->no_faktur_penjualan ? $request->no_faktur_penjualan : '';
        $keterangan = $request->keterangan ? $request->keterangan : '';
        $nama_pelanggan = $request->nama_pelanggan ? $request->nama_pelanggan : '';
        $alamat_pelanggan = $request->alamat_pelanggan ? $request->alamat_pelanggan : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mPiutangPelanggan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'piutang_pelanggan.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'piutang_pelanggan.id_penjualan')
            ->where(function ($query) use ($where_date) {
                $query
                    ->whereBetween('ppl_tanggal', $where_date)
                    ->orWhereBetween('ppl_jatuh_tempo', $where_date);
            })
            ->whereLike('ppl_no_faktur', $no_faktur_piutang)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('ppl_keterangan', $keterangan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->whereLike('plg_alamat', $alamat_pelanggan)
            ->whereIn('ppl_status', ['lunas', 'lunas_karena_retur','lunas_karena_penjualan_edit'])
            ->orderBy('id_piutang_pelanggan', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'no' => 1
        ];


        if ($request->filter_type == 'download_excel') {
            return view('hutangPiutang.piutangPelanggan.piutangPelangganLunasExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('hutangPiutang.piutangPelanggan.piutangPelangganLunasPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('hutangPiutang.piutangPelanggan.piutangPelangganLunasPdf', $data);

            return $pdf
                ->setPaper('A4', 'landscape')
                ->stream('Piutang Pelanggan Belum Lunas ' . $date_from . ' - ' . $date_to);
        }

    }

}
