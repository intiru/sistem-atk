<?php

namespace app\Http\Controllers\KartuStok;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHistoryPenyesuaianStok;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class KartuStok extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->menuActive = $cons['kartu_stok'];
        $this->breadcrumb = [
            [
                'label' => $cons['kartu_stok'],
                'route' => route('kartuStokList')
            ]
        ];
    }

    function index(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }

        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request, ['date', 'nama_barang', 'kode_barang', 'keterangan', 'download']);
        $kode_barang = $filter_component['kode_barang'];
        $nama_barang = $filter_component['nama_barang'];
        $keterangan = $filter_component['keterangan'];
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "tanggal"],
            ["data" => "kode_barang"],
            ["data" => "nama_barang"],
//            ["data" => "no_batch"],
//            ["data" => "expired_date"],
            ["data" => "stok_awal"],
            ["data" => "stok_masuk"],
            ["data" => "stok_keluar"],
//            ["data" => "sisa_barang"],
            ["data" => "sisa_total_barang"],
            ["data" => "keterangan"],
            ["data" => "user"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'kode_barang' => $kode_barang,
                'nama_barang' => $nama_barang,
                'keterangan' => $keterangan,
            ),
        ]);

        return view('kartuStok/kartuStok/kartuStokList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $kode_barang = $data_post['kode_barang'];
        $nama_barang = $data_post['nama_barang'];
        $keterangan = $data_post['keterangan'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mArusStok
            ::leftJoin('barang', 'barang.id_barang', '=', 'arus_stok.id_barang')
            ->leftJoin('stok_barang', 'stok_barang.id_stok_barang', '=', 'arus_stok.id_stok_barang')
            ->leftJoin('tb_user', 'tb_user.id', '=', 'arus_stok.id_user')
            ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->whereLike('asb_keterangan', $keterangan)
            ->whereBetween('arus_stok.created_at', $where_date)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_arus_stok'; //$columns[$request->input('order.0.column')];
//        $order_type = $request->input('order.0.dir');
        $order_type = 'asc';

        $data_list = mArusStok
            ::select([
                'arus_stok.created_at AS arus_stok_date',
                'arus_stok.*',
                'barang.*',
                'stok_barang.*',
                'tb_user.*',
                'tb_karyawan.*',
            ])
            ->leftJoin('barang', 'barang.id_barang', '=', 'arus_stok.id_barang')
            ->leftJoin('stok_barang', 'stok_barang.id_stok_barang', '=', 'arus_stok.id_stok_barang')
            ->leftJoin('tb_user', 'tb_user.id', '=', 'arus_stok.id_user')
            ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->whereLike('asb_keterangan', $keterangan)
            ->whereBetween('arus_stok.created_at', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['tanggal'] = Main::format_datetime($row->arus_stok_date);
            $nestedData['kode_barang'] = $row->brg_kode;
            $nestedData['nama_barang'] = $row->brg_nama;
            $nestedData['no_batch'] = $row->sbr_kode_batch;
//            $nestedData['expired_date'] = Main::format_date_label($row->sbr_expired);
            $nestedData['stok_awal'] = $row->asb_stok_awal >= 0 || $row->asb_stok_awal !== NULL ? Main::format_number($row->asb_stok_awal) : '';
            $nestedData['stok_masuk'] = $row->asb_stok_masuk ? Main::format_number($row->asb_stok_masuk) : '';
            $nestedData['stok_keluar'] = $row->asb_stok_keluar ? Main::format_number($row->asb_stok_keluar) : '';
//            $nestedData['sisa_barang'] = Main::format_number($row->asb_stok_terakhir);
            $nestedData['sisa_total_barang'] = Main::format_number($row->asb_stok_total_terakhir);
            $nestedData['keterangan'] = $row->asb_keterangan;
            $nestedData['user'] = $row->nama_karyawan;

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function download($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $kode_barang = $request->kode_barang ? $request->kode_barang : '';
        $nama_barang = $request->nama_barang ? $request->nama_barang : '';
        $keterangan = $request->keterangan ? $request->keterangan : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mArusStok
            ::select([
                'arus_stok.created_at AS arus_stok_date',
                'arus_stok.*',
                'barang.*',
                'stok_barang.*',
                'tb_user.*',
                'tb_karyawan.*',
            ])
            ->leftJoin('barang', 'barang.id_barang', '=', 'arus_stok.id_barang')
            ->leftJoin('stok_barang', 'stok_barang.id_stok_barang', '=', 'arus_stok.id_stok_barang')
            ->leftJoin('tb_user', 'tb_user.id', '=', 'arus_stok.id_user')
            ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->whereLike('asb_keterangan', $keterangan)
            ->whereBetween('arus_stok.created_at', $where_date)
            ->orderBy('id_arus_stok', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];


        if ($request->filter_type == 'download_excel') {
            return view('kartuStok.kartuStok.kartuStokExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('kartuStok.kartuStok.kartuStokPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('kartuStok.kartuStok.kartuStokPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Kartu Stok');
        }

    }

}
