<?php

namespace app\Http\Controllers\Laporan;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHistoryPenyesuaianStok;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Laporan extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['laporan'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => route('laporanList')
            ]
        ];
    }

    function index(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }

        $filter_component = Main::date_filter($request, ['date','download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $data = Main::data($this->breadcrumb);

        $nominal = $this->nominal($date_from_db, $date_to_db);
        $pembelian = $nominal['pembelian'];
        $penjualan = $nominal['penjualan'];
        $hutang = $nominal['hutang'];
        $piutang = $nominal['piutang'];
        $profit = $nominal['profit'];


        $datatable_column_pembelian = [
            ["data" => "no"],
            ["data" => "tanggal"],
            ["data" => "nama_barang"],
            ["data" => "harga_beli"],
            ["data" => "qty"],
            ["data" => "total_beli"],
        ];

        $datatable_column_penjualan = [
            ["data" => "no"],
            ["data" => "tanggal"],
            ["data" => "nama_barang"],
            ["data" => "qty"],
            ["data" => "total_jual"],
        ];

        $data = array_merge($data, [
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),

            'datatable_column_pembelian' => $datatable_column_pembelian,
            'datatable_column_penjualan' => $datatable_column_penjualan,
            'pembelian_total' => $pembelian,
            'penjualan_total' => $penjualan,
            'hutang_total' => $hutang,
            'piutang_total' => $piutang,
            'profit' => $profit,
            'date_filter' => $date_filter
        ]);

        return view('laporan/laporan/laporanList', $data);
    }

    function data_table_pembelian(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPembelianDetail
            ::join('pembelian', 'pembelian.id_pembelian', 'pembelian_detail.id_pembelian')
            ->whereBetween('pbl_tanggal_order', $where_date)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_pembelian_detail'; //$columns[$request->input('order.0.column')];
//        $order_type = $request->input('order.0.dir');
        $order_type = 'asc';

        $data_list = mPembelianDetail
            ::join('pembelian', 'pembelian.id_pembelian', 'pembelian_detail.id_pembelian')
            ->leftJoin('barang', 'barang.id_barang', 'pembelian_detail.id_barang')
            ->whereBetween('pbl_tanggal_order', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['tanggal'] = Main::format_datetime($row->pbl_tanggal_order);
            $nestedData['nama_barang'] = $row->brg_kode . ' ' . $row->brg_nama;
            $nestedData['kode_batch'] = $row->pbd_kode_batch;
            $nestedData['harga_beli'] = Main::format_money($row->pbd_harga_beli);
            $nestedData['qty'] = Main::format_number($row->pbd_qty);
            $nestedData['total_beli'] = Main::format_money($row->pbd_sub_total);

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
        );

        return $json_data;
    }

    function data_table_penjualan(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPenjualanDetail
            ::join('penjualan', 'penjualan.id_penjualan', 'penjualan_detail.id_penjualan')
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'penjualan_detail.id_penjualan_detail'; //$columns[$request->input('order.0.column')];
//        $order_type = $request->input('order.0.dir');
        $order_type = 'asc';

        $data_list = mPenjualanDetail
            ::join('penjualan', 'penjualan.id_penjualan', 'penjualan_detail.id_penjualan')
            ->leftJoin('barang', 'barang.id_barang', 'penjualan_detail.id_barang')
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['tanggal'] = Main::format_datetime($row->pjl_tanggal_penjualan);
            $nestedData['nama_barang'] = $row->brg_kode . ' ' . $row->brg_nama;
            $nestedData['qty'] = Main::format_number($row->pjd_qty);
            $nestedData['total_jual'] = Main::format_money($row->pjd_sub_total);

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function download($request)
    {

        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $date_from_label = Main::format_date_label($date_from_db);
        $date_to_label = Main::format_date_label($date_to_db);

        $nominal = $this->nominal($date_from_db, $date_to_db);
        $pembelian = $nominal['pembelian'];
        $penjualan = $nominal['penjualan'];
        $hutang = $nominal['hutang'];
        $piutang = $nominal['piutang'];
        $profit = $nominal['profit'];

        $data_pembelian = mPembelianDetail
            ::join('pembelian', 'pembelian.id_pembelian', 'pembelian_detail.id_pembelian')
            ->leftJoin('barang', 'barang.id_barang', 'pembelian_detail.id_barang')
            ->whereBetween('pbl_tanggal_order', $where_date)
            ->orderBy('id_pembelian_detail', 'ASC')
            ->get();

        $data_penjualan = mPenjualanDetail
            ::join('penjualan', 'penjualan.id_penjualan', 'penjualan_detail.id_penjualan')
            ->leftJoin('barang', 'barang.id_barang', 'penjualan_detail.id_barang')
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->orderBy('id_penjualan_detail', 'ASC')
            ->get();

        $data = [
            'date_from' => $date_from_label,
            'date_to' => $date_to_label,
            'pembelian' => $pembelian,
            'penjualan' => $penjualan,
            'hutang' => $hutang,
            'piutang' => $piutang,
            'profit' => $profit,
            'data_pembelian' => $data_pembelian,
            'data_penjualan' => $data_penjualan,
        ];


        if ($request->filter_type == 'download_excel') {
            return view('laporan/laporan/laporanExcel', $data);
        }


        if ($request->filter_type == 'download_pdf') {
            return view('laporan/laporan/laporanPdf', $data);
        }
    }

    function nominal($date_from_db, $date_to_db) {

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $pembelian = mPembelian
            ::whereBetween('pbl_tanggal_order', $where_date)
            ->sum('pbl_grand_total');
        $penjualan = mPenjualan
            ::whereBetween('pjl_tanggal_penjualan', $where_date)
            ->sum('pjl_grand_total');
        $hutang = mHutangSupplier
            ::whereBetween('hsp_tanggal', $where_date)
            ->sum('hsp_sisa');
        $piutang = mPiutangPelanggan
            ::whereBetween('ppl_tanggal', $where_date)
            ->sum('ppl_sisa');
        $profit = $penjualan - $pembelian;

        return [
            'pembelian' => $pembelian,
            'penjualan' => $penjualan,
            'hutang' => $hutang,
            'piutang' => $piutang,
            'profit' => $profit
        ];
    }

}
