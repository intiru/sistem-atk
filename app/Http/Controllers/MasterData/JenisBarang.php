<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mJenisBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class JenisBarang extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_jenis_barang'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['nama_jenis_barang', 'keterangan']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $nama_jenis_barang = $filter_component['nama_jenis_barang'];
        $keterangan = $filter_component['keterangan'];

        $data = Main::data($this->breadcrumb);
        $data_list = DB::table('jenis_barang');
        $datatable_column = [
            ["data" => "no"],
            ["data" => "jbr_nama"],
            ["data" => "jbr_keterangan"],
            ["data" => "options"],
        ];
        $data = array_merge($data, [
            'data' => $data_list,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'nama_jenis_barang' => $nama_jenis_barang,
                'keterangan' => $keterangan,
            ),
        ]);

        return view('masterData/jenisBarang/jenisBarangList', $data);
    }

    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $nama_jenis_barang = $data_post['nama_jenis_barang'];
        $keterangan = $data_post['keterangan'];


            $total_data = mJenisBarang
                ::whereLike('id_jenis_barang', $keywords)
                ->orWhereLike('jbr_nama', $nama_jenis_barang)
                ->orWhereLike('jbr_keterangan', $keterangan)
                ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_jenis_barang'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

            $data_list = mJenisBarang
                ::whereLike('id_jenis_barang', $keywords)
                ->orWhereLike('jbr_nama', $nama_jenis_barang)
                ->orWhereLike('jbr_keterangan', $keterangan)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_jenis_barang = Main::encrypt($row->id_jenis_barang);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['jbr_nama'] = $row->jbr_nama;
            $nestedData['jbr_keterangan'] = $row->jbr_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('jenisBarangEditModal', ['id_jenis_barang' => $id_jenis_barang]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus text-danger"
                           href="#"
                            data-route="' . route('jenisBarangDelete', ['id_jenis_barang' => $id_jenis_barang]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }
    function insert(Request $request)
    {
        $request->validate([
            'jbr_nama' => 'required',
        ]);

        $data = $request->except('_token');
        mJenisBarang::create($data);
    }

    function edit_modal($id_jenis_barang)
    {
        $id_jenis_barang = Main::decrypt($id_jenis_barang);
        $edit = mJenisBarang::where('id_jenis_barang', $id_jenis_barang)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/jenisBarang/jenisBarangEditModal', $data);
    }

    function delete($id_jenis_barang)
    {
        $id_jenis_barang = Main::decrypt($id_jenis_barang);
        mJenisBarang::where('id_jenis_barang', $id_jenis_barang)->delete();
    }

    function update(Request $request, $id_jenis_barang)
    {
        $id_jenis_barang = Main::decrypt($id_jenis_barang);
        $request->validate([
            'jbr_nama' => 'required',
        ]);
        $data = $request->except("_token");
        mJenisBarang::where(['id_jenis_barang' => $id_jenis_barang])->update($data);
    }
}
