<?php

namespace app\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use app\Models\mKaryawan;

class Karyawan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_staf'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['keywords', 'nama_staf', 'posisi', 'alamat', 'telepon', 'email']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $nama_staf = $filter_component['nama_staf'];
        $posisi = $filter_component['posisi'];
        $alamat = $filter_component['alamat'];
        $telepon = $filter_component['telepon'];
        $email = $filter_component['email'];

        $data = Main::data($this->breadcrumb);
        $data_list = DB::table('karyawan');
        $datatable_column = [
            ["data" => "no"],
            ["data" => "nama_karyawan"],
            ["data" => "posisi_karyawan"],
            ["data" => "telp_karyawan"],
            ["data" => "alamat_karyawan"],
            ["data" => "email_karyawan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'nama_staf' => $nama_staf,
                'posisi' => $posisi,
                'alamat' => $alamat,
                'telepon' => $telepon,
                'email' => $email
            ),
        ]);
        return view('masterData/karyawan/karyawanList', $data);
    }

    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $nama_staf = $data_post['nama_staf'];
        $posisi = $data_post['posisi'];
        $alamat = $data_post['alamat'];
        $telepon = $data_post['telepon'];
        $email = $data_post['email'];

            $total_data = mKaryawan
                ::whereLike('id', $keywords)
                ->orWhereLike('nama_karyawan', $nama_staf)
                ->orWhereLike('posisi_karyawan', $posisi)
                ->orWhereLike('alamat_karyawan', $alamat)
                ->orWhereLike('telp_karyawan', $telepon)
                ->orWhereLike('email_karyawan', $email)
                ->count();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

            $data_list = mKaryawan
                ::whereLike('id', $keywords)
                ->orWhereLike('nama_karyawan', $nama_staf)
                ->orWhereLike('posisi_karyawan', $posisi)
                ->orWhereLike('alamat_karyawan', $alamat)
                ->orWhereLike('telp_karyawan', $telepon)
                ->orWhereLike('email_karyawan', $email)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();


        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id = Main::encrypt($row->id);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['nama_karyawan'] = $row->nama_karyawan;
            $nestedData['posisi_karyawan'] = $row->posisi_karyawan;
            $nestedData['telp_karyawan'] = $row->telp_karyawan;
            $nestedData['alamat_karyawan'] = $row->alamat_karyawan;
            $nestedData['email_karyawan'] = $row->email_karyawan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('karyawanEditModal', ['id' => $id]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus text-danger"
                           href="#"
                            data-route="' . route('karyawanDelete', ['id' => $id]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'nama_karyawan' => 'required',
            'posisi_karyawan' => 'required',
            'telp_karyawan' => 'required',
            'alamat_karyawan' => 'required',
            'email_karyawan' => 'required|email',
        ]);

        $data = $request->except(['_token']);

        mKaryawan::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $karyawan = mKaryawan::where('id', $id)->first();
        $data = [
            'edit' => $karyawan
        ];

        return view('masterData/karyawan/karyawanEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mKaryawan::where('id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'nama_karyawan' => 'required',
            'posisi_karyawan' => 'required',
            'telp_karyawan' => 'required',
            'alamat_karyawan' => 'required',
            'email_karyawan' => 'required|email'
        ]);

        $id = Main::decrypt($id);
        $data = $request->except(["_token"]);

        mKaryawan::where(['id' => $id])->update($data);
    }
}
