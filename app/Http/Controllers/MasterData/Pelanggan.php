<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mBarangKategori;
use app\Models\mPelanggan;
use app\Models\mSatuan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class Pelanggan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_pelanggan'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['keywords', 'kode_pelanggan', 'nama_pelanggan', 'alamat', 'telepon', 'email', 'keterangan']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $kode_pelanggan = $filter_component['kode_pelanggan'];
        $nama_pelanggan = $filter_component['nama_pelanggan'];
        $alamat = $filter_component['alamat'];
        $telepon = $filter_component['telepon'];
        $email = $filter_component['email'];
        $keterangan = $filter_component['keterangan'];

        $data = Main::data($this->breadcrumb);
        $data_list = DB::table('pelanggan');
        $datatable_column = [
            ["data" => "no"],
            ["data" => "plg_kode"],
            ["data" => "plg_nama"],
            ["data" => "plg_alamat"],
            ["data" => "plg_telepon"],
            ["data" => "plg_email"],
            ["data" => "plg_keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'kode_pelanggan' => $kode_pelanggan,
                'nama_pelanggan' => $nama_pelanggan,
                'alamat' => $alamat,
                'telepon' => $telepon,
                'email' => $email,
                'keterangan' => $keterangan

            ),
        ]);

        return view('masterData/pelanggan/pelangganList', $data);
    }

    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $kode_pelanggan = $data_post['kode_pelanggan'];
        $nama_pelanggan = $data_post['nama_pelanggan'];
        $alamat = $data_post['alamat'];
        $telepon = $data_post['telepon'];
        $email = $data_post['email'];
        $keterangan = $data_post['keterangan'];

            $total_data = mPelanggan
                ::whereLike('id_pelanggan', $keywords)
                ->orWhereLike('plg_kode', $kode_pelanggan)
                ->orWhereLike('plg_nama', $nama_pelanggan)
                ->orWhereLike('plg_alamat', $alamat)
                ->orWhereLike('plg_telepon', $telepon)
                ->orWhereLike('plg_email', $email)
                ->orWhereLike('plg_keterangan', $keterangan)
                ->count();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_pelanggan'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

            $data_list = mPelanggan
                ::whereLike('id_pelanggan', $keywords)
                ->orWhereLike('plg_kode', $kode_pelanggan)
                ->orWhereLike('plg_nama', $nama_pelanggan)
                ->orWhereLike('plg_alamat', $alamat)
                ->orWhereLike('plg_telepon', $telepon)
                ->orWhereLike('plg_email', $email)
                ->orWhereLike('plg_keterangan', $keterangan)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();


        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_pelanggan = Main::encrypt($row->id_pelanggan);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['plg_kode'] = $row->plg_kode;
            $nestedData['plg_nama'] = $row->plg_nama;
            $nestedData['plg_alamat'] = $row->plg_alamat;
            $nestedData['plg_telepon'] = $row->plg_telepon;
            $nestedData['plg_email'] = $row->plg_email;
            $nestedData['plg_keterangan'] = $row->plg_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('pelangganEditModal', ['id_pelanggan' => $id_pelanggan]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus text-danger"
                           href="#"
                            data-route="' . route('pelangganDelete', ['id_pelanggan' => $id_pelanggan]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'plg_kode' => 'required',
            'plg_nama' => 'required',
            'plg_alamat' => 'required',
            'plg_telepon' => 'required',
            'plg_email' => 'required',
        ]);

        $data = $request->except('_token');
        mPelanggan::create($data);
    }

    function edit_modal($id_pelanggan)
    {
        $id_pelanggan = Main::decrypt($id_pelanggan);
        $edit = mPelanggan::where('id_pelanggan', $id_pelanggan)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/pelanggan/pelangganEditModal', $data);
    }

    function delete($id_pelanggan)
    {
        $id_pelanggan = Main::decrypt($id_pelanggan);
        mPelanggan::where('id_pelanggan', $id_pelanggan)->delete();
    }

    function update(Request $request, $id_pelanggan)
    {
        $id_pelanggan = Main::decrypt($id_pelanggan);
        $request->validate([
            'plg_kode' => 'required',
            'plg_nama' => 'required',
            'plg_alamat' => 'required',
            'plg_telepon' => 'required',
            'plg_email' => 'required',
        ]);
        $data = $request->except("_token");
        mPelanggan::where(['id_pelanggan' => $id_pelanggan])->update($data);
    }
}
