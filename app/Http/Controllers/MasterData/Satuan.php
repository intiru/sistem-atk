<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mBarangKategori;
use app\Models\mSatuan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class Satuan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_satuan'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['keywords', 'nama_satuan', 'keterangan']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $nama_satuan = $filter_component['nama_satuan'];
        $keterangan = $filter_component['keterangan'];

        $data = Main::data($this->breadcrumb);
        $data_list = DB::table('satuan');
        $datatable_column = [
            ["data" => "no"],
            ["data" => "stn_nama"],
            ["data" => "stn_keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'nama_satuan' => $nama_satuan,
                'keterangan' => $keterangan,
            ),

        ]);

        return view('masterData/satuan/satuanList', $data);
    }

    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $nama_satuan = $data_post['nama_satuan'];
        $keterangan = $data_post['keterangan'];

            $total_data = mSatuan
                ::whereLike('id_satuan', $keywords)
                ->orWhereLike('stn_nama', $nama_satuan)
                ->orWhereLike('stn_keterangan', $keterangan)
                ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_satuan'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

            $data_list = mSatuan
                ::whereLike('id_satuan', $keywords)
                ->orWhereLike('stn_nama', $nama_satuan)
                ->orWhereLike('stn_keterangan', $keterangan)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_satuan = Main::encrypt($row->id_satuan);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['stn_nama'] = $row->stn_nama;
            $nestedData['stn_keterangan'] = $row->stn_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('satuanEditModal', ['id_satuan' => $id_satuan]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus text-danger"
                           href="#"
                            data-route="' . route('satuanDelete', ['id_satuan' => $id_satuan]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'stn_nama' => 'required'
        ]);

        $data = $request->except('_token');
        mSatuan::create($data);
    }

    function edit_modal($id_satuan)
    {
        $id_satuan = Main::decrypt($id_satuan);
        $edit = mSatuan::where('id_satuan', $id_satuan)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/satuan/satuanEditModal', $data);
    }

    function delete($id_satuan)
    {
        $id_satuan = Main::decrypt($id_satuan);
        mSatuan::where('id_satuan', $id_satuan)->delete();
    }

    function update(Request $request, $id_satuan)
    {
        $id_satuan = Main::decrypt($id_satuan);
        $request->validate([
            'stn_nama' => 'required'
        ]);
        $data = $request->except("_token");
        mSatuan::where(['id_satuan' => $id_satuan])->update($data);
    }
}
