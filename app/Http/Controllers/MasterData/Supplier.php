<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mBarangKategori;
use app\Models\mSatuan;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class Supplier extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_supplier'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['keywords', 'kode_supplier', 'nama_supplier', 'alamat', 'telepon', 'email', 'keterangan']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $kode_supplier = $filter_component ['kode_supplier'];
        $nama_supplier = $filter_component ['nama_supplier'];
        $alamat = $filter_component ['alamat'];
        $telepon = $filter_component ['telepon'];
        $email = $filter_component ['email'];
        $keterangan = $filter_component ['keterangan'];

        $data = Main::data($this->breadcrumb);
        $data_list = DB::table('supplier');
        $datatable_column = [
            ["data" => "no"],
            ["data" => "spl_kode"],
            ["data" => "spl_nama"],
            ["data" => "spl_alamat"],
            ["data" => "spl_phone"],
            ["data" => "spl_email"],
            ["data" => "spl_keterangan"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'kode_supplier' => $kode_supplier,
                'nama_supplier' => $nama_supplier,
                'alamat' => $alamat,
                'telepon' => $telepon,
                'email' => $email,
                'keterangan' => $keterangan
            ),

        ]);

        return view('masterData/supplier/supplierList', $data);
    }

    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $kode_supplier = $data_post['kode_supplier'];
        $nama_supplier = $data_post['nama_supplier'];
        $alamat = $data_post['alamat'];
        $telepon = $data_post['telepon'];
        $email = $data_post['email'];
        $keterangan = $data_post['keterangan'];

            $total_data = mSupplier
                ::whereLike('id_supplier', $keywords)
                ->orWhereLike('spl_kode', $kode_supplier)
                ->orWhereLike('spl_nama', $nama_supplier)
                ->orWhereLike('spl_alamat', $alamat)
                ->orWhereLike('spl_phone', $telepon)
                ->orWhereLike('spl_email', $email)
                ->orWhereLike('spl_keterangan', $keterangan)
                ->count();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_supplier'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

            $data_list = mSupplier
                ::whereLike('id_supplier', $keywords)
                ->orWhereLike('spl_kode', $kode_supplier)
                ->orWhereLike('spl_nama', $nama_supplier)
                ->orWhereLike('spl_alamat', $alamat)
                ->orWhereLike('spl_phone', $telepon)
                ->orWhereLike('spl_email', $email)
                ->orWhereLike('spl_keterangan', $keterangan)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();


        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_supplier = Main::encrypt($row->id_supplier);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['spl_kode'] = $row->spl_kode;
            $nestedData['spl_nama'] = $row->spl_nama;
            $nestedData['spl_alamat'] = $row->spl_alamat;
            $nestedData['spl_phone'] = $row->spl_phone;
            $nestedData['spl_email'] = $row->spl_email;
            $nestedData['spl_keterangan'] = $row->spl_keterangan;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('supplierEditModal', ['id_supplier' => $id_supplier]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus text-danger"
                           href="#"
                            data-route="' . route('supplierDelete', ['id_supplier' => $id_supplier]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'spl_nama' => 'required',
            'spl_phone' => 'required',
            'spl_email' => 'required',
            'spl_alamat' => 'required',
        ]);

        $data = $request->except('_token');
        mSupplier::create($data);
    }

    function edit_modal($id_supplier)
    {
        $id_supplier = Main::decrypt($id_supplier);
        $edit = mSupplier::where('id_supplier', $id_supplier)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/supplier/supplierEditModal', $data);
    }

    function delete($id_supplier)
    {
        $id_supplier = Main::decrypt($id_supplier);
        mSupplier::where('id_supplier', $id_supplier)->delete();
    }

    function update(Request $request, $id_supplier)
    {
        $id_supplier = Main::decrypt($id_supplier);
        $request->validate([
            'spl_nama' => 'required',
            'spl_phone' => 'required',
            'spl_email' => 'required',
            'spl_alamat' => 'required',
        ]);
        $data = $request->except("_token");
        mSupplier::where(['id_supplier' => $id_supplier])->update($data);
    }
}
