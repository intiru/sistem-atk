<?php

namespace app\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use app\Rules\UsernameChecker;
use app\Rules\UsernameCheckerUpdate;
use Illuminate\Support\Facades\Config;

use app\Models\mKaryawan;
use app\Models\mUser;
use app\Models\mUserRole;

class User extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_user'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['keywords', 'nama_staf', 'username', 'posisi', 'nama_role']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $nama_staf = $filter_component['nama_staf'];
        $username = $filter_component['username'];
        $posisi = $filter_component['posisi'];
        $nama_role = $filter_component['nama_role'];

        $data = Main::data($this->breadcrumb);
        $karyawan = mKaryawan
            ::select(['id', 'nama_karyawan'])
            ->orderBy('nama_karyawan', 'ASC')
            ->get();
        $user_role = mUserRole::orderBy('role_name', 'ASC')->get();
        $user = mUser::with(['user_role', 'karyawan'])->orderBy('username', 'ASC')->get();
        $data_list = DB::table('user');

        $datatable_column = [
            ["data" => "no"],
            ["data" => "username"],
            ["data" => "nama_karyawan"],
            ["data" => "posisi_karyawan"],
            ["data" => "role_name"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'karyawan' => $karyawan,
            'user_role' => $user_role,
            'data' => $user,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'nama_staf' => $nama_staf,
                'username' => $username,
                'posisi' => $posisi,
                'nama_role' => $nama_role
            ),
        ]);

        return view('masterData/user/userList', $data);
    }

    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $nama_staf = $data_post['nama_staf'];
        $username = $data_post['username'];
        $posisi = $data_post['posisi'];
        $nama_role = $data_post['nama_role'];


            $total_data = mUser
                ::leftJoin('tb_user_role', 'tb_user_role.id', '=', 'tb_user.id_user_role')
                ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
                ->whereLike('tb_user.id', $keywords)
                ->orWhereLike('tb_user.username', $username)
                ->orWhereLike('tb_karyawan.nama_karyawan', $nama_staf)
                ->orWhereLike('tb_karyawan.posisi_karyawan', $posisi)
                ->orWhereLike('tb_user_role.role_name', $nama_role)
                ->count();


        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'tb_user.id'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');


            $data_list = mUser
                ::leftJoin('tb_user_role', 'tb_user_role.id', '=', 'tb_user.id_user_role')
                ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
                ->whereLike('tb_user.id', $keywords)
                ->orWhereLike('tb_user.username', $username)
                ->orWhereLike('tb_karyawan.nama_karyawan', $nama_staf)
                ->orWhereLike('tb_karyawan.posisi_karyawan', $posisi)
                ->orWhereLike('tb_user_role.role_name', $nama_role)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();


        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id = Main::encrypt($row->id);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['username'] = $row->username;
            $nestedData['nama_karyawan'] = $row->karyawan->nama_karyawan;
            $nestedData['posisi_karyawan'] = $row->karyawan->posisi_karyawan;
            $nestedData['role_name'] = $row->user_role->role_name;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('userEditModal', ['id' => $id]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus text-danger"
                           href="#"
                            data-route="' . route('userDelete', ['id' => $id]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_karyawan' => 'required',
            'id_user_role' => 'required',
            'username' => ['required', new UsernameChecker],
            'password' => 'required'
        ]);

        $data = $request->except('_token');
        $data['password'] = Hash::make($data['password']);
        mUser::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $user = mUser::where('id', $id)->first();
        $karyawan = mKaryawan
            ::select(['id', 'nama_karyawan'])
            ->orderBy('nama_karyawan', 'ASC')
            ->get();
        $user_role = mUserRole::orderBy('role_name', 'ASC')->get();
        $data = [
            'edit' => $user,
            'karyawan' => $karyawan,
            'user_role' => $user_role
        ];

        return view('masterData/user/userEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mUser::where('id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'id_karyawan' => 'required',
            'id_user_role' => 'required',
            'username' => ['required', new UsernameCheckerUpdate($id)],
            //'password' => 'required'
        ]);
        $data = $request->except("_token");
        if ($request->input('password')) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        mUser::where(['id' => $id])->update($data);
    }
}
