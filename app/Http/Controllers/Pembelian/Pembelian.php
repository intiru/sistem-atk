<?php

namespace app\Http\Controllers\Pembelian;

use app\Helpers\Main;
use app\Http\Controllers\Controller;
use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHutangSupplier;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPiutangLain;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Pembelian extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');
        $this->menuActive = $cons['pembelian'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['pembelian'],
                'route' => route('pembelianList')
            ]
        ];
    }

    function index(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }


        $filter_component = Main::date_filter($request, ['date', 'keywords', 'no_faktur_pembelian', 'no_faktur_supplier', 'nama_supplier', 'download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $no_faktur_pembelian = $filter_component['no_faktur_pembelian'];
        $no_faktur_supplier = $filter_component['no_faktur_supplier'];
        $nama_supplier = $filter_component['nama_supplier'];

        $data = Main::data($this->breadcrumb);
        $datatable_column = [
            ["data" => "no"],
            ["data" => "no_faktur"],
//            ["data" => "no_faktur_supplier"],
            ["data" => "spl_nama"],
            ["data" => "tanggal_order"],
            ["data" => "jenis_pembayaran"],
            ["data" => "qty_total"],
            ["data" => "grand_total"],
            ["data" => "simpan_status"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'no_faktur_pembelian' => $no_faktur_pembelian,
                'no_faktur_supplier' => $no_faktur_supplier,
                'nama_supplier' => $nama_supplier
            ),
        ]);

        return view('pembelian/pembelian/pembelianList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];
        $no_faktur_pembelian = $data_post['no_faktur_pembelian'];
        $no_faktur_supplier = $data_post['no_faktur_supplier'];
        $nama_supplier = $data_post['nama_supplier'];

        $total_data = mPembelian
            ::leftJoin('supplier', 'supplier.id_supplier', '=', 'pembelian.id_supplier')
            ->whereBetween('pbl_tanggal_order', $where_date)
            ->whereLike('pbl_no_faktur', $no_faktur_pembelian)
            ->whereLike('pbl_no_faktur_supplier', $no_faktur_supplier)
            ->whereLike('spl_nama', $nama_supplier)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_pembelian'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');
        $simpan_status = '';
        $delete_warning = '';

        $data_list = mPembelian
            ::select([
                'id_pembelian',
                'pbl_no_faktur',
                'pbl_no_faktur_supplier',
                'pbl_tanggal_order',
                'pbl_jenis_pembayaran',
                'pbl_grand_total',
                'pbl_simpan_status',
                'spl_nama',
            ])
            ->leftJoin('supplier', 'supplier.id_supplier', '=', 'pembelian.id_supplier')
            ->whereBetween('pbl_tanggal_order', $where_date)
            ->whereLike('pbl_no_faktur', $no_faktur_pembelian)
            ->whereLike('pbl_no_faktur_supplier', $no_faktur_supplier)
            ->whereLike('spl_nama', $nama_supplier)
            ->withCount([
                'pembelian_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(pbd_qty)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_pembelian = Main::encrypt($row->id_pembelian);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            if ($row->pbl_simpan_status == 'simpan') {
                $simpan_status = '<span class="m-badge m-badge--warning m-badge--wide">Simpan</span>';
                $delete_warning = 'Sistem akan menghapus data Pembelian ini';
            } elseif ($row->pbl_simpan_status == 'posting') {
                $simpan_status = '<span class="m-badge m-badge--success m-badge--wide">Posting</span>';
                $delete_warning = 'Sistem akan menghapus pembelian ini & Melakukan check stok yang ada untuk Dikurangi.';
            }

            $nestedData['no'] = $no;
            $nestedData['no_faktur'] = $row->pbl_no_faktur;
//            $nestedData['no_faktur_supplier'] = $row->pbl_no_faktur_supplier;
            $nestedData['spl_nama'] = $row->spl_nama;
            $nestedData['tanggal_order'] = Main::format_datetime($row->pbl_tanggal_order);
            $nestedData['jenis_pembayaran'] = $row->pbl_jenis_pembayaran;
            $nestedData['grand_total'] = Main::format_number($row->pbl_grand_total);
            $nestedData['qty_total'] = Main::format_number($row->total_qty);
            $nestedData['simpan_status'] = $simpan_status;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">';

            /**
             * Jika belum di posting, maka dapat edit stok dan harga,
             * karena masih belum masih ke dalam stok dan kartu stok
             *
             * jika sudah posting, makan hanya dapat edit harga saja
             */
            if ($row->pbl_simpan_status == 'simpan') {
                $nestedData['options'] .= '<a class="akses-edit dropdown-item"
                           href="' . route('pembelianEditStok', ['id_pembelian' => $id_pembelian]) . '">
                            <i class="la la-edit"></i>
                            Edit Harga & Barang
                        </a>';
            } elseif ($row->pbl_simpan_status == 'posting') {
                $nestedData['options'] .= '<a class="akses-edit dropdown-item"
                           href="' . route('pembelianEditHarga', ['id_pembelian' => $id_pembelian]) . '">
                            <i class="la la-edit"></i>
                            Edit Harga
                        </a>';
            }

            $nestedData['options'] .= '
                        <a class="akses-detail btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('pembelianDetailModal', ['id_pembelian' => $id_pembelian]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>';

            if ($row->pbl_simpan_status == 'simpan') {

                $nestedData['options'] .= '
							<div class="dropdown-divider"></div>
                            <a class="akses-posting dropdown-item text-accent btn-confirm-custom"
                                href="#"
                                data-title="Yakin Posting Pembelian '.$row->pbl_no_faktur.' ?"
                                data-description="Item Pembelian Barang akan masuk kedalam <strong>Stok Barang</strong>, jika ada kekeliruan untuk mengubahnya perlu diatur pada <strong>Penyesuaian Stok</>"
                                data-type="warning"
                                data-method="get"
                                data-route="' . route('pembelianProsesPosting', ['id_pembelian' => $id_pembelian]) . '">
                                <i class="la la-refresh text-accent"></i>
                                Posting Pembelian
                            </a>';

            }

            $nestedData['options'] .= '
							<div class="dropdown-divider"></div>
                            <a class="akses-hapus btn-confirm-custom dropdown-item text-danger"
                            href="#"
                                data-title="Yakin Menghapus Pembelian ' . $row->pbl_no_faktur . ' ? "
                                data-description="' . $delete_warning . '"
                                data-type="warning"
                                data-method="delete"
                                data-route="' . route('pembelianDelete', ['id_pembelian' => $id_pembelian]) . '">
                                <i class="la la-remove text-danger"></i>
                                Hapus Pembelian
                            </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function create()
    {
        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Pembelian Baru',
                    'route' => ''
                ]
            ]
        );
        $data = Main::data($breadcrumb, $this->menuActive);
        $supplier = mSupplier::orderBy('spl_nama', 'ASC')->get();
        $barang = mBarang::orderBy('brg_kode')->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();

        $data = array_merge(
            $data, array(
                'supplier' => $supplier,
                'barang' => $barang,
                'satuan' => $satuan
            )
        );

        return view('pembelian/pembelian/pembelianCreate', $data);
    }

    function no_faktur(Request $request)
    {
        $kode_supplier = $request->input('kode_supplier');
        $id_supplier = $request->input('id_supplier');
        $pbl_no_faktur = Main::fakturPembelian($kode_supplier);


        return [
            $pbl_no_faktur
        ];
    }

    /**
     * @param Request $request
     *
     * Proses berikut ini digunakan untuk memilah apakah proses penyimpanan pembelian hanya simpan saja
     * atau, mempengaruhi stok
     *
     * karena terkadang, admin web, hanya ingin menyimpan saja, tanpa melakukan posting data,
     * untuk dapat mengecheck kembali dikemudian saat.
     *
     *
     */
    function insert(Request $request)
    {
        $pbl_simpan_status = $request->input('pbl_simpan_status');
        if ($pbl_simpan_status == 'simpan') {
            return $this->insert_data($request);
        } elseif ($pbl_simpan_status == 'posting') {
            return $this->insert_posting($request);
        }
    }

    function insert_data($request)
    {
        $rules = [
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_beli' => 'required',
            'harga_beli.*' => 'required',
            'qty' => 'required',
            'qty.*' => 'required',
            'total_ppn' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_supplier' => 'Supplier',
            'id_barang' => 'Barang',
            'harga_beli' => 'Harga Beli',
            'qty' => 'Qty Barang',
            'total_ppn' => 'Total PPN',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_beli.' . $i] = 'Harga Beli ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_supplier = $request->input('id_supplier');
        $pbl_no_faktur_supplier = $request->input('pbl_no_faktur_supplier');
        $kode_supplier = $request->input('kode_supplier');
        $id_barang_arr = $request->input('id_barang');

        $pbl_no_faktur = Main::fakturPembelian($kode_supplier);
        $pbl_tanggal_order = Main::format_datetime_db($request->input('tanggal'));
        $pbl_jenis_pembayaran = $request->input('pembayaran');
        $pbl_keterangan = $request->input('keterangan');

        $pbd_harga_net_arr = $request->input('harga_net');
        $pbd_sub_total_arr = $request->input('sub_total');
        $pbd_harga_beli_arr = $request->input('harga_beli');
        $pbd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pbd_ppn_persen = $this->ppnPersen;
        $pbd_qty_arr = $request->input('qty');

        $pbl_total = $request->input('total');
        $pbl_total_ppn = Main::format_number_db($request->input('total_ppn'));
        $pbl_ppn_persentase = $this->ppnPersen;
        $pbl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pbl_potongan = Main::format_number_db($request->input('potongan'));
        $pbl_grand_total = $request->input('grand_total');
        $pbl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pbl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $hsp_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $created_at = date('Y-m-d H:i:s');
        $updated_at = $created_at;


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $pembelian_data = [
                'id_supplier' => $id_supplier,
                'id_user' => $id_user,
                'pbl_no_faktur' => $pbl_no_faktur,
                'pbl_no_faktur_supplier' => $pbl_no_faktur_supplier,
                'pbl_tanggal_order' => $pbl_tanggal_order,
                'pbl_jenis_pembayaran' => $pbl_jenis_pembayaran,
                'pbl_total' => $pbl_total,
                'pbl_total_ppn' => $pbl_total_ppn,
                'pbl_ppn_persentase' => $pbl_ppn_persentase,
                'pbl_biaya_tambahan' => $pbl_biaya_tambahan,
                'pbl_potongan' => $pbl_potongan,
                'pbl_grand_total' => $pbl_grand_total,
                'pbl_jumlah_bayar' => $pbl_jumlah_bayar,
                'pbl_sisa_pembayaran' => $pbl_sisa_pembayaran,
                'pbl_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                'pbl_keterangan' => $pbl_keterangan,
            ];

            $response_pembelian = mPembelian::create($pembelian_data);
            $id_pembelian = $response_pembelian->id_pembelian;

            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke pembelian detail
                 */
                $pbd_harga_beli = Main::format_number_db($pbd_harga_beli_arr[$index]);
                $pbd_ppn_nominal = Main::format_number_db($pbd_ppn_nominal_arr[$index]);
                $pbd_harga_net = Main::format_number_db($pbd_harga_net_arr[$index]);
                $pbd_qty = Main::format_number_db($pbd_qty_arr[$index]);
                $pbd_sub_total = $pbd_sub_total_arr[$index];

                $pembelian_detail_data = [
                    'id_pembelian' => $id_pembelian,
                    'id_barang' => $id_barang,
                    'pbd_harga_beli' => $pbd_harga_beli,
                    'pbd_ppn_nominal' => $pbd_ppn_nominal,
                    'pbd_harga_net' => $pbd_harga_net,
                    'pbd_qty' => $pbd_qty,
                    'pbd_sub_total' => $pbd_sub_total,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];

                mPembelianDetail::create($pembelian_detail_data);

            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function insert_posting($request)
    {
        $rules = [
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_beli' => 'required',
            'harga_beli.*' => 'required',
            'qty' => 'required',
            'qty.*' => 'required',
            'total_ppn' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_supplier' => 'Supplier',
            'id_barang' => 'Barang',
            'harga_beli' => 'Harga Beli',
            'qty' => 'Qty Barang',
            'total_ppn' => 'Total PPN',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_beli.' . $i] = 'Harga Beli ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_supplier = $request->input('id_supplier');
        $pbl_no_faktur_supplier = $request->input('pbl_no_faktur_supplier');
        $kode_supplier = $request->input('kode_supplier');
        $id_barang_arr = $request->input('id_barang');

        $pbl_no_faktur = Main::fakturPembelian($kode_supplier);
        $pbl_tanggal_order = Main::format_datetime_db($request->input('tanggal'));
        $pbl_jenis_pembayaran = $request->input('pembayaran');
        $pbl_keterangan = $request->input('keterangan');

        $pbd_harga_net_arr = $request->input('harga_net');
        $pbd_sub_total_arr = $request->input('sub_total');
        $pbd_harga_beli_arr = $request->input('harga_beli');
        $id_satuan_arr = $request->input('id_satuan');
        $pbd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pbd_ppn_persen = $this->ppnPersen;
        $pbd_qty_arr = $request->input('qty');

        $pbl_total = $request->input('total');
        $pbl_total_ppn = Main::format_number_db($request->input('total_ppn'));
        $pbl_ppn_persentase = $this->ppnPersen;
        $pbl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pbl_potongan = Main::format_number_db($request->input('potongan'));
        $pbl_grand_total = $request->input('grand_total');
        $pbl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pbl_sisa_pembayaran = $request->input('sisa_pembayaran');
        $pbl_simpan_status = $request->input('pbl_simpan_status');

        $hsp_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $created_at = date('Y-m-d H:i:s');
        $updated_at = $created_at;


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $pembelian_data = [
                'id_supplier' => $id_supplier,
                'id_user' => $id_user,
                'pbl_no_faktur' => $pbl_no_faktur,
                'pbl_no_faktur_supplier' => $pbl_no_faktur_supplier,
                'pbl_tanggal_order' => $pbl_tanggal_order,
                'pbl_jenis_pembayaran' => $pbl_jenis_pembayaran,
                'pbl_total' => $pbl_total,
                'pbl_total_ppn' => $pbl_total_ppn,
                'pbl_ppn_persentase' => $pbl_ppn_persentase,
                'pbl_biaya_tambahan' => $pbl_biaya_tambahan,
                'pbl_potongan' => $pbl_potongan,
                'pbl_grand_total' => $pbl_grand_total,
                'pbl_jumlah_bayar' => $pbl_jumlah_bayar,
                'pbl_sisa_pembayaran' => $pbl_sisa_pembayaran,
                'pbl_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                'pbl_keterangan' => $pbl_keterangan,
                'pbl_simpan_status' => $pbl_simpan_status,
                'pbl_posting_waktu' => date('Y-m-d H:i:s'),
                'pbl_posting_id_user' => $id_user
            ];


            $response_pembelian = mPembelian::create($pembelian_data);
            $id_pembelian = $response_pembelian->id_pembelian;

            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke pembelian detail
                 */
                $pbd_harga_beli = Main::format_number_db($pbd_harga_beli_arr[$index]);
                $pbd_ppn_nominal = Main::format_number_db($pbd_ppn_nominal_arr[$index]);
                $pbd_harga_net = Main::format_number_db($pbd_harga_net_arr[$index]);
                $pbd_qty = Main::format_number_db($pbd_qty_arr[$index]);
                $pbd_sub_total = $pbd_sub_total_arr[$index];

                $pembelian_detail_data = [
                    'id_pembelian' => $id_pembelian,
                    'id_barang' => $id_barang,
                    'pbd_harga_beli' => $pbd_harga_beli,
                    'pbd_ppn_nominal' => $pbd_ppn_nominal,
                    'pbd_harga_net' => $pbd_harga_net,
                    'pbd_qty' => $pbd_qty,
                    'pbd_sub_total' => $pbd_sub_total,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];

                $response_pembelian_detail = mPembelianDetail::create($pembelian_detail_data);
                $id_pembelian_detail = $response_pembelian_detail->id_pembelian_detail;

                /**
                 * Memasukkan ke stok barang
                 */
                $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                $stok_barang_data = [
                    'id_barang' => $id_barang,
                    'id_pembelian' => $id_pembelian,
                    'id_pembelian_detail' => $id_pembelian_detail,
                    'id_supplier' => $id_supplier,
                    'sbr_qty' => $pbd_qty,
                    'sbr_harga_beli' => $pbd_harga_beli,
                    'sbr_status' => 'ready',
                ];

                $response_stok_barang = mStokBarang::create($stok_barang_data);
                $id_stok_barang = $response_stok_barang->id_stok_barang;
                $asb_stok_terakhir = $pbd_qty;
                $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                $arus_stok_data = [
                    'id_pembelian' => $id_pembelian,
                    'id_pembelian_detail' => $id_pembelian_detail,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'id_user' => $id_user,
                    'asb_stok_awal' => $asb_stok_awal,
                    'asb_stok_masuk' => $pbd_qty,
                    'asb_stok_keluar' => 0,
                    'asb_stok_terakhir' => $asb_stok_terakhir,
                    'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                    'asb_keterangan' => 'Pembelian Barang No Faktur : ' . $pbl_no_faktur,
                    'asb_tipe_proses' => 'insert',
                    'asb_nama_proses' => 'pembelian'
                ];

                mArusStok::create($arus_stok_data);
            }

            if ($pbl_sisa_pembayaran > 0) {
                $hsp_no_faktur = Main::fakturHutangSupplier($kode_supplier);

                $hutang_supplier_data = [
                    'id_supplier' => $id_supplier,
                    'id_pembelian' => $id_pembelian,
                    'hsp_no_faktur' => $hsp_no_faktur,
                    'hsp_tanggal' => $pbl_tanggal_order,
                    'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                    'hsp_total' => $pbl_sisa_pembayaran,
                    'hsp_sisa' => $pbl_sisa_pembayaran,
                    'hsp_keterangan' => 'Hutang Supplier #' . $kode_supplier . ' dengan no faktur ' . $pbl_no_faktur,
                    'hsp_status' => 'belum_lunas'
                ];

                mHutangSupplier::create($hutang_supplier_data);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    /**
     * @param $id_pembelian
     * @return \Illuminate\Http\RedirectResponse
     *
     * Fungsi Proses Posting merupakan proses yang digunakan untuk memasukkan data qty pembelian barang
     * ke dalam stok barang dan kartu stok
     *
     * sehingga nanti bisa dilakukan penjualan barang
     */
    function proses_posting($id_pembelian)
    {
        $id_pembelian = Main::decrypt($id_pembelian);
        $user = Session::get('user');
        $id_user = $user['id'];
        $pembelian = mPembelian::where('id_pembelian', $id_pembelian)->first();
        $pembelian_detail = mPembelianDetail::where('id_pembelian', $id_pembelian)->get();

        $id_supplier = $pembelian->id_supplier;
        $pbl_no_faktur = $pembelian->pbl_no_faktur;
        $pbl_sisa_pembayaran = $pembelian->pbl_sisa_pembayaran;
        $kode_supplier = mSupplier::where('id_supplier', $id_supplier)->value('spl_kode');
        $pbl_tanggal_order = $pembelian->pbl_tanggal_order;
        $hsp_tanggal_jatuh_tempo = $pembelian->pbl_jatuh_tempo;


        /**
         * Kondisi dibawah digunakan untuk proses antisipasi jika URL nya digunakan berkali kali,
         * jika sudah digunakan dan statusnya posting, maka tidak bisa melakukan posting kembali
         */
        if ($pembelian->pbl_simpan_status == 'simpan') {

            DB::beginTransaction();
            try {

                /**
                 * Update Proses Posting ke Pembelian
                 */
                $pembelian_update = [
                    'pbl_simpan_status' => 'posting',
                    'pbl_posting_waktu' => date('Y-m-d H:i:s'),
                    'pbl_posting_id_user' => $id_user
                ];

                mPembelian::where('id_pembelian', $id_pembelian)->update($pembelian_update);


                /**
                 * Proses memasukkan qty pembelian barang ke stok barang
                 * dan kartu stok
                 */
                foreach ($pembelian_detail as $row) {

                    $id_barang = $row->id_barang;
                    $id_pembelian_detail = $row->id_pembelian_detail;
                    $pbd_qty = $row->pbd_qty;
                    $pbd_harga_beli = $row->pbd_harga_beli;

                    /**
                     * Memasukkan ke stok barang
                     */
                    $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                    $stok_barang_data = [
                        'id_barang' => $id_barang,
                        'id_pembelian' => $id_pembelian,
                        'id_pembelian_detail' => $id_pembelian_detail,
                        'id_supplier' => $id_supplier,
                        'sbr_qty' => $pbd_qty,
                        'sbr_harga_beli' => $pbd_harga_beli,
                        'sbr_status' => 'ready',
                    ];

                    $response_stok_barang = mStokBarang::create($stok_barang_data);
                    $id_stok_barang = $response_stok_barang->id_stok_barang;
                    $asb_stok_terakhir = $pbd_qty;
                    $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                    $arus_stok_data = [
                        'id_pembelian' => $id_pembelian,
                        'id_pembelian_detail' => $id_pembelian_detail,
                        'id_barang' => $id_barang,
                        'id_stok_barang' => $id_stok_barang,
                        'id_user' => $id_user,
                        'asb_stok_awal' => $asb_stok_awal,
                        'asb_stok_masuk' => $pbd_qty,
                        'asb_stok_keluar' => 0,
                        'asb_stok_terakhir' => $asb_stok_terakhir,
                        'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                        'asb_keterangan' => 'Pembelian Barang No Faktur : ' . $pbl_no_faktur,
                        'asb_tipe_proses' => 'insert',
                        'asb_nama_proses' => 'pembelian'
                    ];

                    mArusStok::create($arus_stok_data);
                }

                if ($pbl_sisa_pembayaran > 0) {
                    $hsp_no_faktur = Main::fakturHutangSupplier($kode_supplier);

                    $hutang_supplier_data = [
                        'id_supplier' => $id_supplier,
                        'id_pembelian' => $id_pembelian,
                        'hsp_no_faktur' => $hsp_no_faktur,
                        'hsp_tanggal' => $pbl_tanggal_order,
                        'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                        'hsp_total' => $pbl_sisa_pembayaran,
                        'hsp_sisa' => $pbl_sisa_pembayaran,
                        'hsp_keterangan' => 'Hutang Supplier #' . $kode_supplier . ' dengan no faktur ' . $pbl_no_faktur,
                        'hsp_status' => 'belum_lunas'
                    ];

                    mHutangSupplier::create($hutang_supplier_data);
                }

                DB::commit();
            } catch (Exception $e) {
                throw $e;
                DB::rollBack();
            }
        }

        return redirect()->route('pembelianList');
    }

    function edit_harga($id_pembelian)
    {
        $id_pembelian = Main::decrypt($id_pembelian);

        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Edit Pembelian',
                    'route' => ''
                ]
            ]
        );

        $data = Main::data($breadcrumb, $this->menuActive);
        $supplier = mSupplier::orderBy('spl_nama', 'ASC')->get();
        $barang = mBarang::orderBy('brg_kode')->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $edit = mPembelian
            ::with([
                'supplier',
                'pembelian_detail',
                'pembelian_detail.barang',
            ])
            ->where('id_pembelian', $id_pembelian)
            ->first();

        $data = array_merge(
            $data, array(
                'supplier' => $supplier,
                'barang' => $barang,
                'satuan' => $satuan,
                'edit' => $edit
            )
        );

        return view('pembelian/pembelian/pembelianEditHarga', $data);
    }

    function edit_stok($id_pembelian)
    {
        $id_pembelian = Main::decrypt($id_pembelian);

        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Edit Pembelian',
                    'route' => ''
                ]
            ]
        );

        $data = Main::data($breadcrumb, $this->menuActive);
        $supplier = mSupplier::orderBy('spl_nama', 'ASC')->get();
        $barang = mBarang::orderBy('brg_kode')->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $edit = mPembelian
            ::with([
                'supplier',
                'pembelian_detail',
                'pembelian_detail.barang',
            ])
            ->where('id_pembelian', $id_pembelian)
            ->first();

        $data = array_merge(
            $data, array(
                'supplier' => $supplier,
                'barang' => $barang,
                'satuan' => $satuan,
                'edit' => $edit
            )
        );

        return view('pembelian/pembelian/pembelianEditStok', $data);
    }

    /**
     * @param Request $request
     * @param $id_pembelian
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     *
     * Pada proses update disini tidak memperbarui stok barang,
     * stok barang dapat diperbarui di Penyesuaian stok,
     * untuk memfokuskan fungsi menu penyesuaian Stok
     *
     * sehingga proses disini hanya memperbarui data harga beli, ppn, biaya tambahan, potongan, dan jumlah bayar
     */
    function update_harga(Request $request, $id_pembelian)
    {
        $rules = [
            'harga_beli' => 'required',
            'harga_beli.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'pbl_no_faktur_supplier' => 'No Faktur Supplier',
            'harga_beli' => 'Harga Beli',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['harga_beli.' . $i] = 'Harga Beli ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_pembelian = Main::decrypt($id_pembelian);
        $id_pembelian_detail_arr = $request->input('id_pembelian_detail');
        $id_supplier = $request->input('id_supplier');
        $kode_supplier = $request->input('kode_supplier');
        $pbl_no_faktur = $request->input('no_faktur');
        $pbl_no_faktur_supplier = $request->input('pbl_no_faktur_supplier');

        $pbl_tanggal_order = Main::format_datetime_db($request->input('tanggal'));
        $pbl_jenis_pembayaran = $request->input('pembayaran');
        $pbl_keterangan = $request->input('keterangan');

        $pbd_harga_net_arr = $request->input('harga_net');
        $pbd_sub_total_arr = $request->input('sub_total');
        $pbd_harga_beli_arr = $request->input('harga_beli');
//        $pbd_harga_jual_arr = $request->input('harga_jual');
        $pbd_kode_batch_arr = $request->input('kode_batch');
        $pbd_konsinyasi_status_arr = $request->input('konsinyasi');
        $pbd_expired_date_arr = $request->input('expired_date');
        $id_satuan_arr = $request->input('id_satuan');
        $pbd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pbd_ppn_persen = $this->ppnPersen;
        $pbd_qty_arr = $request->input('qty');

        $pbl_total = $request->input('total');
        $pbl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pbl_potongan = Main::format_number_db($request->input('potongan'));
        $pbl_grand_total = $request->input('grand_total');
        $pbl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pbl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $hsp_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $hutang_supplier = mHutangSupplier::where('id_pembelian', $id_pembelian)->first();
        $pembelian = mPembelian::where('id_pembelian', $id_pembelian)->first();


        DB::beginTransaction();
        try {


            /**
             * Proses pembaruan keterangan pada kartu stok
             */

            $arus_stok_data = mArusStok::where('id_pembelian', $id_pembelian)->get();
            foreach ($arus_stok_data as $row) {
                $id_arus_stok = $row->id_arus_stok;
                $asb_keterangan = $row->asb_keterangan;
                $asb_keterangan = str_replace($pembelian->pbl_no_faktur, $pbl_no_faktur, $asb_keterangan);
                $arus_stok_update = [
                    'asb_keterangan' => $asb_keterangan
                ];
                mArusStok
                    ::where('id_arus_stok', $id_arus_stok)
                    ->update($arus_stok_update);
            }

            /**
             * Proses pembaruan keterangan pada hutang supplier
             */

            $hutang_supplier_data = mHutangSupplier::where('id_pembelian', $id_pembelian)->get();
            foreach($hutang_supplier_data as $row) {
                $id_hutang_supplier = $row->id_hutang_supplier;
                $hsp_keterangan = $row->hsp_keterangan;
                $hsp_keterangan = str_replace($pembelian->pbl_no_faktur, $pbl_no_faktur, $hsp_keterangan);
                $hutang_supplier_update = [
                    'hsp_keterangan' => $hsp_keterangan
                ];

                mHutangSupplier::where('id_hutang_supplier', $id_hutang_supplier)->update($hutang_supplier_update);
            }


            /**
             * Memasukkan ke pembelian data
             */
            $pembelian_data = [
                'id_supplier' => $id_supplier,
                'pbl_no_faktur' => $pbl_no_faktur,
                'pbl_no_faktur_supplier' => $pbl_no_faktur_supplier,
                'pbl_tanggal_order' => $pbl_tanggal_order,
                'pbl_jenis_pembayaran' => $pbl_jenis_pembayaran,
                'pbl_total' => $pbl_total,
                'pbl_biaya_tambahan' => $pbl_biaya_tambahan,
                'pbl_potongan' => $pbl_potongan,
                'pbl_grand_total' => $pbl_grand_total,
                'pbl_jumlah_bayar' => $pbl_jumlah_bayar,
                'pbl_sisa_pembayaran' => $pbl_sisa_pembayaran,
                'pbl_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                'pbl_keterangan' => $pbl_keterangan,
            ];

            mPembelian::where('id_pembelian', $id_pembelian)->update($pembelian_data);

            foreach ($id_pembelian_detail_arr as $index => $id_pembelian_detail) {

                /**
                 * Memperbarui data ke pembelian detail
                 */
                $pbd_harga_beli = Main::format_number_db($pbd_harga_beli_arr[$index]);
                $id_satuan = isset($id_satuan_arr[$index]) ? $id_satuan_arr[$index] : '';
                $pbd_ppn_nominal = Main::format_number_db($pbd_ppn_nominal_arr[$index]);
                $pbd_harga_net = Main::format_number_db($pbd_harga_net_arr[$index]);
                $pbd_sub_total = $pbd_sub_total_arr[$index];

                $pembelian_detail_data = [
                    'id_satuan' => $id_satuan,
                    'pbd_harga_beli' => $pbd_harga_beli,
                    'pbd_ppn_persen' => $pbd_ppn_persen,
                    'pbd_ppn_nominal' => $pbd_ppn_nominal,
                    'pbd_harga_net' => $pbd_harga_net,
                    'pbd_sub_total' => $pbd_sub_total,
                ];

                mPembelianDetail::where('id_pembelian_detail', $id_pembelian_detail)->update($pembelian_detail_data);

                /**
                 * Memperbarui data stok barang
                 */
                $stok_barang_data = [
                    'id_satuan' => $id_satuan,
                    'sbr_harga_beli' => $pbd_harga_beli,
                    'sbr_status' => 'ready',
                ];

                mStokBarang
                    ::where('id_pembelian_detail', $id_pembelian_detail)
                    ->update($stok_barang_data);
            }

            if ($hutang_supplier) {
                $hsp_total = $hutang_supplier->hsp_total;
                $hsp_status = $hutang_supplier->hsp_status;
                $hsp_sisa = $hutang_supplier->hsp_sisa;
            } else {
                $hsp_total = NULL;
                $hsp_status = NULL;
                $hsp_sisa = NULL;
            }

            /**
             * Jika sisa pembayaran pembelian dengan hutang supplier berbeda, karena ada perubahan harga pembelian barang
             */
            if ($pbl_sisa_pembayaran !== $hsp_total) {

                /**
                 * Jika sisa pembayaran pembelian lebih besar dari total hutang supplier,
                 * Maka, total, status, dan sisa hutang supplier diperbarui
                 */
                if (
                    $pbl_sisa_pembayaran > $hsp_total
                    && $hsp_status == 'belum_lunas') {

                    $hsp_sisa = ($pbl_sisa_pembayaran - $hsp_total) + $hsp_sisa;

                    $hutang_supplier_data = [
                        'hsp_total' => $pbl_sisa_pembayaran,
                        'hsp_status' => 'belum_lunas',
                        'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                        'hsp_sisa' => $hsp_sisa
                    ];

                    mHutangSupplier::where('id_pembelian', $id_pembelian)->update($hutang_supplier_data);

                    /**
                     * Jika sisa pembayaran lebih kecil dari hutang supplier,
                     * maka pembaruan total, tanggal, dan sisa pembayaran
                     */
                } else if (
                    $pbl_sisa_pembayaran < $hsp_total
                    && $hsp_status == 'belum_lunas') {

                    $hutang_supplier_data = [
                        'hsp_total' => $pbl_sisa_pembayaran,
                        'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                        'hsp_sisa' => $pbl_sisa_pembayaran
                    ];

                    mHutangSupplier::where('id_pembelian', $id_pembelian)->update($hutang_supplier_data);

                    /**
                     * Jika sisa pembayaran pembelian kurang dari total hutang supplier sebelumnya dan statusnya sudah lunas,
                     * maka kelebihan pembayaran tersebut akan menjadi piutang lain,
                     * dengan total piutang lain yaitu total hutang supplier - sisa pembayaran
                     */
                } else if (
                    $pbl_sisa_pembayaran < $hsp_total
                    && $hsp_status == 'lunas') {

                    /**
                     * Pembaruan hutang supplier
                     */

                    $hsp_total = $pbl_sisa_pembayaran < 0 ? 0 : $pbl_sisa_pembayaran;

                    $hutang_supplier_data = [
                        'hsp_total' => $hsp_total,
                        'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                    ];

                    mHutangSupplier::where('id_pembelian', $id_pembelian)->update($hutang_supplier_data);

                    /**
                     * Create piutang lain dari kelebihan bayar hutang supplier
                     */
                    $ptl_no_faktur = Main::fakturPiutangLain($kode_supplier);
                    $ptl_tanggal = date('Y-m-d');
                    $ptl_jatuh_tempo = date('Y-m-d');
                    $ptl_total = $hutang_supplier->hsp_total - $pbl_sisa_pembayaran;
                    $ptl_sisa = 0;
                    $ptl_status = 'belum_lunas';
                    $ptl_keterangan = 'Piutang Supplier karena pembaruan data Pembelian yang hutang suppllier sudah lunas, faktur pembelian ' . $pembelian->pbl_no_faktur . ' & faktur hutang supplier ' . $hutang_supplier->hsp_no_faktur;

                    $piutang_lain_data = [
                        'id_pembelian' => $id_pembelian,
                        'ptl_no_faktur' => $ptl_no_faktur,
                        'ptl_tanggal' => $ptl_tanggal,
                        'ptl_jatuh_tempo' => $ptl_jatuh_tempo,
                        'ptl_total' => $ptl_total,
                        'ptl_sisa' => $ptl_sisa,
                        'ptl_status' => $ptl_status,
                        'ptl_keterangan' => $ptl_keterangan
                    ];

                    mPiutangLain::create($piutang_lain_data);

                }
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    /**
     * @param Request $request
     * @param $id_pembelian
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     *
     * Pada proses update disini tidak memperbarui stok barang,
     * Disini hanya memperbarui pembelian barang yang belum masuk stok barang
     * sehingga dapat leluasa untuk diperbaiki
     */
    function update_stok(Request $request, $id_pembelian)
    {
        $rules = [
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_beli' => 'required',
            'harga_beli.*' => 'required',
            'qty' => 'required',
            'qty.*' => 'required',
            'total_ppn' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_supplier' => 'Supplier',
            'id_barang' => 'Barang',
            'harga_beli' => 'Harga Beli',
            'qty' => 'Qty Barang',
            'total_ppn' => 'Total PPN',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_beli.' . $i] = 'Harga Beli ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_pembelian = Main::decrypt($id_pembelian);

        $id_supplier = $request->input('id_supplier');
        $pbl_no_faktur_supplier = $request->input('pbl_no_faktur_supplier');
        $kode_supplier = $request->input('kode_supplier');
        $id_barang_arr = $request->input('id_barang');

        $pbl_no_faktur = Main::fakturPembelian($kode_supplier);
        $pbl_tanggal_order = Main::format_datetime_db($request->input('tanggal'));
        $pbl_jenis_pembayaran = $request->input('pembayaran');
        $pbl_keterangan = $request->input('keterangan');

        $pbd_harga_net_arr = $request->input('harga_net');
        $pbd_sub_total_arr = $request->input('sub_total');
        $pbd_harga_beli_arr = $request->input('harga_beli');
        $pbd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pbd_ppn_persen = $this->ppnPersen;
        $pbd_qty_arr = $request->input('qty');

        $pbl_total = $request->input('total');
        $pbl_total_ppn = Main::format_number_db($request->input('total_ppn'));
        $pbl_ppn_persentase = $this->ppnPersen;
        $pbl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pbl_potongan = Main::format_number_db($request->input('potongan'));
        $pbl_grand_total = $request->input('grand_total');
        $pbl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pbl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $hsp_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $created_at = date('Y-m-d H:i:s');
        $updated_at = $created_at;


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data untuk diperbarui
             */
            $pembelian_update = [
                'id_supplier' => $id_supplier,
                'id_user' => $id_user,
                'pbl_no_faktur' => $pbl_no_faktur,
                'pbl_no_faktur_supplier' => $pbl_no_faktur_supplier,
                'pbl_tanggal_order' => $pbl_tanggal_order,
                'pbl_jenis_pembayaran' => $pbl_jenis_pembayaran,
                'pbl_total' => $pbl_total,
                'pbl_total_ppn' => $pbl_total_ppn,
                'pbl_ppn_persentase' => $pbl_ppn_persentase,
                'pbl_biaya_tambahan' => $pbl_biaya_tambahan,
                'pbl_potongan' => $pbl_potongan,
                'pbl_grand_total' => $pbl_grand_total,
                'pbl_jumlah_bayar' => $pbl_jumlah_bayar,
                'pbl_sisa_pembayaran' => $pbl_sisa_pembayaran,
                'pbl_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                'pbl_keterangan' => $pbl_keterangan,
            ];

            mPembelian::where('id_pembelian', $id_pembelian)->update($pembelian_update);

            /**
             * Delete data pembelian detail sebelum dimasukkan data yang terbaru
             */
            mPembelianDetail::where('id_pembelian', $id_pembelian)->delete();


            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke pembelian detail
                 */
                $pbd_harga_beli = Main::format_number_db($pbd_harga_beli_arr[$index]);
                $pbd_ppn_nominal = Main::format_number_db($pbd_ppn_nominal_arr[$index]);
                $pbd_harga_net = Main::format_number_db($pbd_harga_net_arr[$index]);
                $pbd_qty = Main::format_number_db($pbd_qty_arr[$index]);
                $pbd_sub_total = $pbd_sub_total_arr[$index];

                $pembelian_detail_data = [
                    'id_pembelian' => $id_pembelian,
                    'id_barang' => $id_barang,
                    'pbd_harga_beli' => $pbd_harga_beli,
                    'pbd_ppn_nominal' => $pbd_ppn_nominal,
                    'pbd_harga_net' => $pbd_harga_net,
                    'pbd_qty' => $pbd_qty,
                    'pbd_sub_total' => $pbd_sub_total,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];

                mPembelianDetail::create($pembelian_detail_data);

            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail_modal($id_pembelian)
    {
        $id_pembelian = Main::decrypt($id_pembelian);
        $pembelian = mPembelian
            ::with([
                'supplier',
                'user',
                'user.karyawan',
                'pembelian_detail',
                'pembelian_detail.barang',
                'pembelian_detail.satuan',
            ])
            ->leftJoin('tb_user', 'tb_user.id', '=', 'pembelian.pbl_posting_id_user')
            ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
            ->where('id_pembelian', $id_pembelian)
            ->first();

        $data = [
            'pembelian' => $pembelian,
        ];

        return view('pembelian.pembelian.pembelianDetailModal', $data);
    }

    function download($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $no_faktur_pembelian = $request->no_faktur_pembelian ? $request->no_faktur_pembelian : '';
        $no_faktur_supplier = $request->no_faktur_supplier ? $request->no_faktur_supplier : '';
        $nama_supplier = $request->nama_supplier ? $request->nama_supplier : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mPembelian
            ::select([
                'id_pembelian',
                'pbl_no_faktur',
                'pbl_no_faktur_supplier',
                'pbl_tanggal_order',
                'pbl_jenis_pembayaran',
                'pbl_grand_total',
                'spl_nama'
            ])
            ->leftJoin('supplier', 'supplier.id_supplier', '=', 'pembelian.id_supplier')
            ->whereBetween('pbl_tanggal_order', $where_date)
            ->whereLike('pbl_no_faktur', $no_faktur_pembelian)
            ->whereLike('pbl_no_faktur_supplier', $no_faktur_supplier)
            ->whereLike('spl_nama', $nama_supplier)
            ->withCount([
                'pembelian_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(pbd_qty)'));
                }
            ])
            ->orderBy('id_pembelian', 'DESC')
            ->get();

        $data = [
            'data_pembelian' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];

        if ($request->filter_type == 'download_excel') {
            return view('pembelian/pembelian/pembelianExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('pembelian/pembelian/pembelianPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('pembelian/pembelian/pembelianPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Pembelian');
        }


    }

    function delete($id_pembelian)
    {
        $user = Session::get('user');
        $id_user = $user['id'];
        $id_pembelian = Main::decrypt($id_pembelian);
        $pembelian = mPembelian::where('id_pembelian', $id_pembelian)->first();
        $pbl_simpan_status = $pembelian->pbl_simpan_status;
        $pbl_no_faktur = $pembelian->pbl_no_faktur;


        DB::beginTransaction();
        try {

            if ($pbl_simpan_status == 'simpan') {
                mPembelian::where('id_pembelian', $id_pembelian)->delete();
                mPembelianDetail::where('id_pembelian', $id_pembelian)->delete();
            } elseif ($pbl_simpan_status == 'posting') {

                $pembelian_detail = mPembelianDetail::where('id_pembelian', $id_pembelian)->get();
                $delete_status = TRUE;
                $barang_stok_dan_qty_pembelian = [];
                foreach ($pembelian_detail as $row) {
                    $id_barang = $row->id_barang;
                    $pbd_qty = $row->pbd_qty;

                    $stok_barang_now = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                    if ($stok_barang_now < $pbd_qty) {
                        $delete_status = FALSE;
                        $brg_nama = mBarang::where('id_barang', $id_barang)->value('brg_nama');
                        $barang_stok_dan_qty_pembelian[] = [
                            'id_barang' => $id_barang,
                            'brg_nama' => $brg_nama,
                            'stok_barang_now' => $stok_barang_now,
                            'stok_pembelian' => $pbd_qty
                        ];
                    }
                }


                if ($delete_status) {

                    /**
                     * Proses hapus data hutang supplier jika ada
                     */

                    $hutang_supplier_check = mHutangSupplier::where('id_pembelian', $id_pembelian)->count();

                    /**
                     * Check hutang supplier jika ada
                     */
                    if ($hutang_supplier_check > 0) {
                        $hutang_supplier = mHutangSupplier::where('id_pembelian', $id_pembelian)->first();
                        $hsp_status = $hutang_supplier->hsp_status;
                        $hsp_no_faktur = $hutang_supplier->hsp_no_faktur;
                        $hsp_total = $hutang_supplier->hsp_total;
                        $hsp_sisa = $hutang_supplier->hsp_sisa;

                        /**
                         * Jika hutang supplier sudah lunas, tidak bisa menghapus hutang supplier dan pengurangan stok barang
                         * serta penghapusan data pembelian
                         */
                        if ($hsp_status == 'lunas') {
                            $title = 'Perhatian';
                            $description = 'Terdapat Hutang Supplier #' . $hsp_no_faktur . ' yang sudah lunas, sehingga tidak dapat menghapus pembelian. Silahkan melakukan Penghapusan Barang pada Menu Penyesuaian Stok';

                            $response = [
                                'title' => $title,
                                'description' => $description,
                                'type' => 'warning'
                            ];
                            return Response::json($response, 401);

                            /**
                             * Jika hutang supplier belum lunas namun sudah sempat terbayarkan, tidak dapat menghapus hutang supplier
                             *serta penghapusan data pembelian
                             */
                        } elseif ($hsp_status == 'belum_lunas' && $hsp_total != $hsp_sisa) {
                            $title = 'Perhatian';
                            $description = 'Terdapat Hutang Supplier #' . $hsp_no_faktur . ' yang sudah sempat terbayarkan, sehingga tidak dapat menghapus pembelian. Silahkan melakukan Penghapusan Barang pada Menu Penyesuaian Stok';

                            $response = [
                                'title' => $title,
                                'description' => $description,
                                'type' => 'warning'
                            ];
                            return Response::json($response, 401);

                            /**
                             * Proses penghapusan data Hutang Supplier dan lanjutkan proses penghapusan pembelian dan pengurangan stok barang
                             */
                        } else {
                            mHutangSupplier::where('id_pembelian', $id_pembelian)->delete();
                        }
                    }

                    /**
                     * Proses pengurangan barang sesuai dengan pembelian barang
                     * pada stok barang
                     * lalu dicatat pada arus stok
                     */
                    foreach ($pembelian_detail as $row) {
                        $id_pembelian_detail = $row->id_pembelian_detail;
                        $id_barang = $row->id_barang;
                        $pbd_qty = $row->pbd_qty;
                        $stok_barang_data = mStokBarang
                            ::where('id_barang', $id_barang)
                            ->where('sbr_qty', '>', 0)
                            ->get();

                        /**
                         * Jika qty pembelian detail lebih dari 0, maka akan dilakukan proses dibawah
                         */
                        if ($pbd_qty > 0) {
                            foreach ($stok_barang_data as $row_stok_barang) {
                                $sbr_qty = $row_stok_barang->sbr_qty;
                                $id_stok_barang = $row_stok_barang->id_stok_barang;

                                $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                                /**
                                 * Saat Stok Barang dari Barang Pembelian Detail dan Qty Pembelian Detail tidak 0
                                 * makan akan dilakukan proses pengurangan stok dan mencatat pada kartu stok
                                 */
                                if ($sbr_qty >= $pbd_qty && $pbd_qty != 0) {
                                    $sbr_qty_hasil = $sbr_qty - $pbd_qty;
                                    $abs_stok_keluar = $pbd_qty;
                                    $pbd_qty = 0;

                                    $stok_barang_update = [
                                        'sbr_qty' => $sbr_qty_hasil
                                    ];

                                    mStokBarang::where('id_stok_barang', $id_stok_barang)->update($stok_barang_update);

                                    $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                                    $arus_stok_insert = [
                                        'id_pembelian' => $id_pembelian,
                                        'id_pembelian_detail' => $id_pembelian_detail,
                                        'id_barang' => $id_barang,
                                        'id_stok_barang' => $id_stok_barang,
                                        'id_user' => $id_user,
                                        'asb_stok_awal' => $asb_stok_awal,
                                        'asb_stok_keluar' => $abs_stok_keluar,
                                        'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                                        'asb_keterangan' => 'Hapus Pembelian Barang No Faktur : ' . $pbl_no_faktur,
                                        'asb_tipe_proses' => 'delete',
                                        'asb_nama_proses' => 'pembelian'
                                    ];
                                    mArusStok::create($arus_stok_insert);

                                    /**
                                     * Jika QTY Stok Barang dari Pembelian Barang Detail kurang dari Stok Barang yang sekarang di Looping
                                     * akan dilakukan pengurangan sementara, lalu dilanjutkan ke iterasi selanjutnya.
                                     */
                                } elseif ($sbr_qty < $pbd_qty) {
                                    $sbr_qty_hasil = 0;
                                    $pbd_qty = $pbd_qty - $sbr_qty;
                                    $abs_stok_keluar = $sbr_qty;

                                    /**
                                     * Proses Update Stok Barang
                                     */
                                    $stok_barang_update = [
                                        'sbr_qty' => $sbr_qty_hasil
                                    ];
                                    mStokBarang::where('id_stok_barang', $id_stok_barang)->update($stok_barang_update);

                                    /**
                                     * Proses membuat arus stok barang baru
                                     */
                                    $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                                    $arus_stok_insert = [
                                        'id_pembelian' => $id_pembelian,
                                        'id_pembelian_detail' => $id_pembelian_detail,
                                        'id_barang' => $id_barang,
                                        'id_stok_barang' => $id_stok_barang,
                                        'id_user' => $id_user,
                                        'asb_stok_awal' => $asb_stok_awal,
                                        'asb_stok_keluar' => $abs_stok_keluar,
                                        'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                                        'asb_keterangan' => 'Hapus Pembelian Barang No Faktur : ' . $pbl_no_faktur,
                                        'asb_tipe_proses' => 'delete',
                                        'asb_nama_proses' => 'pembelian'
                                    ];
                                    mArusStok::create($arus_stok_insert);
                                }
                            }
                        }
                    }

                    /**
                     * Proses hapus data pada pembelian dan detail pembelian
                     */
                    mPembelian::where('id_pembelian', $id_pembelian)->delete();
                    mPembelianDetail::where('id_pembelian', $id_pembelian)->delete();

                    /**
                     * Jika Stok Barang kurang dari yang dibeli sebelumnya,
                     * kondisi ini terjadi saat pembelian barang, sudah jauh hari dilakukan,
                     * namun baru sekarang akan dihapus pembeliannya,
                     * sehingga stok barang pada pembelian barang yang sudah dilakukan sebelumnya,
                     * sudah digunakan barangnya pada Penjualan atau di menu yang lainnya
                     */
                } else {
                    $title = 'Stok Barang Kurang';
                    $description = '';
                    foreach ($barang_stok_dan_qty_pembelian as $key => $row) {
                        $brg_nama = $row['brg_nama'];
                        $stok_barang_now = $row['stok_barang_now'];
                        $stok_pembelian = $row['stok_pembelian'];
                        $description .= ++$key . '. Stok ' . $brg_nama . ' : ' . $stok_barang_now . ' <strong>kurang dari sama dengan</strong> Stok Pembelian : ' . $stok_pembelian . '<br />';
                    }
                    $description .= '<br />
                    <span class="text-warning font-weight-bold">Note:</span><br />
                    Karena sudah dilakukan <strong>Posting</strong>, <br />
                    Lakukan Pembelian Barang atau Penyesuaian Stok Terlebih Dahulu untuk memenuhi Penghapusan Pembelian Barang';

                    $response = [
                        'title' => $title,
                        'description' => $description,
                        'type' => 'warning'
                    ];

                    return Response::json($response, 401);
                }


            }
            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

}
