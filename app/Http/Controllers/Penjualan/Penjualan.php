<?php

namespace app\Http\Controllers\Penjualan;

use app\Helpers\Main;
use app\Http\Controllers\Controller;
use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mPelanggan;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mPiutangPelangganPembayaran;
use app\Models\mReturBarang;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Rules\rPenjualanQtyCheck;
use app\Rules\rPenjualanUpdateQtyCheck;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Penjualan extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['penjualan'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['penjualan'],
                'route' => route('penjualanList')
            ]
        ];
    }

    function index(Request $request)
    {

        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }

        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request, ['date', 'no_faktur_penjualan', 'nama_pelanggan', 'download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $no_faktur_penjualan = $filter_component['no_faktur_penjualan'];
        $nama_pelanggan = $filter_component['nama_pelanggan'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "no_faktur"],
            ["data" => "nama_pelanggan"],
            ["data" => "tanggal_penjualan"],
            ["data" => "qty_total"],
            ["data" => "grand_total"],
            ["data" => "simpan_status"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'no_faktur_penjualan' => $no_faktur_penjualan,
                'nama_pelanggan' => $nama_pelanggan
            ),
        ]);

        return view('penjualan/penjualan/penjualanList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $no_faktur_penjualan = $data_post['no_faktur_penjualan'];
        $nama_pelanggan = $data_post['nama_pelanggan'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPenjualan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'penjualan.id_pelanggan')
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_penjualan'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');
        $simpan_status = '';
        $delete_warning = '';

        $data_list = mPenjualan
            ::select([
                'id_penjualan',
                'pjl_no_faktur',
                'pjl_tanggal_penjualan',
                'pjl_jenis_pembayaran',
                'pjl_grand_total',
                'plg_nama',
                'pjl_simpan_status'
            ])
            ->leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'penjualan.id_pelanggan')
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->withCount([
                'penjualan_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(pjd_qty)'));
                }
            ])
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_penjualan = Main::encrypt($row->id_penjualan);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $penjualan_edit_menu = '';
            $penjualan_edit_menu_alert_class = '';
            $penjualan_edit_menu_alert_data = '';
            $penjualan_edit_menu_link = route('penjualanEditHarga', ['id_penjualan' => $id_penjualan]);

            /**
             * Check apakah penjualan ini ada piutang pelanggannya, jika ada dan sudah terbayarkan,
             * maka tidak bisa melakukan edit penjualan, tapi jika belum ada pembayaran
             * dapat melakukan edit penjualan, namun nanti nominal piutang pelanggan akan diperbarui dengan nominal yang baru
             */
            $piutang_pelanggan = mPiutangPelanggan
                ::where([
                    'id_penjualan' => $row->id_penjualan,
                    'ppl_status' => 'belum_lunas'
                ]);
            if ($piutang_pelanggan->count() > 0) {
                $piutang_pelanggan = $piutang_pelanggan->first();
                if ($piutang_pelanggan->ppl_total != $piutang_pelanggan->ppl_sisa) {
                    $penjualan_edit_menu_alert_class = ' btn-alert-custom disabled';
                    $penjualan_edit_menu_alert_description = '
                        Penjualan No Faktur <strong>' . $row->pjl_no_faktur . '</strong> <br />
                        terdapat <strong>Piutang Pelanggan</strong><br />
                        yang sudah terbayarkan dan sudah terproses, sehingga <strong>Tidak Bisa Edit</strong> Penjualan ini.<br />
                        <br />
                        Jika ingin Update Penjualan ini, diharapkan pada <strong>Menu Penyesuaian Stok</strong><br />
                        atau<br />
                        Dapat membuat <strong>Penjualan Baru</strong>
                        <br />
                        <br />
                        Terima Kasih<br />🙏🏻" 
                    ';
                    $penjualan_edit_menu_alert_data = ' data-type="warning" data-title="Perhatian" data-description="' . $penjualan_edit_menu_alert_description . '"';
                    $penjualan_edit_menu_link = '';
                }
            }

            /**
             * Check apakah ada return barang pada penjualan ini, jika ada, maka tidak bisa di lakukan edit penjualan
             * karena sudah ada proses pengembalian barang
             */
            $check_retur_barang = mReturBarang::where('id_penjualan', $row->id_penjualan)->count();
            if ($check_retur_barang > 0) {
                $penjualan_edit_menu_alert_class = ' btn-alert-custom disabled';
                $penjualan_edit_menu_alert_description = '
                        Penjualan No Faktur <strong>' . $row->pjl_no_faktur . '</strong><br />
                        sudah melakukan<br />
                        <strong>Retur Barang</strong>, sehingga tidak bisa melakukan Edit Penjualan<br />
                        <br />
                        Jika ingin Update Penjualan ini, diharapkan pada <strong>Menu Penyesuaian Stok</strong><br />
                        atau<br />
                        Dapat membuat <strong>Penjualan Baru</strong><br />
                        <br />
                        Terima Kasih<br />🙏🏻" 
                    ';
                $penjualan_edit_menu_alert_data = ' data-type="warning" data-title="Perhatian" data-description="' . $penjualan_edit_menu_alert_description . '"';
                $penjualan_edit_menu_link = '';
            }

            if($row->pjl_simpan_status == 'simpan') {
                $penjualan_edit_menu = '
                    <a class="akses-action_penjualan_edit dropdown-item"
                       href="' . route('penjualanEditStok', ['id_penjualan' => $id_penjualan]) . '">
                        <i class="la la-edit"></i>
                        Edit Harga & Barang
                    </a>';
            } elseif($row->pjl_simpan_status == 'posting') {
                $penjualan_edit_menu = '
                    <a class="akses-action_penjualan_edit dropdown-item ' . $penjualan_edit_menu_alert_class . '"
                       href="' . $penjualan_edit_menu_link . '" 
                       ' . $penjualan_edit_menu_alert_data . '>
                        <i class="la la-edit"></i>
                        Edit Harga
                    </a>';
            }

            if ($row->pjl_simpan_status == 'simpan') {
                $simpan_status = '<span class="m-badge m-badge--warning m-badge--wide">Simpan</span>';
            } elseif ($row->pjl_simpan_status == 'posting') {
                $simpan_status = '<span class="m-badge m-badge--success m-badge--wide">Posting</span>';
            }

            $nestedData['no'] = $no;
            $nestedData['no_faktur'] = $row->pjl_no_faktur;
            $nestedData['nama_pelanggan'] = $row->plg_nama;
            $nestedData['tanggal_penjualan'] = Main::format_datetime($row->pjl_tanggal_penjualan);
            $nestedData['jenis_pembayaran'] = $row->pjl_jenis_pembayaran;
            $nestedData['grand_total'] = Main::format_number($row->pjl_grand_total);
            $nestedData['qty_total'] = Main::format_number($row->total_qty);
            $nestedData['simpan_status'] = $simpan_status;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        ' . $penjualan_edit_menu;

            if($row->pjl_simpan_status == 'simpan') {
                $nestedData['options'] .= '
                        <a class="akses-action_penjualan_cetak_retur dropdown-item btn-alert-custom disabled"
                           href="#"
                           data-title="Tidak Dapat Cetak Invoice '.$row->pjl_no_faktur.'"
                           data-description="Diperlukan Posting Penjualan Terlebih Dahulu untuk Cetak Invoice Penjualan."
                           data-type="warning">
                            <i class="la la-print"></i>
                            Cetak Invoice Penjualan
                        </a>';
            } elseif($row->pjl_simpan_status == 'posting') {
                $nestedData['options'] .= '
                        <a class="akses-action_penjualan_cetak_retur dropdown-item"
                            target="_blank"
                           href="' . route('penjualanInvoicePrint', ['id_penjualan' => $id_penjualan]) . '">
                            <i class="la la-print"></i>
                            Cetak Invoice Penjualan
                        </a>';
            }

            $nestedData['options'] .= '
                        <a class="akses-action_penjualan_detail btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('penjualanDetailModal', ['id_penjualan' => $id_penjualan]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>';

            if ($row->pjl_simpan_status == 'simpan') {
                $nestedData['options'] .= '
							<div class="dropdown-divider"></div>
                            <a class="akses-posting dropdown-item text-accent btn-confirm-custom"
                                href="#"
                                data-title="Yakin Posting Penjualan ' . $row->pjl_no_faktur . ' ?"
                                data-description="Item Penjualan Barang akan keluar dari <strong>Stok Barang</strong>, jika ada kekeliruan untuk mengubahnya perlu diatur pada <strong>Penyesuaian Stok</>"
                                data-type="warning"
                                data-method="get"
                                data-route="' . route('penjualanProsesPosting', ['id_penjualan' => $id_penjualan]) . '">
                                <i class="la la-refresh text-accent"></i>
                                Posting Pembelian
                            </a>';
            }

            $nestedData['options'] .= '<div class="dropdown-divider"></div>
                        <a class="btn-hapus akses-delete dropdown-item text-danger"
                           href="#"
                           data-route="' . route('penjualanDelete', ['id_penjualan' => $id_penjualan]) . '">
                            <i class="la la-remove text-danger"></i>
                            Hapus Penjualan
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function delete($id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);
        $id_user = Session::get('user')['id'];
        $denied = FALSE;

        /**
         * Proses pengecheckan piutang pelanggan terhadap penjualan yang sudah terjadi
         * jika ada piutang pelanggan piutang pelanggan akan dihapus,
         * namun jika piutang pelanggan sudah ada pembayaran,
         * akan muncul notifikasi bahwa perlu menghapus pembayaran pada piutang pelanggan tersebut
         */
        $check_piutang_pelanggan = mPiutangPelanggan::where('id_penjualan', $id_penjualan)->count();
        if ($check_piutang_pelanggan > 0) {
            $id_piutang_pelanggan = mPiutangPelanggan::where('id_penjualan', $id_penjualan)->value('id_piutang_pelanggan');
            $check_piutang_pelanggan_pembayaran = mPiutangPelangganPembayaran
                ::where('id_piutang_pelanggan', $id_piutang_pelanggan)
                ->count();

            if ($check_piutang_pelanggan_pembayaran > 0) {
                $denied = TRUE;
                $title = 'Perhatian';
                $message = 'Terdapat Pembayaran pada Piutang Pelanggan, Sehingga belum dapat menghapus penjualan';
            }
        }

        if ($denied) {
            return [
                'title' => $title,
                'message' => $message
            ];
        }


        DB::beginTransaction();
        try {

            /**
             * Proses mengembalikan stok barang
             */
            $penjualan_detail = mPenjualanDetail::where('id_penjualan', $id_penjualan)->get();
            $penjualan = mPenjualan::where('id_penjualan', $id_penjualan)->first();
            foreach ($penjualan_detail as $row) {
                $id_penjualan_detail = $row->id_penjualan_detail;
                $arus_stok_data = mArusStok
                    ::where('id_penjualan_detail', $id_penjualan_detail)
                    ->where('asb_stok_keluar', '!=', NULL)
                    ->where('asb_stok_keluar', '!=', 0)
                    ->get();
                foreach ($arus_stok_data as $row_2) {
                    $id_stok_barang = $row_2->id_stok_barang;
                    $id_barang = $row_2->id_barang;
                    $asb_stok_keluar = $row_2->asb_stok_keluar;
                    /**
                     * Jika ada ID_STOK_BARANG
                     */
                    if ($id_stok_barang) {
                        $sbr_qty = mStokBarang::where('id_stok_barang', $id_stok_barang)->value('sbr_qty');
                        $sbr_qty_new = $sbr_qty + $asb_stok_keluar;
                        $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                        $asb_stok_masuk = $asb_stok_keluar;
                        mStokBarang::where('id_stok_barang', $id_stok_barang)->update(['sbr_qty' => $sbr_qty_new]);
                        $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                        $arus_stok_insert = [
                            'id_penjualan' => $id_penjualan,
                            'id_penjualan_detail' => $id_penjualan_detail,
                            'id_barang' => $id_barang,
                            'id_stok_barang' => $id_stok_barang,
                            'id_user' => $id_user,
                            'asb_stok_awal' => $asb_stok_awal,
                            'asb_stok_masuk' => $asb_stok_masuk,
                            'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                            'asb_keterangan' => 'Hapus Penjualan Barang No Faktur : ' . $penjualan->pjl_no_faktur . ' & Barang dikembalikan',
                            'asb_tipe_proses' => 'delete',
                            'asb_nama_proses' => 'penjualan'

                        ];
                        mArusStok::create($arus_stok_insert);

                        /**
                         * Jika ID_STOK_BARANG "TIDAK ADA"
                         */
                    } else {
                        $sbr_harga_jual = $row->pjd_harga_jual;

                        $asb_stok_masuk = $asb_stok_keluar;
                        $sbr_qty = $asb_stok_keluar;

                        /**
                         * Mengetahui total stok awal barang
                         */
                        $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                        /**
                         * Mengembalikan barang penjualan dengan memasukkan stok barang baru
                         * karena stok barang tidak tersimpan pada arus stok / kartu stok
                         */
                        $stok_barang_insert = [
                            'id_barang' => $id_barang,
                            'id_penjualan' => $id_penjualan,
                            'id_penjualan_detail' => $id_penjualan_detail,
                            'sbr_qty' => $sbr_qty,
                            'sbr_harga_jual' => $sbr_harga_jual,
                            'sbr_status' => 'restore_penjualan_delete'
                        ];
                        mStokBarang::create($stok_barang_insert);


                        /**
                         * Menyimpan data baru ke arus stok
                         */
                        $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                        $arus_stok_insert = [
                            'id_penjualan' => $id_penjualan,
                            'id_penjualan_detail' => $id_penjualan_detail,
                            'id_barang' => $id_barang,
                            'id_stok_barang' => $id_stok_barang,
                            'id_user' => $id_user,
                            'asb_stok_awal' => $asb_stok_awal,
                            'asb_stok_masuk' => $asb_stok_masuk,
                            'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                            'asb_keterangan' => 'Hapus Penjualan Barang No Faktur : ' . $penjualan->pjl_no_faktur . ' & Barang dikembalikan',
                            'asb_tipe_proses' => 'delete',
                            'asb_nama_proses' => 'penjualan'

                        ];
                        mArusStok::create($arus_stok_insert);
                    }
                }
            }


            mPenjualan::where('id_penjualan', $id_penjualan)->delete();
            mPenjualanDetail::where('id_penjualan', $id_penjualan)->delete();


            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

    }

    function create()
    {
        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Penjualan Baru',
                    'route' => ''
                ]
            ]
        );
        $data = Main::data($breadcrumb, $this->menuActive);
        $pelanggan = mPelanggan::orderBy('plg_nama', 'ASC')->get();
        $barang = mBarang
            ::withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->orderBy('brg_kode')
            ->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $pelanggan_first = $pelanggan[0];

        $data = array_merge(
            $data, array(
                'pelanggan' => $pelanggan,
                'pelanggan_first' => $pelanggan_first,
                'barang' => $barang,
                'satuan' => $satuan
            )
        );

        return view('penjualan/penjualan/penjualanCreate', $data);
    }

    function no_faktur(Request $request)
    {
        $kode_pelanggan = $request->input('kode_pelanggan');
        $id_pelanggan = $request->input('id_pelanggan');
        $pjl_no_faktur = Main::fakturPenjualan($kode_pelanggan);


        return [
            $pjl_no_faktur
        ];
    }

    function stok_barang(Request $request)
    {
        $id_barang = $request->input('id_barang');
        $stok_barang = mStokBarang
            ::with([
                'supplier:id_supplier,spl_kode,spl_nama',
                'satuan:id_satuan,stn_nama'
            ])
            ->where('id_barang', $id_barang)
            ->get();
        $data = [
            'stok_barang' => $stok_barang
        ];

        return view('penjualan/penjualan/tableBarangStok', $data);
    }

    function insert(Request $request)
    {
        $pjl_simpan_status = $request->input('pjl_simpan_status');
        if ($pjl_simpan_status == 'simpan') {
            return $this->insert_data($request);
        } elseif ($pjl_simpan_status == 'posting') {
            return $this->insert_posting($request);
        }
    }

    function insert_data($request)
    {
        $rules = [
            'id_pelanggan' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_jual' => 'required',
            'harga_jual.*' => 'required',
            'qty' => [
                'required',
                /**
                 * Digunakan untuk mengecheck stok barang sekarang dengan yang dibeli
                 */
                new rPenjualanQtyCheck($request)
            ],
            'qty.*' => 'required',
            'potongan_harga_barang' => 'required',
            'potongan_harga_barang.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_pelanggan' => 'Pelanggan',
            'id_barang' => 'Barang',
            'harga_jual' => 'Harga',
            'qty' => 'Qty Barang',
            'potongan_harga_barang' => 'Potongan Harga Barang',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
            $attr['potongan_harga_barang.' . $i] = 'Potongan Harga Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_pelanggan = $request->input('id_pelanggan');
        $kode_pelanggan = $request->input('plg_kode');

        $pjl_simpan_status = $request->input('pjl_simpan_status');
        $pjl_no_faktur = Main::fakturPenjualan($kode_pelanggan);;
        $pjl_tanggal_penjualan = Main::format_datetime_db($request->input('tanggal'));
        $pjl_jenis_pembayaran = $request->input('pembayaran');

        $id_barang_arr = $request->input('id_barang');
        $id_stok_barang_arr = $request->input('id_stok_barang');
        $pjd_harga_net_arr = $request->input('harga_net');
        $pjd_harga_beli_arr = $request->input('harga_beli');
        $pjd_harga_jual_arr = $request->input('harga_jual');
        $pjd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pjd_ppn_persen = $this->ppnPersen;
        $pjd_qty_arr = $request->input('qty');
        $pjd_sub_total_arr = $request->input('sub_total');
        $pjd_potongan_harga_barang_arr = $request->input('potongan_harga_barang');

        $pjl_total = $request->input('total');
        $pjl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pjl_potongan = Main::format_number_db($request->input('potongan'));
        $pjl_grand_total = $request->input('grand_total');
        $pjl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pjl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $ppl_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $pjl_keterangan = $request->input('pjl_keterangan');


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $penjualan_data = [
                'id_pelanggan' => $id_pelanggan,
                'id_user' => $id_user,
                'pjl_no_faktur' => $pjl_no_faktur,
                'pjl_tanggal_penjualan' => $pjl_tanggal_penjualan,
                'pjl_jenis_pembayaran' => $pjl_jenis_pembayaran,
                'pjl_total' => $pjl_total,
                'pjl_biaya_tambahan' => $pjl_biaya_tambahan,
                'pjl_potongan' => $pjl_potongan,
                'pjl_grand_total' => $pjl_grand_total,
                'pjl_jumlah_bayar' => $pjl_jumlah_bayar,
                'pjl_sisa_pembayaran' => $pjl_sisa_pembayaran,
                'pjl_keterangan' => $pjl_keterangan,
                'pjl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
                'pjl_simpan_status' => $pjl_simpan_status
            ];

            $response_penjualan = mPenjualan::create($penjualan_data);
            $id_penjualan = $response_penjualan->id_penjualan;

            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke penjualan detail
                 */
                $id_stok_barang = $id_stok_barang_arr[$index];
                $pjd_harga_net = Main::format_number_db($pjd_harga_net_arr[$index]);
                $pjd_harga_beli = Main::format_number_db($pjd_harga_beli_arr[$index]);
                $pjd_harga_jual = Main::format_number_db($pjd_harga_jual_arr[$index]);
                $pjd_potongan_harga_barang = Main::format_number_db($pjd_potongan_harga_barang_arr[$index]);
                $pjd_sub_total = $pjd_sub_total_arr[$index];

                $pjd_ppn_nominal = Main::format_number_db($pjd_ppn_nominal_arr[$index]);
                $pjd_qty = Main::format_number_db($pjd_qty_arr[$index]);
                $pjd_qty_sisa = $pjd_qty;
                $penjualan_detail_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'pjd_qty' => $pjd_qty,
                    'pjd_qty_sisa' => $pjd_qty_sisa,
                    'pjd_harga_jual' => $pjd_harga_jual,
                    'pjd_hpp' => $pjd_harga_beli,
//                    'pjd_potongan' => 0,
                    'pjd_ppn_persen' => $pjd_ppn_persen,
                    'pjd_ppn_nominal' => $pjd_ppn_nominal,
                    'pjd_harga_net' => $pjd_harga_net,
                    'pjd_potongan_harga_barang' => $pjd_potongan_harga_barang,
                    'pjd_sub_total' => $pjd_sub_total,
                ];

                mPenjualanDetail::create($penjualan_detail_data);


            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function insert_posting($request)
    {
        $rules = [
            'id_pelanggan' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_jual' => 'required',
            'harga_jual.*' => 'required',
            'qty' => [
                'required',
                /**
                 * Digunakan untuk mengecheck stok barang sekarang dengan yang dibeli
                 */
                new rPenjualanQtyCheck($request)
            ],
            'qty.*' => 'required',
            'potongan_harga_barang' => 'required',
            'potongan_harga_barang.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_pelanggan' => 'Pelanggan',
            'id_barang' => 'Barang',
            'harga_jual' => 'Harga',
            'qty' => 'Qty Barang',
            'potongan_harga_barang' => 'Potongan Harga Barang',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
            $attr['potongan_harga_barang.' . $i] = 'Potongan Harga Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_pelanggan = $request->input('id_pelanggan');
        $kode_pelanggan = $request->input('plg_kode');
        $pjl_simpan_status = $request->input('pjl_simpan_status');

        $pjl_no_faktur = Main::fakturPenjualan($kode_pelanggan);;
        $pjl_tanggal_penjualan = Main::format_datetime_db($request->input('tanggal'));
        $pjl_jenis_pembayaran = $request->input('pembayaran');

        $id_barang_arr = $request->input('id_barang');
        $id_stok_barang_arr = $request->input('id_stok_barang');
        $pjd_harga_net_arr = $request->input('harga_net');
        $pjd_harga_beli_arr = $request->input('harga_beli');
        $pjd_harga_jual_arr = $request->input('harga_jual');
        $pjd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pjd_ppn_persen = $this->ppnPersen;
        $pjd_qty_arr = $request->input('qty');
        $pjd_sub_total_arr = $request->input('sub_total');
        $pjd_potongan_harga_barang_arr = $request->input('potongan_harga_barang');

        $pjl_total = $request->input('total');
        $pjl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pjl_potongan = Main::format_number_db($request->input('potongan'));
        $pjl_grand_total = $request->input('grand_total');
        $pjl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pjl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $ppl_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $pjl_keterangan = $request->input('pjl_keterangan');


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $penjualan_data = [
                'id_pelanggan' => $id_pelanggan,
                'id_user' => $id_user,
                'pjl_no_faktur' => $pjl_no_faktur,
                'pjl_tanggal_penjualan' => $pjl_tanggal_penjualan,
                'pjl_jenis_pembayaran' => $pjl_jenis_pembayaran,
                'pjl_total' => $pjl_total,
                'pjl_biaya_tambahan' => $pjl_biaya_tambahan,
                'pjl_potongan' => $pjl_potongan,
                'pjl_grand_total' => $pjl_grand_total,
                'pjl_jumlah_bayar' => $pjl_jumlah_bayar,
                'pjl_sisa_pembayaran' => $pjl_sisa_pembayaran,
                'pjl_keterangan' => $pjl_keterangan,
                'pjl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
                'pjl_simpan_status' => $pjl_simpan_status,
                'pjl_posting_waktu' => date('Y-m-d H:i:s'),
                'pjl_posting_id_user' => $id_user
            ];

            $response_penjualan = mPenjualan::create($penjualan_data);
            $id_penjualan = $response_penjualan->id_penjualan;

            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke penjualan detail
                 */
                $id_stok_barang = $id_stok_barang_arr[$index];
                $pjd_harga_net = Main::format_number_db($pjd_harga_net_arr[$index]);
                $pjd_harga_beli = Main::format_number_db($pjd_harga_beli_arr[$index]);
                $pjd_harga_jual = Main::format_number_db($pjd_harga_jual_arr[$index]);
                $pjd_potongan_harga_barang = Main::format_number_db($pjd_potongan_harga_barang_arr[$index]);
                $pjd_sub_total = $pjd_sub_total_arr[$index];

                $pjd_ppn_nominal = Main::format_number_db($pjd_ppn_nominal_arr[$index]);
                $pjd_qty = Main::format_number_db($pjd_qty_arr[$index]);
                $pjd_qty_sisa = $pjd_qty;
                $penjualan_detail_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'pjd_qty' => $pjd_qty,
                    'pjd_qty_sisa' => $pjd_qty_sisa,
                    'pjd_harga_jual' => $pjd_harga_jual,
                    'pjd_hpp' => $pjd_harga_beli,
//                    'pjd_potongan' => 0,
                    'pjd_ppn_persen' => $pjd_ppn_persen,
                    'pjd_ppn_nominal' => $pjd_ppn_nominal,
                    'pjd_harga_net' => $pjd_harga_net,
                    'pjd_potongan_harga_barang' => $pjd_potongan_harga_barang,
                    'pjd_sub_total' => $pjd_sub_total,
                ];

                $response_penjualan_detail = mPenjualanDetail::create($penjualan_detail_data);
                $id_penjualan_detail = $response_penjualan_detail->id_penjualan_detail;

                /**
                 * Mengurangi dari stok barang
                 */
                $this->proses_pengurangan_stok($id_barang, $pjd_qty, $id_penjualan, $id_penjualan_detail, $id_user, $pjl_no_faktur);


            }

            if ($pjl_sisa_pembayaran > 0) {
                $ppl_no_faktur = Main::fakturPiutangPelanggan($kode_pelanggan);

                $piutang_pelanggan_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_pelanggan' => $id_pelanggan,
                    'ppl_no_faktur' => $ppl_no_faktur,
                    'ppl_tanggal' => $pjl_tanggal_penjualan,
                    'ppl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
                    'ppl_total' => $pjl_sisa_pembayaran,
                    'ppl_sisa' => $pjl_sisa_pembayaran,
                    'ppl_keterangan' => 'Piutang Pelanggan #' . $ppl_no_faktur . ' dengan no faktur ' . $pjl_no_faktur,
                    'ppl_status' => 'belum_lunas'
                ];

                mPiutangPelanggan::create($piutang_pelanggan_data);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        return [
            'redirect' => route('penjualanInvoicePrint', ['id_penjualan' => Main::encrypt($id_penjualan)])
        ];
    }

    /**
     * @param $id_barang
     * @param $pjd_qty
     * @return array
     *
     * Fungsi ini digunakan untuk mengurangi stok yang ada pada barang tersebut,
     * kemungkinan barang tersebut memiliki banyak stok,
     * sehingga perlu perulangan untuk  mengurangi stok sampai 0 jika stok nya kurang di masing-masing stok
     *
     * dan di dalamn nya ada pengurangan stok barang yang di catat pada arus stok / kartu stok
     */
    function proses_pengurangan_stok($id_barang, $pjd_qty, $id_penjualan, $id_penjualan_detail, $id_user, $pjl_no_faktur)
    {
        $stok_barang = mStokBarang::where('id_barang', $id_barang)->where('sbr_qty', '>', 0)->get();
        $id_stok_barang_arr = [];
        foreach ($stok_barang as $row) {
            if ($pjd_qty > $row->sbr_qty) {
                $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                $pjd_qty = $pjd_qty - $row->sbr_qty;
                $asb_stok_keluar = $row->sbr_qty;
                $id_stok_barang = $row->id_stok_barang;
                $id_stok_barang_arr[$id_stok_barang] = $asb_stok_keluar;
                mStokBarang
                    ::where('id_stok_barang', $id_stok_barang)
                    ->update([
                        'sbr_qty' => 0
                    ]);

                /**
                 * Proses memasukkan data ke arus stok barang
                 */
                $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                $arus_stok_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_penjualan_detail' => $id_penjualan_detail,
                    'id_barang' => $id_barang,
                    /**
                     * ini dikomentar, karena pengurangan stok saat penjualan itu tidak per batch/seri barang,
                     * namun secara global, sehingga id_stok_barang tidak disimpan disini,
                     * namun proses pengurangan setiap stok barang tersebut sampai dengan habis, disimpan ke dalam id_stok_barang_json
                     */
                    'id_stok_barang' => $id_stok_barang,
//                    'id_stok_barang_json' => json_encode($id_stok_barang_hasil_pengurangan_arr),
                    'id_user' => $id_user,
                    'asb_stok_awal' => $asb_stok_awal,
//                    'asb_stok_masuk' => 0,
                    'asb_stok_keluar' => $asb_stok_keluar,
                    /**
                     * Ini dikomentar, karena maksudnya stok ini adalah stok per batch/seri stok barang,
                     * sedangkan proses yang sekarang, stok barang dicampur semuanya dari awal sampai akhir
                     */
//                    'asb_stok_terakhir' => $asb_stok_terakhir,
                    'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                    'asb_keterangan' => 'Penjualan Barang No Faktur : ' . $pjl_no_faktur,
                    'asb_tipe_proses' => 'update',
                    'asb_nama_proses' => 'penjualan'
                ];

                mArusStok::create($arus_stok_data);
            } else {
                $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                $sbr_qty_now = $row->sbr_qty - $pjd_qty;
                $id_stok_barang = $row->id_stok_barang;
                $id_stok_barang_arr[$id_stok_barang] = $pjd_qty;
                mStokBarang
                    ::where('id_stok_barang', $row->id_stok_barang)
                    ->update([
                        'sbr_qty' => $sbr_qty_now
                    ]);

                /**
                 * Proses memasukkan data ke arus stok barang
                 */
                $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                $arus_stok_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_penjualan_detail' => $id_penjualan_detail,
                    'id_barang' => $id_barang,
                    /**
                     * ini dikomentar, karena pengurangan stok saat penjualan itu tidak per batch/seri barang,
                     * namun secara global, sehingga id_stok_barang tidak disimpan disini,
                     * namun proses pengurangan setiap stok barang tersebut sampai dengan habis, disimpan ke dalam id_stok_barang_json
                     */
                    'id_stok_barang' => $id_stok_barang,
//                    'id_stok_barang_json' => json_encode($id_stok_barang_hasil_pengurangan_arr),
                    'id_user' => $id_user,
                    'asb_stok_awal' => $asb_stok_awal,
//                    'asb_stok_masuk' => 0,
                    'asb_stok_keluar' => $pjd_qty,
                    /**
                     * Ini dikomentar, karena maksudnya stok ini adalah stok per batch/seri stok barang,
                     * sedangkan proses yang sekarang, stok barang dicampur semuanya dari awal sampai akhir
                     */
//                    'asb_stok_terakhir' => $asb_stok_terakhir,
                    'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                    'asb_keterangan' => 'Penjualan Barang No Faktur : ' . $pjl_no_faktur,
                    'asb_tipe_proses' => 'update',
                    'asb_nama_proses' => 'penjualan'
                ];

                mArusStok::create($arus_stok_data);


                break;
            }
        }

        mPenjualanDetail
            ::where('id_penjualan_detail', $id_penjualan_detail)
            ->update(['id_stok_barang_json' => json_encode($id_stok_barang_arr)]);

    }

    function proses_posting($id_penjualan)
    {

        $user = Session::get('user');
        $id_user = $user['id'];
        $id_penjualan = Main::decrypt($id_penjualan);
        $penjualan_detail = mPenjualanDetail::where('id_penjualan', $id_penjualan)->get();
        $penjualan = mPenjualan::where('id_penjualan', $id_penjualan)->first();
        $id_pelanggan = $penjualan->id_pelanggan;
        $pelanggan = mPelanggan::where('id_pelanggan', $id_pelanggan)->first();
        $pjl_no_faktur = $penjualan->pjl_no_faktur;
        $pjl_sisa_pembayaran = $penjualan->pjl_sisa_pembayaran;
        $kode_pelanggan = $pelanggan->pjl_kode;
        $pjl_tanggal_penjualan = $penjualan->pjl_tanggal_penjualan;
        $pjl_tanggal_penjualan = Main::format_datetime_db($pjl_tanggal_penjualan);
        $pjl_jatuh_tempo = $penjualan->pjl_jatuh_tempo;
        $ppl_tanggal_jatuh_tempo = Main::format_date_db($pjl_jatuh_tempo);
        $stok_barang_habis = FALSE;
        $daftar_barang_habis = '';
        $no = 1;

        DB::beginTransaction();
        try {

            /**
             * Proses pengecheckan stok barang dengan qty penjualan barang
             */
            foreach ($penjualan_detail as $row) {
                $id_barang = $row->id_barang;
                $pjd_qty = $row->pjd_qty;
                $stok_barang_now = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                if ($pjd_qty > $stok_barang_now) {
                    $stok_barang_habis = TRUE;
                    $barang = mBarang::where('id_barang', $id_barang)->first();
                    $brg_nama = $barang->brg_nama;
                    $brg_kode = $barang->brg_kode;
                    $daftar_barang_habis .= $no . '. (' . $brg_kode . ') ' . $brg_nama;
                    $no++;
                }
            }

            if ($stok_barang_habis) {
                $response = [
                    'title' => 'Stok Barang Tidak Mencukupi',
                    'description' => 'Berikut Stok Barang yang belum Mencukupi: <br />' . $daftar_barang_habis,
                    'type' => 'warning'
                ];
                return Response::json($response, 401);
            }

            /**
             * Proses Update Penjualan
             */

            $penjualan_update = [
                'pjl_simpan_status' => 'posting',
                'pjl_posting_waktu' => date('Y-m-d H:i:s'),
                'pjl_posting_id_user' => $id_user
            ];

            mPenjualan::where('id_penjualan', $id_penjualan)->update($penjualan_update);


            /**
             * Proses pengurangan Stok Barang tergantung dari Penjualan Barang
             */
            foreach ($penjualan_detail as $row) {
                $id_penjualan_detail = $row->id_penjualan_detail;
                $id_barang = $row->id_barang;
                $pjd_qty = $row->pjd_qty;

                /**
                 * Mengurangi dari stok barang
                 */
                $this->proses_pengurangan_stok($id_barang, $pjd_qty, $id_penjualan, $id_penjualan_detail, $id_user, $pjl_no_faktur);
            }

            /**
             * Proses memasukkan data ke piutang pelanggan
             */
            if ($pjl_sisa_pembayaran > 0) {
                $ppl_no_faktur = Main::fakturPiutangPelanggan($kode_pelanggan);

                $piutang_pelanggan_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_pelanggan' => $id_pelanggan,
                    'ppl_no_faktur' => $ppl_no_faktur,
                    'ppl_tanggal' => $pjl_tanggal_penjualan,
                    'ppl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
                    'ppl_total' => $pjl_sisa_pembayaran,
                    'ppl_sisa' => $pjl_sisa_pembayaran,
                    'ppl_keterangan' => 'Piutang Pelanggan #' . $ppl_no_faktur . ' dengan no faktur ' . $pjl_no_faktur,
                    'ppl_status' => 'belum_lunas'
                ];

                mPiutangPelanggan::create($piutang_pelanggan_data);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        return [
            'redirect' => route('penjualanInvoicePrint', ['id_penjualan' => Main::encrypt($id_penjualan)])
        ];
    }

    function edit_harga($id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);
        $penjualan = mPenjualan
            ::with([
                'pelanggan',
                'user',
                'user.karyawan',
                'penjualan_detail',
                'penjualan_detail.barang'
            ])
            ->where('id_penjualan', $id_penjualan)
            ->first();

        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Penjualan Edit',
                    'route' => ''
                ],
                [
                    'label' => $penjualan->pjl_no_faktur,
                    'route' => ''
                ]
            ]
        );


        $data = Main::data($breadcrumb, $this->menuActive);
        $pelanggan = mPelanggan::orderBy('plg_nama', 'ASC')->get();
        $barang = mBarang
            ::withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->orderBy('brg_kode')
            ->get();

        $data = array_merge(
            $data, array(
                'penjualan' => $penjualan,
                'pelanggan' => $pelanggan,
                'barang' => $barang,
            )
        );

        return view('penjualan/penjualan/penjualanEditHarga', $data);
    }

    function edit_stok($id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);
        $penjualan = mPenjualan
            ::with([
                'pelanggan',
                'user',
                'user.karyawan',
                'penjualan_detail',
                'penjualan_detail.barang'
            ])
            ->where('id_penjualan', $id_penjualan)
            ->first();

        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Penjualan Edit',
                    'route' => ''
                ],
                [
                    'label' => $penjualan->pjl_no_faktur,
                    'route' => ''
                ]
            ]
        );


        $data = Main::data($breadcrumb, $this->menuActive);
        $pelanggan = mPelanggan::orderBy('plg_nama', 'ASC')->get();
        $barang = mBarang
            ::withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->orderBy('brg_kode')
            ->get();

        $data = array_merge(
            $data, array(
                'penjualan' => $penjualan,
                'pelanggan' => $pelanggan,
                'barang' => $barang,
            )
        );

        return view('penjualan/penjualan/penjualanEditStok', $data);
    }

    function update_harga(Request $request, $id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);
        $rules = [
            'id_pelanggan' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_jual' => 'required',
            'harga_jual.*' => 'required',
            'qty' => [
                'required',
                /**
                 * Digunakan untuk mengecheck stok barang sekarang dengan yang dibeli
                 */
                new rPenjualanUpdateQtyCheck($request, $id_penjualan)
            ],
            'qty.*' => 'required',
            'potongan_harga_barang' => 'required',
            'potongan_harga_barang.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_pelanggan' => 'Pelanggan',
            'id_barang' => 'Barang',
            'harga_jual' => 'Harga',
            'qty' => 'Qty Barang',
            'potongan_harga_barang' => 'Potongan Harga Barang',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
            $attr['potongan_harga_barang.' . $i] = 'Potongan Harga Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $penjualan = mPenjualan::where('id_penjualan', $id_penjualan)->first();
        $id_pelanggan = $request->input('id_pelanggan');
        $kode_pelanggan = $request->input('plg_kode');
        $no_faktur = $request->input('no_faktur');

        $pjl_tanggal_penjualan = Main::format_datetime_db($request->input('tanggal'));
        $pjl_jenis_pembayaran = $request->input('pembayaran');

        $id_barang_arr = $request->input('id_barang');
        $id_stok_barang_arr = $request->input('id_stok_barang');
        $pjd_harga_net_arr = $request->input('harga_net');
        $pjd_harga_beli_arr = $request->input('harga_beli');
        $pjd_harga_jual_arr = $request->input('harga_jual');
        $pjd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pjd_ppn_persen = $this->ppnPersen;
        $pjd_qty_arr = $request->input('qty');
        $pjd_sub_total_arr = $request->input('sub_total');
        $pjd_potongan_harga_barang_arr = $request->input('potongan_harga_barang');

        $pjl_total = $request->input('total');
        $pjl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pjl_potongan = Main::format_number_db($request->input('potongan'));
        $pjl_grand_total = $request->input('grand_total');
        $pjl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pjl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $ppl_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $pjl_keterangan = $request->input('pjl_keterangan');


        DB::beginTransaction();
        try {

            /**
             * memperbarui penjualan data
             */
            $penjualan_data = [
                'id_pelanggan' => $id_pelanggan,
                'id_user' => $id_user,
                /**
                 * Ini disable dulu, karena faktur penjualan digunakan setelah sudah dipakai oleh pelanggan,
                 * agar dapat dicari nantinya datanya di sistem,
                 * kalau di update juga no faktur nya, akan tidak bisa ditemukan lain kali
                 */
//                'pjl_no_faktur' => $no_faktur,
                'pjl_tanggal_penjualan' => $pjl_tanggal_penjualan,
                'pjl_jenis_pembayaran' => $pjl_jenis_pembayaran,
                'pjl_total' => $pjl_total,
                'pjl_biaya_tambahan' => $pjl_biaya_tambahan,
                'pjl_potongan' => $pjl_potongan,
                'pjl_grand_total' => $pjl_grand_total,
                'pjl_jumlah_bayar' => $pjl_jumlah_bayar,
                'pjl_sisa_pembayaran' => $pjl_sisa_pembayaran,
                'pjl_keterangan' => $pjl_keterangan,
                'pjl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
            ];

            mPenjualan::where('id_penjualan', $id_penjualan)->update($penjualan_data);



            /**
             * !!! Ini disable dulu, karena faktur penjualan digunakan setelah sudah dipakai oleh pelanggan,
             * agar dapat dicari nantinya datanya di sistem,
             * kalau di update juga no faktur nya, akan tidak bisa ditemukan lain kali
             *
             * --- Memperbarui keterangan pada arus stok dan piutang pelanggan jika ada perubahan pelanggan
             */
//            if ($penjualan->id_pelanggan != $id_pelanggan) {
            if (FALSE) {

                $pjl_no_faktur_old = $penjualan->pjl_no_faktur;
                $pjl_no_faktur_new = $no_faktur;
                $piutang_pelanggan_data = mPiutangPelanggan::where('id_penjualan', $id_penjualan)->get();
                $penjualan_arus_stok = mArusStok::where('id_penjualan', $id_penjualan)->get();

//
//                return Response::json([
//                    'id_pelanggan_old' => $penjualan->id_pelanggan,
//                    'id_pelanggan_new' => $id_pelanggan,
//                    'pjl_no_faktur_old' => $pjl_no_faktur_old,
//                    'pjl_no_faktur_new' => $pjl_no_faktur_new
//                ], 401);

                foreach ($penjualan_arus_stok as $row) {
                    $id_arus_stok = $row->id_arus_stok;
                    $asb_keterangan = $row->asb_keterangan;
                    $asb_keterangan = str_replace($pjl_no_faktur_old, $pjl_no_faktur_new, $asb_keterangan);
                    $arus_stok_update = [
                        'asb_keterangan' => $asb_keterangan
                    ];
                    mArusStok::where('id_arus_stok', $id_arus_stok)->update($arus_stok_update);
                }


                foreach ($piutang_pelanggan_data as $row) {
                    $id_piutang_pelanggan = $row->id_piutang_pelanggan;
                    $ppl_keterangan = $row->ppl_keterangan;
                    $ppl_keterangan = str_replace($pjl_no_faktur_old, $pjl_no_faktur_new, $ppl_keterangan);
                    $piutang_pelanggan_update = [
                        'ppl_keterangan'=> $ppl_keterangan
                    ];
                    mPiutangPelanggan::where('id_piutang_pelanggan', $id_piutang_pelanggan)->update($piutang_pelanggan_update);
                }

            }


            /**
             * Mengembalikan qty barang pada penjualan ke stok barang, sebelum melakukan pembaruan penjualan
             */
            $penjualan_detail_list = mPenjualanDetail::where('id_penjualan', $id_penjualan)->get();


            foreach ($penjualan_detail_list as $row) {

                /**
                 * Stok Awal Barang
                 */
                $asb_stok_awal = mStokBarang::where('id_barang', $row->id_barang)->sum('sbr_qty');

                /**
                 * Membuat Stok Barang Baru
                 */
                $stok_barang_data = [
                    'id_barang' => $row->id_barang,
                    'id_penjualan' => $row->id_penjualan,
                    'id_penjualan_detail' => $row->id_penjualan_detail,
                    'sbr_qty' => $row->pjd_qty,
                    'sbr_harga_jual' => $row->pjd_harga_jual,
                    'sbr_status' => 'restore_penjualan_edit'
                ];
                mStokBarang::create($stok_barang_data);


                /**
                 * Memasukkan data stok baru ke arus stok
                 */
                $asb_stok_total_terakhir = mStokBarang::where('id_barang', $row->id_barang)->sum('sbr_qty');
                $arus_stok_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_penjualan_detail' => $row->id_penjualan_detail,
                    'id_barang' => $row->id_barang,
                    /**
                     * ini dikomentar, karena pengurangan stok saat penjualan itu tidak per batch/seri barang,
                     * namun secara global, sehingga id_stok_barang tidak disimpan disini,
                     * namun proses pengurangan setiap stok barang tersebut sampai dengan habis, disimpan ke dalam id_stok_barang_json
                     */
//                    'id_stok_barang' => $id_stok_barang,
//                    'id_stok_barang_json' => json_encode($id_stok_barang_hasil_pengurangan_arr),
                    'id_user' => $id_user,
                    'asb_stok_awal' => $asb_stok_awal,
                    'asb_stok_masuk' => $row->pjd_qty,
                    'asb_stok_keluar' => 0,
                    /**
                     * Ini dikomentar, karena maksudnya stok ini adalah stok per batch/seri stok barang,
                     * sedangkan proses yang sekarang, stok barang dicampur semuanya dari awal sampai akhir
                     */
//                    'asb_stok_terakhir' => $asb_stok_terakhir,
                    'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                    'asb_keterangan' => 'Edit Penjualan Barang No Faktur : ' . $penjualan->pjl_no_faktur,
                    'asb_tipe_proses' => 'update',
                    'asb_nama_proses' => 'penjualan'
                ];

                mArusStok::create($arus_stok_data);
            }

            /**
             * Delete Penjualan detail untuk digantikan dengan data penjualan yang diperbarui
             */
            mPenjualanDetail::where('id_penjualan', $id_penjualan)->delete();

            /**
             * Proses memasukkan penjualan detail ke dalam database
             */
            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke penjualan detail
                 */
                $id_stok_barang = $id_stok_barang_arr[$index];
                $pjd_harga_net = Main::format_number_db($pjd_harga_net_arr[$index]);
//                echo '1-'.json_encode($pjd_harga_beli_arr);
                $pjd_harga_beli = Main::format_number_db($pjd_harga_beli_arr[$index]);
                $pjd_harga_jual = Main::format_number_db($pjd_harga_jual_arr[$index]);
                $pjd_potongan_harga_barang = Main::format_number_db($pjd_potongan_harga_barang_arr[$index]);
                $pjd_sub_total = $pjd_sub_total_arr[$index];

                $pjd_ppn_nominal = Main::format_number_db($pjd_ppn_nominal_arr[$index]);
                $pjd_qty = Main::format_number_db($pjd_qty_arr[$index]);
                $pjd_qty_sisa = $pjd_qty;
                $penjualan_detail_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'pjd_qty' => $pjd_qty,
                    'pjd_qty_sisa' => $pjd_qty_sisa,
                    'pjd_harga_jual' => $pjd_harga_jual,
                    'pjd_hpp' => $pjd_harga_beli,
//                    'pjd_potongan' => 0,
                    'pjd_ppn_persen' => $pjd_ppn_persen,
                    'pjd_ppn_nominal' => $pjd_ppn_nominal,
                    'pjd_harga_net' => $pjd_harga_net,
                    'pjd_potongan_harga_barang' => $pjd_potongan_harga_barang,
                    'pjd_sub_total' => $pjd_sub_total,
                ];

                $response_penjualan_detail = mPenjualanDetail::create($penjualan_detail_data);
                $id_penjualan_detail = $response_penjualan_detail->id_penjualan_detail;

                /**
                 * Mengurangi dari stok barang dan insert arus stok
                 */
                $this->proses_pengurangan_stok($id_barang, $pjd_qty, $id_penjualan, $id_penjualan_detail, $id_user, $penjualan->pjl_no_faktur);
            }

            /**
             * Proses pembaruan data ke piutang pelanggan
             */
            $piutang_pelanggan = mPiutangPelanggan::where('id_penjualan', $id_penjualan);
            if ($piutang_pelanggan->count() > 0) {
                mPiutangPelanggan
                    ::where([
                        'id_penjualan' => $id_penjualan
                    ])
                    ->update([
                        'ppl_sisa' => 0,
                        'ppl_status' => 'lunas_karena_penjualan_edit'
                    ]);
            }

            /**
             * Proses memasukkan kembali piutang pelanggan jika sudah ada
             */
            if ($pjl_sisa_pembayaran > 0) {
                $ppl_no_faktur = Main::fakturPiutangPelanggan($kode_pelanggan);

                $piutang_pelanggan_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_pelanggan' => $id_pelanggan,
                    'ppl_no_faktur' => $ppl_no_faktur,
                    'ppl_tanggal' => $pjl_tanggal_penjualan,
                    'ppl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
                    'ppl_total' => $pjl_sisa_pembayaran,
                    'ppl_sisa' => $pjl_sisa_pembayaran,
                    'ppl_keterangan' => 'Piutang Pelanggan #' . $ppl_no_faktur . ' dengan no faktur ' . $penjualan->pjl_no_faktur,
                    'ppl_status' => 'belum_lunas'
                ];

                mPiutangPelanggan::create($piutang_pelanggan_data);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        return [
            'redirect' => route('penjualanInvoicePrint', ['id_penjualan' => Main::encrypt($id_penjualan)])
        ];
    }

    function update_stok(Request $request, $id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);
        $rules = [
            'id_pelanggan' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'harga_jual' => 'required',
            'harga_jual.*' => 'required',
            'qty' => [
                'required',
                /**
                 * Digunakan untuk mengecheck stok barang sekarang dengan yang dibeli
                 */
                new rPenjualanUpdateQtyCheck($request, $id_penjualan)
            ],
            'qty.*' => 'required',
            'potongan_harga_barang' => 'required',
            'potongan_harga_barang.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_pelanggan' => 'Pelanggan',
            'id_barang' => 'Barang',
            'harga_jual' => 'Harga',
            'qty' => 'Qty Barang',
            'potongan_harga_barang' => 'Potongan Harga Barang',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
            $attr['potongan_harga_barang.' . $i] = 'Potongan Harga Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $penjualan = mPenjualan::where('id_penjualan', $id_penjualan)->first();
        $id_pelanggan = $request->input('id_pelanggan');
        $kode_pelanggan = $request->input('plg_kode');
        $no_faktur = $request->input('no_faktur');

        $pjl_tanggal_penjualan = Main::format_datetime_db($request->input('tanggal'));
        $pjl_jenis_pembayaran = $request->input('pembayaran');

        $id_barang_arr = $request->input('id_barang');
        $id_stok_barang_arr = $request->input('id_stok_barang');
        $pjd_harga_net_arr = $request->input('harga_net');
        $pjd_harga_beli_arr = $request->input('harga_beli');
        $pjd_harga_jual_arr = $request->input('harga_jual');
        $pjd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pjd_ppn_persen = $this->ppnPersen;
        $pjd_qty_arr = $request->input('qty');
        $pjd_sub_total_arr = $request->input('sub_total');
        $pjd_potongan_harga_barang_arr = $request->input('potongan_harga_barang');

        $pjl_total = $request->input('total');
        $pjl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pjl_potongan = Main::format_number_db($request->input('potongan'));
        $pjl_grand_total = $request->input('grand_total');
        $pjl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pjl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $ppl_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $pjl_keterangan = $request->input('pjl_keterangan');


        DB::beginTransaction();
        try {

            /**
             * memperbarui penjualan data
             */
            $penjualan_data = [
                'id_pelanggan' => $id_pelanggan,
                'id_user' => $id_user,
                'pjl_no_faktur' => $no_faktur,
                'pjl_tanggal_penjualan' => $pjl_tanggal_penjualan,
                'pjl_jenis_pembayaran' => $pjl_jenis_pembayaran,
                'pjl_total' => $pjl_total,
                'pjl_biaya_tambahan' => $pjl_biaya_tambahan,
                'pjl_potongan' => $pjl_potongan,
                'pjl_grand_total' => $pjl_grand_total,
                'pjl_jumlah_bayar' => $pjl_jumlah_bayar,
                'pjl_sisa_pembayaran' => $pjl_sisa_pembayaran,
                'pjl_keterangan' => $pjl_keterangan,
                'pjl_jatuh_tempo' => $ppl_tanggal_jatuh_tempo,
            ];

            mPenjualan::where('id_penjualan', $id_penjualan)->update($penjualan_data);
            mPenjualanDetail::where('id_penjualan', $id_penjualan)->delete();


            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke penjualan detail
                 */
                $id_stok_barang = $id_stok_barang_arr[$index];
                $pjd_harga_net = Main::format_number_db($pjd_harga_net_arr[$index]);
                $pjd_harga_beli = Main::format_number_db($pjd_harga_beli_arr[$index]);
                $pjd_harga_jual = Main::format_number_db($pjd_harga_jual_arr[$index]);
                $pjd_potongan_harga_barang = Main::format_number_db($pjd_potongan_harga_barang_arr[$index]);
                $pjd_sub_total = $pjd_sub_total_arr[$index];

                $pjd_ppn_nominal = Main::format_number_db($pjd_ppn_nominal_arr[$index]);
                $pjd_qty = Main::format_number_db($pjd_qty_arr[$index]);
                $pjd_qty_sisa = $pjd_qty;
                $penjualan_detail_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'pjd_qty' => $pjd_qty,
                    'pjd_qty_sisa' => $pjd_qty_sisa,
                    'pjd_harga_jual' => $pjd_harga_jual,
                    'pjd_hpp' => $pjd_harga_beli,
//                    'pjd_potongan' => 0,
                    'pjd_ppn_persen' => $pjd_ppn_persen,
                    'pjd_ppn_nominal' => $pjd_ppn_nominal,
                    'pjd_harga_net' => $pjd_harga_net,
                    'pjd_potongan_harga_barang' => $pjd_potongan_harga_barang,
                    'pjd_sub_total' => $pjd_sub_total,
                ];

                mPenjualanDetail::create($penjualan_detail_data);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail_modal($id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);

        $penjualan = mPenjualan
            ::with([
                'pelanggan',
                'user',
                'user.karyawan',
                'penjualan_detail',
                'penjualan_detail.barang'
            ])
            ->leftJoin('tb_user', 'tb_user.id', '=', 'penjualan.pjl_posting_id_user')
            ->leftJoin('tb_karyawan', 'tb_karyawan.id', '=', 'tb_user.id_karyawan')
            ->where('id_penjualan', $id_penjualan)
            ->first();

        $data = [
            'penjualan' => $penjualan,
        ];

        return view('penjualan.penjualan.penjualanDetailModal', $data);
    }

    function invoice_print($id_penjualan)
    {
        $id_penjualan = Main::decrypt($id_penjualan);

        $penjualan = mPenjualan
            ::with([
                'pelanggan',
                'penjualan_detail',
                'penjualan_detail.barang',
            ])
            ->where('id_penjualan', $id_penjualan)
            ->first();

        $data = [
            'penjualan' => $penjualan,
        ];

        return view('penjualan.penjualan.penjualanInvoicePdf', $data);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('penjualan.penjualan.penjualanInvoicePdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Penjualan Invoice ' . $penjualan->pjl_no_faktur);

    }

    function download($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $no_faktur_penjualan = $request->no_faktur_penjualan ? $request->no_faktur_penjualan : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mPenjualan
            ::select([
                'id_penjualan',
                'pjl_no_faktur',
                'pjl_tanggal_penjualan',
                'pjl_jenis_pembayaran',
                'pjl_grand_total',
            ])
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->withCount([
                'penjualan_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(pjd_qty)'));
                }
            ])
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->orderBy('pjl_no_faktur', 'ASC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];


        if ($request->filter_type == 'download_excel') {
            return view('penjualan.penjualan.penjualanExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('penjualan.penjualan.penjualanPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('penjualan.penjualan.penjualanPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Rekap Penjualan ' . $date_from . ' - ' . $date_to);
        }

    }
}
