<?php

namespace app\Http\Controllers\ReturBarang;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mPiutangPelangganPembayaran;
use app\Models\mReturBarang;
use app\Models\mReturBarangDetail;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use app\Rules\rPenjualanQtyCheck;
use app\Rules\rReturBarangQtyCheck;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ReturBarang extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->menuActive = $cons['retur_barang'];
        $this->breadcrumb = [
            [
                'label' => $cons['retur_barang'],
                'route' => route('returBarangList')
            ]
        ];
    }

    function index(Request $request)
    {

        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }

        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request, ['date', 'no_faktur_penjualan', 'no_faktur_retur', 'nama_pelanggan', 'download']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $no_faktur_penjualan = $filter_component['no_faktur_penjualan'];
        $no_faktur_retur = $filter_component['no_faktur_retur'];
        $nama_pelanggan = $filter_component['nama_pelanggan'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "no_faktur_penjualan"],
            ["data" => "no_faktur_retur"],
            ["data" => "nama_pelanggan"],
            ["data" => "tanggal_retur"],
            ["data" => "qty_total"],
            ["data" => "grand_total"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords,
                'no_faktur_penjualan' => $no_faktur_penjualan,
                'no_faktur_retur' => $no_faktur_retur,
                'nama_pelanggan' => $nama_pelanggan
            ),
        ]);

        return view('returBarang.returBarang.returBarangList', $data);
    }

    function data_table(Request $request)
    {
        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $no_faktur_penjualan = $data_post['no_faktur_penjualan'];
        $no_faktur_retur = $data_post['no_faktur_retur'];
        $nama_pelanggan = $data_post['nama_pelanggan'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mReturBarang
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'retur_barang.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'retur_barang.id_penjualan')
            ->whereBetween('rbr_tanggal_retur', $where_date)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('rbr_no_faktur', $no_faktur_retur)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_retur_barang'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mReturBarang
            ::select([
                'id_retur_barang',
                'retur_barang.id_penjualan',
                'pjl_no_faktur',
                'rbr_no_faktur',
                'rbr_tanggal_retur',
                'rbr_grand_total',
                'plg_nama'
            ])
            ->leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'retur_barang.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'retur_barang.id_penjualan')
            ->whereBetween('rbr_tanggal_retur', $where_date)
            ->withCount([
                'retur_barang_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(rbd_qty)'));
                }
            ])
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('rbr_no_faktur', $no_faktur_retur)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_retur_barang = Main::encrypt($row->id_retur_barang);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['no_faktur_penjualan'] = $row->pjl_no_faktur;
            $nestedData['no_faktur_retur'] = $row->rbr_no_faktur;
            $nestedData['nama_pelanggan'] = $row->plg_nama;
            $nestedData['tanggal_retur'] = Main::format_datetime($row->rbr_tanggal_retur);
            $nestedData['qty_total'] = Main::format_number($row->total_qty);
            $nestedData['grand_total'] = Main::format_number($row->rbr_grand_total);
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_penjualan_detail dropdown-item"
                            target="_blank"
                           href="' . route('returBarangPrintNota', ['id_retur_barang' => $id_retur_barang]) . '">
                            <i class="la la-print"></i>
                            Cetak Nota Retur
                        </a>
                        <a class="akses-action_penjualan_detail btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('returBarangDetailModal', ['id_retur_barang' => $id_retur_barang]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function create(Request $request)
    {
        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Retur Barang Baru',
                    'route' => ''
                ]
            ]
        );
        $data = Main::data($breadcrumb, $this->menuActive);

        $barang = mBarang
            ::withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->orderBy('brg_kode')
            ->get();

        /**
         * Data Table Penjualan di Retur Barang
         */

        $filter_component = Main::date_filter($request, ['date', 'no_faktur_penjualan', 'nama_pelanggan']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];
        $no_faktur_penjualan = $filter_component['no_faktur_penjualan'];
        $nama_pelanggan = $filter_component['nama_pelanggan'];
        $id_penjualan = $request->input('id_penjualan');
        $id_penjualan = $id_penjualan ? Main::decrypt($id_penjualan) : '';
        $penjualan = mPenjualan
            ::with([
                'penjualan_detail',
                'penjualan_detail.barang'
            ])
            ->leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'penjualan.id_pelanggan')
            ->where('id_penjualan', $id_penjualan)
            ->first();

        if($penjualan) {
            $penjualan_detail = $penjualan->penjualan_detail;
        } else {
            $penjualan = [
                'pjl_no_faktur' => '',
                'plg_kode' => '',
                'id_pelanggan' => '',
                'plg_nama' => '',
                'plg_alamat' => '',
            ];
            $penjualan_detail = [];
        }

        $rbr_no_faktur = Main::fakturReturBarang($penjualan['plg_kode']);
        $penjualan = $penjualan ? $penjualan : [];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "no_faktur"],
            ["data" => "nama_pelanggan"],
            ["data" => "tanggal_penjualan"],
            ["data" => "jenis_pembayaran"],
            ["data" => "qty_total"],
            ["data" => "grand_total"],
            ["data" => "options"],
        ];

        $data = array_merge(
            $data,
            array(
                'datatable_column' => $datatable_column,
                'date_filter' => $date_filter,
                'table_data_post' => array(
                    'date_from_db' => $date_from_db,
                    'date_to_db' => $date_to_db,
                    'keywords' => $keywords,
                    'no_faktur_penjualan' => $no_faktur_penjualan,
                    'nama_pelanggan' => $nama_pelanggan
                ),
                'barang' => $barang,
                'penjualan' => $penjualan,
                'rbr_no_faktur' => $rbr_no_faktur,
                'id_penjualan' => $id_penjualan,
                'penjualan_detail' => $penjualan_detail
            )
        );

        return view('returBarang.returBarang.returBarangCreate', $data);
    }

    function penjualan_barang_list(Request $request)
    {
        $id_penjualan = $request->input('id_penjualan');

        $penjualan_detail = mPenjualanDetail
            ::leftJoin('barang', 'barang.id_barang', '=', 'penjualan_detail.id_barang')
            ->where('id_penjualan', $id_penjualan)
            ->get();

        $data = [
            'no' => 1,
            'penjualan_detail' => $penjualan_detail
        ];

        return [
            'html' => (string)view('returBarang.returBarang.returBarangPenjualanList', $data)
        ];
    }

    function data_table_penjualan(Request $request)
    {
        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $no_faktur_penjualan = $data_post['no_faktur_penjualan'];
        $nama_pelanggan = $data_post['nama_pelanggan'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $params_get = $request->has('params_get') ? $request->input('params_get') : [];

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPenjualan
            ::leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'penjualan.id_pelanggan')
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_penjualan'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mPenjualan
            ::select([
                'id_penjualan',
                'pjl_no_faktur',
                'pjl_tanggal_penjualan',
                'pjl_jenis_pembayaran',
                'pjl_grand_total',
                'plg_nama',
                'plg_kode',
                'plg_alamat'
            ])
            ->leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'penjualan.id_pelanggan')
            ->whereBetween('pjl_tanggal_penjualan', $where_date)
            ->withCount([
                'penjualan_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(pjd_qty)'));
                }
            ])
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_penjualan = Main::encrypt($row->id_penjualan);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['no_faktur'] = $row->pjl_no_faktur;
            $nestedData['nama_pelanggan'] = $row->plg_nama;
            $nestedData['tanggal_penjualan'] = Main::format_datetime($row->pjl_tanggal_penjualan);
            $nestedData['jenis_pembayaran'] = $row->pjl_jenis_pembayaran;
            $nestedData['grand_total'] = Main::format_number($row->pjl_grand_total);
            $nestedData['qty_total'] = Main::format_number($row->total_qty);
            $nestedData['options'] = '
                <a data-id-penjualan="' . $row->id_penjualan . '"
                    data-pjl-no-faktur="' . $row->pjl_no_faktur . '"
                    data-plg-nama="' . $row->plg_nama . '"
                    data-plg-kode="' . $row->plg_kode . '"
                    data-plg_alamat="' . $row->plg_alamat . '"
                    href="' . route('returBarangCreate', array_merge($params_get, ['id_penjualan' => $id_penjualan])) . '" 
                    class="btn-penjualan-select1 btn btn-success btn-sm m-btn--pill">
                    <i class="la la-check"></i> Pilih Penjualan
                </a>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $rules = [
            'qty' => [
                'required',
                /**
                 * Digunakan untuk mengecheck stok barang sekarang dengan yang dibeli
                 */
                new rReturBarangQtyCheck($request)
            ],
            'qty.*' => 'required',
            'potongan_harga_barang' => 'required',
            'potongan_harga_barang.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'harga_jual' => 'Harga',
            'qty' => 'Qty Barang',
            'potongan_harga_barang' => 'Potongan Harga Barang',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
            $attr['potongan_harga_barang.' . $i] = 'Potongan Harga Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_penjualan = $request->input('id_penjualan');
        $id_pelanggan = $request->input('id_pelanggan');
        $rbr_urutan = $request->input('urutan');
        $rbr_no_faktur = $request->input('no_faktur');
        $plg_kode = $request->input('plg_kode');
        $rbr_total = $request->input('total');
        $rbr_grand_total = $request->input('grand_total');
        $rbr_sisa_pembayaran = $request->input('sisa_pembayaran');
        $rbr_tanggal_retur = $request->input('tanggal');
        $rbr_tanggal_retur = Main::format_datetime_db($rbr_tanggal_retur);
        $rbr_jenis_pembayaran = $request->input('jenis_pembayaran');
        $rbr_keterangan = $request->input('keterangan');
        $id_penjualan_detail_arr = $request->input('id_penjualan_detail');
        $rbd_harga_jual_arr = $request->input('harga_jual');
        $rbd_qty_jual_arr = $request->input('qty_jual');
        $id_barang_arr = $request->input('id_barang');
        $rbd_sub_total_arr = $request->input('sub_total');
        $rbd_qty_arr = $request->input('qty');
        $rbd_potongan_harga_barang_arr = $request->input('potongan_harga_barang');
        $rbr_biaya_tambahan = $request->input('biaya_tambahan');
        $rbr_biaya_tambahan = Main::format_number_db($rbr_biaya_tambahan);
        $rbr_potongan = $request->input('potongan');
        $rbr_potongan = Main::format_number_db($rbr_potongan);
        $rbr_jumlah_bayar = $request->input('jumlah_bayar');
        $rbr_jumlah_bayar = Main::format_number_db($rbr_jumlah_bayar);

        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $retur_data = [
                'id_penjualan' => $id_penjualan,
                'id_pelanggan' => $id_pelanggan,
                'id_user' => $id_user,
                'rbr_urutan' => $rbr_urutan,
                'rbr_no_faktur' => $rbr_no_faktur,
                'rbr_tanggal_retur' => $rbr_tanggal_retur,
                'rbr_jenis_pembayaran' => $rbr_jenis_pembayaran,
                'rbr_total' => $rbr_total,
                'rbr_biaya_tambahan' => $rbr_biaya_tambahan,
                'rbr_potongan' => $rbr_potongan,
                'rbr_grand_total' => $rbr_grand_total,
                'rbr_jumlah_bayar' => $rbr_jumlah_bayar,
                'rbr_sisa_pembayaran' => $rbr_sisa_pembayaran,
                'rbr_keterangan' => $rbr_keterangan
            ];

            $id_retur_barang = mReturBarang::create($retur_data)->id_retur_barang;

            foreach ($id_penjualan_detail_arr as $index => $id_penjualan_detail) {

                /**
                 * Memasukkan ke Retur Barang Detail
                 */
                $id_barang = $id_barang_arr[$index];
                $rbd_qty = $rbd_qty_arr[$index];
                $rbd_qty = Main::format_number_db($rbd_qty);
                $rbd_harga_jual = $rbd_harga_jual_arr[$index];
                $rbd_harga_jual = Main::format_number_db($rbd_harga_jual);
                $rbd_potongan_harga_barang = $rbd_potongan_harga_barang_arr[$index];
                $rbd_potongan_harga_barang = Main::format_number_db($rbd_potongan_harga_barang);
                $rbd_sub_total = $rbd_sub_total_arr[$index];
                $rbd_sub_total = Main::format_number_db($rbd_sub_total);

                $retur_barang_detil_data = [
                    'id_retur_barang' => $id_retur_barang,
                    'id_penjualan_detil' => $id_penjualan_detail,
                    'id_barang' => $id_barang,
                    'rbd_qty' => $rbd_qty,
                    'rbd_harga_jual' => $rbd_harga_jual,
                    'rbd_potongan_harga_barang' => $rbd_potongan_harga_barang,
                    'rbd_sub_total' => $rbd_sub_total,
                ];

                $id_retur_barang_detil = mReturBarangDetail::create($retur_barang_detil_data)->id_retur_barang_detil;


                /**
                 * Mengurangi qty penjualan sisa setelah di return
                 */

                $penjualan_detail = mPenjualanDetail::where('id_penjualan_detail', $id_penjualan_detail)->first();
                $pjd_qty_sisa = $penjualan_detail->pjd_qty_sisa - $rbd_qty;
                $penjualan_detail_update = [
                    'pjd_qty_sisa' => $pjd_qty_sisa
                ];
                mPenjualanDetail::where('id_penjualan_detail', $id_penjualan_detail)->update($penjualan_detail_update);

                /**
                 * Proses menambahkan ke stok barang
                 */

                $asb_stok_awal = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                $data_insert = [
                    'id_barang' => $id_barang,
                    'id_penjualan' => $id_penjualan,
                    'id_penjualan_detail' => $id_penjualan_detail,
                    'sbr_qty' => $rbd_qty,
                    'sbr_status' => 'return'
                ];
                $id_stok_barang = mStokBarang::create($data_insert)->id_stok_barang;

                mPenjualanDetail::where('id_penjualan_detail', $id_penjualan_detail)->update(['id_stok_barang_json' => json_encode([$id_stok_barang => $rbd_qty])]);


                /**
                 * Proses memasukkan data ke arus stok barang
                 */
                $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
                $arus_stok_data = [
                    'id_penjualan' => $id_penjualan,
                    'id_penjualan_detail' => $id_penjualan_detail,
                    'id_barang' => $id_barang,
                    'id_retur_barang' => $id_retur_barang,
                    'id_retur_barang_detail' => $id_retur_barang_detil,
                    'id_stok_barang' => $id_stok_barang,
                    'id_user' => $id_user,
                    'asb_stok_awal' => $asb_stok_awal,
                    'asb_stok_masuk' => $rbd_qty,
//                    'asb_stok_keluar' => $pjd_qty,
                    /**
                     * Ini dikomentar, karena maksudnya stok ini adalah stok per batch/seri stok barang,
                     * sedangkan proses yang sekarang, stok barang dicampur semuanya dari awal sampai akhir
                     */
//                    'asb_stok_terakhir' => $asb_stok_terakhir,
                    'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                    'asb_keterangan' => 'Retur Barang No Faktur : ' . $rbr_no_faktur,
                    'asb_tipe_proses' => 'update',
                    'asb_nama_proses' => 'retur'
                ];

                mArusStok::create($arus_stok_data);
            }

            /**
             * Proses check piutang pelanggan saat retur barang, dengan kondisi:
             * - Jika piutang pelanggan sudah mencukupi total uang yang di return, maka tidak ada pengurangan dari
             *   penjualan yang sudah dilakukan
             * - Jika piutang pelanggan belum mencukupi total yang yang di return, maka proses pengurangan penjualan
             *   terjadi mulai dari jumlah barang beserta jumlah uang yang dikembalikan kepada pelanggan
             */

            $check_piutang_pelanggan = mPiutangPelanggan
                ::where([
                    'id_penjualan' => $id_penjualan,
                    'ppl_status' => 'belum_lunas'
                ])
                ->count();

            if ($check_piutang_pelanggan > 0) {
                $piutang_pelanggan = mPiutangPelanggan
                    ::where([
                        'id_penjualan' => $id_penjualan
                    ])
                    ->first();

                if ($rbr_grand_total == $piutang_pelanggan->ppl_sisa) {

                    /**
                     * Update piutang pelanggan
                     */
                    mPiutangPelanggan
                        ::where('id_penjualan', $id_penjualan)
                        ->update([
                            'ppl_sisa' => 0,
                            'ppl_status' => 'lunas_karena_retur'
                        ]);

                    /**
                     * Update jumlah pengembalian uang saat retur barang untuk cetak nota
                     */
                    $retur_barang_update = [
                        'rbr_jumlah_pengembalian_uang' => 0,
                        'rbr_jumlah_pengembalian_uang_penjelasan' => 'Karena total uang retur (' . Main::format_number($rbr_grand_total) . ') sama dengan sisa piutang (' . Main::format_number($piutang_pelanggan->ppl_sisa) . '), Sehingga Tidak ada uang pengembalian'
                    ];
                    mReturBarang::where('id_retur_barang', $id_retur_barang)->update($retur_barang_update);

                    /**
                     * Mengisikan ke history piutang pelanggan
                     */
                    $piutang_pelanggan_pembayaran_create = [
                        'id_piutang_pelanggan' => $piutang_pelanggan->id_piutang_pelanggan,
                        'id_user' => $id_user,
                        'id_retur_barang' => $id_retur_barang,
                        'ppp_total_piutang' => $piutang_pelanggan->ppl_sisa,
                        'ppp_jumlah_bayar' => $rbr_grand_total,
                        'ppp_sisa_bayar' => 0,
                        'ppp_tanggal_bayar' => Main::date(),
                        'ppp_keterangan' => 'Lunas karena return barang no faktur retur ' . $rbr_no_faktur
                    ];
                    mPiutangPelangganPembayaran::create($piutang_pelanggan_pembayaran_create);

                } elseif ($rbr_grand_total > $piutang_pelanggan->ppl_sisa) {

                    /**
                     * Update Piutang Pelanggan
                     */
                    $rbr_jumlah_pengembalian_uang = $rbr_grand_total - $piutang_pelanggan->ppl_sisa;
                    mPiutangPelanggan
                        ::where('id_penjualan', $id_penjualan)
                        ->update([
                            'ppl_sisa' => 0,
                            'ppl_status' => 'lunas_karena_retur'
                        ]);

                    /**
                     * Update jumlah pengembalian uang saat retur barang untuk cetak nota
                     */
                    $retur_barang_update = [
                        'rbr_jumlah_pengembalian_uang' => $rbr_jumlah_pengembalian_uang,
                        'rbr_jumlah_pengembalian_uang_penjelasan' => 'Karena total uang retur (' . Main::format_number($rbr_grand_total) . ') lebih besar dari sisa piutang (' . Main::format_number($piutang_pelanggan->ppl_sisa) . '), Maka pengembalian uang = ' . Main::format_number($rbr_jumlah_pengembalian_uang).' & Sisa Piutang Pelanggan = 0'
                    ];
                    mReturBarang::where('id_retur_barang', $id_retur_barang)->update($retur_barang_update);

                    /**
                     * Mengisikan ke history piutang pelanggan
                     */
                    $piutang_pelanggan_pembayaran_create = [
                        'id_piutang_pelanggan' => $piutang_pelanggan->id_piutang_pelanggan,
                        'id_user' => $id_user,
                        'id_retur_barang' => $id_retur_barang,
                        'ppp_total_piutang' => $piutang_pelanggan->ppl_sisa,
                        'ppp_jumlah_bayar' => $rbr_grand_total,
                        'ppp_sisa_bayar' => 0,
                        'ppp_tanggal_bayar' => Main::date(),
                        'ppp_keterangan' => 'Lunas karena return barang no faktur retur ' . $rbr_no_faktur
                    ];
                    mPiutangPelangganPembayaran::create($piutang_pelanggan_pembayaran_create);
                } elseif ($rbr_grand_total < $piutang_pelanggan->ppl_sisa) {

                    /**
                     * Update Piutang Pelanggan
                     */
                    $ppl_sisa = $piutang_pelanggan->ppl_sisa - $rbr_grand_total;
                    mPiutangPelanggan
                        ::where('id_penjualan', $id_penjualan)
                        ->update([
                            'ppl_sisa' => $ppl_sisa,
                            'ppl_status' => 'belum_lunas'
                        ]);

                    /**
                     * Update jumlah pengembalian uang saat retur barang untuk cetak nota
                     */
                    $rbr_jumlah_pengembalian_uang = 0;
                    $retur_barang_update = [
                        'rbr_jumlah_pengembalian_uang' => $rbr_jumlah_pengembalian_uang,
                        'rbr_jumlah_pengembalian_uang_penjelasan' => 'Karena total uang retur (' . Main::format_number($rbr_grand_total) . ') lebih kecil dari sisa piutang (' . Main::format_number($piutang_pelanggan->ppl_sisa) . '), Maka pengembalian uang = ' . Main::format_number($rbr_jumlah_pengembalian_uang).' & Sisa Piutang Pelanggan = '.Main::format_number($ppl_sisa)
                    ];
                    mReturBarang::where('id_retur_barang', $id_retur_barang)->update($retur_barang_update);

                    /**
                     * Mengisikan ke history piutang pelanggan
                     */
                    $piutang_pelanggan_pembayaran_create = [
                        'id_piutang_pelanggan' => $piutang_pelanggan->id_piutang_pelanggan,
                        'id_user' => $id_user,
                        'id_retur_barang' => $id_retur_barang,
                        'ppp_total_piutang' => $piutang_pelanggan->ppl_sisa,
                        'ppp_jumlah_bayar' => $rbr_grand_total,
                        'ppp_sisa_bayar' => $ppl_sisa,
                        'ppp_tanggal_bayar' => Main::date(),
                        'ppp_keterangan' => 'Pembayaran return barang no faktur retur ' . $rbr_no_faktur
                    ];
                    mPiutangPelangganPembayaran::create($piutang_pelanggan_pembayaran_create);
                }


            } else {
                $rbr_jumlah_pengembalian_uang = $rbr_grand_total;
                $retur_barang_update = [
                    'rbr_jumlah_pengembalian_uang' => $rbr_jumlah_pengembalian_uang,
                    'rbr_jumlah_pengembalian_uang_penjelasan' => 'Karena Retur Barang tidak ada piutang pelanggan, Maka pengembalian uang sama dengan total uang retur barang (' . Main::format_number($rbr_grand_total) . ')'
                ];
                mReturBarang::where('id_retur_barang', $id_retur_barang)->update($retur_barang_update);
            }

//            return response([
//                'errors' => [
//                    'stok_barang' => [
//                        [
//                            0 => 'Stok barang <strong>Tidak Mencukupi</strong> Pembelian, harap check kembali jumlah stok yang tersedia'
//                        ]
//                    ]
//                ]
//            ], 422);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        return [
            'redirect' => route('returBarangPrintNota', ['id_retur_barang' => Main::encrypt($id_retur_barang)])
        ];
    }

    function nota_retur_barang($id_retur_barang)
    {
        $id_retur_barang = Main::decrypt($id_retur_barang);
        $retur_barang = mReturBarang
            ::with([
                'pelanggan',
                'retur_barang_detail',
                'retur_barang_detail.barang'
            ])
            ->where('id_retur_barang', $id_retur_barang)
            ->first();

        $data = [
            'retur_barang' => $retur_barang
        ];

        return view('returBarang.returBarang.returBarangPrintNota', $data);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('returBarang.returBarang.returBarangPrintNota', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Nota Retur Barang');

    }

    function detail_modal($id_retur_barang)
    {
        $id_retur_barang = Main::decrypt($id_retur_barang);

        $retur_barang = mReturBarang
            ::with([
                'pelanggan',
                'retur_barang_detail',
                'retur_barang_detail.barang'
            ])
            ->where('id_retur_barang', $id_retur_barang)
            ->first();

        $data = [
            'retur_barang' => $retur_barang
        ];

        return view('returBarang.returBarang.returBarangDetailModal', $data);
    }

    function download($request)
    {
        $date_from = $request->date_from ? $request->date_from : date('d-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("d-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $no_faktur_penjualan = $request->no_faktur_penjualan ? $request->no_faktur_penjualan : '';
        $no_faktur_retur = $request->no_faktur_retur ? $request->no_faktur_retur : '';
        $nama_pelanggan = $request->nama_pelanggan ? $request->nama_pelanggan : '';

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data_list = mReturBarang
            ::select([
                'id_retur_barang',
                'retur_barang.id_penjualan',
                'pjl_no_faktur',
                'rbr_no_faktur',
                'rbr_jenis_pembayaran',
                'rbr_jumlah_pengembalian_uang',
                'rbr_jumlah_pengembalian_uang_penjelasan',
                'rbr_tanggal_retur',
                'rbr_grand_total',
                'plg_nama'
            ])
            ->leftJoin('pelanggan', 'pelanggan.id_pelanggan', '=', 'retur_barang.id_pelanggan')
            ->leftJoin('penjualan', 'penjualan.id_penjualan', '=', 'retur_barang.id_penjualan')
            ->whereBetween('rbr_tanggal_retur', $where_date)
            ->withCount([
                'retur_barang_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(rbd_qty)'));
                }
            ])
            ->whereLike('pjl_no_faktur', $no_faktur_penjualan)
            ->whereLike('rbr_no_faktur', $no_faktur_retur)
            ->whereLike('plg_nama', $nama_pelanggan)
            ->orderBy('id_retur_barang', 'DESC')
            ->get();

        $data = [
            'data_list' => $data_list,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];


        if ($request->filter_type == 'download_excel') {
            return view('returBarang.returBarang.returBarangExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('returBarang.returBarang.returBarangPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('returBarang.returBarang.returBarangPdf', $data);

            return $pdf
                ->setPaper('A4', 'landscape')
                ->stream('Retur Barang');
        }

    }

}
