<?php

namespace app\Http\Controllers\StokAlert;

use app\Models\mBarang;
use app\Models\mStokBarang;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\DB;

class StokAlert extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');

        $this->menuActive = $cons['stok_alert'];
        $this->breadcrumb = [
            [
                'label' => $cons['stok_alert'],
                'route' => route('stokAlertList')
            ]
        ];
    }

    function index(Request $request)
    {
        if (in_array($request->filter_type, ['download_pdf', 'download_excel'])) {
            return $this->download($request);
        }

        $data = Main::data($this->breadcrumb);
        $filter_component = Main::date_filter($request, ['download', 'kode_barang', 'nama_barang']);
        $date_filter = $filter_component['date_filter'];
        $kode_barang = $filter_component['kode_barang'];
        $nama_barang = $filter_component['nama_barang'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "brg_kode"],
            ["data" => "brg_nama"],
            ["data" => "jbr_nama"],
            ["data" => "brg_golongan"],
            ["data" => "brg_minimal_stok"],
            ["data" => "total_stok"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'kode_barang' => $kode_barang,
                'nama_barang' => $nama_barang,
            ),
        ]);

        return view('stokAlert/stokAlert/stokAlertList', $data);
    }


    function data_table(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $data_post = $request->input('data');
        $kode_barang = $data_post['kode_barang'];
        $nama_barang = $data_post['nama_barang'];
        $order_column = 'brg_kode'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $total_data_list = mBarang
            ::leftJoin('jenis_barang', 'jenis_barang.id_jenis_barang', '=', 'barang.id_jenis_barang')
            ->withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->get();

        $total_data = 0;
        foreach ($total_data_list as $row) {
            if ($row->brg_minimal_stok >= $row->total_stok) {
                $total_data++;
            }
        }

        $data_list = mBarang
            ::leftJoin('jenis_barang', 'jenis_barang.id_jenis_barang', '=', 'barang.id_jenis_barang')
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $data = array();
        $t = 0;
        $no = 0;
        foreach ($data_list as $key => $row) {
            $total_stok = mStokBarang::where('id_barang', $row->id_barang)->sum('sbr_qty');
            if ($row->brg_minimal_stok >= $total_stok) {

                if ($order_type == 'asc') {
                    $no++;
                } else {
                    $no = $total_data - $t - $start;
                }

                $nestedData['no'] = $no;
                $nestedData['brg_kode'] = $row->brg_kode;
                $nestedData['brg_nama'] = $row->brg_nama;
                $nestedData['jbr_nama'] = $row->jbr_nama;
                $nestedData['brg_golongan'] = Main::barang_golongan_label($row->brg_golongan);
                $nestedData['brg_minimal_stok'] = Main::format_number($row->brg_minimal_stok);
                $nestedData['total_stok'] = Main::format_number($total_stok);

                $data[] = $nestedData;

                $t++;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function download($request)
    {
        $kode_barang = $request->kode_barang ? $request->kode_barang : '';
        $nama_barang = $request->nama_barang ? $request->nama_barang : '';

        $data_list = mBarang
            ::leftJoin('jenis_barang', 'jenis_barang.id_jenis_barang', '=', 'barang.id_jenis_barang')
            ->whereLike('brg_kode', $kode_barang)
            ->whereLike('brg_nama', $nama_barang)
            ->orderBy('brg_kode', 'ASC')
            ->orderBy('brg_nama', 'ASC')
            ->get();

        $data = [
            'no' => 1,
            'data_list' => $data_list
        ];

        if ($request->filter_type == 'download_excel') {
            return view('stokAlert.stokAlert.stokAlertExcel', $data);
        }

        if ($request->filter_type == 'download_pdf') {
            return view('stokAlert.stokAlert.stokAlertPdf', $data);
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('stokAlert.stokAlert.stokAlertPdf', $data);

            return $pdf
                ->setPaper('A4', 'portrait')
                ->stream('Stok Alert');
        }
    }

}
