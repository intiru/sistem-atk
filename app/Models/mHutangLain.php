<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mHutangLain extends Model
{
    use SoftDeletes;

    protected $table = 'hutang_lain';
    protected $primaryKey = 'id_hutang_lain';
    protected $fillable = [
        'id_supplier',
        'hul_no_faktur',
        'hul_tanggal',
        'hul_tanggal_jatuh_tempo',
        'hul_total',
        'hul_sisa',
        'hul_keterangan',
        'hul_status',
    ];

    public function supplier()
    {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function hutang_lain_pembayaran()
    {
        return $this->hasMany(mHutangLainPembayaran::class, 'id_hutang_lain');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

//    public function getUpdatedAtAttribute()
//    {
//        return \Carbon\Carbon::parse($this->attributes['updated_at'])
//            ->diffForHumans();
//    }
}
