<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPiutangLain extends Model
{
    use SoftDeletes;

    protected $table = 'piutang_lain';
    protected $primaryKey = 'id_piutang_lain';
    protected $fillable = [
        'id_pelanggan',
        'ptl_no_faktur',
        'ptl_tanggal',
        'ptl_tanggal_jatuh_tempo',
        'ptl_total',
        'ptl_sisa',
        'ptl_status',
        'ptl_keterangan',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(mPelanggan::class, 'id_pelanggan');
    }

    public function piutang_lain_pembayaran()
    {
        return $this->belongsTo(mPiutangLainPembayaran::class, 'id_piutang_lain');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

//    public function getUpdatedAtAttribute()
//    {
//        return \Carbon\Carbon::parse($this->attributes['updated_at'])
//            ->diffForHumans();
//    }
}
