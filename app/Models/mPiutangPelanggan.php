<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPiutangPelanggan extends Model
{
    use SoftDeletes;

    protected $table = 'piutang_pelanggan';
    protected $primaryKey = 'id_piutang_pelanggan';
    protected $fillable = [
        'id_penjualan',
        'id_pelanggan',
        'id_retur_barang',
        'ppl_no_faktur',
        'ppl_tanggal',
        'ppl_jatuh_tempo',
        'ppl_total',
        'ppl_sisa',
        'ppl_status',
        'ppl_keterangan',
    ];

    public function penjualan()
    {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    public function pelanggan()
    {
        return $this->belongsTo(mPelanggan::class, 'id_pelanggan');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

/*    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }*/
}
