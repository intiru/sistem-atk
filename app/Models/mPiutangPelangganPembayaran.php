<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPiutangPelangganPembayaran extends Model
{
    use SoftDeletes;

    protected $table = 'piutang_pelanggan_pembayaran';
    protected $primaryKey = 'id_piutang_pelanggan_pembayaran';
    protected $fillable = [
        'id_piutang_pelanggan',
        'id_user',
        'ppp_total_piutang',
        'ppp_jumlah_bayar',
        'ppp_sisa_bayar',
        'ppp_tanggal_bayar',
        'ppp_keterangan'
    ];

    public function piutang_pelanggan()
    {
        return $this->belongsTo(mPiutangPelanggan::class, 'id_piutang_pelanggan');
    }

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
