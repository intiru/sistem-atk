<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mReturBarangDetail extends Model
{
    use SoftDeletes;

    protected $table = 'retur_barang_detail';
    protected $primaryKey = 'id_retur_barang_detail';
    protected $fillable = [
        'id_retur_barang',
        'id_penjualan_detail',
        'id_barang',
        'rbd_qty',
        'rbd_harga_jual',
        'rbd_potongan_harga_barang',
        'rbd_sub_total'
    ];

    public function retur_barang() {
        return $this->belongsTo(mReturBarang::class, 'id_retur_barang');
    }

    public function penjualan_detail() {
        return $this->belongsTo(mPenjualanDetail::class, 'id_penjualan_detail');
    }

    public function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
