<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mStokBarang extends Model
{
    use SoftDeletes;

    protected $table = 'stok_barang';
    protected $primaryKey = 'id_stok_barang';
    protected $fillable = [
        'id_barang',
        'id_satuan',
        'id_pembelian',
        'id_pembelian_detail',
        'id_penjualan',
        'id_penjualan_detail',
        'id_supplier',
        'id_stok_barang_json',
        'sbr_kode_batch',
        'sbr_expired',
        'sbr_qty',
        'sbr_harga_beli',
        'sbr_harga_jual',
        'sbr_status',
        'sbr_konsinyasi_status',
    ];

    public function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function supplier()
    {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function satuan()
    {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
