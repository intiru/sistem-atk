<?php

namespace app\Rules;

use app\Models\mStokBarang;
use Illuminate\Contracts\Validation\Rule;
use app\Models\mUser;

/**
 * Digunakan untuk mengecheck apakah total stok barang tersedia sesuai dengan qty penjualan ?
 * dan jika pembelian barang lebih dari satu, dengan barang yang sama, proses dibawah ini akan memberikan proses validasinya
 *
 * Class rPenjualanQtyCheck
 * @package app\Rules
 */
class rReturBarangQtyCheck implements Rule
{
    private $value = '';
    private $request;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $qty_jual_arr = $this->request->qty_jual;
        $qty_arr = $this->request->qty;
        $status_check = TRUE;

        foreach ($qty_jual_arr as $key => $qty_jual) {
            $qty = $qty_arr[$key];
            if ($qty_jual < $qty) {
                $status_check = FALSE;
                break;
            }
        }

        return $status_check;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "QTY Retur <strong>Tidak bisa Melebihi</strong> QTY Jual";
    }
}
