/*
 Navicat MySQL Data Transfer

 Source Server         : intiru.com - system
 Source Server Type    : MySQL
 Source Server Version : 100334
 Source Host           : 103.153.3.23:3306
 Source Schema         : intiruc2_atk_bimantara_media_bali

 Target Server Type    : MySQL
 Target Server Version : 100334
 File Encoding         : 65001

 Date: 28/04/2022 11:49:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for arus_stok
-- ----------------------------
DROP TABLE IF EXISTS `arus_stok`;
CREATE TABLE `arus_stok` (
  `id_arus_stok` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_pembelian_detail` int(11) DEFAULT NULL,
  `id_penjualan` int(11) DEFAULT NULL,
  `id_penjualan_detail` int(11) DEFAULT NULL,
  `id_retur_barang` int(11) DEFAULT NULL,
  `id_retur_barang_detail` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_stok_barang` int(11) DEFAULT NULL,
  `id_history_penyesuaian_stok` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `asb_stok_awal` int(11) DEFAULT NULL,
  `asb_stok_masuk` decimal(20,0) DEFAULT NULL,
  `asb_stok_keluar` decimal(20,0) DEFAULT NULL,
  `asb_stok_terakhir` decimal(20,0) DEFAULT NULL,
  `asb_stok_total_terakhir` decimal(20,0) DEFAULT NULL,
  `asb_keterangan` text DEFAULT NULL,
  `asb_tipe_proses` enum('insert','update','delete') DEFAULT NULL,
  `asb_nama_proses` enum('pembelian','penjualan','penyesuaian_stok','retur') DEFAULT 'pembelian',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_arus_stok`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1601 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of arus_stok
-- ----------------------------
BEGIN;
INSERT INTO `arus_stok` VALUES (1207, 394, 1112, NULL, NULL, NULL, NULL, 1135, 1143, NULL, 36, 0, 20, 0, 20, 20, 'Pembelian Barang No Faktur : PBL/TI/2022/03/001', 'insert', 'pembelian', '2022-03-30 14:38:23', '2022-03-30 14:38:23', NULL);
INSERT INTO `arus_stok` VALUES (1208, 395, 1113, NULL, NULL, NULL, NULL, 1139, 1144, NULL, 36, 0, 12, 0, 12, 12, 'Pembelian Barang No Faktur : PBL/TI/2022/03/002', 'insert', 'pembelian', '2022-03-30 14:48:40', '2022-03-30 14:48:40', NULL);
INSERT INTO `arus_stok` VALUES (1209, 395, 1114, NULL, NULL, NULL, NULL, 1140, 1145, NULL, 36, 0, 12, 0, 12, 12, 'Pembelian Barang No Faktur : PBL/TI/2022/03/002', 'insert', 'pembelian', '2022-03-30 14:48:40', '2022-03-30 14:48:40', NULL);
INSERT INTO `arus_stok` VALUES (1210, 396, 1115, NULL, NULL, NULL, NULL, 1152, 1146, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/RET/2022/03/003', 'insert', 'pembelian', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `arus_stok` VALUES (1211, 396, 1116, NULL, NULL, NULL, NULL, 1155, 1147, NULL, 36, 0, 6, 0, 6, 6, 'Pembelian Barang No Faktur : PBL/RET/2022/03/003', 'insert', 'pembelian', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `arus_stok` VALUES (1212, 396, 1117, NULL, NULL, NULL, NULL, 1157, 1148, NULL, 36, 0, 4, 0, 4, 4, 'Pembelian Barang No Faktur : PBL/RET/2022/03/003', 'insert', 'pembelian', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `arus_stok` VALUES (1213, 396, 1118, NULL, NULL, NULL, NULL, 1158, 1149, NULL, 36, 0, 3, 0, 3, 3, 'Pembelian Barang No Faktur : PBL/RET/2022/03/003', 'insert', 'pembelian', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `arus_stok` VALUES (1214, 397, 1119, NULL, NULL, NULL, NULL, 1172, 1150, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/004', 'insert', 'pembelian', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `arus_stok` VALUES (1215, 397, 1120, NULL, NULL, NULL, NULL, 1173, 1151, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/004', 'insert', 'pembelian', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `arus_stok` VALUES (1216, 397, 1121, NULL, NULL, NULL, NULL, 1174, 1152, NULL, 36, 0, 3, 0, 3, 3, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/004', 'insert', 'pembelian', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `arus_stok` VALUES (1217, 397, 1122, NULL, NULL, NULL, NULL, 1175, 1153, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/004', 'insert', 'pembelian', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `arus_stok` VALUES (1218, 397, 1123, NULL, NULL, NULL, NULL, 1176, 1154, NULL, 36, 0, 4, 0, 4, 4, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/004', 'insert', 'pembelian', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `arus_stok` VALUES (1219, 397, 1124, NULL, NULL, NULL, NULL, 1177, 1155, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/004', 'insert', 'pembelian', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `arus_stok` VALUES (1220, 397, 1125, NULL, NULL, NULL, NULL, 1178, 1156, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/004', 'insert', 'pembelian', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `arus_stok` VALUES (1221, 398, 1126, NULL, NULL, NULL, NULL, 1179, 1157, NULL, 36, 0, 12, 0, 12, 12, 'Pembelian Barang No Faktur : PBL/POT/2022/03/005', 'insert', 'pembelian', '2022-03-30 16:37:59', '2022-03-30 16:37:59', NULL);
INSERT INTO `arus_stok` VALUES (1222, 399, 1127, NULL, NULL, NULL, NULL, 1180, 1158, NULL, 36, 0, 12, 0, 12, 12, 'Pembelian Barang No Faktur : PBL/AP/2022/03/006', 'insert', 'pembelian', '2022-03-30 16:43:33', '2022-03-30 16:43:33', NULL);
INSERT INTO `arus_stok` VALUES (1223, 400, 1128, NULL, NULL, NULL, NULL, 1182, 1159, NULL, 36, 0, 12, 0, 12, 12, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/007', 'insert', 'pembelian', '2022-03-30 16:57:11', '2022-03-30 16:57:11', NULL);
INSERT INTO `arus_stok` VALUES (1224, 400, 1129, NULL, NULL, NULL, NULL, 1181, 1160, NULL, 36, 0, 40, 0, 40, 40, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/007', 'insert', 'pembelian', '2022-03-30 16:57:11', '2022-03-30 16:57:11', NULL);
INSERT INTO `arus_stok` VALUES (1225, 401, 1130, NULL, NULL, NULL, NULL, 1181, 1161, NULL, 36, 40, 20, 0, 20, 60, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/008', 'insert', 'pembelian', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `arus_stok` VALUES (1226, 401, 1131, NULL, NULL, NULL, NULL, 1183, 1162, NULL, 36, 0, 10, 0, 10, 10, 'Pembelian Barang No Faktur : PBL/CSA/2022/03/008', 'insert', 'pembelian', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `arus_stok` VALUES (1227, 402, 1132, NULL, NULL, NULL, NULL, 1184, 1163, NULL, 36, 0, 400, 0, 400, 400, 'Pembelian Barang No Faktur : PBL/AS/2022/03/009', 'insert', 'pembelian', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `arus_stok` VALUES (1228, 402, 1133, NULL, NULL, NULL, NULL, 1185, 1164, NULL, 36, 0, 150, 0, 150, 150, 'Pembelian Barang No Faktur : PBL/AS/2022/03/009', 'insert', 'pembelian', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `arus_stok` VALUES (1229, 402, 1134, NULL, NULL, NULL, NULL, 1186, 1165, NULL, 36, 0, 100, 0, 100, 100, 'Pembelian Barang No Faktur : PBL/AS/2022/03/009', 'insert', 'pembelian', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `arus_stok` VALUES (1230, 402, 1135, NULL, NULL, NULL, NULL, 1187, 1166, NULL, 36, 0, 3, 0, 3, 3, 'Pembelian Barang No Faktur : PBL/AS/2022/03/009', 'insert', 'pembelian', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `arus_stok` VALUES (1231, 402, 1136, NULL, NULL, NULL, NULL, 1188, 1167, NULL, 36, 0, 80, 0, 80, 80, 'Pembelian Barang No Faktur : PBL/AS/2022/03/009', 'insert', 'pembelian', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `arus_stok` VALUES (1232, 402, 1137, NULL, NULL, NULL, NULL, 1189, 1168, NULL, 36, 0, 40, 0, 40, 40, 'Pembelian Barang No Faktur : PBL/AS/2022/03/009', 'insert', 'pembelian', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `arus_stok` VALUES (1233, 402, 1138, NULL, NULL, NULL, NULL, 1190, 1169, NULL, 36, 0, 30, 0, 30, 30, 'Pembelian Barang No Faktur : PBL/AS/2022/03/009', 'insert', 'pembelian', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `arus_stok` VALUES (1234, 403, 1139, NULL, NULL, NULL, NULL, 1191, 1170, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/MSC/2022/03/010', 'insert', 'pembelian', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `arus_stok` VALUES (1235, 403, 1140, NULL, NULL, NULL, NULL, 1192, 1171, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/MSC/2022/03/010', 'insert', 'pembelian', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `arus_stok` VALUES (1236, 403, 1141, NULL, NULL, NULL, NULL, 1193, 1172, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/MSC/2022/03/010', 'insert', 'pembelian', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `arus_stok` VALUES (1237, 403, 1142, NULL, NULL, NULL, NULL, 1194, 1173, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/MSC/2022/03/010', 'insert', 'pembelian', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `arus_stok` VALUES (1238, 404, 1143, NULL, NULL, NULL, NULL, 1195, 1174, NULL, 36, 0, 40, 0, 40, 40, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1239, 404, 1144, NULL, NULL, NULL, NULL, 1196, 1175, NULL, 36, 0, 57, 0, 57, 57, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1240, 404, 1145, NULL, NULL, NULL, NULL, 1197, 1176, NULL, 36, 0, 25, 0, 25, 25, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1241, 404, 1146, NULL, NULL, NULL, NULL, 1198, 1177, NULL, 36, 0, 25, 0, 25, 25, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1242, 404, 1147, NULL, NULL, NULL, NULL, 1199, 1178, NULL, 36, 0, 100, 0, 100, 100, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1243, 404, 1148, NULL, NULL, NULL, NULL, 1200, 1179, NULL, 36, 0, 60, 0, 60, 60, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1244, 404, 1149, NULL, NULL, NULL, NULL, 1201, 1180, NULL, 36, 0, 60, 0, 60, 60, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1245, 404, 1150, NULL, NULL, NULL, NULL, 1202, 1181, NULL, 36, 0, 57, 0, 57, 57, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1246, 404, 1151, NULL, NULL, NULL, NULL, 1203, 1182, NULL, 36, 0, 6, 0, 6, 6, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1247, 404, 1152, NULL, NULL, NULL, NULL, 1204, 1183, NULL, 36, 0, 12, 0, 12, 12, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1248, 404, 1153, NULL, NULL, NULL, NULL, 1205, 1184, NULL, 36, 0, 15, 0, 15, 15, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1249, 404, 1154, NULL, NULL, NULL, NULL, 1206, 1185, NULL, 36, 0, 12, 0, 12, 12, 'Pembelian Barang No Faktur : PBL/NM/2022/03/011', 'insert', 'pembelian', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `arus_stok` VALUES (1250, 405, 1155, NULL, NULL, NULL, NULL, 1208, 1186, NULL, 36, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/BE/2022/03/012', 'insert', 'pembelian', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `arus_stok` VALUES (1251, 405, 1156, NULL, NULL, NULL, NULL, 1207, 1187, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/BE/2022/03/012', 'insert', 'pembelian', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `arus_stok` VALUES (1252, 405, 1157, NULL, NULL, NULL, NULL, 1209, 1188, NULL, 36, 0, 6, 0, 6, 6, 'Pembelian Barang No Faktur : PBL/BE/2022/03/012', 'insert', 'pembelian', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `arus_stok` VALUES (1253, 405, 1158, NULL, NULL, NULL, NULL, 1210, 1189, NULL, 36, 0, 10, 0, 10, 10, 'Pembelian Barang No Faktur : PBL/BE/2022/03/012', 'insert', 'pembelian', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `arus_stok` VALUES (1254, 405, 1159, NULL, NULL, NULL, NULL, 1211, 1190, NULL, 36, 0, 120, 0, 120, 120, 'Pembelian Barang No Faktur : PBL/BE/2022/03/012', 'insert', 'pembelian', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `arus_stok` VALUES (1255, 405, 1160, NULL, NULL, NULL, NULL, 1212, 1191, NULL, 36, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/BE/2022/03/012', 'insert', 'pembelian', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `arus_stok` VALUES (1256, 405, 1161, NULL, NULL, NULL, NULL, 1213, 1192, NULL, 36, 0, 3, 0, 3, 3, 'Pembelian Barang No Faktur : PBL/BE/2022/03/012', 'insert', 'pembelian', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `arus_stok` VALUES (1257, 406, 1162, NULL, NULL, NULL, NULL, 1214, 1193, NULL, 36, 0, 10, 0, 10, 10, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1258, 406, 1163, NULL, NULL, NULL, NULL, 1215, 1194, NULL, 36, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1259, 406, 1164, NULL, NULL, NULL, NULL, 1216, 1195, NULL, 36, 0, 7, 0, 7, 7, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1260, 406, 1165, NULL, NULL, NULL, NULL, 1217, 1196, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1261, 406, 1166, NULL, NULL, NULL, NULL, 1218, 1197, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1262, 406, 1167, NULL, NULL, NULL, NULL, 1219, 1198, NULL, 36, 0, 3, 0, 3, 3, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1263, 406, 1168, NULL, NULL, NULL, NULL, 1220, 1199, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1264, 406, 1169, NULL, NULL, NULL, NULL, 1221, 1200, NULL, 36, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1265, 406, 1170, NULL, NULL, NULL, NULL, 1222, 1201, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1266, 406, 1171, NULL, NULL, NULL, NULL, 1223, 1202, NULL, 36, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1267, 406, 1172, NULL, NULL, NULL, NULL, 1224, 1203, NULL, 36, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/BPN/2022/03/013', 'insert', 'pembelian', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `arus_stok` VALUES (1268, 407, 1173, NULL, NULL, NULL, NULL, 1225, 1204, NULL, 36, 0, 7, 0, 7, 7, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `arus_stok` VALUES (1269, 407, 1174, NULL, NULL, NULL, NULL, 1226, 1205, NULL, 36, 0, 10, 0, 10, 10, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `arus_stok` VALUES (1270, 407, 1175, NULL, NULL, NULL, NULL, 1227, 1206, NULL, 36, 0, 10, 0, 10, 10, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `arus_stok` VALUES (1271, 407, 1176, NULL, NULL, NULL, NULL, 1228, 1207, NULL, 36, 0, 9, 0, 9, 9, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `arus_stok` VALUES (1272, 408, 1177, NULL, NULL, NULL, NULL, 1209, 1208, NULL, 36, 6, 2, 0, 2, 8, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `arus_stok` VALUES (1273, 408, 1178, NULL, NULL, NULL, NULL, 1210, 1209, NULL, 36, 10, 3, 0, 3, 13, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `arus_stok` VALUES (1274, 408, 1179, NULL, NULL, NULL, NULL, 1225, 1210, NULL, 36, 7, 6, 0, 6, 13, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `arus_stok` VALUES (1275, 408, 1180, NULL, NULL, NULL, NULL, 1213, 1211, NULL, 36, 3, 2, 0, 2, 5, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `arus_stok` VALUES (1276, 409, 1181, NULL, NULL, NULL, NULL, 1229, 1212, NULL, 36, 0, 65, 0, 65, 65, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `arus_stok` VALUES (1277, 409, 1182, NULL, NULL, NULL, NULL, 1229, 1213, NULL, 36, 65, 5, 0, 5, 70, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `arus_stok` VALUES (1278, 409, 1183, NULL, NULL, NULL, NULL, 1229, 1214, NULL, 36, 70, 10, 0, 10, 80, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `arus_stok` VALUES (1279, 410, 1184, NULL, NULL, NULL, NULL, 1229, 1215, NULL, 36, 80, 500, 0, 500, 580, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:26:31', '2022-04-02 09:26:31', NULL);
INSERT INTO `arus_stok` VALUES (1280, 411, 1185, NULL, NULL, NULL, NULL, 1229, 1216, NULL, 36, 580, 642, 0, 642, 1222, 'Pembelian Barang No Faktur : PBL/BE/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:28:42', '2022-04-02 09:28:42', NULL);
INSERT INTO `arus_stok` VALUES (1281, 412, 1186, NULL, NULL, NULL, NULL, 1230, 1217, NULL, 36, 0, 4, 0, 4, 4, 'Pembelian Barang No Faktur : PBL/MSC/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:34:07', '2022-04-02 09:34:07', NULL);
INSERT INTO `arus_stok` VALUES (1282, 413, 1187, NULL, NULL, NULL, NULL, 1231, 1218, NULL, 36, 0, 7, 0, 7, 7, 'Pembelian Barang No Faktur : PBL/FXB/2022/04/001', 'insert', 'pembelian', '2022-04-02 09:39:37', '2022-04-02 09:39:37', NULL);
INSERT INTO `arus_stok` VALUES (1283, 424, 1189, NULL, NULL, NULL, NULL, 1214, 1219, NULL, 36, 10, 3, 0, 3, 13, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1284, 424, 1190, NULL, NULL, NULL, NULL, 1215, 1220, NULL, 36, 2, 3, 0, 3, 5, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1285, 424, 1191, NULL, NULL, NULL, NULL, 1216, 1221, NULL, 36, 7, 3, 0, 3, 10, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1286, 424, 1192, NULL, NULL, NULL, NULL, 1217, 1222, NULL, 36, 5, 3, 0, 3, 8, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1287, 424, 1193, NULL, NULL, NULL, NULL, 1218, 1223, NULL, 36, 5, 3, 0, 3, 8, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1288, 424, 1194, NULL, NULL, NULL, NULL, 1235, 1224, NULL, 36, 0, 5, 0, 5, 5, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1289, 424, 1195, NULL, NULL, NULL, NULL, 1236, 1225, NULL, 36, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1290, 424, 1196, NULL, NULL, NULL, NULL, 1219, 1226, NULL, 36, 3, 3, 0, 3, 6, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1291, 424, 1197, NULL, NULL, NULL, NULL, 1220, 1227, NULL, 36, 1, 1, 0, 1, 2, 'Pembelian Barang No Faktur : PBL/BPN/2022/04/001', 'insert', 'pembelian', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `arus_stok` VALUES (1292, 425, 1198, NULL, NULL, NULL, NULL, 1238, 1228, NULL, 35, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/JF/2022/04/002', 'insert', 'pembelian', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `arus_stok` VALUES (1293, 425, 1199, NULL, NULL, NULL, NULL, 1239, 1229, NULL, 35, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/JF/2022/04/002', 'insert', 'pembelian', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `arus_stok` VALUES (1294, 425, 1200, NULL, NULL, NULL, NULL, 1241, 1230, NULL, 35, 0, 18, 0, 18, 18, 'Pembelian Barang No Faktur : PBL/JF/2022/04/002', 'insert', 'pembelian', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `arus_stok` VALUES (1295, 425, 1201, NULL, NULL, NULL, NULL, 1242, 1231, NULL, 35, 0, 2, 0, 2, 2, 'Pembelian Barang No Faktur : PBL/JF/2022/04/002', 'insert', 'pembelian', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `arus_stok` VALUES (1296, 425, 1202, NULL, NULL, NULL, NULL, 1240, 1232, NULL, 35, 0, 1, 0, 1, 1, 'Pembelian Barang No Faktur : PBL/JF/2022/04/002', 'insert', 'pembelian', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `arus_stok` VALUES (1297, NULL, NULL, 39, 51, NULL, NULL, 1242, NULL, NULL, 35, 2, NULL, 1, NULL, 1, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/001', 'update', 'penjualan', '2022-04-13 15:28:05', '2022-04-13 15:28:05', NULL);
INSERT INTO `arus_stok` VALUES (1298, NULL, NULL, 40, 52, NULL, NULL, 1241, NULL, NULL, 35, 18, NULL, 3, NULL, 15, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/001', 'update', 'penjualan', '2022-04-13 15:42:23', '2022-04-13 15:42:23', NULL);
INSERT INTO `arus_stok` VALUES (1299, NULL, NULL, 41, 53, NULL, NULL, 1241, NULL, NULL, 35, 15, NULL, 5, NULL, 10, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/001', 'update', 'penjualan', '2022-04-13 15:46:31', '2022-04-13 15:46:31', NULL);
INSERT INTO `arus_stok` VALUES (1300, NULL, NULL, 42, 54, NULL, NULL, 1241, NULL, NULL, 35, 10, NULL, 5, NULL, 5, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/001', 'update', 'penjualan', '2022-04-13 15:47:18', '2022-04-13 15:47:18', NULL);
INSERT INTO `arus_stok` VALUES (1301, NULL, NULL, 43, 55, NULL, NULL, 1241, NULL, NULL, 35, 5, NULL, 5, NULL, 0, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/001', 'update', 'penjualan', '2022-04-13 15:47:57', '2022-04-13 15:47:57', NULL);
INSERT INTO `arus_stok` VALUES (1302, NULL, NULL, 44, 56, NULL, NULL, 1239, NULL, NULL, 35, 2, NULL, 2, NULL, 0, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/001', 'update', 'penjualan', '2022-04-13 15:49:32', '2022-04-13 15:49:32', NULL);
INSERT INTO `arus_stok` VALUES (1303, NULL, NULL, NULL, NULL, NULL, NULL, 1150, 0, 37, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:51', 'insert', 'penyesuaian_stok', '2022-04-14 13:51:36', '2022-04-14 13:51:36', NULL);
INSERT INTO `arus_stok` VALUES (1304, NULL, NULL, NULL, NULL, NULL, NULL, 1167, 0, 38, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:51', 'insert', 'penyesuaian_stok', '2022-04-14 13:51:59', '2022-04-14 13:51:59', NULL);
INSERT INTO `arus_stok` VALUES (1305, NULL, NULL, NULL, NULL, NULL, NULL, 1171, 0, 39, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:52', 'insert', 'penyesuaian_stok', '2022-04-14 13:52:19', '2022-04-14 13:52:19', NULL);
INSERT INTO `arus_stok` VALUES (1306, NULL, NULL, NULL, NULL, NULL, NULL, 1170, 0, 40, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:52', 'insert', 'penyesuaian_stok', '2022-04-14 13:52:36', '2022-04-14 13:52:36', NULL);
INSERT INTO `arus_stok` VALUES (1307, NULL, NULL, NULL, NULL, NULL, NULL, 1169, 0, 41, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:52', 'insert', 'penyesuaian_stok', '2022-04-14 13:52:55', '2022-04-14 13:52:55', NULL);
INSERT INTO `arus_stok` VALUES (1308, NULL, NULL, NULL, NULL, NULL, NULL, 1168, 0, 42, 36, 0, 12, 0, 12, 12, 'Penyesuaian Stok tanggal 14-04-2022 13:53', 'insert', 'penyesuaian_stok', '2022-04-14 13:53:11', '2022-04-14 13:53:11', NULL);
INSERT INTO `arus_stok` VALUES (1309, NULL, NULL, NULL, NULL, NULL, NULL, 1166, 0, 43, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:53', 'insert', 'penyesuaian_stok', '2022-04-14 13:53:44', '2022-04-14 13:53:44', NULL);
INSERT INTO `arus_stok` VALUES (1310, NULL, NULL, NULL, NULL, NULL, NULL, 1165, 0, 44, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:54', 'insert', 'penyesuaian_stok', '2022-04-14 13:54:00', '2022-04-14 13:54:00', NULL);
INSERT INTO `arus_stok` VALUES (1311, NULL, NULL, NULL, NULL, NULL, NULL, 1164, 0, 45, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:54', 'insert', 'penyesuaian_stok', '2022-04-14 13:54:15', '2022-04-14 13:54:15', NULL);
INSERT INTO `arus_stok` VALUES (1312, NULL, NULL, NULL, NULL, NULL, NULL, 1163, 0, 46, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:54', 'insert', 'penyesuaian_stok', '2022-04-14 13:54:29', '2022-04-14 13:54:29', NULL);
INSERT INTO `arus_stok` VALUES (1313, NULL, NULL, NULL, NULL, NULL, NULL, 1162, 0, 47, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:54', 'insert', 'penyesuaian_stok', '2022-04-14 13:54:41', '2022-04-14 13:54:41', NULL);
INSERT INTO `arus_stok` VALUES (1314, NULL, NULL, NULL, NULL, NULL, NULL, 1161, 0, 48, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:54', 'insert', 'penyesuaian_stok', '2022-04-14 13:54:58', '2022-04-14 13:54:58', NULL);
INSERT INTO `arus_stok` VALUES (1315, NULL, NULL, NULL, NULL, NULL, NULL, 1159, 0, 49, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:55', 'insert', 'penyesuaian_stok', '2022-04-14 13:55:13', '2022-04-14 13:55:13', NULL);
INSERT INTO `arus_stok` VALUES (1316, NULL, NULL, NULL, NULL, NULL, NULL, 1149, 0, 50, 36, 0, 600, 0, 600, 600, 'Penyesuaian Stok tanggal 14-04-2022 13:55', 'insert', 'penyesuaian_stok', '2022-04-14 13:55:31', '2022-04-14 13:55:31', NULL);
INSERT INTO `arus_stok` VALUES (1317, NULL, NULL, NULL, NULL, NULL, NULL, 1153, 0, 51, 36, 0, 100, 0, 100, 100, 'Penyesuaian Stok tanggal 14-04-2022 13:56', 'insert', 'penyesuaian_stok', '2022-04-14 13:56:08', '2022-04-14 13:56:08', NULL);
INSERT INTO `arus_stok` VALUES (1318, NULL, NULL, NULL, NULL, NULL, NULL, 1154, 0, 52, 36, 0, 200, 0, 200, 200, 'Penyesuaian Stok tanggal 14-04-2022 13:56', 'insert', 'penyesuaian_stok', '2022-04-14 13:56:24', '2022-04-14 13:56:24', NULL);
INSERT INTO `arus_stok` VALUES (1319, NULL, NULL, NULL, NULL, NULL, NULL, 1156, 0, 53, 36, 0, 200, 0, 200, 200, 'Penyesuaian Stok tanggal 14-04-2022 13:56', 'insert', 'penyesuaian_stok', '2022-04-14 13:56:44', '2022-04-14 13:56:44', NULL);
INSERT INTO `arus_stok` VALUES (1320, NULL, NULL, NULL, NULL, NULL, NULL, 1145, 0, 54, 36, 0, 600, 0, 600, 600, 'Penyesuaian Stok tanggal 14-04-2022 14:59', 'insert', 'penyesuaian_stok', '2022-04-14 14:59:26', '2022-04-14 14:59:26', NULL);
INSERT INTO `arus_stok` VALUES (1321, NULL, NULL, NULL, NULL, NULL, NULL, 1144, 0, 55, 36, 0, 600, 0, 600, 600, 'Penyesuaian Stok tanggal 14-04-2022 14:59', 'insert', 'penyesuaian_stok', '2022-04-14 14:59:50', '2022-04-14 14:59:50', NULL);
INSERT INTO `arus_stok` VALUES (1322, NULL, NULL, NULL, NULL, NULL, NULL, 1143, 0, 56, 36, 0, 600, 0, 600, 600, 'Penyesuaian Stok tanggal 14-04-2022 15:00', 'insert', 'penyesuaian_stok', '2022-04-14 15:00:03', '2022-04-14 15:00:03', NULL);
INSERT INTO `arus_stok` VALUES (1323, NULL, NULL, NULL, NULL, NULL, NULL, 1142, 0, 57, 36, 0, 600, 0, 600, 600, 'Penyesuaian Stok tanggal 14-04-2022 15:00', 'insert', 'penyesuaian_stok', '2022-04-14 15:00:16', '2022-04-14 15:00:16', NULL);
INSERT INTO `arus_stok` VALUES (1324, NULL, NULL, 45, 57, NULL, NULL, 1149, NULL, NULL, 36, 600, NULL, 5, NULL, 595, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1325, NULL, NULL, 45, 58, NULL, NULL, 1150, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1326, NULL, NULL, 45, 59, NULL, NULL, 1153, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1327, NULL, NULL, 45, 60, NULL, NULL, 1154, NULL, NULL, 36, 200, NULL, 5, NULL, 195, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1328, NULL, NULL, 45, 61, NULL, NULL, 1156, NULL, NULL, 36, 200, NULL, 5, NULL, 195, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1329, NULL, NULL, 45, 62, NULL, NULL, 1159, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1330, NULL, NULL, 45, 63, NULL, NULL, 1161, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1331, NULL, NULL, 45, 64, NULL, NULL, 1164, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1332, NULL, NULL, 45, 65, NULL, NULL, 1163, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1333, NULL, NULL, 45, 66, NULL, NULL, 1162, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1334, NULL, NULL, 45, 67, NULL, NULL, 1165, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1335, NULL, NULL, 45, 68, NULL, NULL, 1166, NULL, NULL, 36, 100, NULL, 5, NULL, 95, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/001', 'update', 'penjualan', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `arus_stok` VALUES (1336, NULL, NULL, 46, 69, NULL, NULL, 1149, NULL, NULL, 36, 595, NULL, 5, NULL, 590, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/002', 'update', 'penjualan', '2022-04-14 15:14:18', '2022-04-14 15:14:18', NULL);
INSERT INTO `arus_stok` VALUES (1337, NULL, NULL, 46, 70, NULL, NULL, 1150, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/002', 'update', 'penjualan', '2022-04-14 15:14:18', '2022-04-14 15:14:18', NULL);
INSERT INTO `arus_stok` VALUES (1338, NULL, NULL, 47, 71, NULL, NULL, 1149, NULL, NULL, 36, 590, NULL, 5, NULL, 585, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/003', 'update', 'penjualan', '2022-04-14 15:18:51', '2022-04-14 15:18:51', NULL);
INSERT INTO `arus_stok` VALUES (1339, NULL, NULL, 47, 72, NULL, NULL, 1150, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/003', 'update', 'penjualan', '2022-04-14 15:18:51', '2022-04-14 15:18:51', NULL);
INSERT INTO `arus_stok` VALUES (1340, NULL, NULL, 48, 73, NULL, NULL, 1149, NULL, NULL, 36, 585, NULL, 3, NULL, 582, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/004', 'update', 'penjualan', '2022-04-14 15:21:23', '2022-04-14 15:21:23', NULL);
INSERT INTO `arus_stok` VALUES (1341, NULL, NULL, 48, 74, NULL, NULL, 1150, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/004', 'update', 'penjualan', '2022-04-14 15:21:23', '2022-04-14 15:21:23', NULL);
INSERT INTO `arus_stok` VALUES (1342, NULL, NULL, 49, 75, NULL, NULL, 1149, NULL, NULL, 36, 582, NULL, 2, NULL, 580, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/005', 'update', 'penjualan', '2022-04-14 15:23:41', '2022-04-14 15:23:41', NULL);
INSERT INTO `arus_stok` VALUES (1343, NULL, NULL, 49, 76, NULL, NULL, 1150, NULL, NULL, 36, 82, NULL, 1, NULL, 81, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/005', 'update', 'penjualan', '2022-04-14 15:23:41', '2022-04-14 15:23:41', NULL);
INSERT INTO `arus_stok` VALUES (1344, NULL, NULL, 50, 77, NULL, NULL, 1149, NULL, NULL, 36, 580, NULL, 2, NULL, 578, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/006', 'update', 'penjualan', '2022-04-14 15:27:15', '2022-04-14 15:27:15', NULL);
INSERT INTO `arus_stok` VALUES (1345, NULL, NULL, 50, 78, NULL, NULL, 1150, NULL, NULL, 36, 81, NULL, 2, NULL, 79, 'Penjualan Barang No Faktur : PJL/PPN/2022/04/006', 'update', 'penjualan', '2022-04-14 15:27:15', '2022-04-14 15:27:15', NULL);
INSERT INTO `arus_stok` VALUES (1346, 426, 1203, NULL, NULL, NULL, NULL, 1243, 1254, NULL, 36, 0, 318, 0, 318, 318, 'Pembelian Barang No Faktur : PBL/YW/2022/04/002', 'insert', 'pembelian', '2022-04-14 15:40:38', '2022-04-14 15:40:38', NULL);
INSERT INTO `arus_stok` VALUES (1347, NULL, NULL, 51, 79, NULL, NULL, 1243, NULL, NULL, 36, 318, NULL, 302, NULL, 16, 'Penjualan Barang No Faktur : PJL/PG/2022/04/007', 'update', 'penjualan', '2022-04-14 15:41:55', '2022-04-14 15:41:55', NULL);
INSERT INTO `arus_stok` VALUES (1348, 427, 1204, NULL, NULL, NULL, NULL, 1255, 1255, NULL, 36, 0, 45, 0, 45, 45, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `arus_stok` VALUES (1349, 427, 1205, NULL, NULL, NULL, NULL, 1256, 1256, NULL, 36, 0, 37, 0, 37, 37, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `arus_stok` VALUES (1350, 427, 1206, NULL, NULL, NULL, NULL, 1257, 1257, NULL, 36, 0, 61, 0, 61, 61, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `arus_stok` VALUES (1351, 427, 1207, NULL, NULL, NULL, NULL, 1258, 1258, NULL, 36, 0, 63, 0, 63, 63, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `arus_stok` VALUES (1352, 427, 1208, NULL, NULL, NULL, NULL, 1259, 1259, NULL, 36, 0, 28, 0, 28, 28, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `arus_stok` VALUES (1353, 427, 1209, NULL, NULL, NULL, NULL, 1260, 1260, NULL, 36, 0, 156, 0, 156, 156, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `arus_stok` VALUES (1354, 428, 1210, NULL, NULL, NULL, NULL, 1259, 1261, NULL, 36, 28, 42, 0, 42, 70, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 13:00:15', '2022-04-19 13:00:15', NULL);
INSERT INTO `arus_stok` VALUES (1355, 429, 1211, NULL, NULL, NULL, NULL, 1259, 1262, NULL, 36, 70, 72, 0, 72, 142, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 13:01:52', '2022-04-19 13:01:52', NULL);
INSERT INTO `arus_stok` VALUES (1356, NULL, NULL, NULL, NULL, NULL, NULL, 1261, 0, 58, 36, 0, 50, 0, 50, 50, 'Penyesuaian Stok tanggal 19-04-2022 14:18', 'insert', 'penyesuaian_stok', '2022-04-19 14:18:52', '2022-04-19 14:18:52', NULL);
INSERT INTO `arus_stok` VALUES (1357, NULL, NULL, NULL, NULL, NULL, NULL, 1262, 0, 59, 36, 0, 5, 0, 5, 5, 'Penyesuaian Stok tanggal 19-04-2022 14:19', 'insert', 'penyesuaian_stok', '2022-04-19 14:19:13', '2022-04-19 14:19:13', NULL);
INSERT INTO `arus_stok` VALUES (1358, NULL, NULL, NULL, NULL, NULL, NULL, 1263, 0, 60, 36, 0, 2, 0, 2, 2, 'Penyesuaian Stok tanggal 19-04-2022 14:19', 'insert', 'penyesuaian_stok', '2022-04-19 14:19:28', '2022-04-19 14:19:28', NULL);
INSERT INTO `arus_stok` VALUES (1359, NULL, NULL, NULL, NULL, NULL, NULL, 1264, 0, 61, 36, 0, 2, 0, 2, 2, 'Penyesuaian Stok tanggal 19-04-2022 14:19', 'insert', 'penyesuaian_stok', '2022-04-19 14:19:44', '2022-04-19 14:19:44', NULL);
INSERT INTO `arus_stok` VALUES (1360, NULL, NULL, NULL, NULL, NULL, NULL, 1267, 0, 62, 36, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 19-04-2022 14:20', 'insert', 'penyesuaian_stok', '2022-04-19 14:20:02', '2022-04-19 14:20:02', NULL);
INSERT INTO `arus_stok` VALUES (1361, NULL, NULL, NULL, NULL, NULL, NULL, 1266, 0, 63, 36, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 19-04-2022 14:20', 'insert', 'penyesuaian_stok', '2022-04-19 14:20:12', '2022-04-19 14:20:12', NULL);
INSERT INTO `arus_stok` VALUES (1362, NULL, NULL, NULL, NULL, NULL, NULL, 1265, 0, 64, 36, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 19-04-2022 14:20', 'insert', 'penyesuaian_stok', '2022-04-19 14:20:24', '2022-04-19 14:20:24', NULL);
INSERT INTO `arus_stok` VALUES (1363, NULL, NULL, 52, 80, NULL, NULL, 1198, NULL, NULL, 36, 25, NULL, 2, NULL, 23, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1364, NULL, NULL, 52, 81, NULL, NULL, 1197, NULL, NULL, 36, 25, NULL, 2, NULL, 23, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1365, NULL, NULL, 52, 82, NULL, NULL, 1261, NULL, NULL, 36, 50, NULL, 50, NULL, 0, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1366, NULL, NULL, 52, 83, NULL, NULL, 1262, NULL, NULL, 36, 5, NULL, 5, NULL, 0, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1367, NULL, NULL, 52, 84, NULL, NULL, 1263, NULL, NULL, 36, 2, NULL, 2, NULL, 0, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1368, NULL, NULL, 52, 85, NULL, NULL, 1264, NULL, NULL, 36, 2, NULL, 2, NULL, 0, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1369, NULL, NULL, 52, 86, NULL, NULL, 1265, NULL, NULL, 36, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1370, NULL, NULL, 52, 87, NULL, NULL, 1266, NULL, NULL, 36, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1371, NULL, NULL, 52, 88, NULL, NULL, 1267, NULL, NULL, 36, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1372, NULL, NULL, 52, 89, NULL, NULL, 1202, NULL, NULL, 36, 57, NULL, 10, NULL, 47, 'Penjualan Barang No Faktur : PJL/PYG/2022/04/008', 'update', 'penjualan', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `arus_stok` VALUES (1373, NULL, NULL, 53, 90, NULL, NULL, 1149, NULL, NULL, 36, 578, NULL, 2, NULL, 576, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1374, NULL, NULL, 53, 91, NULL, NULL, 1150, NULL, NULL, 36, 79, NULL, 2, NULL, 77, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1375, NULL, NULL, 53, 92, NULL, NULL, 1153, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1376, NULL, NULL, 53, 93, NULL, NULL, 1154, NULL, NULL, 36, 195, NULL, 5, NULL, 190, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1377, NULL, NULL, 53, 94, NULL, NULL, 1156, NULL, NULL, 36, 195, NULL, 5, NULL, 190, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1378, NULL, NULL, 53, 95, NULL, NULL, 1159, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1379, NULL, NULL, 53, 96, NULL, NULL, 1161, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1380, NULL, NULL, 53, 97, NULL, NULL, 1164, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1381, NULL, NULL, 53, 98, NULL, NULL, 1163, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1382, NULL, NULL, 53, 99, NULL, NULL, 1162, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1383, NULL, NULL, 53, 100, NULL, NULL, 1165, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1384, NULL, NULL, 53, 101, NULL, NULL, 1166, NULL, NULL, 36, 95, NULL, 5, NULL, 90, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/008', 'update', 'penjualan', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `arus_stok` VALUES (1385, 430, 1212, NULL, NULL, NULL, NULL, 1151, 1270, NULL, 36, 0, 30, 0, 30, 30, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 14:48:37', '2022-04-19 14:48:37', NULL);
INSERT INTO `arus_stok` VALUES (1386, NULL, NULL, 54, 102, NULL, NULL, 1149, NULL, NULL, 36, 576, NULL, 5, NULL, 571, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/009', 'update', 'penjualan', '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `arus_stok` VALUES (1387, NULL, NULL, 54, 103, NULL, NULL, 1150, NULL, NULL, 36, 77, NULL, 5, NULL, 72, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/009', 'update', 'penjualan', '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `arus_stok` VALUES (1388, NULL, NULL, 54, 104, NULL, NULL, 1151, NULL, NULL, 36, 30, NULL, 4, NULL, 26, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/009', 'update', 'penjualan', '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `arus_stok` VALUES (1389, NULL, NULL, 55, 105, NULL, NULL, 1149, NULL, NULL, 36, 571, NULL, 5, NULL, 566, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1390, NULL, NULL, 55, 106, NULL, NULL, 1150, NULL, NULL, 36, 72, NULL, 5, NULL, 67, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1391, NULL, NULL, 55, 107, NULL, NULL, 1153, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1392, NULL, NULL, 55, 108, NULL, NULL, 1154, NULL, NULL, 36, 190, NULL, 5, NULL, 185, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1393, NULL, NULL, 55, 109, NULL, NULL, 1156, NULL, NULL, 36, 190, NULL, 5, NULL, 185, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1394, NULL, NULL, 55, 110, NULL, NULL, 1159, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1395, NULL, NULL, 55, 111, NULL, NULL, 1161, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1396, NULL, NULL, 55, 112, NULL, NULL, 1164, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1397, NULL, NULL, 55, 113, NULL, NULL, 1163, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1398, NULL, NULL, 55, 114, NULL, NULL, 1162, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1399, NULL, NULL, 55, 115, NULL, NULL, 1165, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1400, NULL, NULL, 55, 116, NULL, NULL, 1166, NULL, NULL, 36, 90, NULL, 5, NULL, 85, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/010', 'update', 'penjualan', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `arus_stok` VALUES (1401, NULL, NULL, 56, 117, NULL, NULL, 1149, NULL, NULL, 36, 566, NULL, 3, NULL, 563, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1402, NULL, NULL, 56, 118, NULL, NULL, 1150, NULL, NULL, 36, 67, NULL, 3, NULL, 64, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1403, NULL, NULL, 56, 119, NULL, NULL, 1153, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1404, NULL, NULL, 56, 120, NULL, NULL, 1154, NULL, NULL, 36, 185, NULL, 3, NULL, 182, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1405, NULL, NULL, 56, 121, NULL, NULL, 1156, NULL, NULL, 36, 185, NULL, 3, NULL, 182, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1406, NULL, NULL, 56, 122, NULL, NULL, 1159, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1407, NULL, NULL, 56, 123, NULL, NULL, 1161, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1408, NULL, NULL, 56, 124, NULL, NULL, 1164, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1409, NULL, NULL, 56, 125, NULL, NULL, 1163, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1410, NULL, NULL, 56, 126, NULL, NULL, 1162, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1411, NULL, NULL, 56, 127, NULL, NULL, 1165, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1412, NULL, NULL, 56, 128, NULL, NULL, 1166, NULL, NULL, 36, 85, NULL, 3, NULL, 82, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/011', 'update', 'penjualan', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `arus_stok` VALUES (1413, NULL, NULL, 57, 129, NULL, NULL, 1149, NULL, NULL, 36, 563, NULL, 1, NULL, 562, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/012', 'update', 'penjualan', '2022-04-19 15:23:21', '2022-04-19 15:23:21', NULL);
INSERT INTO `arus_stok` VALUES (1414, NULL, NULL, 57, 130, NULL, NULL, 1150, NULL, NULL, 36, 64, NULL, 1, NULL, 63, 'Penjualan Barang No Faktur : PJL/SELTIM/2022/04/012', 'update', 'penjualan', '2022-04-19 15:23:21', '2022-04-19 15:23:21', NULL);
INSERT INTO `arus_stok` VALUES (1415, NULL, NULL, 58, 131, NULL, NULL, 1149, NULL, NULL, 36, 562, NULL, 2, NULL, 560, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1416, NULL, NULL, 58, 132, NULL, NULL, 1150, NULL, NULL, 36, 63, NULL, 2, NULL, 61, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1417, NULL, NULL, 58, 133, NULL, NULL, 1153, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1418, NULL, NULL, 58, 134, NULL, NULL, 1154, NULL, NULL, 36, 182, NULL, 2, NULL, 180, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1419, NULL, NULL, 58, 135, NULL, NULL, 1156, NULL, NULL, 36, 182, NULL, 2, NULL, 180, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1420, NULL, NULL, 58, 136, NULL, NULL, 1159, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1421, NULL, NULL, 58, 137, NULL, NULL, 1161, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1422, NULL, NULL, 58, 138, NULL, NULL, 1164, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1423, NULL, NULL, 58, 139, NULL, NULL, 1163, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1424, NULL, NULL, 58, 140, NULL, NULL, 1162, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1425, NULL, NULL, 58, 141, NULL, NULL, 1165, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1426, NULL, NULL, 58, 142, NULL, NULL, 1166, NULL, NULL, 36, 82, NULL, 2, NULL, 80, 'Penjualan Barang No Faktur : PJL/SLMG/2022/04/013', 'update', 'penjualan', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `arus_stok` VALUES (1427, NULL, NULL, 59, 143, NULL, NULL, 1149, NULL, NULL, 36, 560, NULL, 1, NULL, 559, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/014', 'update', 'penjualan', '2022-04-19 15:38:44', '2022-04-19 15:38:44', NULL);
INSERT INTO `arus_stok` VALUES (1428, NULL, NULL, 59, 144, NULL, NULL, 1150, NULL, NULL, 36, 61, NULL, 1, NULL, 60, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/014', 'update', 'penjualan', '2022-04-19 15:38:44', '2022-04-19 15:38:44', NULL);
INSERT INTO `arus_stok` VALUES (1429, NULL, NULL, 60, 145, NULL, NULL, 1149, NULL, NULL, 36, 559, NULL, 3, NULL, 556, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1430, NULL, NULL, 60, 146, NULL, NULL, 1150, NULL, NULL, 36, 60, NULL, 3, NULL, 57, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1431, NULL, NULL, 60, 147, NULL, NULL, 1153, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1432, NULL, NULL, 60, 148, NULL, NULL, 1154, NULL, NULL, 36, 180, NULL, 3, NULL, 177, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1433, NULL, NULL, 60, 149, NULL, NULL, 1156, NULL, NULL, 36, 180, NULL, 3, NULL, 177, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1434, NULL, NULL, 60, 150, NULL, NULL, 1159, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1435, NULL, NULL, 60, 151, NULL, NULL, 1161, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1436, NULL, NULL, 60, 152, NULL, NULL, 1164, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1437, NULL, NULL, 60, 153, NULL, NULL, 1163, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1438, NULL, NULL, 60, 154, NULL, NULL, 1162, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1439, NULL, NULL, 60, 155, NULL, NULL, 1165, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1440, NULL, NULL, 60, 156, NULL, NULL, 1166, NULL, NULL, 36, 80, NULL, 3, NULL, 77, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/015', 'update', 'penjualan', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `arus_stok` VALUES (1441, NULL, NULL, 61, 157, NULL, NULL, 1149, NULL, NULL, 36, 556, NULL, 3, NULL, 553, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1442, NULL, NULL, 61, 158, NULL, NULL, 1150, NULL, NULL, 36, 57, NULL, 3, NULL, 54, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1443, NULL, NULL, 61, 159, NULL, NULL, 1153, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1444, NULL, NULL, 61, 160, NULL, NULL, 1154, NULL, NULL, 36, 177, NULL, 3, NULL, 174, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1445, NULL, NULL, 61, 161, NULL, NULL, 1156, NULL, NULL, 36, 177, NULL, 3, NULL, 174, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1446, NULL, NULL, 61, 162, NULL, NULL, 1159, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1447, NULL, NULL, 61, 163, NULL, NULL, 1161, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1448, NULL, NULL, 61, 164, NULL, NULL, 1164, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1449, NULL, NULL, 61, 165, NULL, NULL, 1163, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1450, NULL, NULL, 61, 166, NULL, NULL, 1162, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1451, NULL, NULL, 61, 167, NULL, NULL, 1165, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1452, NULL, NULL, 61, 168, NULL, NULL, 1166, NULL, NULL, 36, 77, NULL, 3, NULL, 74, 'Penjualan Barang No Faktur : PJL/MRG/2022/04/016', 'update', 'penjualan', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `arus_stok` VALUES (1453, NULL, NULL, 62, 169, NULL, NULL, 1149, NULL, NULL, 36, 553, NULL, 3, NULL, 550, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/017', 'update', 'penjualan', '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `arus_stok` VALUES (1454, NULL, NULL, 62, 170, NULL, NULL, 1150, NULL, NULL, 36, 54, NULL, 3, NULL, 51, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/017', 'update', 'penjualan', '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `arus_stok` VALUES (1455, NULL, NULL, 62, 171, NULL, NULL, 1151, NULL, NULL, 36, 26, NULL, 3, NULL, 23, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/017', 'update', 'penjualan', '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `arus_stok` VALUES (1456, NULL, NULL, 63, 172, NULL, NULL, 1149, NULL, NULL, 36, 550, NULL, 5, NULL, 545, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/018', 'update', 'penjualan', '2022-04-19 15:50:07', '2022-04-19 15:50:07', NULL);
INSERT INTO `arus_stok` VALUES (1457, NULL, NULL, 63, 173, NULL, NULL, 1150, NULL, NULL, 36, 51, NULL, 5, NULL, 46, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/018', 'update', 'penjualan', '2022-04-19 15:50:07', '2022-04-19 15:50:07', NULL);
INSERT INTO `arus_stok` VALUES (1458, NULL, NULL, 64, 174, NULL, NULL, 1149, NULL, NULL, 36, 545, NULL, 5, NULL, 540, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/019', 'update', 'penjualan', '2022-04-19 15:55:00', '2022-04-19 15:55:00', NULL);
INSERT INTO `arus_stok` VALUES (1459, NULL, NULL, 64, 175, NULL, NULL, 1150, NULL, NULL, 36, 46, NULL, 5, NULL, 41, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/019', 'update', 'penjualan', '2022-04-19 15:55:00', '2022-04-19 15:55:00', NULL);
INSERT INTO `arus_stok` VALUES (1460, NULL, NULL, 65, 176, NULL, NULL, 1149, NULL, NULL, 36, 540, NULL, 5, NULL, 535, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1461, NULL, NULL, 65, 177, NULL, NULL, 1150, NULL, NULL, 36, 41, NULL, 5, NULL, 36, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1462, NULL, NULL, 65, 178, NULL, NULL, 1153, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1463, NULL, NULL, 65, 179, NULL, NULL, 1154, NULL, NULL, 36, 174, NULL, 5, NULL, 169, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1464, NULL, NULL, 65, 180, NULL, NULL, 1156, NULL, NULL, 36, 174, NULL, 5, NULL, 169, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1465, NULL, NULL, 65, 181, NULL, NULL, 1159, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1466, NULL, NULL, 65, 182, NULL, NULL, 1161, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1467, NULL, NULL, 65, 183, NULL, NULL, 1164, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1468, NULL, NULL, 65, 184, NULL, NULL, 1163, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1469, NULL, NULL, 65, 185, NULL, NULL, 1162, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1470, NULL, NULL, 65, 186, NULL, NULL, 1165, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1471, NULL, NULL, 65, 187, NULL, NULL, 1166, NULL, NULL, 36, 74, NULL, 5, NULL, 69, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/020', 'update', 'penjualan', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `arus_stok` VALUES (1472, 431, 1213, NULL, NULL, NULL, NULL, 1146, 1271, NULL, 36, 0, 500, 0, 500, 500, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 16:20:56', '2022-04-19 16:20:56', NULL);
INSERT INTO `arus_stok` VALUES (1473, 431, 1214, NULL, NULL, NULL, NULL, 1147, 1272, NULL, 36, 0, 500, 0, 500, 500, 'Pembelian Barang No Faktur : PBL/YW/2022/04/003', 'insert', 'pembelian', '2022-04-19 16:20:56', '2022-04-19 16:20:56', NULL);
INSERT INTO `arus_stok` VALUES (1474, NULL, NULL, 66, 188, NULL, NULL, 1142, NULL, NULL, 36, 600, NULL, 2, NULL, 598, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/021', 'update', 'penjualan', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `arus_stok` VALUES (1475, NULL, NULL, 66, 189, NULL, NULL, 1144, NULL, NULL, 36, 600, NULL, 2, NULL, 598, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/021', 'update', 'penjualan', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `arus_stok` VALUES (1476, NULL, NULL, 66, 190, NULL, NULL, 1145, NULL, NULL, 36, 600, NULL, 2, NULL, 598, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/021', 'update', 'penjualan', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `arus_stok` VALUES (1477, NULL, NULL, 66, 191, NULL, NULL, 1143, NULL, NULL, 36, 600, NULL, 2, NULL, 598, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/021', 'update', 'penjualan', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `arus_stok` VALUES (1478, NULL, NULL, 66, 192, NULL, NULL, 1146, NULL, NULL, 36, 500, NULL, 2, NULL, 498, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/021', 'update', 'penjualan', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `arus_stok` VALUES (1479, NULL, NULL, 66, 193, NULL, NULL, 1147, NULL, NULL, 36, 500, NULL, 2, NULL, 498, 'Penjualan Barang No Faktur : PJL/PNB/2022/04/021', 'update', 'penjualan', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `arus_stok` VALUES (1480, NULL, NULL, 67, 194, NULL, NULL, 1142, NULL, NULL, 36, 598, NULL, 2, NULL, 596, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/022', 'update', 'penjualan', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `arus_stok` VALUES (1481, NULL, NULL, 67, 195, NULL, NULL, 1144, NULL, NULL, 36, 598, NULL, 2, NULL, 596, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/022', 'update', 'penjualan', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `arus_stok` VALUES (1482, NULL, NULL, 67, 196, NULL, NULL, 1145, NULL, NULL, 36, 598, NULL, 2, NULL, 596, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/022', 'update', 'penjualan', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `arus_stok` VALUES (1483, NULL, NULL, 67, 197, NULL, NULL, 1143, NULL, NULL, 36, 598, NULL, 2, NULL, 596, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/022', 'update', 'penjualan', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `arus_stok` VALUES (1484, NULL, NULL, 67, 198, NULL, NULL, 1146, NULL, NULL, 36, 498, NULL, 2, NULL, 496, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/022', 'update', 'penjualan', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `arus_stok` VALUES (1485, NULL, NULL, 67, 199, NULL, NULL, 1147, NULL, NULL, 36, 498, NULL, 2, NULL, 496, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/022', 'update', 'penjualan', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `arus_stok` VALUES (1486, NULL, NULL, 68, 200, NULL, NULL, 1142, NULL, NULL, 36, 596, NULL, 2, NULL, 594, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/023', 'update', 'penjualan', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `arus_stok` VALUES (1487, NULL, NULL, 68, 201, NULL, NULL, 1144, NULL, NULL, 36, 596, NULL, 2, NULL, 594, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/023', 'update', 'penjualan', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `arus_stok` VALUES (1488, NULL, NULL, 68, 202, NULL, NULL, 1145, NULL, NULL, 36, 596, NULL, 2, NULL, 594, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/023', 'update', 'penjualan', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `arus_stok` VALUES (1489, NULL, NULL, 68, 203, NULL, NULL, 1143, NULL, NULL, 36, 596, NULL, 2, NULL, 594, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/023', 'update', 'penjualan', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `arus_stok` VALUES (1490, NULL, NULL, 68, 204, NULL, NULL, 1146, NULL, NULL, 36, 496, NULL, 2, NULL, 494, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/023', 'update', 'penjualan', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `arus_stok` VALUES (1491, NULL, NULL, 68, 205, NULL, NULL, 1147, NULL, NULL, 36, 496, NULL, 2, NULL, 494, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/023', 'update', 'penjualan', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `arus_stok` VALUES (1492, NULL, NULL, 69, 206, NULL, NULL, 1142, NULL, NULL, 36, 594, NULL, 2, NULL, 592, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/024', 'update', 'penjualan', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `arus_stok` VALUES (1493, NULL, NULL, 69, 207, NULL, NULL, 1144, NULL, NULL, 36, 594, NULL, 2, NULL, 592, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/024', 'update', 'penjualan', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `arus_stok` VALUES (1494, NULL, NULL, 69, 208, NULL, NULL, 1145, NULL, NULL, 36, 594, NULL, 2, NULL, 592, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/024', 'update', 'penjualan', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `arus_stok` VALUES (1495, NULL, NULL, 69, 209, NULL, NULL, 1143, NULL, NULL, 36, 594, NULL, 2, NULL, 592, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/024', 'update', 'penjualan', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `arus_stok` VALUES (1496, NULL, NULL, 69, 210, NULL, NULL, 1146, NULL, NULL, 36, 494, NULL, 2, NULL, 492, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/024', 'update', 'penjualan', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `arus_stok` VALUES (1497, NULL, NULL, 69, 211, NULL, NULL, 1147, NULL, NULL, 36, 494, NULL, 2, NULL, 492, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/024', 'update', 'penjualan', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `arus_stok` VALUES (1498, NULL, NULL, 70, 212, NULL, NULL, 1149, NULL, NULL, 36, 535, NULL, 5, NULL, 530, 'Penjualan Barang No Faktur : PJL/NGR/2022/04/025', 'update', 'penjualan', '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `arus_stok` VALUES (1499, NULL, NULL, 70, 213, NULL, NULL, 1150, NULL, NULL, 36, 36, NULL, 5, NULL, 31, 'Penjualan Barang No Faktur : PJL/NGR/2022/04/025', 'update', 'penjualan', '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `arus_stok` VALUES (1500, NULL, NULL, 70, 214, NULL, NULL, 1151, NULL, NULL, 36, 23, NULL, 5, NULL, 18, 'Penjualan Barang No Faktur : PJL/NGR/2022/04/025', 'update', 'penjualan', '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `arus_stok` VALUES (1501, NULL, NULL, 71, 215, NULL, NULL, 1142, NULL, NULL, 36, 592, NULL, 2, NULL, 590, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/026', 'update', 'penjualan', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `arus_stok` VALUES (1502, NULL, NULL, 71, 216, NULL, NULL, 1144, NULL, NULL, 36, 592, NULL, 2, NULL, 590, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/026', 'update', 'penjualan', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `arus_stok` VALUES (1503, NULL, NULL, 71, 217, NULL, NULL, 1145, NULL, NULL, 36, 592, NULL, 2, NULL, 590, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/026', 'update', 'penjualan', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `arus_stok` VALUES (1504, NULL, NULL, 71, 218, NULL, NULL, 1143, NULL, NULL, 36, 592, NULL, 2, NULL, 590, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/026', 'update', 'penjualan', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `arus_stok` VALUES (1505, NULL, NULL, 71, 219, NULL, NULL, 1146, NULL, NULL, 36, 492, NULL, 2, NULL, 490, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/026', 'update', 'penjualan', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `arus_stok` VALUES (1506, NULL, NULL, 71, 220, NULL, NULL, 1147, NULL, NULL, 36, 492, NULL, 2, NULL, 490, 'Penjualan Barang No Faktur : PJL/PNBL/2022/04/026', 'update', 'penjualan', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `arus_stok` VALUES (1507, NULL, NULL, 72, 221, NULL, NULL, 1162, NULL, NULL, 35, 69, NULL, 1, NULL, 68, 'Penjualan Barang No Faktur : PJL/BTR/2022/04/027', 'update', 'penjualan', '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `arus_stok` VALUES (1508, NULL, NULL, 72, 222, NULL, NULL, 1163, NULL, NULL, 35, 69, NULL, 1, NULL, 68, 'Penjualan Barang No Faktur : PJL/BTR/2022/04/027', 'update', 'penjualan', '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `arus_stok` VALUES (1509, NULL, NULL, 72, 223, NULL, NULL, 1164, NULL, NULL, 35, 69, NULL, 1, NULL, 68, 'Penjualan Barang No Faktur : PJL/BTR/2022/04/027', 'update', 'penjualan', '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `arus_stok` VALUES (1510, NULL, NULL, 72, 224, NULL, NULL, 1165, NULL, NULL, 35, 69, NULL, 1, NULL, 68, 'Penjualan Barang No Faktur : PJL/BTR/2022/04/027', 'update', 'penjualan', '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `arus_stok` VALUES (1511, NULL, NULL, 72, 225, NULL, NULL, 1166, NULL, NULL, 35, 69, NULL, 1, NULL, 68, 'Penjualan Barang No Faktur : PJL/BTR/2022/04/027', 'update', 'penjualan', '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `arus_stok` VALUES (1512, NULL, NULL, 73, 226, NULL, NULL, 1167, NULL, NULL, 35, 100, NULL, 3, NULL, 97, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/028', 'update', 'penjualan', '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `arus_stok` VALUES (1513, NULL, NULL, 73, 227, NULL, NULL, 1168, NULL, NULL, 35, 12, NULL, 3, NULL, 9, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/028', 'update', 'penjualan', '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `arus_stok` VALUES (1514, NULL, NULL, 73, 228, NULL, NULL, 1170, NULL, NULL, 35, 100, NULL, 3, NULL, 97, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/028', 'update', 'penjualan', '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `arus_stok` VALUES (1515, NULL, NULL, 73, 229, NULL, NULL, 1169, NULL, NULL, 35, 100, NULL, 3, NULL, 97, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/028', 'update', 'penjualan', '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `arus_stok` VALUES (1516, NULL, NULL, 73, 230, NULL, NULL, 1171, NULL, NULL, 35, 100, NULL, 3, NULL, 97, 'Penjualan Barang No Faktur : PJL/KDR/2022/04/028', 'update', 'penjualan', '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `arus_stok` VALUES (1517, NULL, NULL, 74, 231, NULL, NULL, 1167, NULL, NULL, 35, 97, NULL, 3, NULL, 94, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/029', 'update', 'penjualan', '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `arus_stok` VALUES (1518, NULL, NULL, 74, 232, NULL, NULL, 1168, NULL, NULL, 35, 9, NULL, 3, NULL, 6, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/029', 'update', 'penjualan', '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `arus_stok` VALUES (1519, NULL, NULL, 74, 233, NULL, NULL, 1170, NULL, NULL, 35, 97, NULL, 3, NULL, 94, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/029', 'update', 'penjualan', '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `arus_stok` VALUES (1520, NULL, NULL, 74, 234, NULL, NULL, 1169, NULL, NULL, 35, 97, NULL, 3, NULL, 94, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/029', 'update', 'penjualan', '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `arus_stok` VALUES (1521, NULL, NULL, 74, 235, NULL, NULL, 1171, NULL, NULL, 35, 97, NULL, 3, NULL, 94, 'Penjualan Barang No Faktur : PJL/TBN/2022/04/029', 'update', 'penjualan', '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `arus_stok` VALUES (1522, NULL, NULL, NULL, NULL, NULL, NULL, 1292, 0, 65, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:46', 'insert', 'penyesuaian_stok', '2022-04-22 08:46:41', '2022-04-22 08:46:41', NULL);
INSERT INTO `arus_stok` VALUES (1523, NULL, NULL, NULL, NULL, NULL, NULL, 1291, 0, 66, 35, 0, 5, 0, 5, 5, 'Penyesuaian Stok tanggal 22-04-2022 08:46', 'insert', 'penyesuaian_stok', '2022-04-22 08:46:52', '2022-04-22 08:46:52', NULL);
INSERT INTO `arus_stok` VALUES (1524, NULL, NULL, NULL, NULL, NULL, NULL, 1290, 0, 67, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:47', 'insert', 'penyesuaian_stok', '2022-04-22 08:47:04', '2022-04-22 08:47:04', NULL);
INSERT INTO `arus_stok` VALUES (1525, NULL, NULL, NULL, NULL, NULL, NULL, 1289, 0, 68, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:47', 'insert', 'penyesuaian_stok', '2022-04-22 08:47:13', '2022-04-22 08:47:13', NULL);
INSERT INTO `arus_stok` VALUES (1526, NULL, NULL, NULL, NULL, NULL, NULL, 1288, 0, 69, 35, 0, 9, 0, 9, 9, 'Penyesuaian Stok tanggal 22-04-2022 08:47', 'insert', 'penyesuaian_stok', '2022-04-22 08:47:22', '2022-04-22 08:47:22', NULL);
INSERT INTO `arus_stok` VALUES (1527, NULL, NULL, NULL, NULL, NULL, NULL, 1287, 0, 70, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:47', 'insert', 'penyesuaian_stok', '2022-04-22 08:47:29', '2022-04-22 08:47:29', NULL);
INSERT INTO `arus_stok` VALUES (1528, NULL, NULL, NULL, NULL, NULL, NULL, 1286, 0, 71, 35, 0, 2, 0, 2, 2, 'Penyesuaian Stok tanggal 22-04-2022 08:47', 'insert', 'penyesuaian_stok', '2022-04-22 08:47:38', '2022-04-22 08:47:38', NULL);
INSERT INTO `arus_stok` VALUES (1529, NULL, NULL, NULL, NULL, NULL, NULL, 1285, 0, 72, 35, 0, 17, 0, 17, 17, 'Penyesuaian Stok tanggal 22-04-2022 08:47', 'insert', 'penyesuaian_stok', '2022-04-22 08:47:45', '2022-04-22 08:47:45', NULL);
INSERT INTO `arus_stok` VALUES (1530, NULL, NULL, NULL, NULL, NULL, NULL, 1284, 0, 73, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:47', 'insert', 'penyesuaian_stok', '2022-04-22 08:47:53', '2022-04-22 08:47:53', NULL);
INSERT INTO `arus_stok` VALUES (1531, NULL, NULL, NULL, NULL, NULL, NULL, 1283, 0, 74, 35, 0, 110, 0, 110, 110, 'Penyesuaian Stok tanggal 22-04-2022 08:49', 'insert', 'penyesuaian_stok', '2022-04-22 08:49:22', '2022-04-22 08:49:22', NULL);
INSERT INTO `arus_stok` VALUES (1532, NULL, NULL, NULL, NULL, NULL, NULL, 1282, 0, 75, 35, 0, 110, 0, 110, 110, 'Penyesuaian Stok tanggal 22-04-2022 08:49', 'insert', 'penyesuaian_stok', '2022-04-22 08:49:37', '2022-04-22 08:49:37', NULL);
INSERT INTO `arus_stok` VALUES (1533, NULL, NULL, NULL, NULL, NULL, NULL, 1281, 0, 76, 35, 0, 110, 0, 110, 110, 'Penyesuaian Stok tanggal 22-04-2022 08:49', 'insert', 'penyesuaian_stok', '2022-04-22 08:49:48', '2022-04-22 08:49:48', NULL);
INSERT INTO `arus_stok` VALUES (1534, NULL, NULL, NULL, NULL, NULL, NULL, 1280, 0, 77, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:50', 'insert', 'penyesuaian_stok', '2022-04-22 08:50:04', '2022-04-22 08:50:04', NULL);
INSERT INTO `arus_stok` VALUES (1535, NULL, NULL, NULL, NULL, NULL, NULL, 1279, 0, 78, 35, 0, 55, 0, 55, 55, 'Penyesuaian Stok tanggal 22-04-2022 08:50', 'insert', 'penyesuaian_stok', '2022-04-22 08:50:28', '2022-04-22 08:50:28', NULL);
INSERT INTO `arus_stok` VALUES (1536, NULL, NULL, NULL, NULL, NULL, NULL, 1278, 0, 79, 35, 0, 55, 0, 55, 55, 'Penyesuaian Stok tanggal 22-04-2022 08:50', 'insert', 'penyesuaian_stok', '2022-04-22 08:50:50', '2022-04-22 08:50:50', NULL);
INSERT INTO `arus_stok` VALUES (1537, NULL, NULL, NULL, NULL, NULL, NULL, 1274, 0, 80, 35, 0, 109, 0, 109, 109, 'Penyesuaian Stok tanggal 22-04-2022 08:51', 'insert', 'penyesuaian_stok', '2022-04-22 08:51:12', '2022-04-22 08:51:12', NULL);
INSERT INTO `arus_stok` VALUES (1538, NULL, NULL, NULL, NULL, NULL, NULL, 1275, 0, 81, 35, 0, 109, 0, 109, 109, 'Penyesuaian Stok tanggal 22-04-2022 08:51', 'insert', 'penyesuaian_stok', '2022-04-22 08:51:29', '2022-04-22 08:51:29', NULL);
INSERT INTO `arus_stok` VALUES (1539, NULL, NULL, NULL, NULL, NULL, NULL, 1276, 0, 82, 35, 0, 110, 0, 110, 110, 'Penyesuaian Stok tanggal 22-04-2022 08:51', 'insert', 'penyesuaian_stok', '2022-04-22 08:51:45', '2022-04-22 08:51:45', NULL);
INSERT INTO `arus_stok` VALUES (1540, NULL, NULL, NULL, NULL, NULL, NULL, 1277, 0, 83, 35, 0, 164, 0, 164, 164, 'Penyesuaian Stok tanggal 22-04-2022 08:52', 'insert', 'penyesuaian_stok', '2022-04-22 08:52:19', '2022-04-22 08:52:19', NULL);
INSERT INTO `arus_stok` VALUES (1541, NULL, NULL, NULL, NULL, NULL, NULL, 1272, 0, 84, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:52', 'insert', 'penyesuaian_stok', '2022-04-22 08:52:40', '2022-04-22 08:52:40', NULL);
INSERT INTO `arus_stok` VALUES (1542, NULL, NULL, NULL, NULL, NULL, NULL, 1272, 0, 85, 35, 0, 1, 0, 1, 2, 'Penyesuaian Stok tanggal 22-04-2022 08:52', 'insert', 'penyesuaian_stok', '2022-04-22 08:52:40', '2022-04-22 08:52:40', NULL);
INSERT INTO `arus_stok` VALUES (1543, NULL, NULL, NULL, NULL, NULL, NULL, 1271, 0, 86, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:52', 'insert', 'penyesuaian_stok', '2022-04-22 08:52:54', '2022-04-22 08:52:54', NULL);
INSERT INTO `arus_stok` VALUES (1544, NULL, NULL, NULL, NULL, NULL, NULL, 1270, 0, 87, 35, 0, 1, 0, 1, 1, 'Penyesuaian Stok tanggal 22-04-2022 08:53', 'insert', 'penyesuaian_stok', '2022-04-22 08:53:10', '2022-04-22 08:53:10', NULL);
INSERT INTO `arus_stok` VALUES (1545, NULL, NULL, NULL, NULL, NULL, NULL, 1269, 0, 88, 35, 0, 2, 0, 2, 2, 'Penyesuaian Stok tanggal 22-04-2022 08:53', 'insert', 'penyesuaian_stok', '2022-04-22 08:53:29', '2022-04-22 08:53:29', NULL);
INSERT INTO `arus_stok` VALUES (1546, 404, 1147, NULL, NULL, NULL, NULL, 1199, 1178, 89, 36, 100, 457, 0, 557, 557, 'Penyesuaian Stok tanggal 22-04-2022 08:56', 'update', 'penyesuaian_stok', '2022-04-22 08:56:44', '2022-04-22 08:56:44', NULL);
INSERT INTO `arus_stok` VALUES (1547, 404, 1147, NULL, NULL, NULL, NULL, 1199, 1178, 90, 36, 557, 100, 0, 657, 657, 'Penyesuaian Stok tanggal 22-04-2022 08:59', 'update', 'penyesuaian_stok', '2022-04-22 08:59:29', '2022-04-22 08:59:29', NULL);
INSERT INTO `arus_stok` VALUES (1548, NULL, NULL, 75, 236, NULL, NULL, 1199, NULL, NULL, 35, 657, NULL, 327, NULL, 330, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1549, NULL, NULL, 75, 237, NULL, NULL, 1274, NULL, NULL, 35, 109, NULL, 109, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1550, NULL, NULL, 75, 238, NULL, NULL, 1277, NULL, NULL, 35, 164, NULL, 109, NULL, 55, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1551, NULL, NULL, 75, 239, NULL, NULL, 1275, NULL, NULL, 35, 109, NULL, 109, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1552, NULL, NULL, 75, 240, NULL, NULL, 1276, NULL, NULL, 35, 110, NULL, 109, NULL, 1, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1553, NULL, NULL, 75, 241, NULL, NULL, 1281, NULL, NULL, 35, 110, NULL, 109, NULL, 1, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1554, NULL, NULL, 75, 242, NULL, NULL, 1282, NULL, NULL, 35, 110, NULL, 109, NULL, 1, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1555, NULL, NULL, 75, 243, NULL, NULL, 1198, NULL, NULL, 35, 23, NULL, 5, NULL, 18, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1556, NULL, NULL, 75, 244, NULL, NULL, 1197, NULL, NULL, 35, 23, NULL, 5, NULL, 18, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1557, NULL, NULL, 75, 245, NULL, NULL, 1283, NULL, NULL, 35, 110, NULL, 109, NULL, 1, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1558, NULL, NULL, 75, 246, NULL, NULL, 1230, NULL, NULL, 35, 4, NULL, 1, NULL, 3, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/030', 'update', 'penjualan', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `arus_stok` VALUES (1559, NULL, NULL, 76, 247, NULL, NULL, 1269, NULL, NULL, 35, 2, NULL, 2, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1560, NULL, NULL, 76, 248, NULL, NULL, 1270, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1561, NULL, NULL, 76, 249, NULL, NULL, 1271, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1562, NULL, NULL, 76, 250, NULL, NULL, 1272, NULL, NULL, 35, 2, NULL, 2, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1563, NULL, NULL, 76, 251, NULL, NULL, 1198, NULL, NULL, 35, 18, NULL, 1, NULL, 17, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1564, NULL, NULL, 76, 252, NULL, NULL, 1197, NULL, NULL, 35, 18, NULL, 1, NULL, 17, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1565, NULL, NULL, 76, 253, NULL, NULL, 1284, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1566, NULL, NULL, 76, 254, NULL, NULL, 1285, NULL, NULL, 35, 17, NULL, 17, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1567, NULL, NULL, 76, 255, NULL, NULL, 1286, NULL, NULL, 35, 2, NULL, 2, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1568, NULL, NULL, 76, 256, NULL, NULL, 1287, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1569, NULL, NULL, 76, 257, NULL, NULL, 1288, NULL, NULL, 35, 9, NULL, 9, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1570, NULL, NULL, 76, 258, NULL, NULL, 1289, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1571, NULL, NULL, 76, 259, NULL, NULL, 1290, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1572, NULL, NULL, 76, 260, NULL, NULL, 1291, NULL, NULL, 35, 5, NULL, 5, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/031', 'update', 'penjualan', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `arus_stok` VALUES (1573, 432, 1215, NULL, NULL, NULL, NULL, 1275, 1297, NULL, 36, 0, 102, 0, 102, 102, 'Pembelian Barang No Faktur : PBL/NM/2022/04/003', 'insert', 'pembelian', '2022-04-22 09:20:16', '2022-04-22 09:20:16', NULL);
INSERT INTO `arus_stok` VALUES (1574, NULL, NULL, 77, 261, NULL, NULL, 1199, NULL, NULL, 35, 330, NULL, 6, NULL, 324, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1575, NULL, NULL, 77, 262, NULL, NULL, 1200, NULL, NULL, 35, 60, NULL, 1, NULL, 59, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1576, NULL, NULL, 77, 263, NULL, NULL, 1277, NULL, NULL, 35, 55, NULL, 1, NULL, 54, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1577, NULL, NULL, 77, 264, NULL, NULL, 1278, NULL, NULL, 35, 55, NULL, 1, NULL, 54, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1578, NULL, NULL, 77, 265, NULL, NULL, 1279, NULL, NULL, 35, 55, NULL, 1, NULL, 54, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1579, NULL, NULL, 77, 266, NULL, NULL, 1280, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1580, NULL, NULL, 77, 267, NULL, NULL, 1275, NULL, NULL, 35, 102, NULL, 1, NULL, 101, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1581, NULL, NULL, 77, 268, NULL, NULL, 1276, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1582, NULL, NULL, 77, 269, NULL, NULL, 1281, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1583, NULL, NULL, 77, 270, NULL, NULL, 1282, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1584, NULL, NULL, 77, 271, NULL, NULL, 1198, NULL, NULL, 35, 17, NULL, 1, NULL, 16, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1585, NULL, NULL, 77, 272, NULL, NULL, 1197, NULL, NULL, 35, 17, NULL, 1, NULL, 16, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1586, NULL, NULL, 77, 273, NULL, NULL, 1292, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1587, NULL, NULL, 77, 274, NULL, NULL, 1283, NULL, NULL, 35, 1, NULL, 1, NULL, 0, 'Penjualan Barang No Faktur : PJL/KAR/2022/04/032', 'update', 'penjualan', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `arus_stok` VALUES (1588, 404, 1153, NULL, NULL, NULL, NULL, 1205, 1184, 91, 35, 15, 40, 0, 55, 55, 'Penyesuaian Stok tanggal 22-04-2022 09:28', 'update', 'penyesuaian_stok', '2022-04-22 09:28:55', '2022-04-22 09:28:55', NULL);
INSERT INTO `arus_stok` VALUES (1589, NULL, NULL, 78, 275, NULL, NULL, 1149, NULL, NULL, 36, 530, NULL, 2, NULL, 528, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1590, NULL, NULL, 78, 276, NULL, NULL, 1150, NULL, NULL, 36, 31, NULL, 2, NULL, 29, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1591, NULL, NULL, 78, 277, NULL, NULL, 1153, NULL, NULL, 36, 69, NULL, 2, NULL, 67, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1592, NULL, NULL, 78, 278, NULL, NULL, 1154, NULL, NULL, 36, 169, NULL, 2, NULL, 167, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1593, NULL, NULL, 78, 279, NULL, NULL, 1156, NULL, NULL, 36, 169, NULL, 2, NULL, 167, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1594, NULL, NULL, 78, 280, NULL, NULL, 1159, NULL, NULL, 36, 69, NULL, 2, NULL, 67, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1595, NULL, NULL, 78, 281, NULL, NULL, 1161, NULL, NULL, 36, 69, NULL, 2, NULL, 67, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1596, NULL, NULL, 78, 282, NULL, NULL, 1162, NULL, NULL, 36, 68, NULL, 2, NULL, 66, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1597, NULL, NULL, 78, 283, NULL, NULL, 1163, NULL, NULL, 36, 68, NULL, 2, NULL, 66, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1598, NULL, NULL, 78, 284, NULL, NULL, 1164, NULL, NULL, 36, 68, NULL, 2, NULL, 66, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1599, NULL, NULL, 78, 285, NULL, NULL, 1165, NULL, NULL, 36, 68, NULL, 2, NULL, 66, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `arus_stok` VALUES (1600, NULL, NULL, 78, 286, NULL, NULL, 1166, NULL, NULL, 36, 68, NULL, 2, NULL, 66, 'Penjualan Barang No Faktur : PJL/BP/2022/04/033', 'update', 'penjualan', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `brg_harga_jual` decimal(10,0) DEFAULT NULL,
  `brg_kode` varchar(255) DEFAULT NULL,
  `brg_nama` varchar(255) DEFAULT NULL,
  `brg_golongan` enum('luar','dalam') DEFAULT NULL,
  `brg_minimal_stok` int(11) DEFAULT NULL,
  `brg_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_barang`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1294 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of barang
-- ----------------------------
BEGIN;
INSERT INTO `barang` VALUES (1135, 43, 24, 22000, '-', 'ABC ALKALINE AA ISI 2 PCS', NULL, 1, '-', '2022-03-30 14:34:16', '2022-03-30 14:34:16', NULL);
INSERT INTO `barang` VALUES (1136, 29, 19, 82500, '-', 'Metode Ringkas Terpadu 6A', NULL, 1, '-', '2022-03-30 14:40:32', '2022-03-30 15:01:16', NULL);
INSERT INTO `barang` VALUES (1137, 29, 19, 77000, '-', 'Metode Ringkas Terpadu 6B', NULL, 1, '-', '2022-03-30 14:41:16', '2022-03-30 15:01:23', NULL);
INSERT INTO `barang` VALUES (1138, 29, 19, 77000, '-', 'Metode Ringkas Terpadu 6C', NULL, 1, '-', '2022-03-30 14:41:58', '2022-03-30 15:01:30', NULL);
INSERT INTO `barang` VALUES (1139, 44, 21, 27500, '-', 'MINYAK KAYU PUTIH CAP LANG 30 ML', NULL, 1, '-', '2022-03-30 14:42:14', '2022-03-30 14:42:14', NULL);
INSERT INTO `barang` VALUES (1140, 44, 21, 18000, '-', 'FRES CARE ORIGINAL 10 ML', NULL, 1, '-', '2022-03-30 14:44:15', '2022-03-30 14:44:15', NULL);
INSERT INTO `barang` VALUES (1141, 29, 19, 77000, '-', 'Metode Ringkas Terpadu 6D', NULL, 1, '-', '2022-03-30 14:44:17', '2022-03-30 15:01:38', NULL);
INSERT INTO `barang` VALUES (1142, 30, 19, 140000, '-', 'KSN IPA SD/MI : Tumbuhan dan Ekosistem', NULL, 0, '-', '2022-03-30 14:51:37', '2022-04-14 15:43:31', NULL);
INSERT INTO `barang` VALUES (1143, 30, 19, 95000, '-', 'KSN IPA SD/MI : Zat, Materi dan Kalor', NULL, 0, '-', '2022-03-30 14:56:38', '2022-04-19 13:56:33', NULL);
INSERT INTO `barang` VALUES (1144, 30, 19, 115000, '-', 'KSN IPA SD/MI : Mekanika', NULL, 1, '-', '2022-03-30 14:57:04', '2022-03-30 15:02:03', NULL);
INSERT INTO `barang` VALUES (1145, 30, 19, 135000, '-', 'KSN IPA SD/MI : Hewan dan Manusia', NULL, 1, '-', '2022-03-30 14:57:38', '2022-03-30 15:02:11', NULL);
INSERT INTO `barang` VALUES (1146, 30, 19, 70000, '-', 'Kump. Soal Pem. OSN Matematika SD Jilid 1 2008', NULL, 1, '-', '2022-03-30 14:58:38', '2022-03-30 15:02:49', NULL);
INSERT INTO `barang` VALUES (1147, 30, 19, 95000, '-', 'Kump. Soal Pem. OSN Matematika SD Jilid 2 2009/2014', NULL, 1, '-', '2022-03-30 15:00:03', '2022-03-30 15:02:41', NULL);
INSERT INTO `barang` VALUES (1148, 30, 19, 95000, '-', 'Kump. Soal Pem. OSN Matematika SD Jilid 2 2009/2014', NULL, 1, '-', '2022-03-30 15:00:03', '2022-04-19 16:04:22', '2022-04-19 16:04:22');
INSERT INTO `barang` VALUES (1149, 36, 19, 85000, '-', 'Super Master KSN IPS SMP/MTs', NULL, 1, '-', '2022-03-30 15:01:10', '2022-03-30 15:01:10', NULL);
INSERT INTO `barang` VALUES (1150, 36, 19, 260000, '-', 'Super Master KSN Matematika SMP/MTs', NULL, 1, '-', '2022-03-30 15:03:27', '2022-03-30 15:03:27', NULL);
INSERT INTO `barang` VALUES (1151, 36, 19, 146000, '-', 'Konsep Dasar Lengkap OSN Fisika SMP', NULL, 1, '-', '2022-03-30 15:03:27', '2022-03-30 15:04:48', NULL);
INSERT INTO `barang` VALUES (1152, 46, 26, 52500, '-', 'Egrang Bathok', NULL, 1, '-', '2022-03-30 15:40:01', '2022-03-30 15:40:01', NULL);
INSERT INTO `barang` VALUES (1153, 45, 19, 59000, '-', 'Super Coach IPS SMP/MTs Kelas VII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:40:09', '2022-03-30 15:40:09', NULL);
INSERT INTO `barang` VALUES (1154, 45, 19, 93000, '-', 'Super Coach IPA SMP/MTs Kelas VII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:40:38', '2022-03-30 15:40:38', NULL);
INSERT INTO `barang` VALUES (1155, 46, 26, 50000, '-', 'Bakiak P3 Anak', NULL, 1, '-', '2022-03-30 15:40:41', '2022-03-30 15:40:41', NULL);
INSERT INTO `barang` VALUES (1156, 45, 19, 66000, '-', 'Super Coach Matematika SMP/MTs Kelas VII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:41:19', '2022-03-30 15:41:19', NULL);
INSERT INTO `barang` VALUES (1157, 47, 26, 157500, '-', 'Meronce Geometri', NULL, 1, '-', '2022-03-30 15:41:45', '2022-03-30 15:47:57', NULL);
INSERT INTO `barang` VALUES (1158, 46, 26, 300000, '-', 'Papan Lempar Bola', NULL, 1, '-', '2022-03-30 15:42:16', '2022-03-30 15:42:16', NULL);
INSERT INTO `barang` VALUES (1159, 45, 19, 57500, '-', 'Super Coach Bahasa Indonesia SMP/MTs Kelas VII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:47:30', '2022-03-30 15:47:30', NULL);
INSERT INTO `barang` VALUES (1160, 45, 19, 57500, '-', 'Super Coach Bahasa Indonesia SMP/MTs Kelas VII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:47:30', '2022-04-14 13:44:23', '2022-04-14 13:44:23');
INSERT INTO `barang` VALUES (1161, 45, 19, 36000, '-', 'Super Coach Bahasa Inggris SMP/MTs Kelas VII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:49:01', '2022-03-30 15:49:01', NULL);
INSERT INTO `barang` VALUES (1162, 45, 19, 54000, '-', 'Super Coach Matematika SMP/MTs Kelas VIII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:49:35', '2022-03-30 15:49:35', NULL);
INSERT INTO `barang` VALUES (1163, 45, 19, 96500, '-', 'Super Coach IPA SMP/MTs Kelas VIII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:50:11', '2022-03-30 15:50:11', NULL);
INSERT INTO `barang` VALUES (1164, 45, 19, 46500, '-', 'Super Coach IPS SMP/MTs Kelas VIII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:50:37', '2022-03-30 15:50:37', NULL);
INSERT INTO `barang` VALUES (1165, 45, 19, 80500, '-', 'Super Coach Pola Belajar Siswa Mandiri Bahasa Indonesia SMP/MTs Kelas VIII', NULL, 1, '-', '2022-03-30 15:51:15', '2022-03-30 15:54:07', NULL);
INSERT INTO `barang` VALUES (1166, 45, 19, 63000, '-', 'Super Coach Bahasa Inggris SMP/MTs Kelas VIII Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:54:54', '2022-03-30 15:54:54', NULL);
INSERT INTO `barang` VALUES (1167, 45, 19, 40000, '-', 'Super Coach Matematika SMP/MTs Kelas IX Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:55:36', '2022-03-30 15:55:36', NULL);
INSERT INTO `barang` VALUES (1168, 45, 19, 71500, '-', 'Super Coach IPA SMP/MTs Kelas IX Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:56:07', '2022-03-30 15:56:07', NULL);
INSERT INTO `barang` VALUES (1169, 45, 19, 66500, '-', 'Super Coach IPS SMP/MTs Kelas IX Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:56:27', '2022-03-30 15:56:27', NULL);
INSERT INTO `barang` VALUES (1170, 45, 19, 67500, '-', 'Super Coach Bahasa Inggris SMP/MTs Kelas IX Kur. 2013 Rev', NULL, 1, '-', '2022-03-30 15:56:56', '2022-03-30 15:56:56', NULL);
INSERT INTO `barang` VALUES (1171, 45, 19, 72000, '-', 'Super Coach Pola Belajar Siswa Mandiri Bahasa Indonesia SMP/MTs Kelas IX', NULL, 1, '-', '2022-03-30 15:57:41', '2022-03-30 15:57:41', NULL);
INSERT INTO `barang` VALUES (1172, 48, 21, 54000, '-', 'Baygon Citrus  Fresh 600 ML', NULL, 1, '-', '2022-03-30 16:08:23', '2022-03-30 16:08:23', NULL);
INSERT INTO `barang` VALUES (1173, 48, 21, 54000, '-', 'Baygon Aer Cherry Blossom 600+75 ML', NULL, 1, '-', '2022-03-30 16:09:40', '2022-03-30 16:09:40', NULL);
INSERT INTO `barang` VALUES (1174, 48, 21, 54000, '-', 'Baygon 600 ML Biru', NULL, 1, '-', '2022-03-30 16:10:20', '2022-03-30 16:10:20', NULL);
INSERT INTO `barang` VALUES (1175, 48, 21, 54000, '-', 'Baygon Aerosol Fruity Breeze 600 ML', NULL, 1, '-', '2022-03-30 16:12:41', '2022-03-30 16:12:41', NULL);
INSERT INTO `barang` VALUES (1176, 41, 21, 20000, '-', 'S.O.S Pembersih Lantai 800 ML', NULL, 1, '-', '2022-03-30 16:15:11', '2022-03-30 16:15:11', NULL);
INSERT INTO `barang` VALUES (1177, 41, 21, 20000, '-', 'S.O.S Pembersih Lantai 780 ML', NULL, 1, '-', '2022-03-30 16:16:39', '2022-03-30 16:16:39', NULL);
INSERT INTO `barang` VALUES (1178, 41, 21, 25000, '-', 'Wipol 450 ML', NULL, 1, '-', '2022-03-30 16:19:06', '2022-03-30 16:19:06', NULL);
INSERT INTO `barang` VALUES (1179, 44, 21, 264000, '-', 'Minyak Oles Bokashi 140 ml', NULL, 1, '-', '2022-03-30 16:34:12', '2022-03-30 16:34:12', NULL);
INSERT INTO `barang` VALUES (1180, 44, 21, 58000, '-', 'Minyak Sanhong  B 50 ML', NULL, 1, '-', '2022-03-30 16:41:20', '2022-03-30 16:41:20', NULL);
INSERT INTO `barang` VALUES (1181, 49, 24, 24000, '-', 'Kapas Bola Medisoft 75 Gr', NULL, 1, '-', '2022-03-30 16:53:22', '2022-03-30 16:53:22', NULL);
INSERT INTO `barang` VALUES (1182, 44, 21, 22000, '-', 'Minyak Telon Lang 30 ML', NULL, 1, '-', '2022-03-30 16:55:25', '2022-03-30 16:55:25', NULL);
INSERT INTO `barang` VALUES (1183, 49, 24, 12000, '-', 'Cotton Buds', NULL, 1, '-', '2022-03-30 16:58:20', '2022-03-30 16:58:20', NULL);
INSERT INTO `barang` VALUES (1184, 49, 24, 6000, '-', 'Hansaplast Elastis @100', NULL, 1, '-', '2022-03-31 13:25:27', '2022-03-31 13:25:27', NULL);
INSERT INTO `barang` VALUES (1185, 49, 27, 14000, '-', 'Promag Tablet Kunyah', NULL, 1, '-', '2022-03-31 13:31:53', '2022-03-31 13:31:53', NULL);
INSERT INTO `barang` VALUES (1186, 49, 27, 15000, '-', 'Neo Entrostop', NULL, 1, '-', '2022-03-31 13:33:32', '2022-03-31 13:33:32', NULL);
INSERT INTO `barang` VALUES (1187, 49, 21, 24000, '-', 'Betadine Solution 15 ML', NULL, 1, '-', '2022-03-31 13:37:25', '2022-03-31 13:37:25', NULL);
INSERT INTO `barang` VALUES (1188, 49, 27, 1000, '-', 'Sanmol 500 MG', NULL, 1, '-', '2022-03-31 13:39:15', '2022-03-31 13:39:15', NULL);
INSERT INTO `barang` VALUES (1189, 49, 28, 55200, '-', 'Counterpain 15 GR', NULL, 1, '-', '2022-03-31 13:41:05', '2022-03-31 13:41:05', NULL);
INSERT INTO `barang` VALUES (1190, 49, 24, 27000, '-', 'Obat Mata Insto 7,5 ML', NULL, 1, '-', '2022-03-31 13:42:28', '2022-03-31 13:42:28', NULL);
INSERT INTO `barang` VALUES (1191, 50, 21, 150000, '-', 'Tinta Printer Epson Black 003', NULL, 1, '-', '2022-03-31 14:16:53', '2022-03-31 14:18:41', NULL);
INSERT INTO `barang` VALUES (1192, 50, 21, 150000, '-', 'Tinta Printer Epson 003 Cyan', NULL, 1, '-', '2022-03-31 14:18:20', '2022-03-31 14:18:20', NULL);
INSERT INTO `barang` VALUES (1193, 50, 21, 150000, '-', 'Tinta Printer Epson 003 Magenta', NULL, 1, '-', '2022-03-31 14:19:34', '2022-03-31 14:19:34', NULL);
INSERT INTO `barang` VALUES (1194, 50, 21, 150000, '-', 'Tinta Printer Epson 003 Yellow', NULL, 1, '-', '2022-03-31 14:20:24', '2022-03-31 14:20:24', NULL);
INSERT INTO `barang` VALUES (1195, 51, 24, 6500, '-', 'Kertas Origami Forte 14 x 14 cm', NULL, 1, '-', '2022-03-31 14:44:34', '2022-03-31 14:44:34', NULL);
INSERT INTO `barang` VALUES (1196, 52, 24, 60000, '-', 'Crayon Joyko 24 warna', NULL, 1, '-', '2022-03-31 14:45:40', '2022-03-31 14:45:40', NULL);
INSERT INTO `barang` VALUES (1197, 51, 20, 61500, '-', 'Kertas HVS SIDU A4 70 gr', NULL, 1, '-', '2022-03-31 14:46:42', '2022-03-31 14:46:42', NULL);
INSERT INTO `barang` VALUES (1198, 51, 20, 68500, '-', 'Kertas HVS SIDU F4 70 gr', NULL, 1, '-', '2022-03-31 14:48:14', '2022-03-31 14:48:14', NULL);
INSERT INTO `barang` VALUES (1199, 53, 24, 6500, '-', 'Buku Gambar SIDU A4', NULL, 1, '-', '2022-03-31 14:49:29', '2022-03-31 14:49:29', NULL);
INSERT INTO `barang` VALUES (1200, 53, 24, 6000, '-', 'Buku Tulis SIDU 38 Kotak Tanggung', NULL, 1, '-', '2022-03-31 14:50:52', '2022-03-31 14:50:52', NULL);
INSERT INTO `barang` VALUES (1201, 53, 24, 6000, '-', 'Buku Tulis Biasa SIDU isi 38', NULL, 1, '-', '2022-03-31 14:52:09', '2022-03-31 14:52:09', NULL);
INSERT INTO `barang` VALUES (1202, 54, 24, 5000, '-', 'Map Plastik Kancing 1 Big', NULL, 1, '-', '2022-03-31 14:53:42', '2022-03-31 14:53:42', NULL);
INSERT INTO `barang` VALUES (1203, 52, 24, 20000, '-', 'Penggaris Kayu 100 cm', NULL, 1, '-', '2022-03-31 14:54:50', '2022-03-31 14:54:50', NULL);
INSERT INTO `barang` VALUES (1204, 52, 24, 4500, '-', 'Penggaris  Butterfly 30 cm', NULL, 1, '-', '2022-03-31 14:55:53', '2022-03-31 14:55:53', NULL);
INSERT INTO `barang` VALUES (1205, 55, 24, 10000, '-', 'Gunting Tanggung', NULL, 1, '-', '2022-03-31 14:57:39', '2022-03-31 14:57:39', NULL);
INSERT INTO `barang` VALUES (1206, 54, 24, 15000, '-', 'Map Zipper Resleting', NULL, 1, '-', '2022-03-31 14:59:41', '2022-03-31 14:59:41', NULL);
INSERT INTO `barang` VALUES (1207, 41, 26, 157000, '-', 'Keranjang Mainan Besar', NULL, 0, '-', '2022-03-31 16:07:14', '2022-03-31 16:07:14', NULL);
INSERT INTO `barang` VALUES (1208, 41, 26, 73500, '-', 'Keranjang Mainan Sedang Polos', NULL, 0, '-', '2022-03-31 16:07:51', '2022-03-31 16:07:51', NULL);
INSERT INTO `barang` VALUES (1209, 41, 26, 20000, '-', 'Sapu Lidi Tangkai', NULL, 0, '-', '2022-03-31 16:10:12', '2022-03-31 16:10:12', NULL);
INSERT INTO `barang` VALUES (1210, 41, 26, 30000, '-', 'Sapu Ijuk', NULL, 0, '-', '2022-03-31 16:11:30', '2022-03-31 16:11:30', NULL);
INSERT INTO `barang` VALUES (1211, 41, 23, 5000, '-', 'Plastik Sampah', NULL, 0, '-', '2022-03-31 16:12:49', '2022-03-31 16:12:49', NULL);
INSERT INTO `barang` VALUES (1212, 41, 26, 20000, '-', 'Tempat Sampah Tutup', NULL, 0, '-', '2022-03-31 16:14:10', '2022-03-31 16:14:10', NULL);
INSERT INTO `barang` VALUES (1213, 41, 26, 45000, '-', 'Pengepelan Biasa', NULL, 0, '-', '2022-03-31 16:15:41', '2022-03-31 16:15:41', NULL);
INSERT INTO `barang` VALUES (1214, 27, 29, 850000, '-', 'Alat Peraga IPA Listrik Dasar (Elektrik)', NULL, 1, '-', '2022-03-31 16:42:13', '2022-03-31 16:42:13', NULL);
INSERT INTO `barang` VALUES (1215, 27, 26, 850000, '-', 'Alat Peraga IPA Siklus Hujan (Elektrik)', NULL, 1, '-', '2022-03-31 16:43:37', '2022-03-31 16:43:37', NULL);
INSERT INTO `barang` VALUES (1216, 27, 26, 850000, '-', 'Alat peraga IPA Peredaran Darah (Elektrik)', NULL, 1, '-', '2022-03-31 16:44:48', '2022-03-31 16:44:48', NULL);
INSERT INTO `barang` VALUES (1217, 27, 26, 850000, '-', 'Alat Peraga IPA Pernafasan (Elektrik)', NULL, 1, '-', '2022-03-31 16:45:51', '2022-03-31 16:45:51', NULL);
INSERT INTO `barang` VALUES (1218, 27, 26, 850000, '-', 'Alat Peraga IPA Pencernaan Makanan (Elektrik)', NULL, 1, '-', '2022-03-31 16:47:02', '2022-03-31 16:47:02', NULL);
INSERT INTO `barang` VALUES (1219, 27, 26, 325000, '-', 'Metamorfosis Kupu-kupu', NULL, 1, '-', '2022-03-31 16:49:26', '2022-03-31 16:49:26', NULL);
INSERT INTO `barang` VALUES (1220, 27, 26, 325000, '-', 'Metamorfosis Katak', NULL, 1, '-', '2022-03-31 16:50:40', '2022-03-31 16:50:40', NULL);
INSERT INTO `barang` VALUES (1221, 27, 26, 325000, '-', 'Metamorfosis Ulat Sutera', NULL, 1, '-', '2022-03-31 16:51:36', '2022-03-31 16:51:36', NULL);
INSERT INTO `barang` VALUES (1222, 27, 26, 600000, '-', 'Alat Peraga IPA KIT Magnet', NULL, 1, '-', '2022-03-31 16:53:02', '2022-03-31 16:53:02', NULL);
INSERT INTO `barang` VALUES (1223, 27, 26, 600000, '-', 'Alat Peraga IPA KIT Cahaya', NULL, 1, '-', '2022-03-31 16:54:15', '2022-03-31 16:54:15', NULL);
INSERT INTO `barang` VALUES (1224, 27, 26, 440000, '-', 'Alat Peraga Planetarium (Bulan,Bintang dan Matahari)', NULL, 1, '-', '2022-03-31 16:56:39', '2022-03-31 16:56:39', NULL);
INSERT INTO `barang` VALUES (1225, 41, 26, 90000, '-', 'Tempat Sampah Ember + Tutup', NULL, 1, '-', '2022-04-02 08:37:37', '2022-04-02 08:37:37', NULL);
INSERT INTO `barang` VALUES (1226, 41, 26, 30000, '-', 'Sapu Ijuk', NULL, 1, '-', '2022-04-02 08:38:12', '2022-04-02 08:56:13', '2022-04-02 08:56:13');
INSERT INTO `barang` VALUES (1227, 41, 26, 15000, '-', 'Serok Sampah Seng', NULL, 1, '-', '2022-04-02 08:38:55', '2022-04-02 08:38:55', NULL);
INSERT INTO `barang` VALUES (1228, 41, 24, 33000, '-', 'Kemoceng', NULL, 1, '-', '2022-04-02 08:40:03', '2022-04-02 08:40:03', NULL);
INSERT INTO `barang` VALUES (1229, 56, 30, 35000, '-', 'Masker Dewasa 3 Fly', NULL, 0, '-', '2022-04-02 09:20:56', '2022-04-02 09:20:56', NULL);
INSERT INTO `barang` VALUES (1230, 40, 22, 3750000, '-', 'Printer Epson Ecotank L3210', NULL, 0, '-', '2022-04-02 09:32:15', '2022-04-02 09:32:15', NULL);
INSERT INTO `barang` VALUES (1231, 57, 22, 9632600, '-', 'Laptop Asus PL411cj', NULL, 0, '-', '2022-04-02 09:38:12', '2022-04-02 09:38:12', NULL);
INSERT INTO `barang` VALUES (1232, 47, 26, 170000, '-', 'Replika Kendaraan', NULL, 0, '-', '2022-04-02 09:44:16', '2022-04-02 09:44:16', NULL);
INSERT INTO `barang` VALUES (1233, 47, 24, 70000, '-', 'Replika Buah-buahan', NULL, 0, '-', '2022-04-02 09:47:13', '2022-04-02 09:47:13', NULL);
INSERT INTO `barang` VALUES (1234, 47, 26, 75000, '-', 'Replika Makanan', NULL, 0, '-', '2022-04-02 09:47:59', '2022-04-02 09:47:59', NULL);
INSERT INTO `barang` VALUES (1235, 27, 22, 850000, '-', 'Alat Peraga  IPA Tsunami (Elektrik)', NULL, 0, '-', '2022-04-12 12:04:20', '2022-04-12 12:04:20', NULL);
INSERT INTO `barang` VALUES (1236, 27, 22, 850000, '-', 'Alat Peraga IPA Gunung Berapi (Elektrik)', NULL, 0, '-', '2022-04-12 12:05:40', '2022-04-12 12:05:40', NULL);
INSERT INTO `barang` VALUES (1237, 42, 26, 880000, '-', 'Kursi Kayu Spons Guru', NULL, 0, '-', '2022-04-13 10:44:14', '2022-04-13 11:04:01', '2022-04-13 11:04:01');
INSERT INTO `barang` VALUES (1238, 42, 26, 5200000, '-', 'Meja Guru 1 Biro Ukir', NULL, 0, '-', '2022-04-13 11:00:28', '2022-04-13 11:00:28', NULL);
INSERT INTO `barang` VALUES (1239, 42, 26, 2700000, '-', 'Meja Guru 1/2 Biro Ukir', NULL, 0, '-', '2022-04-13 11:00:57', '2022-04-13 11:00:57', NULL);
INSERT INTO `barang` VALUES (1240, 42, 26, 2300000, '-', 'Almari Sekolah', NULL, 0, '-', '2022-04-13 11:02:16', '2022-04-13 11:02:16', NULL);
INSERT INTO `barang` VALUES (1241, 42, 26, 528000, '-', 'Kursi Siswa', NULL, 0, '-', '2022-04-13 11:03:20', '2022-04-13 11:03:20', NULL);
INSERT INTO `barang` VALUES (1242, 42, 26, 880000, '-', 'Kursi Guru Spons', NULL, 0, '-', '2022-04-13 11:03:44', '2022-04-13 11:03:44', NULL);
INSERT INTO `barang` VALUES (1243, 29, 19, 70500, 'ABM', 'Ayo Bel.Menalar Matematika SD Kls 5 K.13 Rev*Suwah', NULL, 0, '-', '2022-04-14 15:38:43', '2022-04-14 15:38:43', NULL);
INSERT INTO `barang` VALUES (1244, 29, 19, 78500, 'ABM', 'Ayo Bel.Menalar Matematika SD Kls 4 K.13 Rev*Suwah', NULL, 0, '-', '2022-04-14 15:46:26', '2022-04-14 15:46:26', NULL);
INSERT INTO `barang` VALUES (1245, 29, 19, 65500, 'ABM', 'Ayo Bel.Menalar Matematika SD Kls 6 K.13 Rev*Suwah', NULL, 0, '-', '2022-04-14 15:47:11', '2022-04-14 15:47:11', NULL);
INSERT INTO `barang` VALUES (1246, 58, 19, 0, 'YW', 'Bunga Putih Abu-abu *Eddy D. Iskandar*', NULL, 0, '-', '2022-04-14 15:49:39', '2022-04-14 15:53:40', NULL);
INSERT INTO `barang` VALUES (1247, 58, 19, 0, 'YW', 'Bawalah Perasaanku *Eddy D.Iskandar*', NULL, 0, '-', '2022-04-14 15:50:56', '2022-04-14 15:50:56', NULL);
INSERT INTO `barang` VALUES (1248, 58, 19, 0, 'YW', 'Aisyah & Adinda', NULL, 0, '-', '2022-04-14 15:51:32', '2022-04-14 15:51:32', NULL);
INSERT INTO `barang` VALUES (1249, 58, 19, 0, 'YW', 'Semau Gue', NULL, 0, '-', '2022-04-14 15:51:57', '2022-04-14 15:51:57', NULL);
INSERT INTO `barang` VALUES (1250, 58, 19, 0, 'YW', 'Soe Isabel', NULL, 0, '-', '2022-04-14 15:52:24', '2022-04-14 15:52:24', NULL);
INSERT INTO `barang` VALUES (1251, 58, 19, 0, 'YW', 'Mutiara Keluarga', NULL, 0, '-', '2022-04-14 15:52:46', '2022-04-14 15:52:46', NULL);
INSERT INTO `barang` VALUES (1252, 58, 19, 0, 'YW', 'Selamat Tinggal Masa Remaja', NULL, 0, '-', '2022-04-14 15:53:16', '2022-04-14 15:53:16', NULL);
INSERT INTO `barang` VALUES (1253, 58, 19, 0, 'YW', 'Voice in The Mirror', NULL, 0, '-', '2022-04-14 15:54:25', '2022-04-14 15:54:25', NULL);
INSERT INTO `barang` VALUES (1254, 58, 19, 0, 'YW', 'Kerinduan Paling Agung', NULL, 0, '-', '2022-04-14 15:54:53', '2022-04-14 15:54:53', NULL);
INSERT INTO `barang` VALUES (1255, 59, 19, 60500, 'BSU', 'BANK SOAL ULANGAN SD/MI KLS 4 SEMESTER 1 KUR.2013', NULL, 0, '-', '2022-04-19 12:44:38', '2022-04-19 12:44:38', NULL);
INSERT INTO `barang` VALUES (1256, 59, 19, 68000, 'BSU', 'BANK SOAL ULANGAN SD/MI KLS 4 SEMESTER 2 KUR.2013', NULL, 0, '-', '2022-04-19 12:45:33', '2022-04-19 12:45:33', NULL);
INSERT INTO `barang` VALUES (1257, 59, 19, 64000, 'BSU', 'BANK SOAL ULANGAN SD/MI KLS 5 SEMESTER 1 KUR.2013', NULL, 0, '-', '2022-04-19 12:46:07', '2022-04-19 12:46:07', NULL);
INSERT INTO `barang` VALUES (1258, 59, 19, 75500, 'BSU', 'BANK SOAL ULANGAN SD/MI KLS 5 SEMESTER 2 KUR.2013', NULL, 0, '-', '2022-04-19 12:46:42', '2022-04-19 12:46:42', NULL);
INSERT INTO `barang` VALUES (1259, 59, 19, 64000, 'BSU', 'BANK SOAL ULANGAN SD/MI KLS 6 SEMESTER 1 KUR.2013', NULL, 0, '-', '2022-04-19 12:47:58', '2022-04-19 12:47:58', NULL);
INSERT INTO `barang` VALUES (1260, 59, 19, 60000, 'BSU', 'BANK SOAL ULANGAN SD/MI KLS 6 SEMESTER 2 KUR.2013', NULL, 0, '-', '2022-04-19 12:48:26', '2022-04-19 12:48:26', NULL);
INSERT INTO `barang` VALUES (1261, 54, 24, 1100, 'MFK', 'MAP FOLIO KERTAS', NULL, 0, '-', '2022-04-19 14:05:13', '2022-04-19 14:05:13', NULL);
INSERT INTO `barang` VALUES (1262, 60, 23, 20000, 'TS', 'TISU PASEO', NULL, 0, '-', '2022-04-19 14:06:25', '2022-04-19 14:06:25', NULL);
INSERT INTO `barang` VALUES (1263, 61, 26, 0, 'SBN', 'SABUN CUCI TANGAN 500 ML', NULL, 0, '-', '2022-04-19 14:08:17', '2022-04-19 14:08:17', NULL);
INSERT INTO `barang` VALUES (1264, 62, 24, 5000, '-', 'PAPER CLIP JOYKO NO 1', NULL, 0, '-', '2022-04-19 14:09:31', '2022-04-19 14:09:31', NULL);
INSERT INTO `barang` VALUES (1265, 62, 30, 12500, '-', 'BINDER CLIPS NO 111/KOTAK', NULL, 0, '-', '2022-04-19 14:10:18', '2022-04-19 14:10:18', NULL);
INSERT INTO `barang` VALUES (1266, 62, 30, 20000, '-', 'BINDER CLIPS NO 200/KOTAK', NULL, 0, '-', '2022-04-19 14:10:49', '2022-04-19 14:10:49', NULL);
INSERT INTO `barang` VALUES (1267, 62, 30, 31500, '-', 'BINDER CLIPS NO 260 /KOTAK', NULL, 0, '-', '2022-04-19 14:11:19', '2022-04-19 14:11:19', NULL);
INSERT INTO `barang` VALUES (1268, 36, 19, 146000, 'KSN', 'KONSEP DASAR LENGKAP OSN FISIKA SMP', NULL, 0, '-', '2022-04-19 14:45:45', '2022-04-19 14:47:38', '2022-04-19 14:47:38');
INSERT INTO `barang` VALUES (1269, 47, 22, 280000, '-', 'KUDA KARET', NULL, 0, '-', '2022-04-22 08:29:17', '2022-04-22 08:29:17', NULL);
INSERT INTO `barang` VALUES (1270, 47, 22, 100000, '-', 'BALOK ISI 16', NULL, 0, '-', '2022-04-22 08:29:45', '2022-04-22 08:29:45', NULL);
INSERT INTO `barang` VALUES (1271, 46, 24, 150000, '-', 'BOLA', NULL, 0, '-', '2022-04-22 08:30:12', '2022-04-22 08:30:12', NULL);
INSERT INTO `barang` VALUES (1272, 47, 29, 175000, '-', 'PERALATAN MASAK', NULL, 0, '-', '2022-04-22 08:30:37', '2022-04-22 08:30:37', NULL);
INSERT INTO `barang` VALUES (1273, 52, 19, 6500, '-', 'BUKU GARIS TIGA', NULL, 0, '-', '2022-04-22 08:31:30', '2022-04-22 08:31:30', NULL);
INSERT INTO `barang` VALUES (1274, 52, 19, 10000, '-', 'BUKU MENJIPLAK', NULL, 0, '-', '2022-04-22 08:31:56', '2022-04-22 08:31:56', NULL);
INSERT INTO `barang` VALUES (1275, 47, 23, 10000, '-', 'PLASTISIN', NULL, 0, '-', '2022-04-22 08:32:56', '2022-04-22 08:32:56', NULL);
INSERT INTO `barang` VALUES (1276, 52, 24, 25000, '-', 'KOTAK PENSIL', NULL, 0, '-', '2022-04-22 08:33:35', '2022-04-22 08:33:35', NULL);
INSERT INTO `barang` VALUES (1277, 52, 24, 23000, '-', 'CRAYON ISI 12', NULL, 0, '-', '2022-04-22 08:34:49', '2022-04-22 08:34:49', NULL);
INSERT INTO `barang` VALUES (1278, 52, 24, 5000, '-', 'PENSIL', NULL, 0, '-', '2022-04-22 08:35:53', '2022-04-22 08:35:53', NULL);
INSERT INTO `barang` VALUES (1279, 52, 24, 2000, '-', 'PENGHAPUS', NULL, 0, '-', '2022-04-22 08:38:05', '2022-04-22 08:38:05', NULL);
INSERT INTO `barang` VALUES (1280, 52, 24, 2000, '-', 'RAUTAN', NULL, 0, '-', '2022-04-22 08:39:01', '2022-04-22 08:39:01', NULL);
INSERT INTO `barang` VALUES (1281, 52, 24, 6000, '-', 'LEM POVINAL', NULL, 0, '-', '2022-04-22 08:39:49', '2022-04-22 08:39:49', NULL);
INSERT INTO `barang` VALUES (1282, 52, 23, 6500, '-', 'KERTAS MELIPAT ORIGAMI', NULL, 0, '-', '2022-04-22 08:40:18', '2022-04-22 08:40:18', NULL);
INSERT INTO `barang` VALUES (1283, 56, 21, 15000, '-', 'HANDSANITIZER 60 ML', NULL, 0, '-', '2022-04-22 08:41:06', '2022-04-22 08:41:06', NULL);
INSERT INTO `barang` VALUES (1284, 52, 24, 45000, '-', 'PULPEN /KOTAK', NULL, 0, '-', '2022-04-22 08:41:58', '2022-04-22 08:41:58', NULL);
INSERT INTO `barang` VALUES (1285, 52, 19, 1100, '-', 'MAP KERTAS FOLIO', NULL, 0, '-', '2022-04-22 08:42:16', '2022-04-22 08:42:16', NULL);
INSERT INTO `barang` VALUES (1286, 41, 26, 25000, '-', 'SAPU IJUK HITAM', NULL, 0, '-', '2022-04-22 08:42:46', '2022-04-22 09:29:32', '2022-04-22 09:29:32');
INSERT INTO `barang` VALUES (1287, 41, 26, 72000, '-', 'ALAT PENGEPELAN', NULL, 0, '-', '2022-04-22 08:43:04', '2022-04-22 08:43:04', NULL);
INSERT INTO `barang` VALUES (1288, 41, 26, 13000, '-', 'TISU NICE', NULL, 0, '-', '2022-04-22 08:43:29', '2022-04-22 08:43:29', NULL);
INSERT INTO `barang` VALUES (1289, 49, 26, 155000, '-', 'ALAT UKUR TINGGI BADAN', NULL, 0, '-', '2022-04-22 08:44:24', '2022-04-22 08:44:24', NULL);
INSERT INTO `barang` VALUES (1290, 56, 26, 100000, '-', 'THERMOMETER', NULL, 0, '-', '2022-04-22 08:44:55', '2022-04-22 08:44:55', NULL);
INSERT INTO `barang` VALUES (1291, 49, 21, 25000, '-', 'MINYAK TELON', NULL, 0, '-', '2022-04-22 08:45:35', '2022-04-22 08:45:35', NULL);
INSERT INTO `barang` VALUES (1292, 56, 30, 35000, '-', 'MASKER', NULL, 0, '-', '2022-04-22 08:46:12', '2022-04-22 08:46:12', NULL);
INSERT INTO `barang` VALUES (1293, 52, 19, 6000, '-', 'BUKU TULIS SATUAN', NULL, 0, '-', '2022-04-22 09:26:04', '2022-04-22 09:27:35', '2022-04-22 09:27:35');
COMMIT;

-- ----------------------------
-- Table structure for barang_harga_jual_history
-- ----------------------------
DROP TABLE IF EXISTS `barang_harga_jual_history`;
CREATE TABLE `barang_harga_jual_history` (
  `id_barang` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `brg_harga_jual` decimal(10,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_barang_harga_jual_history` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_barang_harga_jual_history`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of barang_harga_jual_history
-- ----------------------------
BEGIN;
INSERT INTO `barang_harga_jual_history` VALUES (1135, 36, 22000, '2022-03-30 14:34:16', '2022-03-30 14:34:16', NULL, 7);
INSERT INTO `barang_harga_jual_history` VALUES (1136, 35, 82500, '2022-03-30 14:40:32', '2022-03-30 14:40:32', NULL, 8);
INSERT INTO `barang_harga_jual_history` VALUES (1137, 35, 77000, '2022-03-30 14:41:16', '2022-03-30 14:41:16', NULL, 9);
INSERT INTO `barang_harga_jual_history` VALUES (1138, 35, 77000, '2022-03-30 14:41:58', '2022-03-30 14:41:58', NULL, 10);
INSERT INTO `barang_harga_jual_history` VALUES (1139, 36, 27500, '2022-03-30 14:42:14', '2022-03-30 14:42:14', NULL, 11);
INSERT INTO `barang_harga_jual_history` VALUES (1140, 36, 18000, '2022-03-30 14:44:16', '2022-03-30 14:44:16', NULL, 12);
INSERT INTO `barang_harga_jual_history` VALUES (1141, 35, 77000, '2022-03-30 14:44:17', '2022-03-30 14:44:17', NULL, 13);
INSERT INTO `barang_harga_jual_history` VALUES (1142, 35, 140000, '2022-03-30 14:51:37', '2022-03-30 14:51:37', NULL, 14);
INSERT INTO `barang_harga_jual_history` VALUES (1142, 35, 140000, '2022-03-30 14:54:35', '2022-03-30 14:54:35', NULL, 15);
INSERT INTO `barang_harga_jual_history` VALUES (1143, 35, 95000, '2022-03-30 14:56:38', '2022-03-30 14:56:38', NULL, 16);
INSERT INTO `barang_harga_jual_history` VALUES (1144, 35, 115000, '2022-03-30 14:57:04', '2022-03-30 14:57:04', NULL, 17);
INSERT INTO `barang_harga_jual_history` VALUES (1145, 35, 135000, '2022-03-30 14:57:38', '2022-03-30 14:57:38', NULL, 18);
INSERT INTO `barang_harga_jual_history` VALUES (1146, 35, 70000, '2022-03-30 14:58:38', '2022-03-30 14:58:38', NULL, 19);
INSERT INTO `barang_harga_jual_history` VALUES (1147, 35, 95000, '2022-03-30 15:00:03', '2022-03-30 15:00:03', NULL, 20);
INSERT INTO `barang_harga_jual_history` VALUES (1148, 35, 95000, '2022-03-30 15:00:03', '2022-04-19 16:04:22', '2022-04-19 16:04:22', 21);
INSERT INTO `barang_harga_jual_history` VALUES (1149, 35, 85000, '2022-03-30 15:01:10', '2022-03-30 15:01:10', NULL, 22);
INSERT INTO `barang_harga_jual_history` VALUES (1136, 35, 82500, '2022-03-30 15:01:16', '2022-03-30 15:01:16', NULL, 23);
INSERT INTO `barang_harga_jual_history` VALUES (1137, 35, 77000, '2022-03-30 15:01:23', '2022-03-30 15:01:23', NULL, 24);
INSERT INTO `barang_harga_jual_history` VALUES (1138, 35, 77000, '2022-03-30 15:01:30', '2022-03-30 15:01:30', NULL, 25);
INSERT INTO `barang_harga_jual_history` VALUES (1141, 35, 77000, '2022-03-30 15:01:38', '2022-03-30 15:01:38', NULL, 26);
INSERT INTO `barang_harga_jual_history` VALUES (1142, 35, 140000, '2022-03-30 15:01:47', '2022-03-30 15:01:47', NULL, 27);
INSERT INTO `barang_harga_jual_history` VALUES (1143, 35, 95000, '2022-03-30 15:01:54', '2022-03-30 15:01:54', NULL, 28);
INSERT INTO `barang_harga_jual_history` VALUES (1144, 35, 115000, '2022-03-30 15:02:03', '2022-03-30 15:02:03', NULL, 29);
INSERT INTO `barang_harga_jual_history` VALUES (1145, 35, 135000, '2022-03-30 15:02:11', '2022-03-30 15:02:11', NULL, 30);
INSERT INTO `barang_harga_jual_history` VALUES (1148, 35, 95000, '2022-03-30 15:02:21', '2022-04-19 16:04:22', '2022-04-19 16:04:22', 31);
INSERT INTO `barang_harga_jual_history` VALUES (1147, 35, 95000, '2022-03-30 15:02:31', '2022-03-30 15:02:31', NULL, 32);
INSERT INTO `barang_harga_jual_history` VALUES (1147, 35, 95000, '2022-03-30 15:02:41', '2022-03-30 15:02:41', NULL, 33);
INSERT INTO `barang_harga_jual_history` VALUES (1146, 35, 70000, '2022-03-30 15:02:49', '2022-03-30 15:02:49', NULL, 34);
INSERT INTO `barang_harga_jual_history` VALUES (1150, 35, 260000, '2022-03-30 15:03:27', '2022-03-30 15:03:27', NULL, 35);
INSERT INTO `barang_harga_jual_history` VALUES (1151, 35, 260000, '2022-03-30 15:03:27', '2022-03-30 15:03:27', NULL, 36);
INSERT INTO `barang_harga_jual_history` VALUES (1151, 35, 146000, '2022-03-30 15:04:48', '2022-03-30 15:04:48', NULL, 37);
INSERT INTO `barang_harga_jual_history` VALUES (1152, 36, 52500, '2022-03-30 15:40:01', '2022-03-30 15:40:01', NULL, 38);
INSERT INTO `barang_harga_jual_history` VALUES (1153, 35, 59000, '2022-03-30 15:40:09', '2022-03-30 15:40:09', NULL, 39);
INSERT INTO `barang_harga_jual_history` VALUES (1154, 35, 93000, '2022-03-30 15:40:38', '2022-03-30 15:40:38', NULL, 40);
INSERT INTO `barang_harga_jual_history` VALUES (1155, 36, 50000, '2022-03-30 15:40:41', '2022-03-30 15:40:41', NULL, 41);
INSERT INTO `barang_harga_jual_history` VALUES (1156, 35, 66000, '2022-03-30 15:41:19', '2022-03-30 15:41:19', NULL, 42);
INSERT INTO `barang_harga_jual_history` VALUES (1157, 36, 157500, '2022-03-30 15:41:45', '2022-03-30 15:41:45', NULL, 43);
INSERT INTO `barang_harga_jual_history` VALUES (1158, 36, 300000, '2022-03-30 15:42:16', '2022-03-30 15:42:16', NULL, 44);
INSERT INTO `barang_harga_jual_history` VALUES (1159, 35, 57500, '2022-03-30 15:47:30', '2022-03-30 15:47:30', NULL, 45);
INSERT INTO `barang_harga_jual_history` VALUES (1160, 35, 57500, '2022-03-30 15:47:30', '2022-04-14 13:44:23', '2022-04-14 13:44:23', 46);
INSERT INTO `barang_harga_jual_history` VALUES (1157, 36, 157500, '2022-03-30 15:47:57', '2022-03-30 15:47:57', NULL, 47);
INSERT INTO `barang_harga_jual_history` VALUES (1161, 35, 36000, '2022-03-30 15:49:01', '2022-03-30 15:49:01', NULL, 48);
INSERT INTO `barang_harga_jual_history` VALUES (1162, 35, 54000, '2022-03-30 15:49:35', '2022-03-30 15:49:35', NULL, 49);
INSERT INTO `barang_harga_jual_history` VALUES (1163, 35, 96500, '2022-03-30 15:50:11', '2022-03-30 15:50:11', NULL, 50);
INSERT INTO `barang_harga_jual_history` VALUES (1164, 35, 46500, '2022-03-30 15:50:37', '2022-03-30 15:50:37', NULL, 51);
INSERT INTO `barang_harga_jual_history` VALUES (1165, 35, 46500, '2022-03-30 15:51:15', '2022-03-30 15:51:15', NULL, 52);
INSERT INTO `barang_harga_jual_history` VALUES (1165, 35, 80500, '2022-03-30 15:54:07', '2022-03-30 15:54:07', NULL, 53);
INSERT INTO `barang_harga_jual_history` VALUES (1166, 35, 63000, '2022-03-30 15:54:54', '2022-03-30 15:54:54', NULL, 54);
INSERT INTO `barang_harga_jual_history` VALUES (1167, 35, 40000, '2022-03-30 15:55:36', '2022-03-30 15:55:36', NULL, 55);
INSERT INTO `barang_harga_jual_history` VALUES (1168, 35, 71500, '2022-03-30 15:56:07', '2022-03-30 15:56:07', NULL, 56);
INSERT INTO `barang_harga_jual_history` VALUES (1169, 35, 66500, '2022-03-30 15:56:27', '2022-03-30 15:56:27', NULL, 57);
INSERT INTO `barang_harga_jual_history` VALUES (1170, 35, 67500, '2022-03-30 15:56:56', '2022-03-30 15:56:56', NULL, 58);
INSERT INTO `barang_harga_jual_history` VALUES (1171, 35, 72000, '2022-03-30 15:57:41', '2022-03-30 15:57:41', NULL, 59);
INSERT INTO `barang_harga_jual_history` VALUES (1172, 36, 54000, '2022-03-30 16:08:23', '2022-03-30 16:08:23', NULL, 60);
INSERT INTO `barang_harga_jual_history` VALUES (1173, 36, 54000, '2022-03-30 16:09:40', '2022-03-30 16:09:40', NULL, 61);
INSERT INTO `barang_harga_jual_history` VALUES (1174, 36, 54000, '2022-03-30 16:10:20', '2022-03-30 16:10:20', NULL, 62);
INSERT INTO `barang_harga_jual_history` VALUES (1175, 36, 54000, '2022-03-30 16:12:41', '2022-03-30 16:12:41', NULL, 63);
INSERT INTO `barang_harga_jual_history` VALUES (1176, 36, 20000, '2022-03-30 16:15:11', '2022-03-30 16:15:11', NULL, 64);
INSERT INTO `barang_harga_jual_history` VALUES (1177, 36, 20000, '2022-03-30 16:16:39', '2022-03-30 16:16:39', NULL, 65);
INSERT INTO `barang_harga_jual_history` VALUES (1178, 36, 25000, '2022-03-30 16:19:06', '2022-03-30 16:19:06', NULL, 66);
INSERT INTO `barang_harga_jual_history` VALUES (1179, 36, 264000, '2022-03-30 16:34:12', '2022-03-30 16:34:12', NULL, 67);
INSERT INTO `barang_harga_jual_history` VALUES (1180, 36, 58000, '2022-03-30 16:41:20', '2022-03-30 16:41:20', NULL, 68);
INSERT INTO `barang_harga_jual_history` VALUES (1181, 36, 24000, '2022-03-30 16:53:22', '2022-03-30 16:53:22', NULL, 69);
INSERT INTO `barang_harga_jual_history` VALUES (1182, 36, 22000, '2022-03-30 16:55:25', '2022-03-30 16:55:25', NULL, 70);
INSERT INTO `barang_harga_jual_history` VALUES (1183, 36, 12000, '2022-03-30 16:58:20', '2022-03-30 16:58:20', NULL, 71);
INSERT INTO `barang_harga_jual_history` VALUES (1184, 36, 6000, '2022-03-31 13:25:27', '2022-03-31 13:25:27', NULL, 72);
INSERT INTO `barang_harga_jual_history` VALUES (1185, 36, 14000, '2022-03-31 13:31:53', '2022-03-31 13:31:53', NULL, 73);
INSERT INTO `barang_harga_jual_history` VALUES (1186, 36, 15000, '2022-03-31 13:33:32', '2022-03-31 13:33:32', NULL, 74);
INSERT INTO `barang_harga_jual_history` VALUES (1187, 36, 24000, '2022-03-31 13:37:25', '2022-03-31 13:37:25', NULL, 75);
INSERT INTO `barang_harga_jual_history` VALUES (1188, 36, 1000, '2022-03-31 13:39:15', '2022-03-31 13:39:15', NULL, 76);
INSERT INTO `barang_harga_jual_history` VALUES (1189, 36, 55200, '2022-03-31 13:41:05', '2022-03-31 13:41:05', NULL, 77);
INSERT INTO `barang_harga_jual_history` VALUES (1190, 36, 27000, '2022-03-31 13:42:28', '2022-03-31 13:42:28', NULL, 78);
INSERT INTO `barang_harga_jual_history` VALUES (1191, 36, 150000, '2022-03-31 14:16:53', '2022-03-31 14:16:53', NULL, 79);
INSERT INTO `barang_harga_jual_history` VALUES (1192, 36, 150000, '2022-03-31 14:18:20', '2022-03-31 14:18:20', NULL, 80);
INSERT INTO `barang_harga_jual_history` VALUES (1191, 36, 150000, '2022-03-31 14:18:41', '2022-03-31 14:18:41', NULL, 81);
INSERT INTO `barang_harga_jual_history` VALUES (1193, 36, 150000, '2022-03-31 14:19:34', '2022-03-31 14:19:34', NULL, 82);
INSERT INTO `barang_harga_jual_history` VALUES (1194, 36, 150000, '2022-03-31 14:20:24', '2022-03-31 14:20:24', NULL, 83);
INSERT INTO `barang_harga_jual_history` VALUES (1195, 36, 6500, '2022-03-31 14:44:34', '2022-03-31 14:44:34', NULL, 84);
INSERT INTO `barang_harga_jual_history` VALUES (1196, 36, 60000, '2022-03-31 14:45:40', '2022-03-31 14:45:40', NULL, 85);
INSERT INTO `barang_harga_jual_history` VALUES (1197, 36, 61500, '2022-03-31 14:46:42', '2022-03-31 14:46:42', NULL, 86);
INSERT INTO `barang_harga_jual_history` VALUES (1198, 36, 68500, '2022-03-31 14:48:14', '2022-03-31 14:48:14', NULL, 87);
INSERT INTO `barang_harga_jual_history` VALUES (1199, 36, 6500, '2022-03-31 14:49:29', '2022-03-31 14:49:29', NULL, 88);
INSERT INTO `barang_harga_jual_history` VALUES (1200, 36, 6000, '2022-03-31 14:50:52', '2022-03-31 14:50:52', NULL, 89);
INSERT INTO `barang_harga_jual_history` VALUES (1201, 36, 6000, '2022-03-31 14:52:09', '2022-03-31 14:52:09', NULL, 90);
INSERT INTO `barang_harga_jual_history` VALUES (1202, 36, 5000, '2022-03-31 14:53:42', '2022-03-31 14:53:42', NULL, 91);
INSERT INTO `barang_harga_jual_history` VALUES (1203, 36, 20000, '2022-03-31 14:54:50', '2022-03-31 14:54:50', NULL, 92);
INSERT INTO `barang_harga_jual_history` VALUES (1204, 36, 4500, '2022-03-31 14:55:53', '2022-03-31 14:55:53', NULL, 93);
INSERT INTO `barang_harga_jual_history` VALUES (1205, 36, 10000, '2022-03-31 14:57:39', '2022-03-31 14:57:39', NULL, 94);
INSERT INTO `barang_harga_jual_history` VALUES (1206, 36, 15000, '2022-03-31 14:59:41', '2022-03-31 14:59:41', NULL, 95);
INSERT INTO `barang_harga_jual_history` VALUES (1207, 36, 157000, '2022-03-31 16:07:14', '2022-03-31 16:07:14', NULL, 96);
INSERT INTO `barang_harga_jual_history` VALUES (1208, 36, 73500, '2022-03-31 16:07:51', '2022-03-31 16:07:51', NULL, 97);
INSERT INTO `barang_harga_jual_history` VALUES (1209, 36, 20000, '2022-03-31 16:10:12', '2022-03-31 16:10:12', NULL, 98);
INSERT INTO `barang_harga_jual_history` VALUES (1210, 36, 30000, '2022-03-31 16:11:30', '2022-03-31 16:11:30', NULL, 99);
INSERT INTO `barang_harga_jual_history` VALUES (1211, 36, 5000, '2022-03-31 16:12:49', '2022-03-31 16:12:49', NULL, 100);
INSERT INTO `barang_harga_jual_history` VALUES (1212, 36, 20000, '2022-03-31 16:14:10', '2022-03-31 16:14:10', NULL, 101);
INSERT INTO `barang_harga_jual_history` VALUES (1213, 36, 45000, '2022-03-31 16:15:42', '2022-03-31 16:15:42', NULL, 102);
INSERT INTO `barang_harga_jual_history` VALUES (1214, 36, 850000, '2022-03-31 16:42:13', '2022-03-31 16:42:13', NULL, 103);
INSERT INTO `barang_harga_jual_history` VALUES (1215, 36, 850000, '2022-03-31 16:43:37', '2022-03-31 16:43:37', NULL, 104);
INSERT INTO `barang_harga_jual_history` VALUES (1216, 36, 850000, '2022-03-31 16:44:48', '2022-03-31 16:44:48', NULL, 105);
INSERT INTO `barang_harga_jual_history` VALUES (1217, 36, 850000, '2022-03-31 16:45:51', '2022-03-31 16:45:51', NULL, 106);
INSERT INTO `barang_harga_jual_history` VALUES (1218, 36, 850000, '2022-03-31 16:47:02', '2022-03-31 16:47:02', NULL, 107);
INSERT INTO `barang_harga_jual_history` VALUES (1219, 36, 325000, '2022-03-31 16:49:26', '2022-03-31 16:49:26', NULL, 108);
INSERT INTO `barang_harga_jual_history` VALUES (1220, 36, 325000, '2022-03-31 16:50:40', '2022-03-31 16:50:40', NULL, 109);
INSERT INTO `barang_harga_jual_history` VALUES (1221, 36, 325000, '2022-03-31 16:51:36', '2022-03-31 16:51:36', NULL, 110);
INSERT INTO `barang_harga_jual_history` VALUES (1222, 36, 600000, '2022-03-31 16:53:02', '2022-03-31 16:53:02', NULL, 111);
INSERT INTO `barang_harga_jual_history` VALUES (1223, 36, 600000, '2022-03-31 16:54:15', '2022-03-31 16:54:15', NULL, 112);
INSERT INTO `barang_harga_jual_history` VALUES (1224, 36, 440000, '2022-03-31 16:56:39', '2022-03-31 16:56:39', NULL, 113);
INSERT INTO `barang_harga_jual_history` VALUES (1225, 36, 90000, '2022-04-02 08:37:37', '2022-04-02 08:37:37', NULL, 114);
INSERT INTO `barang_harga_jual_history` VALUES (1226, 36, 30000, '2022-04-02 08:38:12', '2022-04-02 08:56:13', '2022-04-02 08:56:13', 115);
INSERT INTO `barang_harga_jual_history` VALUES (1227, 36, 15000, '2022-04-02 08:38:55', '2022-04-02 08:38:55', NULL, 116);
INSERT INTO `barang_harga_jual_history` VALUES (1228, 36, 33000, '2022-04-02 08:40:03', '2022-04-02 08:40:03', NULL, 117);
INSERT INTO `barang_harga_jual_history` VALUES (1229, 36, 35000, '2022-04-02 09:20:56', '2022-04-02 09:20:56', NULL, 118);
INSERT INTO `barang_harga_jual_history` VALUES (1230, 36, 3750000, '2022-04-02 09:32:15', '2022-04-02 09:32:15', NULL, 119);
INSERT INTO `barang_harga_jual_history` VALUES (1231, 36, 9632600, '2022-04-02 09:38:12', '2022-04-02 09:38:12', NULL, 120);
INSERT INTO `barang_harga_jual_history` VALUES (1232, 36, 170000, '2022-04-02 09:44:16', '2022-04-02 09:44:16', NULL, 121);
INSERT INTO `barang_harga_jual_history` VALUES (1233, 36, 70000, '2022-04-02 09:47:13', '2022-04-02 09:47:13', NULL, 122);
INSERT INTO `barang_harga_jual_history` VALUES (1234, 36, 75000, '2022-04-02 09:47:59', '2022-04-02 09:47:59', NULL, 123);
INSERT INTO `barang_harga_jual_history` VALUES (1235, 36, 850000, '2022-04-12 12:04:20', '2022-04-12 12:04:20', NULL, 124);
INSERT INTO `barang_harga_jual_history` VALUES (1236, 36, 850000, '2022-04-12 12:05:40', '2022-04-12 12:05:40', NULL, 125);
INSERT INTO `barang_harga_jual_history` VALUES (1237, 35, 880000, '2022-04-13 10:44:14', '2022-04-13 11:04:01', '2022-04-13 11:04:01', 126);
INSERT INTO `barang_harga_jual_history` VALUES (1238, 35, 5200000, '2022-04-13 11:00:28', '2022-04-13 11:00:28', NULL, 127);
INSERT INTO `barang_harga_jual_history` VALUES (1239, 35, 2700000, '2022-04-13 11:00:57', '2022-04-13 11:00:57', NULL, 128);
INSERT INTO `barang_harga_jual_history` VALUES (1240, 35, 2300000, '2022-04-13 11:02:16', '2022-04-13 11:02:16', NULL, 129);
INSERT INTO `barang_harga_jual_history` VALUES (1241, 35, 528000, '2022-04-13 11:03:20', '2022-04-13 11:03:20', NULL, 130);
INSERT INTO `barang_harga_jual_history` VALUES (1242, 35, 880000, '2022-04-13 11:03:44', '2022-04-13 11:03:44', NULL, 131);
INSERT INTO `barang_harga_jual_history` VALUES (1243, 36, 70500, '2022-04-14 15:38:43', '2022-04-14 15:38:43', NULL, 132);
INSERT INTO `barang_harga_jual_history` VALUES (1142, 36, 140000, '2022-04-14 15:43:31', '2022-04-14 15:43:31', NULL, 133);
INSERT INTO `barang_harga_jual_history` VALUES (1244, 36, 78500, '2022-04-14 15:46:26', '2022-04-14 15:46:26', NULL, 134);
INSERT INTO `barang_harga_jual_history` VALUES (1245, 36, 65500, '2022-04-14 15:47:11', '2022-04-14 15:47:11', NULL, 135);
INSERT INTO `barang_harga_jual_history` VALUES (1246, 36, 0, '2022-04-14 15:49:39', '2022-04-14 15:49:39', NULL, 136);
INSERT INTO `barang_harga_jual_history` VALUES (1247, 36, 0, '2022-04-14 15:50:56', '2022-04-14 15:50:56', NULL, 137);
INSERT INTO `barang_harga_jual_history` VALUES (1248, 36, 0, '2022-04-14 15:51:32', '2022-04-14 15:51:32', NULL, 138);
INSERT INTO `barang_harga_jual_history` VALUES (1249, 36, 0, '2022-04-14 15:51:57', '2022-04-14 15:51:57', NULL, 139);
INSERT INTO `barang_harga_jual_history` VALUES (1250, 36, 0, '2022-04-14 15:52:24', '2022-04-14 15:52:24', NULL, 140);
INSERT INTO `barang_harga_jual_history` VALUES (1251, 36, 0, '2022-04-14 15:52:46', '2022-04-14 15:52:46', NULL, 141);
INSERT INTO `barang_harga_jual_history` VALUES (1252, 36, 0, '2022-04-14 15:53:16', '2022-04-14 15:53:16', NULL, 142);
INSERT INTO `barang_harga_jual_history` VALUES (1246, 36, 0, '2022-04-14 15:53:40', '2022-04-14 15:53:40', NULL, 143);
INSERT INTO `barang_harga_jual_history` VALUES (1253, 36, 0, '2022-04-14 15:54:25', '2022-04-14 15:54:25', NULL, 144);
INSERT INTO `barang_harga_jual_history` VALUES (1254, 36, 0, '2022-04-14 15:54:53', '2022-04-14 15:54:53', NULL, 145);
INSERT INTO `barang_harga_jual_history` VALUES (1255, 36, 60500, '2022-04-19 12:44:38', '2022-04-19 12:44:38', NULL, 146);
INSERT INTO `barang_harga_jual_history` VALUES (1256, 36, 68000, '2022-04-19 12:45:33', '2022-04-19 12:45:33', NULL, 147);
INSERT INTO `barang_harga_jual_history` VALUES (1257, 36, 64000, '2022-04-19 12:46:07', '2022-04-19 12:46:07', NULL, 148);
INSERT INTO `barang_harga_jual_history` VALUES (1258, 36, 75500, '2022-04-19 12:46:42', '2022-04-19 12:46:42', NULL, 149);
INSERT INTO `barang_harga_jual_history` VALUES (1259, 36, 64000, '2022-04-19 12:47:58', '2022-04-19 12:47:58', NULL, 150);
INSERT INTO `barang_harga_jual_history` VALUES (1260, 36, 60000, '2022-04-19 12:48:26', '2022-04-19 12:48:26', NULL, 151);
INSERT INTO `barang_harga_jual_history` VALUES (1143, 36, 95000, '2022-04-19 13:56:33', '2022-04-19 13:56:33', NULL, 152);
INSERT INTO `barang_harga_jual_history` VALUES (1261, 36, 1100, '2022-04-19 14:05:13', '2022-04-19 14:05:13', NULL, 153);
INSERT INTO `barang_harga_jual_history` VALUES (1262, 36, 20000, '2022-04-19 14:06:25', '2022-04-19 14:06:25', NULL, 154);
INSERT INTO `barang_harga_jual_history` VALUES (1263, 36, 0, '2022-04-19 14:08:17', '2022-04-19 14:08:17', NULL, 155);
INSERT INTO `barang_harga_jual_history` VALUES (1264, 36, 5000, '2022-04-19 14:09:31', '2022-04-19 14:09:31', NULL, 156);
INSERT INTO `barang_harga_jual_history` VALUES (1265, 36, 12500, '2022-04-19 14:10:18', '2022-04-19 14:10:18', NULL, 157);
INSERT INTO `barang_harga_jual_history` VALUES (1266, 36, 20000, '2022-04-19 14:10:49', '2022-04-19 14:10:49', NULL, 158);
INSERT INTO `barang_harga_jual_history` VALUES (1267, 36, 31500, '2022-04-19 14:11:19', '2022-04-19 14:11:19', NULL, 159);
INSERT INTO `barang_harga_jual_history` VALUES (1268, 36, 146000, '2022-04-19 14:45:45', '2022-04-19 14:47:38', '2022-04-19 14:47:38', 160);
INSERT INTO `barang_harga_jual_history` VALUES (1269, 35, 280000, '2022-04-22 08:29:17', '2022-04-22 08:29:17', NULL, 161);
INSERT INTO `barang_harga_jual_history` VALUES (1270, 35, 100000, '2022-04-22 08:29:45', '2022-04-22 08:29:45', NULL, 162);
INSERT INTO `barang_harga_jual_history` VALUES (1271, 35, 150000, '2022-04-22 08:30:12', '2022-04-22 08:30:12', NULL, 163);
INSERT INTO `barang_harga_jual_history` VALUES (1272, 35, 175000, '2022-04-22 08:30:37', '2022-04-22 08:30:37', NULL, 164);
INSERT INTO `barang_harga_jual_history` VALUES (1273, 35, 6500, '2022-04-22 08:31:31', '2022-04-22 08:31:31', NULL, 165);
INSERT INTO `barang_harga_jual_history` VALUES (1274, 35, 10000, '2022-04-22 08:31:56', '2022-04-22 08:31:56', NULL, 166);
INSERT INTO `barang_harga_jual_history` VALUES (1275, 35, 10000, '2022-04-22 08:32:56', '2022-04-22 08:32:56', NULL, 167);
INSERT INTO `barang_harga_jual_history` VALUES (1276, 35, 25000, '2022-04-22 08:33:35', '2022-04-22 08:33:35', NULL, 168);
INSERT INTO `barang_harga_jual_history` VALUES (1277, 35, 23000, '2022-04-22 08:34:49', '2022-04-22 08:34:49', NULL, 169);
INSERT INTO `barang_harga_jual_history` VALUES (1278, 35, 5000, '2022-04-22 08:35:53', '2022-04-22 08:35:53', NULL, 170);
INSERT INTO `barang_harga_jual_history` VALUES (1279, 35, 2000, '2022-04-22 08:38:05', '2022-04-22 08:38:05', NULL, 171);
INSERT INTO `barang_harga_jual_history` VALUES (1280, 35, 2000, '2022-04-22 08:39:02', '2022-04-22 08:39:02', NULL, 172);
INSERT INTO `barang_harga_jual_history` VALUES (1281, 35, 6000, '2022-04-22 08:39:49', '2022-04-22 08:39:49', NULL, 173);
INSERT INTO `barang_harga_jual_history` VALUES (1282, 35, 6500, '2022-04-22 08:40:18', '2022-04-22 08:40:18', NULL, 174);
INSERT INTO `barang_harga_jual_history` VALUES (1283, 35, 15000, '2022-04-22 08:41:06', '2022-04-22 08:41:06', NULL, 175);
INSERT INTO `barang_harga_jual_history` VALUES (1284, 35, 45000, '2022-04-22 08:41:58', '2022-04-22 08:41:58', NULL, 176);
INSERT INTO `barang_harga_jual_history` VALUES (1285, 35, 1100, '2022-04-22 08:42:16', '2022-04-22 08:42:16', NULL, 177);
INSERT INTO `barang_harga_jual_history` VALUES (1286, 35, 25000, '2022-04-22 08:42:46', '2022-04-22 09:29:32', '2022-04-22 09:29:32', 178);
INSERT INTO `barang_harga_jual_history` VALUES (1287, 35, 72000, '2022-04-22 08:43:04', '2022-04-22 08:43:04', NULL, 179);
INSERT INTO `barang_harga_jual_history` VALUES (1288, 35, 13000, '2022-04-22 08:43:29', '2022-04-22 08:43:29', NULL, 180);
INSERT INTO `barang_harga_jual_history` VALUES (1289, 35, 155000, '2022-04-22 08:44:24', '2022-04-22 08:44:24', NULL, 181);
INSERT INTO `barang_harga_jual_history` VALUES (1290, 35, 100000, '2022-04-22 08:44:55', '2022-04-22 08:44:55', NULL, 182);
INSERT INTO `barang_harga_jual_history` VALUES (1291, 35, 25000, '2022-04-22 08:45:35', '2022-04-22 08:45:35', NULL, 183);
INSERT INTO `barang_harga_jual_history` VALUES (1292, 35, 35000, '2022-04-22 08:46:12', '2022-04-22 08:46:12', NULL, 184);
INSERT INTO `barang_harga_jual_history` VALUES (1293, 35, 6000, '2022-04-22 09:26:04', '2022-04-22 09:27:35', '2022-04-22 09:27:35', 185);
COMMIT;

-- ----------------------------
-- Table structure for history_penyesuaian_stok
-- ----------------------------
DROP TABLE IF EXISTS `history_penyesuaian_stok`;
CREATE TABLE `history_penyesuaian_stok` (
  `id_history_penyesuaian_stok` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang` int(11) DEFAULT NULL,
  `id_stok_barang` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `hps_qty_awal` decimal(20,0) DEFAULT NULL,
  `hps_qty_akhir` decimal(20,0) DEFAULT NULL,
  `hps_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_history_penyesuaian_stok`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of history_penyesuaian_stok
-- ----------------------------
BEGIN;
INSERT INTO `history_penyesuaian_stok` VALUES (37, 1150, 0, 36, 0, 100, '-', '2022-04-14 13:51:36', '2022-04-14 13:51:36', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (38, 1167, 0, 36, 0, 100, '-', '2022-04-14 13:51:59', '2022-04-14 13:51:59', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (39, 1171, 0, 36, 0, 100, '-', '2022-04-14 13:52:19', '2022-04-14 13:52:19', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (40, 1170, 0, 36, 0, 100, '-', '2022-04-14 13:52:36', '2022-04-14 13:52:36', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (41, 1169, 0, 36, 0, 100, '-', '2022-04-14 13:52:55', '2022-04-14 13:52:55', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (42, 1168, 0, 36, 0, 12, '-', '2022-04-14 13:53:11', '2022-04-14 13:53:11', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (43, 1166, 0, 36, 0, 100, '-', '2022-04-14 13:53:44', '2022-04-14 13:53:44', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (44, 1165, 0, 36, 0, 100, '-', '2022-04-14 13:54:00', '2022-04-14 13:54:00', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (45, 1164, 0, 36, 0, 100, '-', '2022-04-14 13:54:15', '2022-04-14 13:54:15', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (46, 1163, 0, 36, 0, 100, '-', '2022-04-14 13:54:29', '2022-04-14 13:54:29', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (47, 1162, 0, 36, 0, 100, '-', '2022-04-14 13:54:41', '2022-04-14 13:54:41', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (48, 1161, 0, 36, 0, 100, '-', '2022-04-14 13:54:58', '2022-04-14 13:54:58', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (49, 1159, 0, 36, 0, 100, '-', '2022-04-14 13:55:13', '2022-04-14 13:55:13', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (50, 1149, 0, 36, 0, 600, '-', '2022-04-14 13:55:31', '2022-04-14 13:55:31', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (51, 1153, 0, 36, 0, 100, '-', '2022-04-14 13:56:08', '2022-04-14 13:56:08', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (52, 1154, 0, 36, 0, 200, '-', '2022-04-14 13:56:24', '2022-04-14 13:56:24', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (53, 1156, 0, 36, 0, 200, '-', '2022-04-14 13:56:44', '2022-04-14 13:56:44', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (54, 1145, 0, 36, 0, 600, '-', '2022-04-14 14:59:26', '2022-04-14 14:59:26', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (55, 1144, 0, 36, 0, 600, '-', '2022-04-14 14:59:50', '2022-04-14 14:59:50', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (56, 1143, 0, 36, 0, 600, '-', '2022-04-14 15:00:03', '2022-04-14 15:00:03', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (57, 1142, 0, 36, 0, 600, '-', '2022-04-14 15:00:16', '2022-04-14 15:00:16', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (58, 1261, 0, 36, 0, 50, '-', '2022-04-19 14:18:52', '2022-04-19 14:18:52', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (59, 1262, 0, 36, 0, 5, '-', '2022-04-19 14:19:13', '2022-04-19 14:19:13', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (60, 1263, 0, 36, 0, 2, '-', '2022-04-19 14:19:28', '2022-04-19 14:19:28', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (61, 1264, 0, 36, 0, 2, '-', '2022-04-19 14:19:44', '2022-04-19 14:19:44', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (62, 1267, 0, 36, 0, 1, '-', '2022-04-19 14:20:02', '2022-04-19 14:20:02', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (63, 1266, 0, 36, 0, 1, '-', '2022-04-19 14:20:12', '2022-04-19 14:20:12', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (64, 1265, 0, 36, 0, 1, '-', '2022-04-19 14:20:24', '2022-04-19 14:20:24', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (65, 1292, 0, 35, 0, 1, '-', '2022-04-22 08:46:41', '2022-04-22 08:46:41', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (66, 1291, 0, 35, 0, 5, '-', '2022-04-22 08:46:52', '2022-04-22 08:46:52', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (67, 1290, 0, 35, 0, 1, '-', '2022-04-22 08:47:04', '2022-04-22 08:47:04', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (68, 1289, 0, 35, 0, 1, '-', '2022-04-22 08:47:13', '2022-04-22 08:47:13', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (69, 1288, 0, 35, 0, 9, '-', '2022-04-22 08:47:22', '2022-04-22 08:47:22', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (70, 1287, 0, 35, 0, 1, '-', '2022-04-22 08:47:29', '2022-04-22 08:47:29', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (71, 1286, 0, 35, 0, 2, '-', '2022-04-22 08:47:38', '2022-04-22 08:47:38', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (72, 1285, 0, 35, 0, 17, '-', '2022-04-22 08:47:45', '2022-04-22 08:47:45', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (73, 1284, 0, 35, 0, 1, '-', '2022-04-22 08:47:53', '2022-04-22 08:47:53', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (74, 1283, 0, 35, 0, 110, '-', '2022-04-22 08:49:22', '2022-04-22 08:49:22', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (75, 1282, 0, 35, 0, 110, '-', '2022-04-22 08:49:37', '2022-04-22 08:49:37', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (76, 1281, 0, 35, 0, 110, '-', '2022-04-22 08:49:48', '2022-04-22 08:49:48', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (77, 1280, 0, 35, 0, 1, '-', '2022-04-22 08:50:04', '2022-04-22 08:50:04', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (78, 1279, 0, 35, 0, 55, '-', '2022-04-22 08:50:28', '2022-04-22 08:50:28', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (79, 1278, 0, 35, 0, 55, '-', '2022-04-22 08:50:50', '2022-04-22 08:50:50', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (80, 1274, 0, 35, 0, 109, '-', '2022-04-22 08:51:12', '2022-04-22 08:51:12', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (81, 1275, 0, 35, 0, 109, '-', '2022-04-22 08:51:29', '2022-04-22 08:51:29', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (82, 1276, 0, 35, 0, 110, '-', '2022-04-22 08:51:45', '2022-04-22 08:51:45', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (83, 1277, 0, 35, 0, 164, '-', '2022-04-22 08:52:19', '2022-04-22 08:52:19', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (84, 1272, 0, 35, 0, 1, '-', '2022-04-22 08:52:40', '2022-04-22 08:52:40', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (85, 1272, 0, 35, 0, 1, '-', '2022-04-22 08:52:40', '2022-04-22 08:52:40', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (86, 1271, 0, 35, 0, 1, '-', '2022-04-22 08:52:54', '2022-04-22 08:52:54', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (87, 1270, 0, 35, 0, 1, '-', '2022-04-22 08:53:10', '2022-04-22 08:53:10', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (88, 1269, 0, 35, 0, 2, '-', '2022-04-22 08:53:29', '2022-04-22 08:53:29', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (89, 1199, 1178, 36, 100, 557, '-', '2022-04-22 08:56:44', '2022-04-22 08:56:44', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (90, 1199, 1178, 36, 557, 657, '-', '2022-04-22 08:59:29', '2022-04-22 08:59:29', NULL);
INSERT INTO `history_penyesuaian_stok` VALUES (91, 1205, 1184, 35, 15, 55, '-', '2022-04-22 09:28:55', '2022-04-22 09:28:55', NULL);
COMMIT;

-- ----------------------------
-- Table structure for hutang_lain
-- ----------------------------
DROP TABLE IF EXISTS `hutang_lain`;
CREATE TABLE `hutang_lain` (
  `id_hutang_lain` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) DEFAULT NULL,
  `hul_no_faktur` varchar(25) DEFAULT NULL,
  `hul_tanggal` date DEFAULT NULL,
  `hul_tanggal_jatuh_tempo` date DEFAULT NULL,
  `hul_total` decimal(20,2) DEFAULT NULL,
  `hul_sisa` decimal(20,2) DEFAULT NULL,
  `hul_keterangan` text DEFAULT NULL,
  `hul_status` enum('lunas','belum_lunas') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_hutang_lain`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hutang_lain_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `hutang_lain_pembayaran`;
CREATE TABLE `hutang_lain_pembayaran` (
  `id_hutang_lain_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_hutang_lain` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `hlp_total_hutang` varchar(255) DEFAULT NULL,
  `hlp_jumlah_bayar` decimal(20,0) DEFAULT NULL,
  `hlp_sisa_bayar` varchar(255) DEFAULT NULL,
  `hlp_tanggal_bayar` date DEFAULT NULL,
  `hlp_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_hutang_lain_pembayaran`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hutang_supplier
-- ----------------------------
DROP TABLE IF EXISTS `hutang_supplier`;
CREATE TABLE `hutang_supplier` (
  `id_hutang_supplier` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `hsp_no_faktur` varchar(25) DEFAULT NULL,
  `hsp_tanggal` date DEFAULT NULL,
  `hsp_tanggal_jatuh_tempo` date DEFAULT NULL,
  `hsp_total` decimal(20,2) DEFAULT NULL,
  `hsp_sisa` decimal(20,2) DEFAULT NULL,
  `hsp_keterangan` text DEFAULT NULL,
  `hsp_status` enum('lunas','belum_lunas') DEFAULT 'belum_lunas',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_hutang_supplier`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hutang_supplier
-- ----------------------------
BEGIN;
INSERT INTO `hutang_supplier` VALUES (47, 46, 394, 'HSP/TI/2022/03/001', '2022-03-29', '2022-03-29', 0.00, 0.00, 'Hutang Supplier #TI dengan no faktur PBL/TI/2022/03/001', 'belum_lunas', '2022-03-30 14:38:23', '2022-03-30 14:53:43', NULL);
INSERT INTO `hutang_supplier` VALUES (48, 46, 395, 'HSP/TI/2022/03/002', '2022-03-29', '2022-03-29', 0.00, 0.00, 'Hutang Supplier #TI dengan no faktur PBL/TI/2022/03/002', 'belum_lunas', '2022-03-30 14:48:40', '2022-03-30 14:53:14', NULL);
INSERT INTO `hutang_supplier` VALUES (49, 51, 401, 'HSP/CSA/2022/03/003', '2022-03-25', '2022-03-25', 241000.00, 241000.00, 'Hutang Supplier #CSA dengan no faktur PBL/CSA/2022/03/008', 'belum_lunas', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `hutang_supplier` VALUES (50, 53, 406, 'HSP/BPN/2022/03/004', '2022-02-26', '2022-04-09', 11063000.00, 11063000.00, 'Hutang Supplier #BPN dengan no faktur PBL/BPN/2022/03/013', 'belum_lunas', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `hutang_supplier` VALUES (51, 52, 409, 'HSP/BE/2022/04/001', '2022-02-15', '2022-04-02', 1255000.00, 1255000.00, 'Hutang Supplier #BE dengan no faktur PBL/BE/2022/04/001', 'belum_lunas', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `hutang_supplier` VALUES (52, 53, 424, 'HSP/BPN/2022/04/001', '2022-04-09', '2022-04-30', 7600000.00, 7600000.00, 'Hutang Supplier #BPN dengan no faktur PBL/BPN/2022/04/001', 'belum_lunas', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `hutang_supplier` VALUES (53, 62, 426, 'HSP/YW/2022/04/002', '2022-04-08', '2022-05-14', 11657880.00, 11657880.00, 'Hutang Supplier #YW dengan no faktur PBL/YW/2022/04/002', 'belum_lunas', '2022-04-14 15:40:38', '2022-04-14 15:40:38', NULL);
INSERT INTO `hutang_supplier` VALUES (54, 62, 427, 'HSP/YW/2022/04/003', '2022-02-14', '2022-05-31', 22730000.00, 22730000.00, 'Hutang Supplier #YW dengan no faktur PBL/YW/2022/04/003', 'belum_lunas', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `hutang_supplier` VALUES (55, 62, 428, 'HSP/YW/2022/04/003', '2022-02-25', '2022-05-31', 2436000.00, 2436000.00, 'Hutang Supplier #YW dengan no faktur PBL/YW/2022/04/003', 'belum_lunas', '2022-04-19 13:00:15', '2022-04-19 13:00:15', NULL);
INSERT INTO `hutang_supplier` VALUES (56, 62, 429, 'HSP/YW/2022/04/003', '2022-02-25', '2022-05-31', 4176000.00, 4176000.00, 'Hutang Supplier #YW dengan no faktur PBL/YW/2022/04/003', 'belum_lunas', '2022-04-19 13:01:52', '2022-04-19 13:01:52', NULL);
INSERT INTO `hutang_supplier` VALUES (57, 62, 430, 'HSP/YW/2022/04/003', '2022-03-09', '2022-05-31', 3525000.00, 3525000.00, 'Hutang Supplier #YW dengan no faktur PBL/YW/2022/04/003', 'belum_lunas', '2022-04-19 14:48:37', '2022-04-19 14:48:37', NULL);
INSERT INTO `hutang_supplier` VALUES (58, 62, 431, 'HSP/YW/2022/04/003', '2022-03-09', '2022-05-31', 76000000.00, 76000000.00, 'Hutang Supplier #YW dengan no faktur PBL/YW/2022/04/003', 'belum_lunas', '2022-04-19 16:20:56', '2022-04-19 16:20:56', NULL);
INSERT INTO `hutang_supplier` VALUES (59, 50, 432, 'HSP/NM/2022/04/003', '2022-04-18', '2022-04-18', 601800.00, 601800.00, 'Hutang Supplier #NM dengan no faktur PBL/NM/2022/04/003', 'belum_lunas', '2022-04-22 09:20:16', '2022-04-22 09:20:16', NULL);
COMMIT;

-- ----------------------------
-- Table structure for hutang_supplier_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `hutang_supplier_pembayaran`;
CREATE TABLE `hutang_supplier_pembayaran` (
  `id_hutang_supplier_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_hutang_supplier` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `hsp_total_hutang` decimal(20,2) DEFAULT NULL,
  `hsp_jumlah_bayar` decimal(20,2) DEFAULT NULL,
  `hsp_sisa_bayar` decimal(20,2) DEFAULT NULL,
  `hsp_tanggal_bayar` date DEFAULT NULL,
  `hsp_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_hutang_supplier_pembayaran`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hutang_supplier_pembayaran
-- ----------------------------
BEGIN;
INSERT INTO `hutang_supplier_pembayaran` VALUES (13, 48, 36, 270000.00, 0.00, 270000.00, '2022-03-29', 'CASH', '2022-03-30 14:49:57', '2022-03-30 14:49:57', NULL);
INSERT INTO `hutang_supplier_pembayaran` VALUES (14, 47, 36, 230000.00, 0.00, 230000.00, '2022-03-29', 'CASH', '2022-03-30 14:50:18', '2022-03-30 14:50:18', NULL);
COMMIT;

-- ----------------------------
-- Table structure for jenis_barang
-- ----------------------------
DROP TABLE IF EXISTS `jenis_barang`;
CREATE TABLE `jenis_barang` (
  `id_jenis_barang` int(11) NOT NULL AUTO_INCREMENT,
  `jbr_nama` varchar(255) DEFAULT NULL,
  `jbr_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jenis_barang`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of jenis_barang
-- ----------------------------
BEGIN;
INSERT INTO `jenis_barang` VALUES (27, 'Alat Peraga', '-', '2022-03-25 10:00:37', '2022-03-25 10:00:37', NULL);
INSERT INTO `jenis_barang` VALUES (28, 'Buku Teks Utama SD', '-', '2022-03-25 10:00:56', '2022-03-25 10:01:19', NULL);
INSERT INTO `jenis_barang` VALUES (29, 'Buku Teks Pendamping SD', NULL, '2022-03-25 10:01:12', '2022-03-25 10:01:12', NULL);
INSERT INTO `jenis_barang` VALUES (30, 'Buku Olimpiade SD', NULL, '2022-03-25 10:01:48', '2022-03-25 10:01:48', NULL);
INSERT INTO `jenis_barang` VALUES (31, 'Buku Bahasa Bali SD', NULL, '2022-03-25 10:02:33', '2022-03-25 10:02:33', NULL);
INSERT INTO `jenis_barang` VALUES (32, 'Buku Bhs Inggris SD', NULL, '2022-03-25 10:02:42', '2022-03-25 10:02:42', NULL);
INSERT INTO `jenis_barang` VALUES (33, 'Buku Teks Utama SMP', NULL, '2022-03-25 10:06:13', '2022-03-25 10:06:13', NULL);
INSERT INTO `jenis_barang` VALUES (34, 'Buku Teks Pendamping SMP', NULL, '2022-03-25 10:06:29', '2022-03-25 10:06:29', NULL);
INSERT INTO `jenis_barang` VALUES (35, 'Buku Bahasa Bali SMP', NULL, '2022-03-25 10:06:49', '2022-03-25 10:06:49', NULL);
INSERT INTO `jenis_barang` VALUES (36, 'Buku Olimpiade SMP', NULL, '2022-03-25 10:07:05', '2022-03-25 10:07:05', NULL);
INSERT INTO `jenis_barang` VALUES (37, 'Buku Teks Utama SMA', NULL, '2022-03-25 10:26:28', '2022-03-25 10:26:28', NULL);
INSERT INTO `jenis_barang` VALUES (38, 'Buku Teks Pendamping SMA', NULL, '2022-03-25 10:26:42', '2022-03-25 10:26:42', NULL);
INSERT INTO `jenis_barang` VALUES (39, 'Buku Olimpiade SMA', NULL, '2022-03-25 10:27:00', '2022-03-25 10:27:00', NULL);
INSERT INTO `jenis_barang` VALUES (40, 'Printer', NULL, '2022-03-25 11:11:07', '2022-03-25 11:11:07', NULL);
INSERT INTO `jenis_barang` VALUES (41, 'Alat Kebersihan', NULL, '2022-03-25 11:12:57', '2022-03-25 11:12:57', NULL);
INSERT INTO `jenis_barang` VALUES (42, 'Meubeler', NULL, '2022-03-25 11:15:21', '2022-03-25 11:15:21', NULL);
INSERT INTO `jenis_barang` VALUES (43, 'Baterai', NULL, '2022-03-30 14:23:07', '2022-03-30 14:32:18', NULL);
INSERT INTO `jenis_barang` VALUES (44, 'Minyak', NULL, '2022-03-30 14:23:42', '2022-03-30 14:32:35', NULL);
INSERT INTO `jenis_barang` VALUES (45, 'Buku BANK SOAL SMP', '-', '2022-03-30 15:07:22', '2022-03-30 15:07:22', NULL);
INSERT INTO `jenis_barang` VALUES (46, 'APE Luar', NULL, '2022-03-30 15:28:19', '2022-03-30 15:28:19', NULL);
INSERT INTO `jenis_barang` VALUES (47, 'APE Dalam', '-', '2022-03-30 15:47:36', '2022-03-30 15:47:36', NULL);
INSERT INTO `jenis_barang` VALUES (48, 'Pengharum Ruangan', '-', '2022-03-30 16:06:28', '2022-03-30 16:06:28', NULL);
INSERT INTO `jenis_barang` VALUES (49, 'P3K', '-', '2022-03-30 16:51:54', '2022-03-30 16:51:54', NULL);
INSERT INTO `jenis_barang` VALUES (50, 'Tinta', '-', '2022-03-31 14:14:23', '2022-03-31 14:14:23', NULL);
INSERT INTO `jenis_barang` VALUES (51, 'Kertas', '-', '2022-03-31 14:41:29', '2022-03-31 14:41:29', NULL);
INSERT INTO `jenis_barang` VALUES (52, 'Alat Tulis', '-', '2022-03-31 14:42:49', '2022-03-31 14:42:49', NULL);
INSERT INTO `jenis_barang` VALUES (53, 'Buku Tulis', '-', '2022-03-31 14:48:41', '2022-03-31 14:48:41', NULL);
INSERT INTO `jenis_barang` VALUES (54, 'Map', '-', '2022-03-31 14:52:28', '2022-03-31 14:52:28', NULL);
INSERT INTO `jenis_barang` VALUES (55, 'Gunting', '-', '2022-03-31 14:56:31', '2022-03-31 14:56:31', NULL);
INSERT INTO `jenis_barang` VALUES (56, 'Pencegah Covid', '-', '2022-04-02 09:18:24', '2022-04-02 09:18:24', NULL);
INSERT INTO `jenis_barang` VALUES (57, 'Laptop dan Komputer', '-', '2022-04-02 09:36:00', '2022-04-02 09:36:00', NULL);
INSERT INTO `jenis_barang` VALUES (58, 'Novel', '-', '2022-04-14 15:47:54', '2022-04-14 15:47:54', NULL);
INSERT INTO `jenis_barang` VALUES (59, 'Bank Soal SD', '-', '2022-04-19 12:36:08', '2022-04-19 12:36:08', NULL);
INSERT INTO `jenis_barang` VALUES (60, 'TISU', '-', '2022-04-19 14:05:44', '2022-04-19 14:05:44', NULL);
INSERT INTO `jenis_barang` VALUES (61, 'SABUN', '-', '2022-04-19 14:07:02', '2022-04-19 14:07:02', NULL);
INSERT INTO `jenis_barang` VALUES (62, 'CLIP', '-', '2022-04-19 14:08:56', '2022-04-19 14:08:56', NULL);
COMMIT;

-- ----------------------------
-- Table structure for pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `plg_kode` varchar(30) DEFAULT NULL,
  `plg_nama` varchar(255) DEFAULT NULL,
  `plg_alamat` text DEFAULT NULL,
  `plg_telepon` varchar(50) DEFAULT NULL,
  `plg_email` varchar(150) DEFAULT NULL,
  `plg_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pelanggan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pelanggan
-- ----------------------------
BEGIN;
INSERT INTO `pelanggan` VALUES (2, 'MRG', 'SDN 1 MARGA DAUH PURI', 'Marga Dauh Puri Tabanan, Marga, Kec. Marga, Kab. Tabanan, Prov. Bali', '081339564489', '-', '-', '2022-04-13 10:46:22', '2022-04-13 10:46:22', NULL);
INSERT INTO `pelanggan` VALUES (3, 'PNB', 'SDN 1 PENATAHAN', 'PENATAHAN, Kec. Penebel, Kab. Tabanan, Prov. Bali', '087862288538', '-', '-', '2022-04-13 15:41:32', '2022-04-13 15:41:32', NULL);
INSERT INTO `pelanggan` VALUES (4, 'PNB', 'SDN 2 PENATAHAN', 'SANGKETAN, Kec. Penebel, Kab. Tabanan, Prov. Bali', '03613620578', '-', '-', '2022-04-13 15:43:19', '2022-04-13 15:43:19', NULL);
INSERT INTO `pelanggan` VALUES (5, 'PNB', 'SDN 1 JATILUWIH', 'JATILUWIH, Kec. Penebel, Kab. Tabanan, Prov. Bali', '081999542930', '-', '-', '2022-04-13 15:44:11', '2022-04-13 15:44:11', NULL);
INSERT INTO `pelanggan` VALUES (6, 'PNB', 'SDN 6 PENATAHAN', 'Banjar Dinas Pekandelan, SANGKETAN, Kec. Penebel, Kab. Tabanan, Prov. Bali', '081337721879', '-', '-', '2022-04-13 15:45:36', '2022-04-13 15:45:36', NULL);
INSERT INTO `pelanggan` VALUES (7, 'MRG', 'SDN 3 KUKUH', 'Kukuh Desa Tegal, Kukuh, Kec. Marga, Kab. Tabanan, Prov. Bali', '081246545555', '-', '-', '2022-04-13 15:48:56', '2022-04-13 15:48:56', NULL);
INSERT INTO `pelanggan` VALUES (8, 'KDR', 'SMP NEGERI 1 KEDIRI', 'Jl. Taruna Jaya No.25, Kediri, Kec. Kediri, Kabupaten Tabanan', '-', '-', '-', '2022-04-14 13:25:34', '2022-04-14 13:25:34', NULL);
INSERT INTO `pelanggan` VALUES (9, 'PPN', 'SMP NEGERI 2 PUPUAN', 'Jl. Antosari Pupuan, Belimbing, Kec. Pupuan, Kabupaten Tabanan', '-', '-', '-', '2022-04-14 15:12:53', '2022-04-14 15:12:53', NULL);
INSERT INTO `pelanggan` VALUES (10, 'PPN', 'SMP NEGERI 4 PUPUAN', 'Padangan, Kec. Pupuan, Kabupaten Tabanan, Bali 82163', '-', '-', '-', '2022-04-14 15:15:45', '2022-04-14 15:30:17', NULL);
INSERT INTO `pelanggan` VALUES (11, 'PPN', 'SMP NEGERI 6 PUPUAN', 'Pujungan, Kec. Pupuan, Kabupaten Tabanan, Bali 82163', '-', '-', '-', '2022-04-14 15:20:01', '2022-04-14 15:20:01', NULL);
INSERT INTO `pelanggan` VALUES (12, 'PPN', 'SMP NEGERI 5 PUPUAN', 'Munduk Temu, Kec. Pupuan, Kabupaten Tabanan, Bali 82163', '-', '-', '-', '2022-04-14 15:22:38', '2022-04-14 15:22:38', NULL);
INSERT INTO `pelanggan` VALUES (13, 'PPN', 'SMP NEGERI 3 PUPUAN', 'Jl. Saraswati, Bajera, Kec. Selemadeg, Kabupaten Tabanan, Bali 82162', '-', '-', '-', '2022-04-14 15:26:20', '2022-04-14 15:26:20', NULL);
INSERT INTO `pelanggan` VALUES (14, 'PG', 'PAK GUNAWAN', 'DENPASAR', '0813-3177-3824', '-', 'CV WIGUNA', '2022-04-14 15:31:35', '2022-04-14 15:31:35', NULL);
INSERT INTO `pelanggan` VALUES (15, 'WK', 'TK WIDYA KUMARA', 'BATUNYA, BATURITI', '-', '-', '-', '2022-04-19 11:53:48', '2022-04-19 11:53:48', NULL);
INSERT INTO `pelanggan` VALUES (16, 'PB', 'TK PANCARAN BERKAT', 'BATURITI, BATURITI', '-', '-', '-', '2022-04-19 11:54:21', '2022-04-19 11:54:21', NULL);
INSERT INTO `pelanggan` VALUES (17, 'WD1', 'TK WIDYA DHARMA I', 'APUAN, BATURITI', '-', '-', '-', '2022-04-19 11:54:51', '2022-04-19 11:54:51', NULL);
INSERT INTO `pelanggan` VALUES (18, 'WKV', 'TK WIJA KUSUMA V', 'PEREAN TENGAH, BATURITI', '-', '-', '-', '2022-04-19 11:55:38', '2022-04-19 11:55:38', NULL);
INSERT INTO `pelanggan` VALUES (19, 'WKII', 'TK WIJA KUSUMA II', 'PEREAN TENGAH, BATURITI', '-', '-', '-', '2022-04-19 11:56:06', '2022-04-19 11:56:06', NULL);
INSERT INTO `pelanggan` VALUES (20, 'KWG', 'TK KUSA WAHANA GIRI', 'BANGLI, BATURITI', '-', '-', '-', '2022-04-19 11:56:50', '2022-04-19 11:56:50', NULL);
INSERT INTO `pelanggan` VALUES (21, 'KB', 'TK KUMARA BHUANA', 'LUWUS, BATURITI', '-', '-', '-', '2022-04-19 11:57:21', '2022-04-19 11:57:21', NULL);
INSERT INTO `pelanggan` VALUES (22, 'GPII', 'TK GIRI PUTRA II', 'ANGSERI, BATURITI', '-', '-', '-', '2022-04-19 11:57:49', '2022-04-19 11:57:49', NULL);
INSERT INTO `pelanggan` VALUES (23, 'DEL', 'TK DWI EKA LAKSANA', 'BANGLI, BATURITI', '-', '-', '-', '2022-04-19 11:58:16', '2022-04-19 11:58:16', NULL);
INSERT INTO `pelanggan` VALUES (24, 'BS', 'TK BINA SASTRA', 'BATUNYA, BATURITI', '-', '-', '-', '2022-04-19 12:09:29', '2022-04-19 12:09:29', NULL);
INSERT INTO `pelanggan` VALUES (25, 'BP1', 'TK BINA PUTRA I', 'CANDIKUNING, BATURITI', '-', '-', '-', '2022-04-19 12:10:29', '2022-04-19 12:10:29', NULL);
INSERT INTO `pelanggan` VALUES (26, 'PH', 'PAUD PRAJA HITHA', 'PEREAN, BATURITI', '-', '-', '-', '2022-04-19 12:12:25', '2022-04-19 12:12:25', NULL);
INSERT INTO `pelanggan` VALUES (27, 'KBPB', 'KB PANCARAN BERKAT', 'BATURITI, BATURITI', '-', '-', '-', '2022-04-19 12:13:57', '2022-04-19 12:13:57', NULL);
INSERT INTO `pelanggan` VALUES (28, 'KS', 'KB KUMARA SARI', 'PEREAN TENGAH, BATURITI', '-', '-', '-', '2022-04-19 12:17:02', '2022-04-19 12:17:02', NULL);
INSERT INTO `pelanggan` VALUES (29, 'WG', 'PAUD WIDIA GUNA', 'BATUNYA, BATURITI', '-', '-', '-', '2022-04-19 12:17:29', '2022-04-19 12:17:29', NULL);
INSERT INTO `pelanggan` VALUES (30, 'TM', 'TK TUNAS MEKAR', 'ANTAPAN, BATURITI', '-', '-', '-', '2022-04-19 12:17:53', '2022-04-19 12:17:53', NULL);
INSERT INTO `pelanggan` VALUES (31, 'SDK', 'TK STHANA DHARMA KUMARA', 'LUWUS, BATURITI', '-', '-', '-', '2022-04-19 12:18:21', '2022-04-19 12:18:21', NULL);
INSERT INTO `pelanggan` VALUES (32, 'GPIII', 'TK GIRI PUTRA III', 'ANGSERI, BATURITI', '-', '-', '-', '2022-04-19 12:19:10', '2022-04-19 12:19:10', NULL);
INSERT INTO `pelanggan` VALUES (33, 'GPI', 'TK GIRI PUTRA I', 'ANGSERI, BATURITI', '-', '-', '-', '2022-04-19 12:20:15', '2022-04-19 12:20:15', NULL);
INSERT INTO `pelanggan` VALUES (34, 'EAK', 'TK EKA ABDI KUMARA', 'MEKARSARI, BATURITI', '-', '-', '-', '2022-04-19 12:20:51', '2022-04-19 12:20:51', NULL);
INSERT INTO `pelanggan` VALUES (35, 'DS', 'TK DARMA SHANTI', 'BATUNYA, BATURITI', '-', '-', '-', '2022-04-19 12:21:19', '2022-04-19 12:21:19', NULL);
INSERT INTO `pelanggan` VALUES (36, 'WKI', 'TK WIJA KUSUMA I', 'PEREAN KANGIN, BATURITI', '-', '-', '-', '2022-04-19 12:21:48', '2022-04-19 12:21:48', NULL);
INSERT INTO `pelanggan` VALUES (37, 'BPII', 'TK BINA PUTRA II', 'CANDIKUNING, BATURITI', '-', '-', '-', '2022-04-19 12:22:19', '2022-04-19 12:22:19', NULL);
INSERT INTO `pelanggan` VALUES (38, 'WSB', 'TK WIDYA STHANA BATUSESA', 'CANDIKUNING, BATURITI', '-', '-', '-', '2022-04-19 12:23:56', '2022-04-19 12:23:56', NULL);
INSERT INTO `pelanggan` VALUES (39, 'WKIII', 'TK WIJA KUSUMA III', 'PEREAN KANGIN, BATURITI', '-', '-', '-', '2022-04-19 12:24:30', '2022-04-19 12:24:30', NULL);
INSERT INTO `pelanggan` VALUES (40, 'AKS', 'TK ASRAMA KUMARA SENAPAHAN', 'BANJAR ANYAR, KEDIRI', '-', '-', '-', '2022-04-19 12:25:14', '2022-04-19 12:25:14', NULL);
INSERT INTO `pelanggan` VALUES (41, 'TH', 'TK TUNAS HARAPAN', 'CEPAKA, KEDIRI', '-', '-', '-', '2022-04-19 12:25:36', '2022-04-19 12:25:36', NULL);
INSERT INTO `pelanggan` VALUES (42, 'SBM', 'TK SANGGAR BUNGA MEKAR', 'KEDIRI, KEDIRI', '-', '-', '-', '2022-04-19 12:26:14', '2022-04-19 12:26:14', NULL);
INSERT INTO `pelanggan` VALUES (43, 'T', 'TK TIMEUS', 'KEDIRI, KEDIRI', '-', '-', '-', '2022-04-19 12:26:44', '2022-04-19 12:26:44', NULL);
INSERT INTO `pelanggan` VALUES (44, 'PP', 'TK PANJI PUTRA', 'BANJAR ANYAR, KEDIRI', '-', '-', '-', '2022-04-19 12:27:16', '2022-04-19 12:27:16', NULL);
INSERT INTO `pelanggan` VALUES (45, 'WGI', 'TK WIDIA GUNA I', 'ABIANTUWUNG, KEDIRI', '-', '-', '-', '2022-04-19 12:27:44', '2022-04-19 12:27:44', NULL);
INSERT INTO `pelanggan` VALUES (46, 'CKI', 'TK CIPTA KARYA I', 'PEJATEN, KEDIRI', '-', '-', '-', '2022-04-19 12:28:15', '2022-04-19 12:28:15', NULL);
INSERT INTO `pelanggan` VALUES (47, 'KS', 'TK KUMARA SANGGRAHA', 'SERAMPINGAN, SELEMADEG', '-', '-', '-', '2022-04-19 12:28:45', '2022-04-19 12:28:45', NULL);
INSERT INTO `pelanggan` VALUES (48, 'KA', 'TK KUMARA ASIH', 'WANAGIRI KAUH, SELEMADEG', '-', '-', '-', '2022-04-19 12:29:22', '2022-04-19 12:29:22', NULL);
INSERT INTO `pelanggan` VALUES (49, 'BSN', 'TK BHUWANA SUTA NUGRAHA', 'SELEMADEG, SELEMADEG', '-', '-', '-', '2022-04-19 12:30:09', '2022-04-19 12:30:09', NULL);
INSERT INTO `pelanggan` VALUES (50, 'A', 'KB AISYIYAH', 'DAUH PEKEN, TABANAN', '-', '-', '-', '2022-04-19 12:30:35', '2022-04-19 12:30:35', NULL);
INSERT INTO `pelanggan` VALUES (51, 'SPNF', 'SPNF SKB KAB TABANAN', 'KEDIRI, KEDIRI', '-', '-', '-', '2022-04-19 12:31:06', '2022-04-19 12:31:06', NULL);
INSERT INTO `pelanggan` VALUES (52, 'CK', 'KB CIPTA KARYA', 'KEDIRI, KEDIRI', '-', '-', '-', '2022-04-19 12:31:42', '2022-04-19 12:31:42', NULL);
INSERT INTO `pelanggan` VALUES (53, 'PYG', 'SD NEGERI 1 PAYANGAN', 'MARGA', '-', '-', '-', '2022-04-19 14:15:41', '2022-04-19 14:15:41', NULL);
INSERT INTO `pelanggan` VALUES (54, 'KDR', 'SMP NEGERI 5 KEDIRI', 'ABIANTUWUNG, KEDIRI', '-', '-', '-', '2022-04-19 14:30:20', '2022-04-19 14:30:20', NULL);
INSERT INTO `pelanggan` VALUES (55, 'SELTIM', 'SMP NEGERI 1 SELEMADEG TIMUR', 'BANTAS, SELEMADEG TIMUR', '-', '-', '-', '2022-04-19 14:38:34', '2022-04-19 14:38:34', NULL);
INSERT INTO `pelanggan` VALUES (56, 'SLMG', 'SMP NEGERI 2 SELEMADEG', 'WANAGIRI, SELEMADEG', '-', '-', '-', '2022-04-19 14:51:25', '2022-04-19 14:51:25', NULL);
INSERT INTO `pelanggan` VALUES (57, 'SELTIM', 'SMP NEGERI 2 SELEMADEG TIMUR', 'TANGGUNTITI, SELEMADEG TIMUR', '-', '-', '-', '2022-04-19 14:59:30', '2022-04-19 14:59:30', NULL);
INSERT INTO `pelanggan` VALUES (58, 'SELTIM', 'SMP NEGERI 3 SELEMADEG TIMUR', 'GADUNGAN, SELEMADEG TIMUR', '-', '-', '-', '2022-04-19 15:21:34', '2022-04-19 15:21:34', NULL);
INSERT INTO `pelanggan` VALUES (59, 'SLMG', 'SMP NEGERI 1 SELEMADEG', 'BAJERA, SELEMADEG', '-', '-', '-', '2022-04-19 15:24:44', '2022-04-19 15:24:44', NULL);
INSERT INTO `pelanggan` VALUES (60, 'MRG', 'SMP NEGERI 4 MARGA', 'CAU BELAYU, MARGA', '-', '-', '-', '2022-04-19 15:37:36', '2022-04-19 15:37:36', NULL);
INSERT INTO `pelanggan` VALUES (61, 'MRG', 'SMP NEGERI 1 MARGA', 'MARGA, MARGA', '-', '-', '-', '2022-04-19 15:39:37', '2022-04-19 15:39:37', NULL);
INSERT INTO `pelanggan` VALUES (62, 'MRG', 'SMP NEGERI 2 MARGA', 'KUKUH, MARGA', '-', '-', '-', '2022-04-19 15:43:24', '2022-04-19 15:43:24', NULL);
INSERT INTO `pelanggan` VALUES (63, 'PNBL', 'SMP NEGERI 2 PENEBEL', 'PENATAHAN, PENEBEL', '-', '-', '-', '2022-04-19 15:46:54', '2022-04-19 15:46:54', NULL);
INSERT INTO `pelanggan` VALUES (64, 'PNBL', 'SMP NEGERI 1 PENEBEL', 'PENEBEL, PENEBEL', '-', '-', '-', '2022-04-19 15:48:55', '2022-04-19 15:48:55', NULL);
INSERT INTO `pelanggan` VALUES (65, 'PNBL', 'SMP NEGERI 3 PENEBEL', 'SENGANAN, PENEBEL', '-', '-', '-', '2022-04-19 15:52:37', '2022-04-19 15:52:37', NULL);
INSERT INTO `pelanggan` VALUES (66, 'TBN', 'SMP NEGERI 2 TABANAN', 'DELOD PEKEN, TABANAN', '-', '-', '-', '2022-04-19 15:56:40', '2022-04-19 15:56:40', NULL);
INSERT INTO `pelanggan` VALUES (67, 'PNBL', 'SD NEGERI 1 BIAUNG', 'BIAUNG, PENEBEL', '-', '-', '-', '2022-04-19 16:32:57', '2022-04-19 16:32:57', NULL);
INSERT INTO `pelanggan` VALUES (68, 'PNBL', 'SD NEGERI 1 BURUAN', 'BURUAN, PENEBEL', '-', '-', '-', '2022-04-19 16:36:16', '2022-04-19 16:36:16', NULL);
INSERT INTO `pelanggan` VALUES (69, 'PNBL', 'SD NEGERI 1 BABAHAN', 'BABAHAN, PENEBEL', '-', '-', '-', '2022-04-19 16:39:23', '2022-04-19 16:39:23', NULL);
INSERT INTO `pelanggan` VALUES (70, 'NGR', 'SMP NEGERI 4 NEGARA', 'BALUK, NEGARA', '-', '-', '-', '2022-04-20 08:11:46', '2022-04-20 08:11:46', NULL);
INSERT INTO `pelanggan` VALUES (71, 'PNBL', 'SD NEGERI 1 MENGESTA', 'MENGESTA, PENEBEL', '-', '-', '-', '2022-04-20 08:17:50', '2022-04-20 08:17:50', NULL);
INSERT INTO `pelanggan` VALUES (72, 'BTR', 'SMP NEGERI 5 BATURITI', 'Antapan, Kec. Baturiti', '-', '-', '-', '2022-04-21 09:00:00', '2022-04-21 09:00:00', NULL);
INSERT INTO `pelanggan` VALUES (73, 'KDR', 'SMP NEGERI 2 KEDIRI', 'Jl. Padang Padi No.18, Kaliombo, Kec. Kota, Kabupaten Kediri, Jawa Timur 64129', '-', '-', '-', '2022-04-21 09:17:02', '2022-04-21 09:17:02', NULL);
INSERT INTO `pelanggan` VALUES (74, 'TBN', 'SMP NEGERI 4 TABANAN', 'Jl. Wisnu Marga, Marga Dauh Puri, Kec. Marga, Kabupaten Tabanan, Bali 82181', '-', '-', '-', '2022-04-21 09:21:52', '2022-04-21 09:21:52', NULL);
INSERT INTO `pelanggan` VALUES (75, 'KAR', 'TK SARASWATI', 'DELOD PEKEN, TABANAN', '-', '-', '-', '2022-04-22 08:22:11', '2022-04-22 08:22:11', NULL);
INSERT INTO `pelanggan` VALUES (76, 'KAR', 'TPA ST THERESIA TAMAN KANAK KANAK YESUS', 'Jl. Ks. Tubun, Delod Peken, Kec. Tabanan, Kabupaten Tabanan, Bali 82121', '-', '-', '-', '2022-04-22 08:28:18', '2022-04-22 08:28:18', NULL);
INSERT INTO `pelanggan` VALUES (77, 'KAR', 'KB SARASWATI', 'DELOD PEKEN, TABANAN', '-', '-', '-', '2022-04-22 08:28:39', '2022-04-22 08:28:39', NULL);
INSERT INTO `pelanggan` VALUES (78, 'KRM', 'SD NEGERI 1 BATUAJI', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 12:03:06', '2022-04-25 12:03:06', NULL);
INSERT INTO `pelanggan` VALUES (79, 'KRM', 'SD NEGERI 1 KELATING', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 12:14:42', '2022-04-25 14:42:44', NULL);
INSERT INTO `pelanggan` VALUES (80, 'KRM', 'SD NEGERI 1 KERAMBITAN', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 13:43:31', '2022-04-25 13:43:31', NULL);
INSERT INTO `pelanggan` VALUES (81, 'KRM', 'SD NEGERI 1 KESIUT', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 13:46:47', '2022-04-25 13:46:47', NULL);
INSERT INTO `pelanggan` VALUES (82, 'KRM', 'SD NEGERI 1 KUKUH', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 13:51:04', '2022-04-25 13:51:04', NULL);
INSERT INTO `pelanggan` VALUES (83, 'KRM', 'SD NEGERI 1 MELILING', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 13:56:02', '2022-04-25 13:56:02', NULL);
INSERT INTO `pelanggan` VALUES (84, 'KRM', 'SD NEGERI 1 PANGKUNG KARUNG', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 13:59:36', '2022-04-25 13:59:36', NULL);
INSERT INTO `pelanggan` VALUES (85, 'KRM', 'SD NEGERI 1 SAMSAM', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:01:28', '2022-04-25 14:43:10', NULL);
INSERT INTO `pelanggan` VALUES (86, 'KRM', 'SD NEGERI 1 SEMBUNG GEDE', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:03:24', '2022-04-25 14:03:24', NULL);
INSERT INTO `pelanggan` VALUES (87, 'KRM', 'SD NEGERI 1 TIBUBIU', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:05:31', '2022-04-25 14:05:31', NULL);
INSERT INTO `pelanggan` VALUES (88, 'KRM', 'SD NEGERI 1 BATURITI', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:07:50', '2022-04-25 14:07:50', NULL);
INSERT INTO `pelanggan` VALUES (89, 'KRM', 'SD NEGERI 1 TIMPAG', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:08:58', '2022-04-25 14:08:58', NULL);
INSERT INTO `pelanggan` VALUES (90, 'KRM', 'SD NEGERI 1 TISTA', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:11:43', '2022-04-25 14:11:43', NULL);
INSERT INTO `pelanggan` VALUES (91, 'KRM', 'SD NEGERI 2 BATUAJI', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:14:18', '2022-04-25 14:14:18', NULL);
INSERT INTO `pelanggan` VALUES (92, 'KRM', 'SD NEGERI 2 BATURITI', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:17:28', '2022-04-25 14:43:39', NULL);
INSERT INTO `pelanggan` VALUES (93, 'KRM', 'SD NEGERI 2 BELUMBANG', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:19:34', '2022-04-25 14:19:34', NULL);
INSERT INTO `pelanggan` VALUES (94, 'KRM', 'SD NEGERI 2 KELATING', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:21:38', '2022-04-25 14:21:38', NULL);
INSERT INTO `pelanggan` VALUES (95, 'KRM', 'SD NEGERI 2 MELILING', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:23:38', '2022-04-25 14:23:38', NULL);
INSERT INTO `pelanggan` VALUES (96, 'KRM', 'SD NEGERI 2 PANGKUNG KARUNG', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:25:02', '2022-04-25 14:25:02', NULL);
INSERT INTO `pelanggan` VALUES (97, 'KRM', 'SD NEGERI 2 PENARUKAN', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:28:32', '2022-04-25 14:28:32', NULL);
INSERT INTO `pelanggan` VALUES (98, 'KRM', 'SD NEGERI 2 SAMSAM', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:30:17', '2022-04-25 14:30:17', NULL);
INSERT INTO `pelanggan` VALUES (99, 'KRM', 'SD NEGERI 2 SEMBUNG GEDE', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:32:56', '2022-04-25 14:32:56', NULL);
INSERT INTO `pelanggan` VALUES (100, 'KRM', 'SD NEGERI 3 KERAMBITAN', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:34:41', '2022-04-25 14:34:41', NULL);
INSERT INTO `pelanggan` VALUES (101, 'KRM', 'SD NEGERI 3 PENARUKAN', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:36:49', '2022-04-25 14:36:49', NULL);
INSERT INTO `pelanggan` VALUES (102, 'KRM', 'SD NEGERI 3 SEMBUNG GEDE', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:38:09', '2022-04-25 14:38:09', NULL);
INSERT INTO `pelanggan` VALUES (103, 'KRM', 'SD NEGERI 3 TIMPAG', 'KERAMBITAN, TABANAN', '-', '-', '-', '2022-04-25 14:39:10', '2022-04-25 14:39:10', NULL);
INSERT INTO `pelanggan` VALUES (104, 'PPN', 'SD NEGERI 1 BANTIRAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 14:48:43', '2022-04-25 14:48:43', NULL);
INSERT INTO `pelanggan` VALUES (105, 'PPN', 'SD NEGERI 1 BATUNGSEL', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 14:52:10', '2022-04-25 14:52:10', NULL);
INSERT INTO `pelanggan` VALUES (106, 'PPN', 'SD NEGERI 1 BELATUNGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 14:53:58', '2022-04-25 14:53:58', NULL);
INSERT INTO `pelanggan` VALUES (107, 'PPN', 'SD NEGERI 1 BELIMBING', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 14:55:10', '2022-04-25 14:55:10', NULL);
INSERT INTO `pelanggan` VALUES (108, 'PPN', 'SD NEGERI 1 JELIJIH PUNGGANG', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:02:35', '2022-04-25 15:02:35', NULL);
INSERT INTO `pelanggan` VALUES (109, 'PPN', 'SD NEGERI 1 KARYA SARI', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:04:54', '2022-04-25 15:04:54', NULL);
INSERT INTO `pelanggan` VALUES (110, 'PPN', 'SD NEGERI 1 KEBON PADANGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:08:49', '2022-04-25 15:08:49', NULL);
INSERT INTO `pelanggan` VALUES (111, 'PPN', 'SD NEGERI 1 MUNDUKTEMU', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:10:14', '2022-04-25 15:10:14', NULL);
INSERT INTO `pelanggan` VALUES (112, 'PPN', 'SD NEGERI 1 PADANGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:11:30', '2022-04-25 15:11:30', NULL);
INSERT INTO `pelanggan` VALUES (113, 'PPN', 'SD NEGERI 1 PUPUAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:13:06', '2022-04-25 15:13:06', NULL);
INSERT INTO `pelanggan` VALUES (114, 'PPN', 'SD NEGERI 1 SAI', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:19:28', '2022-04-25 15:19:28', NULL);
INSERT INTO `pelanggan` VALUES (115, 'PPN', 'SD NEGERI 1 SANDA', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:22:12', '2022-04-25 15:22:12', NULL);
INSERT INTO `pelanggan` VALUES (116, 'PPN', 'SD NEGERI 2 BANTIRAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:23:29', '2022-04-25 15:23:29', NULL);
INSERT INTO `pelanggan` VALUES (117, 'PPN', 'SD NEGERI 2 BATUNGSEL', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:24:46', '2022-04-25 15:24:46', NULL);
INSERT INTO `pelanggan` VALUES (118, 'PPN', 'SD NEGERI 2 BELIMBING', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:25:51', '2022-04-25 15:25:51', NULL);
INSERT INTO `pelanggan` VALUES (119, 'PPN', 'SD NEGERI 2 MUNDUK TEMU', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:27:30', '2022-04-25 15:27:30', NULL);
INSERT INTO `pelanggan` VALUES (120, 'PPN', 'SD NEGERI 2 PAJAHAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:29:43', '2022-04-25 15:29:43', NULL);
INSERT INTO `pelanggan` VALUES (121, 'PPN', 'SD NEGERI 2 PUPUAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:30:58', '2022-04-25 15:30:58', NULL);
INSERT INTO `pelanggan` VALUES (122, 'PPN', 'SD NEGERI 3 BANTIRAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:32:27', '2022-04-25 15:32:27', NULL);
INSERT INTO `pelanggan` VALUES (123, 'PPN', 'SD NEGERI 3 KEBON PADANGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:34:08', '2022-04-25 15:34:08', NULL);
INSERT INTO `pelanggan` VALUES (124, 'PPN', 'SD NEGERI 3 MUNDUK TEMU', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:35:25', '2022-04-25 15:35:25', NULL);
INSERT INTO `pelanggan` VALUES (125, 'PPN', 'SD NEGERI 3 PAJAHAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:36:58', '2022-04-25 15:36:58', NULL);
INSERT INTO `pelanggan` VALUES (126, 'PPN', 'SD NEGERI 3 PUJUNGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:38:22', '2022-04-25 15:38:22', NULL);
INSERT INTO `pelanggan` VALUES (127, 'PPN', 'SD NEGERI 4 BELIMBING', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:39:27', '2022-04-25 15:39:27', NULL);
INSERT INTO `pelanggan` VALUES (128, 'PPN', 'SD NEGERI 5 BATUNGSEL', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:40:27', '2022-04-25 15:40:27', NULL);
INSERT INTO `pelanggan` VALUES (129, 'PPN', 'SD NEGERI 5 BELIMBING', 'PUPUAN, TABANAN', 'PUPUAN, TABANAN', '-', '-', '2022-04-25 15:41:32', '2022-04-25 15:41:32', NULL);
INSERT INTO `pelanggan` VALUES (130, 'PPN', 'SD NEGERI 5 PUJUNGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:42:31', '2022-04-25 15:42:31', NULL);
INSERT INTO `pelanggan` VALUES (131, 'PPN', 'SD NEGERI 6 PUJUNGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:43:34', '2022-04-25 15:43:34', NULL);
INSERT INTO `pelanggan` VALUES (132, 'PPN', 'SD NEGERI 1 PUJUNGAN', 'PUPUAN, TABANAN', '-', '-', '-', '2022-04-25 15:45:43', '2022-04-25 15:45:43', NULL);
INSERT INTO `pelanggan` VALUES (133, 'BP', 'SMP BINTANG PERSADA TABANAN', 'DAUH PEKEN, TABANAN', '-', '-', '-', '2022-04-25 17:27:08', '2022-04-25 17:27:08', NULL);
COMMIT;

-- ----------------------------
-- Table structure for pembelian
-- ----------------------------
DROP TABLE IF EXISTS `pembelian`;
CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `pbl_urutan` int(11) DEFAULT NULL,
  `pbl_no_faktur` varchar(50) DEFAULT NULL,
  `pbl_no_faktur_supplier` varchar(50) DEFAULT NULL,
  `pbl_tanggal_order` datetime DEFAULT NULL,
  `pbl_jenis_pembayaran` enum('cash','kredit','transfer') DEFAULT 'transfer',
  `pbl_keterangan` text DEFAULT NULL,
  `pbl_total` decimal(20,2) DEFAULT NULL,
  `pbl_total_ppn` decimal(20,2) DEFAULT NULL,
  `pbl_ppn_persentase` decimal(20,2) DEFAULT NULL,
  `pbl_biaya_tambahan` decimal(20,2) DEFAULT NULL,
  `pbl_potongan` decimal(20,2) DEFAULT NULL,
  `pbl_grand_total` decimal(20,2) DEFAULT NULL,
  `pbl_status` enum('order','progress','arrive','paid','hutang') DEFAULT 'order',
  `pbl_jumlah_bayar` decimal(20,2) DEFAULT NULL,
  `pbl_sisa_pembayaran` decimal(20,2) DEFAULT NULL,
  `pbl_jatuh_tempo` date DEFAULT NULL COMMENT 'tanggal ini digunakan jika sisa pembayaran lebih dari 0, sehingga masuk ke dalam hutang supplier',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pembelian`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=433 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pembelian
-- ----------------------------
BEGIN;
INSERT INTO `pembelian` VALUES (394, 46, 36, NULL, 'PBL/TI/2022/03/001', 'PJL-22032900074', '2022-03-29 15:40:00', 'cash', NULL, 230000.00, 0.00, 10.00, 0.00, 0.00, 230000.00, 'order', 230000.00, 0.00, '2022-03-29', '2022-03-30 14:38:23', '2022-03-30 14:53:43', NULL);
INSERT INTO `pembelian` VALUES (395, 46, 36, NULL, 'PBL/TI/2022/03/002', 'PJL-22032900074', '2022-03-29 15:40:00', 'cash', NULL, 270000.00, 0.00, 10.00, 0.00, 0.00, 270000.00, 'order', 270000.00, 0.00, '2022-03-29', '2022-03-30 14:48:40', '2022-03-30 14:53:14', NULL);
INSERT INTO `pembelian` VALUES (396, 57, 36, NULL, 'PBL/RET/2022/03/003', '-', '2022-03-29 12:40:00', 'cash', NULL, 951000.00, 0.00, 10.00, 153600.00, 0.00, 1104600.00, 'order', 1104600.00, 0.00, '2022-03-30', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `pembelian` VALUES (397, 51, 36, NULL, 'PBL/CSA/2022/03/004', 'KO3-290322-0009', '2022-03-29 10:10:00', 'cash', NULL, 373200.00, 0.00, 10.00, 0.00, 0.00, 373200.00, 'order', 373200.00, 0.00, '2022-03-30', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian` VALUES (398, 58, 36, NULL, 'PBL/POT/2022/03/005', '030839', '2022-03-26 16:00:00', 'cash', NULL, 1584000.00, 0.00, 10.00, 0.00, 0.00, 1584000.00, 'order', 1584000.00, 0.00, '2022-03-30', '2022-03-30 16:37:59', '2022-03-30 16:37:59', NULL);
INSERT INTO `pembelian` VALUES (399, 59, 36, NULL, 'PBL/AP/2022/03/006', 'AG-020322-08121', '2022-03-26 16:00:00', 'cash', NULL, 336000.00, 0.00, 10.00, 0.00, 0.00, 336000.00, 'order', 336000.00, 0.00, '2022-03-30', '2022-03-30 16:43:33', '2022-03-30 16:43:33', NULL);
INSERT INTO `pembelian` VALUES (400, 51, 36, NULL, 'PBL/CSA/2022/03/007', 'k12-250322-0037', '2022-03-25 11:55:00', 'cash', NULL, 500800.00, 0.00, 10.00, 0.00, 0.00, 500800.00, 'order', 500800.00, 0.00, '2022-03-30', '2022-03-30 16:57:11', '2022-03-30 16:57:11', NULL);
INSERT INTO `pembelian` VALUES (401, 51, 36, NULL, 'PBL/CSA/2022/03/008', 'K06-250322-0008', '2022-03-25 13:10:00', 'cash', NULL, 241000.00, 0.00, 10.00, 0.00, 0.00, 241000.00, 'order', 0.00, 241000.00, '2022-03-25', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `pembelian` VALUES (402, 60, 36, NULL, 'PBL/AS/2022/03/009', 'NS-004-2203-003005', '2022-03-25 17:35:00', 'cash', NULL, 1983080.00, 0.00, 10.00, 0.00, 49580.00, 1933500.00, 'order', 1933500.00, 0.00, '2022-03-25', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian` VALUES (403, 39, 36, NULL, 'PBL/MSC/2022/03/010', 's10/aur/22/03/12/0001', '2022-03-12 00:00:00', 'cash', NULL, 1560000.00, 0.00, 10.00, 0.00, 0.00, 1560000.00, 'order', 1560000.00, 0.00, '2022-03-31', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `pembelian` VALUES (404, 50, 36, NULL, 'PBL/NM/2022/03/011', 'ND3-2203120039', '2022-03-12 00:00:00', 'cash', NULL, 4908200.00, 0.00, 10.00, 0.00, 0.00, 4908200.00, 'order', 4908200.00, 0.00, '2022-03-31', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian` VALUES (405, 52, 36, NULL, 'PBL/BE/2022/03/012', '-', '2022-03-09 00:00:00', 'cash', NULL, 747000.00, 0.00, 10.00, 0.00, 0.00, 747000.00, 'order', 747000.00, 0.00, '2022-03-31', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian` VALUES (406, 53, 36, NULL, 'PBL/BPN/2022/03/013', 'R-020034', '2022-02-26 00:00:00', 'kredit', NULL, 11720000.00, 0.00, 10.00, 0.00, 657000.00, 11063000.00, 'order', 0.00, 11063000.00, '2022-04-09', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian` VALUES (407, 52, 36, NULL, 'PBL/BE/2022/04/001', '-', '2022-01-10 00:00:00', 'cash', NULL, 850000.00, 0.00, 10.00, 0.00, 0.00, 850000.00, 'order', 850000.00, 0.00, '2022-04-02', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `pembelian` VALUES (408, 52, 36, NULL, 'PBL/BE/2022/04/001', '-', '2022-01-10 00:00:00', 'cash', NULL, 470000.00, 0.00, 10.00, 0.00, 0.00, 470000.00, 'order', 470000.00, 0.00, '2022-04-02', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `pembelian` VALUES (409, 52, 36, NULL, 'PBL/BE/2022/04/001', '-', '2022-02-15 00:00:00', 'cash', NULL, 1255000.00, 0.00, 10.00, 0.00, 0.00, 1255000.00, 'order', 0.00, 1255000.00, '2022-04-02', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `pembelian` VALUES (410, 52, 36, NULL, 'PBL/BE/2022/04/001', '-', '2022-02-16 00:00:00', 'cash', NULL, 7000000.00, 0.00, 10.00, 0.00, 0.00, 7000000.00, 'order', 7000000.00, 0.00, '2022-02-16', '2022-04-02 09:26:31', '2022-04-02 09:26:31', NULL);
INSERT INTO `pembelian` VALUES (411, 52, 36, NULL, 'PBL/BE/2022/04/001', '-', '2022-02-17 00:00:00', 'cash', NULL, 8988000.00, 0.00, 10.00, 0.00, 0.00, 8988000.00, 'order', 8988000.00, 0.00, '2022-02-17', '2022-04-02 09:28:42', '2022-04-02 09:28:42', NULL);
INSERT INTO `pembelian` VALUES (412, 39, 36, NULL, 'PBL/MSC/2022/04/001', 'S10/AUR/22/02/05/0003', '2022-02-05 00:00:00', 'cash', NULL, 9700000.00, 0.00, 10.00, 0.00, 0.00, 9700000.00, 'order', 9700000.00, 0.00, '2022-02-05', '2022-04-02 09:34:07', '2022-04-02 09:34:07', NULL);
INSERT INTO `pembelian` VALUES (413, 44, 36, NULL, 'PBL/FXB/2022/04/001', '71500', '2022-01-21 00:00:00', 'cash', NULL, 53900000.00, 0.00, 10.00, 0.00, 0.00, 53900000.00, 'order', 53900000.00, 0.00, '2022-01-21', '2022-04-02 09:39:37', '2022-04-02 09:39:37', NULL);
INSERT INTO `pembelian` VALUES (424, 53, 36, NULL, 'PBL/BPN/2022/04/001', 'R-040059', '2022-04-09 15:00:00', 'kredit', NULL, 7600000.00, 0.00, 11.00, 0.00, 0.00, 7600000.00, 'order', 0.00, 7600000.00, '2022-04-30', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian` VALUES (425, 61, 35, NULL, 'PBL/JF/2022/04/002', '0001/23/01/22', '2022-01-25 00:30:00', 'cash', 'PO 22/12/2021', 10050000.00, 0.00, 11.00, 0.00, 680000.00, 9370000.00, 'order', 9370000.00, 0.00, '2022-04-13', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `pembelian` VALUES (426, 62, 36, NULL, 'PBL/YW/2022/04/002', 'L22-002700', '2022-04-08 15:35:00', 'kredit', NULL, 22419000.00, 0.00, 11.00, 0.00, 10761120.00, 11657880.00, 'order', 0.00, 11657880.00, '2022-05-14', '2022-04-14 15:40:38', '2022-04-14 15:40:38', NULL);
INSERT INTO `pembelian` VALUES (427, 62, 36, NULL, 'PBL/YW/2022/04/003', 'L22-000830', '2022-02-14 12:05:00', 'kredit', NULL, 22730000.00, 0.00, 11.00, 0.00, 0.00, 22730000.00, 'order', 0.00, 22730000.00, '2022-05-31', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `pembelian` VALUES (428, 62, 36, NULL, 'PBL/YW/2022/04/003', 'L22-001202', '2022-02-25 14:55:00', 'kredit', NULL, 2436000.00, 0.00, 11.00, 0.00, 0.00, 2436000.00, 'order', 0.00, 2436000.00, '2022-05-31', '2022-04-19 13:00:15', '2022-04-19 13:00:15', NULL);
INSERT INTO `pembelian` VALUES (429, 62, 36, NULL, 'PBL/YW/2022/04/003', 'L22-001372', '2022-02-25 14:05:00', 'kredit', NULL, 4176000.00, 0.00, 11.00, 0.00, 0.00, 4176000.00, 'order', 0.00, 4176000.00, '2022-05-31', '2022-04-19 13:01:52', '2022-04-19 13:01:52', NULL);
INSERT INTO `pembelian` VALUES (430, 62, 36, NULL, 'PBL/YW/2022/04/003', 'L22-001588', '2022-03-09 11:45:00', 'kredit', NULL, 3525000.00, 0.00, 11.00, 0.00, 0.00, 3525000.00, 'order', 0.00, 3525000.00, '2022-05-31', '2022-04-19 14:48:37', '2022-04-19 14:48:37', NULL);
INSERT INTO `pembelian` VALUES (431, 62, 36, NULL, 'PBL/YW/2022/04/003', 'l22-001611', '2022-03-09 13:15:00', 'kredit', NULL, 76000000.00, 0.00, 11.00, 0.00, 0.00, 76000000.00, 'order', 0.00, 76000000.00, '2022-05-31', '2022-04-19 16:20:56', '2022-04-19 16:20:56', NULL);
INSERT INTO `pembelian` VALUES (432, 50, 36, NULL, 'PBL/NM/2022/04/003', 'ND-2204180020', '2022-04-18 11:10:00', 'cash', NULL, 601800.00, 0.00, 11.00, 0.00, 0.00, 601800.00, 'order', 0.00, 601800.00, '2022-04-18', '2022-04-22 09:20:16', '2022-04-22 09:20:16', NULL);
COMMIT;

-- ----------------------------
-- Table structure for pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS `pembelian_detail`;
CREATE TABLE `pembelian_detail` (
  `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `pbd_kode_batch` varchar(255) DEFAULT NULL,
  `pbd_harga_beli` decimal(20,2) DEFAULT NULL,
  `pbd_harga_jual` decimal(20,2) DEFAULT NULL,
  `pbd_expired` date DEFAULT NULL,
  `pbd_ppn_persen` decimal(20,2) DEFAULT NULL,
  `pbd_ppn_nominal` decimal(20,2) DEFAULT NULL,
  `pbd_harga_net` decimal(20,2) DEFAULT NULL,
  `pbd_qty` decimal(20,2) DEFAULT NULL,
  `pbd_sub_total` decimal(20,2) DEFAULT NULL,
  `pbd_konsinyasi_status` enum('yes','no') DEFAULT 'yes',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pembelian_detail`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1216 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pembelian_detail
-- ----------------------------
BEGIN;
INSERT INTO `pembelian_detail` VALUES (1112, 394, 1135, NULL, NULL, 11500.00, NULL, NULL, 10.00, 0.00, 0.00, 20.00, 230000.00, 'yes', '2022-03-30 14:38:23', '2022-03-30 14:53:43', NULL);
INSERT INTO `pembelian_detail` VALUES (1113, 395, 1139, NULL, NULL, 10500.00, NULL, NULL, 10.00, 0.00, 0.00, 12.00, 126000.00, 'yes', '2022-03-30 14:48:40', '2022-03-30 14:53:14', NULL);
INSERT INTO `pembelian_detail` VALUES (1114, 395, 1140, NULL, NULL, 12000.00, NULL, NULL, 10.00, 0.00, 0.00, 12.00, 144000.00, 'yes', '2022-03-30 14:48:40', '2022-03-30 14:53:14', NULL);
INSERT INTO `pembelian_detail` VALUES (1115, 396, 1152, NULL, NULL, 17000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 85000.00, 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `pembelian_detail` VALUES (1116, 396, 1155, NULL, NULL, 24000.00, NULL, NULL, NULL, 0.00, 0.00, 6.00, 144000.00, 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `pembelian_detail` VALUES (1117, 396, 1157, NULL, NULL, 68000.00, NULL, NULL, NULL, 0.00, 0.00, 4.00, 272000.00, 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `pembelian_detail` VALUES (1118, 396, 1158, NULL, NULL, 150000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 450000.00, 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `pembelian_detail` VALUES (1119, 397, 1172, NULL, NULL, 37000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 37000.00, 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1120, 397, 1173, NULL, NULL, 37000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 37000.00, 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1121, 397, 1174, NULL, NULL, 36400.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 109200.00, 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1122, 397, 1175, NULL, NULL, 37000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 37000.00, 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1123, 397, 1176, NULL, NULL, 10600.00, NULL, NULL, NULL, 0.00, 0.00, 4.00, 42400.00, 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1124, 397, 1177, NULL, NULL, 10600.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 10600.00, 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1125, 397, 1178, NULL, NULL, 20000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 100000.00, 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1126, 398, 1179, NULL, NULL, 132000.00, NULL, NULL, NULL, 0.00, 0.00, 12.00, 1584000.00, 'yes', '2022-03-30 16:37:59', '2022-03-30 16:37:59', NULL);
INSERT INTO `pembelian_detail` VALUES (1127, 399, 1180, NULL, NULL, 28000.00, NULL, NULL, NULL, 0.00, 0.00, 12.00, 336000.00, 'yes', '2022-03-30 16:43:33', '2022-03-30 16:43:33', NULL);
INSERT INTO `pembelian_detail` VALUES (1128, 400, 1182, NULL, NULL, 9400.00, NULL, NULL, NULL, 0.00, 0.00, 12.00, 112800.00, 'yes', '2022-03-30 16:57:11', '2022-03-30 16:57:11', NULL);
INSERT INTO `pembelian_detail` VALUES (1129, 400, 1181, NULL, NULL, 9700.00, NULL, NULL, NULL, 0.00, 0.00, 40.00, 388000.00, 'yes', '2022-03-30 16:57:11', '2022-03-30 16:57:11', NULL);
INSERT INTO `pembelian_detail` VALUES (1130, 401, 1181, NULL, NULL, 9700.00, NULL, NULL, NULL, 0.00, 0.00, 20.00, 194000.00, 'yes', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1131, 401, 1183, NULL, NULL, 4700.00, NULL, NULL, NULL, 0.00, 0.00, 10.00, 47000.00, 'yes', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1132, 402, 1184, NULL, NULL, 522.00, NULL, NULL, NULL, 0.00, 0.00, 400.00, 208800.00, 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1133, 402, 1185, NULL, NULL, 750.00, NULL, NULL, NULL, 0.00, 0.00, 150.00, 112500.00, 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1134, 402, 1186, NULL, NULL, 751.00, NULL, NULL, NULL, 0.00, 0.00, 100.00, 75100.00, 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1135, 402, 1187, NULL, NULL, 15380.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 46140.00, 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1136, 402, 1188, NULL, NULL, 375.00, NULL, NULL, NULL, 0.00, 0.00, 80.00, 30000.00, 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1137, 402, 1189, NULL, NULL, 27577.00, NULL, NULL, NULL, 0.00, 0.00, 40.00, 1103080.00, 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1138, 402, 1190, NULL, NULL, 13582.00, NULL, NULL, NULL, 0.00, 0.00, 30.00, 407460.00, 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1139, 403, 1191, NULL, NULL, 78000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 390000.00, 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `pembelian_detail` VALUES (1140, 403, 1192, NULL, NULL, 78000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 390000.00, 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `pembelian_detail` VALUES (1141, 403, 1193, NULL, NULL, 78000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 390000.00, 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `pembelian_detail` VALUES (1142, 403, 1194, NULL, NULL, 78000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 390000.00, 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `pembelian_detail` VALUES (1143, 404, 1195, NULL, NULL, 2000.00, NULL, NULL, NULL, 0.00, 0.00, 40.00, 80000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1144, 404, 1196, NULL, NULL, 29700.00, NULL, NULL, NULL, 0.00, 0.00, 57.00, 1692900.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1145, 404, 1197, NULL, NULL, 39000.00, NULL, NULL, NULL, 0.00, 0.00, 25.00, 975000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1146, 404, 1198, NULL, NULL, 44200.00, NULL, NULL, NULL, 0.00, 0.00, 25.00, 1105000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1147, 404, 1199, NULL, NULL, 3200.00, NULL, NULL, NULL, 0.00, 0.00, 100.00, 320000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1148, 404, 1200, NULL, NULL, 2850.00, NULL, NULL, NULL, 0.00, 0.00, 60.00, 171000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1149, 404, 1201, NULL, NULL, 2850.00, NULL, NULL, NULL, 0.00, 0.00, 60.00, 171000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1150, 404, 1202, NULL, NULL, 2100.00, NULL, NULL, NULL, 0.00, 0.00, 57.00, 119700.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1151, 404, 1203, NULL, NULL, 13000.00, NULL, NULL, NULL, 0.00, 0.00, 6.00, 78000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1152, 404, 1204, NULL, NULL, 2100.00, NULL, NULL, NULL, 0.00, 0.00, 12.00, 25200.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1153, 404, 1205, NULL, NULL, 2800.00, NULL, NULL, NULL, 0.00, 0.00, 15.00, 42000.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1154, 404, 1206, NULL, NULL, 10700.00, NULL, NULL, NULL, 0.00, 0.00, 12.00, 128400.00, 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `pembelian_detail` VALUES (1155, 405, 1208, NULL, NULL, 35000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 70000.00, 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian_detail` VALUES (1156, 405, 1207, NULL, NULL, 72000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 72000.00, 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian_detail` VALUES (1157, 405, 1209, NULL, NULL, 10000.00, NULL, NULL, NULL, 0.00, 0.00, 6.00, 60000.00, 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian_detail` VALUES (1158, 405, 1210, NULL, NULL, 15000.00, NULL, NULL, NULL, 0.00, 0.00, 10.00, 150000.00, 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian_detail` VALUES (1159, 405, 1211, NULL, NULL, 2000.00, NULL, NULL, NULL, 0.00, 0.00, 120.00, 240000.00, 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian_detail` VALUES (1160, 405, 1212, NULL, NULL, 55000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 110000.00, 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian_detail` VALUES (1161, 405, 1213, NULL, NULL, 15000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 45000.00, 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `pembelian_detail` VALUES (1162, 406, 1214, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 10.00, 3150000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1163, 406, 1215, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 630000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1164, 406, 1216, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 7.00, 2205000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1165, 406, 1217, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 1575000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1166, 406, 1218, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 1575000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1167, 406, 1219, NULL, NULL, 150000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 450000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1168, 406, 1220, NULL, NULL, 150000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 150000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1169, 406, 1221, NULL, NULL, 150000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 300000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1170, 406, 1222, NULL, NULL, 460000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 460000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1171, 406, 1223, NULL, NULL, 500000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 1000000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1172, 406, 1224, NULL, NULL, 225000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 225000.00, 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `pembelian_detail` VALUES (1173, 407, 1225, NULL, NULL, 60000.00, NULL, NULL, NULL, 0.00, 0.00, 7.00, 420000.00, 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1174, 407, 1226, NULL, NULL, 15000.00, NULL, NULL, NULL, 0.00, 0.00, 10.00, 150000.00, 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1175, 407, 1227, NULL, NULL, 10000.00, NULL, NULL, NULL, 0.00, 0.00, 10.00, 100000.00, 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1176, 407, 1228, NULL, NULL, 20000.00, NULL, NULL, NULL, 0.00, 0.00, 9.00, 180000.00, 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `pembelian_detail` VALUES (1177, 408, 1209, NULL, NULL, 10000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 20000.00, 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `pembelian_detail` VALUES (1178, 408, 1210, NULL, NULL, 20000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 60000.00, 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `pembelian_detail` VALUES (1179, 408, 1225, NULL, NULL, 60000.00, NULL, NULL, NULL, 0.00, 0.00, 6.00, 360000.00, 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `pembelian_detail` VALUES (1180, 408, 1213, NULL, NULL, 15000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 30000.00, 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `pembelian_detail` VALUES (1181, 409, 1229, NULL, NULL, 16000.00, NULL, NULL, NULL, 0.00, 0.00, 65.00, 1040000.00, 'yes', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `pembelian_detail` VALUES (1182, 409, 1229, NULL, NULL, 15000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 75000.00, 'yes', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `pembelian_detail` VALUES (1183, 409, 1229, NULL, NULL, 14000.00, NULL, NULL, NULL, 0.00, 0.00, 10.00, 140000.00, 'yes', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `pembelian_detail` VALUES (1184, 410, 1229, NULL, NULL, 14000.00, NULL, NULL, NULL, 0.00, 0.00, 500.00, 7000000.00, 'yes', '2022-04-02 09:26:31', '2022-04-02 09:26:31', NULL);
INSERT INTO `pembelian_detail` VALUES (1185, 411, 1229, NULL, NULL, 14000.00, NULL, NULL, NULL, 0.00, 0.00, 642.00, 8988000.00, 'yes', '2022-04-02 09:28:42', '2022-04-02 09:28:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1186, 412, 1230, NULL, NULL, 2425000.00, NULL, NULL, NULL, 0.00, 0.00, 4.00, 9700000.00, 'yes', '2022-04-02 09:34:07', '2022-04-02 09:34:07', NULL);
INSERT INTO `pembelian_detail` VALUES (1187, 413, 1231, NULL, NULL, 7700000.00, NULL, NULL, NULL, 0.00, 0.00, 7.00, 53900000.00, 'yes', '2022-04-02 09:39:37', '2022-04-02 09:39:37', NULL);
INSERT INTO `pembelian_detail` VALUES (1189, 424, 1214, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 945000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1190, 424, 1215, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 945000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1191, 424, 1216, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 945000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1192, 424, 1217, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 945000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1193, 424, 1218, NULL, NULL, 315000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 945000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1194, 424, 1235, NULL, NULL, 325000.00, NULL, NULL, NULL, 0.00, 0.00, 5.00, 1625000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1195, 424, 1236, NULL, NULL, 325000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 650000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1196, 424, 1219, NULL, NULL, 150000.00, NULL, NULL, NULL, 0.00, 0.00, 3.00, 450000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1197, 424, 1220, NULL, NULL, 150000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 150000.00, 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `pembelian_detail` VALUES (1198, 425, 1238, NULL, NULL, 2600000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 2600000.00, 'yes', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `pembelian_detail` VALUES (1199, 425, 1239, NULL, NULL, 1800000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 3600000.00, 'yes', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `pembelian_detail` VALUES (1200, 425, 1241, NULL, NULL, 110000.00, NULL, NULL, NULL, 0.00, 0.00, 18.00, 1980000.00, 'yes', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `pembelian_detail` VALUES (1201, 425, 1242, NULL, NULL, 160000.00, NULL, NULL, NULL, 0.00, 0.00, 2.00, 320000.00, 'yes', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `pembelian_detail` VALUES (1202, 425, 1240, NULL, NULL, 1550000.00, NULL, NULL, NULL, 0.00, 0.00, 1.00, 1550000.00, 'yes', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `pembelian_detail` VALUES (1203, 426, 1243, NULL, NULL, 70500.00, NULL, NULL, NULL, 0.00, 0.00, 318.00, 22419000.00, 'yes', '2022-04-14 15:40:38', '2022-04-14 15:40:38', NULL);
INSERT INTO `pembelian_detail` VALUES (1204, 427, 1255, NULL, NULL, 55000.00, NULL, NULL, NULL, 0.00, 0.00, 45.00, 2475000.00, 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1205, 427, 1256, NULL, NULL, 61500.00, NULL, NULL, NULL, 0.00, 0.00, 37.00, 2275500.00, 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1206, 427, 1257, NULL, NULL, 58000.00, NULL, NULL, NULL, 0.00, 0.00, 61.00, 3538000.00, 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1207, 427, 1258, NULL, NULL, 68500.00, NULL, NULL, NULL, 0.00, 0.00, 63.00, 4315500.00, 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1208, 427, 1259, NULL, NULL, 58000.00, NULL, NULL, NULL, 0.00, 0.00, 28.00, 1624000.00, 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1209, 427, 1260, NULL, NULL, 54500.00, NULL, NULL, NULL, 0.00, 0.00, 156.00, 8502000.00, 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `pembelian_detail` VALUES (1210, 428, 1259, NULL, NULL, 58000.00, NULL, NULL, NULL, 0.00, 0.00, 42.00, 2436000.00, 'yes', '2022-04-19 13:00:15', '2022-04-19 13:00:15', NULL);
INSERT INTO `pembelian_detail` VALUES (1211, 429, 1259, NULL, NULL, 58000.00, NULL, NULL, NULL, 0.00, 0.00, 72.00, 4176000.00, 'yes', '2022-04-19 13:01:52', '2022-04-19 13:01:52', NULL);
INSERT INTO `pembelian_detail` VALUES (1212, 430, 1151, NULL, NULL, 117500.00, NULL, NULL, NULL, 0.00, 0.00, 30.00, 3525000.00, 'yes', '2022-04-19 14:48:37', '2022-04-19 14:48:37', NULL);
INSERT INTO `pembelian_detail` VALUES (1213, 431, 1146, NULL, NULL, 63500.00, NULL, NULL, NULL, 0.00, 0.00, 500.00, 31750000.00, 'yes', '2022-04-19 16:20:56', '2022-04-19 16:20:56', NULL);
INSERT INTO `pembelian_detail` VALUES (1214, 431, 1147, NULL, NULL, 88500.00, NULL, NULL, NULL, 0.00, 0.00, 500.00, 44250000.00, 'yes', '2022-04-19 16:20:56', '2022-04-19 16:20:56', NULL);
INSERT INTO `pembelian_detail` VALUES (1215, 432, 1275, NULL, NULL, 5900.00, NULL, NULL, NULL, 0.00, 0.00, 102.00, 601800.00, 'yes', '2022-04-22 09:20:16', '2022-04-22 09:20:16', NULL);
COMMIT;

-- ----------------------------
-- Table structure for penjualan
-- ----------------------------
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE `penjualan` (
  `id_penjualan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `pjl_urutan` bigint(20) DEFAULT NULL,
  `pjl_no_faktur` varchar(100) DEFAULT NULL,
  `pjl_tanggal_penjualan` datetime DEFAULT NULL,
  `pjl_jenis_pembayaran` enum('cash','debit','transfer') DEFAULT NULL,
  `pjl_total` decimal(20,2) DEFAULT NULL,
  `pjl_biaya_tambahan` decimal(20,2) DEFAULT NULL,
  `pjl_potongan` decimal(20,2) DEFAULT NULL,
  `pjl_grand_total` decimal(20,2) DEFAULT NULL,
  `pjl_jumlah_bayar` decimal(20,2) DEFAULT NULL,
  `pjl_sisa_pembayaran` decimal(20,2) DEFAULT NULL,
  `pjl_keterangan` text DEFAULT NULL,
  `pjl_jatuh_tempo` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penjualan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of penjualan
-- ----------------------------
BEGIN;
INSERT INTO `penjualan` VALUES (39, 2, 35, NULL, 'PJL/MRG/2022/04/001', '2022-01-26 08:00:00', 'cash', 880000.00, 0.00, 0.00, 880000.00, 880000.00, 0.00, NULL, '2022-04-13', '2022-04-13 15:28:05', '2022-04-13 15:28:05', NULL);
INSERT INTO `penjualan` VALUES (40, 3, 35, NULL, 'PJL/PNB/2022/04/001', '2022-01-27 08:00:00', 'cash', 1584000.00, 0.00, 0.00, 1584000.00, 1584000.00, 0.00, NULL, '2022-04-13', '2022-04-13 15:42:23', '2022-04-13 15:42:23', NULL);
INSERT INTO `penjualan` VALUES (41, 4, 35, NULL, 'PJL/PNB/2022/04/001', '2022-01-27 08:00:00', 'cash', 2640000.00, 0.00, 0.00, 2640000.00, 2640000.00, 0.00, NULL, '2022-04-13', '2022-04-13 15:46:31', '2022-04-13 15:46:31', NULL);
INSERT INTO `penjualan` VALUES (42, 5, 35, NULL, 'PJL/PNB/2022/04/001', '2022-01-27 08:00:00', 'cash', 2640000.00, 0.00, 0.00, 2640000.00, 2640000.00, 0.00, NULL, '2022-04-13', '2022-04-13 15:47:18', '2022-04-13 15:47:18', NULL);
INSERT INTO `penjualan` VALUES (43, 6, 35, NULL, 'PJL/PNB/2022/04/001', '2022-01-27 08:00:00', 'cash', 2640000.00, 0.00, 0.00, 2640000.00, 2640000.00, 0.00, NULL, '2022-04-13', '2022-04-13 15:47:57', '2022-04-13 15:47:57', NULL);
INSERT INTO `penjualan` VALUES (44, 7, 35, NULL, 'PJL/MRG/2022/04/001', '2022-01-28 08:00:00', 'cash', 5400000.00, 0.00, 0.00, 5400000.00, 5400000.00, 0.00, NULL, '2022-04-13', '2022-04-13 15:49:32', '2022-04-13 15:49:32', NULL);
INSERT INTO `penjualan` VALUES (45, 8, 36, NULL, 'PJL/KDR/2022/04/001', '2022-04-11 09:00:00', '', 4985000.00, 0.00, 0.00, 4985000.00, 0.00, 4985000.00, NULL, '2022-05-14', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan` VALUES (46, 9, 36, NULL, 'PJL/PPN/2022/04/002', '2022-04-11 09:00:00', '', 1725000.00, 0.00, 0.00, 1725000.00, 0.00, 1725000.00, NULL, '2022-05-14', '2022-04-14 15:14:18', '2022-04-14 15:14:18', NULL);
INSERT INTO `penjualan` VALUES (47, 10, 36, NULL, 'PJL/PPN/2022/04/003', '2022-04-11 09:00:00', '', 1725000.00, 0.00, 0.00, 1725000.00, 0.00, 1725000.00, NULL, '2022-05-14', '2022-04-14 15:18:51', '2022-04-14 15:18:51', NULL);
INSERT INTO `penjualan` VALUES (48, 11, 36, NULL, 'PJL/PPN/2022/04/004', '2022-04-11 09:00:00', '', 1035000.00, 0.00, 0.00, 1035000.00, 0.00, 1035000.00, NULL, '2022-05-14', '2022-04-14 15:21:23', '2022-04-14 15:21:23', NULL);
INSERT INTO `penjualan` VALUES (49, 12, 36, NULL, 'PJL/PPN/2022/04/005', '2022-04-11 09:00:00', '', 430000.00, 0.00, 0.00, 430000.00, 0.00, 430000.00, NULL, '2022-05-14', '2022-04-14 15:23:41', '2022-04-14 15:23:41', NULL);
INSERT INTO `penjualan` VALUES (50, 13, 36, NULL, 'PJL/PPN/2022/04/006', '2022-04-11 09:00:00', '', 690000.00, 0.00, 0.00, 690000.00, 0.00, 690000.00, NULL, '2022-05-14', '2022-04-14 15:27:15', '2022-04-14 15:27:15', NULL);
INSERT INTO `penjualan` VALUES (51, 14, 36, NULL, 'PJL/PG/2022/04/007', '2022-04-09 15:40:00', '', 21291000.00, 0.00, 0.00, 21291000.00, 0.00, 21291000.00, NULL, '2022-05-14', '2022-04-14 15:41:55', '2022-04-14 15:41:55', NULL);
INSERT INTO `penjualan` VALUES (52, 53, 36, NULL, 'PJL/PYG/2022/04/008', '2022-01-12 14:30:00', '', 639000.00, 0.00, 0.00, 639000.00, 0.00, 639000.00, '0050/BMB/I/2022', '2022-05-31', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan` VALUES (53, 54, 36, NULL, 'PJL/KDR/2022/04/008', '2022-04-08 11:30:00', '', 3950000.00, 0.00, 0.00, 3950000.00, 0.00, 3950000.00, NULL, '2022-05-31', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan` VALUES (54, 55, 36, NULL, 'PJL/SELTIM/2022/04/009', '2022-04-08 11:45:00', '', 2309000.00, 0.00, 0.00, 2309000.00, 0.00, 2309000.00, NULL, '2022-05-31', '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `penjualan` VALUES (55, 56, 36, NULL, 'PJL/SLMG/2022/04/010', '2022-04-08 11:50:00', '', 4985000.00, 0.00, 0.00, 4985000.00, 0.00, 4985000.00, NULL, '2022-05-31', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan` VALUES (56, 57, 36, NULL, 'PJL/SELTIM/2022/04/011', '2022-04-08 11:35:00', '', 2991000.00, 0.00, 0.00, 2991000.00, 0.00, 2991000.00, NULL, '2022-05-31', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan` VALUES (57, 58, 36, NULL, 'PJL/SELTIM/2022/04/012', '2022-04-08 11:55:00', '', 345000.00, 0.00, 0.00, 345000.00, 0.00, 345000.00, NULL, '2022-05-31', '2022-04-19 15:23:21', '2022-04-19 15:23:21', NULL);
INSERT INTO `penjualan` VALUES (58, 59, 36, NULL, 'PJL/SLMG/2022/04/013', '2022-04-08 11:50:00', '', 1994000.00, 0.00, 0.00, 1994000.00, 0.00, 1994000.00, NULL, '2022-04-19', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan` VALUES (59, 60, 36, NULL, 'PJL/MRG/2022/04/014', '2022-04-07 11:30:00', '', 345000.00, 0.00, 0.00, 345000.00, 0.00, 345000.00, NULL, '2022-05-31', '2022-04-19 15:38:44', '2022-04-19 15:38:44', NULL);
INSERT INTO `penjualan` VALUES (60, 61, 36, NULL, 'PJL/MRG/2022/04/015', '2022-04-07 11:40:00', '', 2991000.00, 0.00, 0.00, 2991000.00, 0.00, 2991000.00, '0612/BMB/IV/2022', '2022-05-31', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan` VALUES (61, 62, 36, NULL, 'PJL/MRG/2022/04/016', '2022-04-07 11:20:00', '', 2991000.00, 0.00, 0.00, 2991000.00, 0.00, 2991000.00, NULL, '2022-05-31', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan` VALUES (62, 63, 36, NULL, 'PJL/PNBL/2022/04/017', '2022-04-07 11:30:00', '', 1473000.00, 0.00, 0.00, 1473000.00, 0.00, 1473000.00, NULL, '2022-05-31', '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `penjualan` VALUES (63, 64, 36, NULL, 'PJL/PNBL/2022/04/018', '2022-04-07 14:00:00', '', 1725000.00, 0.00, 0.00, 1725000.00, 0.00, 1725000.00, NULL, '2022-05-31', '2022-04-19 15:50:07', '2022-04-19 15:50:07', NULL);
INSERT INTO `penjualan` VALUES (64, 65, 36, NULL, 'PJL/PNBL/2022/04/019', '2022-04-07 11:35:00', '', 1725000.00, 0.00, 0.00, 1725000.00, 0.00, 1725000.00, NULL, '2022-05-31', '2022-04-19 15:55:00', '2022-04-19 15:55:00', NULL);
INSERT INTO `penjualan` VALUES (65, 66, 36, NULL, 'PJL/TBN/2022/04/020', '2022-04-07 10:50:00', '', 4985000.00, 0.00, 0.00, 4985000.00, 0.00, 4985000.00, NULL, '2022-05-31', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan` VALUES (66, 5, 36, NULL, 'PJL/PNB/2022/04/021', '2022-04-07 11:40:00', '', 1300000.00, 0.00, 0.00, 1300000.00, 0.00, 1300000.00, NULL, '2022-05-31', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `penjualan` VALUES (67, 67, 36, NULL, 'PJL/PNBL/2022/04/022', '2022-04-07 11:40:00', '', 1300000.00, 0.00, 0.00, 1300000.00, 0.00, 1300000.00, NULL, '2022-05-31', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `penjualan` VALUES (68, 68, 36, NULL, 'PJL/PNBL/2022/04/023', '2022-04-07 13:30:00', '', 1300000.00, 0.00, 0.00, 1300000.00, 0.00, 1300000.00, NULL, '2022-05-31', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `penjualan` VALUES (69, 69, 36, NULL, 'PJL/PNBL/2022/04/024', '2022-04-07 11:40:00', '', 1300000.00, 0.00, 0.00, 1300000.00, 0.00, 1300000.00, NULL, '2022-05-31', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `penjualan` VALUES (70, 70, 36, NULL, 'PJL/NGR/2022/04/025', '2022-04-19 11:20:00', '', 2455000.00, 0.00, 0.00, 2455000.00, 0.00, 2455000.00, 'Penerima : Desak Putu Werdiani', '2022-05-31', '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `penjualan` VALUES (71, 71, 36, NULL, 'PJL/PNBL/2022/04/026', '2022-04-07 11:00:00', '', 1300000.00, 0.00, 0.00, 1300000.00, 0.00, 1300000.00, NULL, '2022-05-31', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `penjualan` VALUES (72, 72, 35, NULL, 'PJL/BTR/2022/04/027', '2022-04-21 09:00:00', '', 340500.00, 0.00, 0.00, 340500.00, 0.00, 340500.00, NULL, '2022-05-31', '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `penjualan` VALUES (73, 73, 35, NULL, 'PJL/KDR/2022/04/028', '2022-04-21 09:18:00', '', 952500.00, 0.00, 0.00, 952500.00, 0.00, 952500.00, NULL, '2022-05-31', '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `penjualan` VALUES (74, 74, 35, NULL, 'PJL/TBN/2022/04/029', '2022-04-21 09:21:00', '', 952500.00, 0.00, 0.00, 952500.00, 0.00, 952500.00, NULL, '2022-05-31', '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `penjualan` VALUES (75, 75, 35, NULL, 'PJL/KAR/2022/04/030', '2022-04-22 08:58:00', '', 16935000.00, 0.00, 0.00, 16935000.00, 0.00, 16935000.00, 'BARANG BELUM LENGKAP', '2022-05-31', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan` VALUES (76, 76, 35, NULL, 'PJL/KAR/2022/04/031', '2022-04-22 09:01:00', '', 1972700.00, 0.00, 0.00, 1972700.00, 0.00, 1972700.00, 'BARANG BELUM LENGKAP', '2022-05-31', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan` VALUES (77, 77, 35, NULL, 'PJL/KAR/2022/04/032', '2022-04-22 09:22:00', '', 304500.00, 0.00, 0.00, 304500.00, 0.00, 304500.00, 'BARANG BELUM LENGKAP', '2022-05-31', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan` VALUES (78, 133, 36, NULL, 'PJL/BP/2022/04/033', '2022-04-26 10:00:00', '', 1994000.00, 0.00, 0.00, 1994000.00, 0.00, 1994000.00, NULL, '2022-05-31', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for penjualan_detail
-- ----------------------------
DROP TABLE IF EXISTS `penjualan_detail`;
CREATE TABLE `penjualan_detail` (
  `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_penjualan` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_stok_barang` int(11) DEFAULT NULL,
  `id_stok_barang_json` text DEFAULT NULL,
  `pjd_qty` decimal(20,2) DEFAULT NULL COMMENT 'qty barang yang digunakan saat membeli',
  `pjd_qty_sisa` decimal(20,0) DEFAULT NULL COMMENT 'digunakan untuk saat retur barang, berapa masih sisa barangnya',
  `pjd_harga_jual` decimal(20,2) DEFAULT NULL,
  `pjd_hpp` decimal(20,2) DEFAULT NULL,
  `pjd_potongan` decimal(20,2) DEFAULT NULL,
  `pjd_ppn_persen` decimal(20,2) DEFAULT NULL,
  `pjd_ppn_nominal` decimal(20,2) DEFAULT NULL,
  `pjd_harga_net` decimal(20,2) DEFAULT NULL,
  `pjd_potongan_harga_barang` decimal(20,0) DEFAULT NULL,
  `pjd_sub_total` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penjualan_detail`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of penjualan_detail
-- ----------------------------
BEGIN;
INSERT INTO `penjualan_detail` VALUES (51, 39, 1242, NULL, NULL, 1.00, 1, 880000.00, 0.00, NULL, 11.00, 0.00, 880000.00, 0, 880000.00, '2022-04-13 15:28:05', '2022-04-13 15:28:05', NULL);
INSERT INTO `penjualan_detail` VALUES (52, 40, 1241, NULL, NULL, 3.00, 3, 528000.00, 0.00, NULL, 11.00, 0.00, 528000.00, 0, 1584000.00, '2022-04-13 15:42:23', '2022-04-13 15:42:23', NULL);
INSERT INTO `penjualan_detail` VALUES (53, 41, 1241, NULL, NULL, 5.00, 5, 528000.00, 0.00, NULL, 11.00, 0.00, 528000.00, 0, 2640000.00, '2022-04-13 15:46:31', '2022-04-13 15:46:31', NULL);
INSERT INTO `penjualan_detail` VALUES (54, 42, 1241, NULL, NULL, 5.00, 5, 528000.00, 0.00, NULL, 11.00, 0.00, 528000.00, 0, 2640000.00, '2022-04-13 15:47:18', '2022-04-13 15:47:18', NULL);
INSERT INTO `penjualan_detail` VALUES (55, 43, 1241, NULL, NULL, 5.00, 5, 528000.00, 0.00, NULL, 11.00, 0.00, 528000.00, 0, 2640000.00, '2022-04-13 15:47:57', '2022-04-13 15:47:57', NULL);
INSERT INTO `penjualan_detail` VALUES (56, 44, 1239, NULL, NULL, 2.00, 2, 2700000.00, 0.00, NULL, 11.00, 0.00, 2700000.00, 0, 5400000.00, '2022-04-13 15:49:32', '2022-04-13 15:49:32', NULL);
INSERT INTO `penjualan_detail` VALUES (57, 45, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (58, 45, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (59, 45, 1153, NULL, NULL, 5.00, 5, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 295000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (60, 45, 1154, NULL, NULL, 5.00, 5, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 465000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (61, 45, 1156, NULL, NULL, 5.00, 5, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 330000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (62, 45, 1159, NULL, NULL, 5.00, 5, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 287500.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (63, 45, 1161, NULL, NULL, 5.00, 5, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 180000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (64, 45, 1164, NULL, NULL, 5.00, 5, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 232500.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (65, 45, 1163, NULL, NULL, 5.00, 5, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 482500.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (66, 45, 1162, NULL, NULL, 5.00, 5, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 270000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (67, 45, 1165, NULL, NULL, 5.00, 5, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 402500.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (68, 45, 1166, NULL, NULL, 5.00, 5, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 315000.00, '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `penjualan_detail` VALUES (69, 46, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-14 15:14:18', '2022-04-14 15:14:18', NULL);
INSERT INTO `penjualan_detail` VALUES (70, 46, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-14 15:14:18', '2022-04-14 15:14:18', NULL);
INSERT INTO `penjualan_detail` VALUES (71, 47, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-14 15:18:51', '2022-04-14 15:18:51', NULL);
INSERT INTO `penjualan_detail` VALUES (72, 47, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-14 15:18:51', '2022-04-14 15:18:51', NULL);
INSERT INTO `penjualan_detail` VALUES (73, 48, 1149, NULL, NULL, 3.00, 3, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 255000.00, '2022-04-14 15:21:23', '2022-04-14 15:21:23', NULL);
INSERT INTO `penjualan_detail` VALUES (74, 48, 1150, NULL, NULL, 3.00, 3, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 780000.00, '2022-04-14 15:21:23', '2022-04-14 15:21:23', NULL);
INSERT INTO `penjualan_detail` VALUES (75, 49, 1149, NULL, NULL, 2.00, 2, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 170000.00, '2022-04-14 15:23:41', '2022-04-14 15:23:41', NULL);
INSERT INTO `penjualan_detail` VALUES (76, 49, 1150, NULL, NULL, 1.00, 1, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 260000.00, '2022-04-14 15:23:41', '2022-04-14 15:23:41', NULL);
INSERT INTO `penjualan_detail` VALUES (77, 50, 1149, NULL, NULL, 2.00, 2, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 170000.00, '2022-04-14 15:27:15', '2022-04-14 15:27:15', NULL);
INSERT INTO `penjualan_detail` VALUES (78, 50, 1150, NULL, NULL, 2.00, 2, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 520000.00, '2022-04-14 15:27:15', '2022-04-14 15:27:15', NULL);
INSERT INTO `penjualan_detail` VALUES (79, 51, 1243, NULL, NULL, 302.00, 302, 70500.00, 0.00, NULL, 11.00, 0.00, 70500.00, 0, 21291000.00, '2022-04-14 15:41:55', '2022-04-14 15:41:55', NULL);
INSERT INTO `penjualan_detail` VALUES (80, 52, 1198, NULL, NULL, 2.00, 2, 68500.00, 0.00, NULL, 11.00, 0.00, 68500.00, 0, 137000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (81, 52, 1197, NULL, NULL, 2.00, 2, 61500.00, 0.00, NULL, 11.00, 0.00, 61500.00, 0, 123000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (82, 52, 1261, NULL, NULL, 50.00, 50, 1100.00, 0.00, NULL, 11.00, 0.00, 1100.00, 0, 55000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (83, 52, 1262, NULL, NULL, 5.00, 5, 20000.00, 0.00, NULL, 11.00, 0.00, 20000.00, 0, 100000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (84, 52, 1263, NULL, NULL, 2.00, 2, 50000.00, 0.00, NULL, 11.00, 5500.00, 55500.00, 0, 100000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (85, 52, 1264, NULL, NULL, 2.00, 2, 5000.00, 0.00, NULL, 11.00, 0.00, 5000.00, 0, 10000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (86, 52, 1265, NULL, NULL, 1.00, 1, 12500.00, 0.00, NULL, 11.00, 0.00, 12500.00, 0, 12500.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (87, 52, 1266, NULL, NULL, 1.00, 1, 20000.00, 0.00, NULL, 11.00, 0.00, 20000.00, 0, 20000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (88, 52, 1267, NULL, NULL, 1.00, 1, 31500.00, 0.00, NULL, 11.00, 0.00, 31500.00, 0, 31500.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (89, 52, 1202, NULL, NULL, 10.00, 10, 5000.00, 0.00, NULL, 11.00, 0.00, 5000.00, 0, 50000.00, '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `penjualan_detail` VALUES (90, 53, 1149, NULL, NULL, 2.00, 2, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 170000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (91, 53, 1150, NULL, NULL, 2.00, 2, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 520000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (92, 53, 1153, NULL, NULL, 5.00, 5, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 295000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (93, 53, 1154, NULL, NULL, 5.00, 5, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 465000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (94, 53, 1156, NULL, NULL, 5.00, 5, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 330000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (95, 53, 1159, NULL, NULL, 5.00, 5, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 287500.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (96, 53, 1161, NULL, NULL, 5.00, 5, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 180000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (97, 53, 1164, NULL, NULL, 5.00, 5, 46500.00, 0.00, NULL, 11.00, 5115.00, 51615.00, 0, 232500.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (98, 53, 1163, NULL, NULL, 5.00, 5, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 482500.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (99, 53, 1162, NULL, NULL, 5.00, 5, 54000.00, 0.00, NULL, 11.00, 5940.00, 59940.00, 0, 270000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (100, 53, 1165, NULL, NULL, 5.00, 5, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 402500.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (101, 53, 1166, NULL, NULL, 5.00, 5, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 315000.00, '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `penjualan_detail` VALUES (102, 54, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `penjualan_detail` VALUES (103, 54, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `penjualan_detail` VALUES (104, 54, 1151, NULL, NULL, 4.00, 4, 146000.00, 0.00, NULL, 11.00, 0.00, 146000.00, 0, 584000.00, '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `penjualan_detail` VALUES (105, 55, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (106, 55, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (107, 55, 1153, NULL, NULL, 5.00, 5, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 295000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (108, 55, 1154, NULL, NULL, 5.00, 5, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 465000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (109, 55, 1156, NULL, NULL, 5.00, 5, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 330000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (110, 55, 1159, NULL, NULL, 5.00, 5, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 287500.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (111, 55, 1161, NULL, NULL, 5.00, 5, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 180000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (112, 55, 1164, NULL, NULL, 5.00, 5, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 232500.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (113, 55, 1163, NULL, NULL, 5.00, 5, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 482500.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (114, 55, 1162, NULL, NULL, 5.00, 5, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 270000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (115, 55, 1165, NULL, NULL, 5.00, 5, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 402500.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (116, 55, 1166, NULL, NULL, 5.00, 5, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 315000.00, '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `penjualan_detail` VALUES (117, 56, 1149, NULL, NULL, 3.00, 3, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 255000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (118, 56, 1150, NULL, NULL, 3.00, 3, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 780000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (119, 56, 1153, NULL, NULL, 3.00, 3, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 177000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (120, 56, 1154, NULL, NULL, 3.00, 3, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 279000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (121, 56, 1156, NULL, NULL, 3.00, 3, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 198000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (122, 56, 1159, NULL, NULL, 3.00, 3, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 172500.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (123, 56, 1161, NULL, NULL, 3.00, 3, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 108000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (124, 56, 1164, NULL, NULL, 3.00, 3, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 139500.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (125, 56, 1163, NULL, NULL, 3.00, 3, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 289500.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (126, 56, 1162, NULL, NULL, 3.00, 3, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 162000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (127, 56, 1165, NULL, NULL, 3.00, 3, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 241500.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (128, 56, 1166, NULL, NULL, 3.00, 3, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 189000.00, '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `penjualan_detail` VALUES (129, 57, 1149, NULL, NULL, 1.00, 1, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 85000.00, '2022-04-19 15:23:21', '2022-04-19 15:23:21', NULL);
INSERT INTO `penjualan_detail` VALUES (130, 57, 1150, NULL, NULL, 1.00, 1, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 260000.00, '2022-04-19 15:23:21', '2022-04-19 15:23:21', NULL);
INSERT INTO `penjualan_detail` VALUES (131, 58, 1149, NULL, NULL, 2.00, 2, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 170000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (132, 58, 1150, NULL, NULL, 2.00, 2, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 520000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (133, 58, 1153, NULL, NULL, 2.00, 2, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 118000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (134, 58, 1154, NULL, NULL, 2.00, 2, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 186000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (135, 58, 1156, NULL, NULL, 2.00, 2, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 132000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (136, 58, 1159, NULL, NULL, 2.00, 2, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 115000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (137, 58, 1161, NULL, NULL, 2.00, 2, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 72000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (138, 58, 1164, NULL, NULL, 2.00, 2, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 93000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (139, 58, 1163, NULL, NULL, 2.00, 2, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 193000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (140, 58, 1162, NULL, NULL, 2.00, 2, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 108000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (141, 58, 1165, NULL, NULL, 2.00, 2, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 161000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (142, 58, 1166, NULL, NULL, 2.00, 2, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 126000.00, '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `penjualan_detail` VALUES (143, 59, 1149, NULL, NULL, 1.00, 1, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 85000.00, '2022-04-19 15:38:44', '2022-04-19 15:38:44', NULL);
INSERT INTO `penjualan_detail` VALUES (144, 59, 1150, NULL, NULL, 1.00, 1, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 260000.00, '2022-04-19 15:38:44', '2022-04-19 15:38:44', NULL);
INSERT INTO `penjualan_detail` VALUES (145, 60, 1149, NULL, NULL, 3.00, 3, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 255000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (146, 60, 1150, NULL, NULL, 3.00, 3, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 780000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (147, 60, 1153, NULL, NULL, 3.00, 3, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 177000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (148, 60, 1154, NULL, NULL, 3.00, 3, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 279000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (149, 60, 1156, NULL, NULL, 3.00, 3, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 198000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (150, 60, 1159, NULL, NULL, 3.00, 3, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 172500.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (151, 60, 1161, NULL, NULL, 3.00, 3, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 108000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (152, 60, 1164, NULL, NULL, 3.00, 3, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 139500.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (153, 60, 1163, NULL, NULL, 3.00, 3, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 289500.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (154, 60, 1162, NULL, NULL, 3.00, 3, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 162000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (155, 60, 1165, NULL, NULL, 3.00, 3, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 241500.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (156, 60, 1166, NULL, NULL, 3.00, 3, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 189000.00, '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `penjualan_detail` VALUES (157, 61, 1149, NULL, NULL, 3.00, 3, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 255000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (158, 61, 1150, NULL, NULL, 3.00, 3, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 780000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (159, 61, 1153, NULL, NULL, 3.00, 3, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 177000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (160, 61, 1154, NULL, NULL, 3.00, 3, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 279000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (161, 61, 1156, NULL, NULL, 3.00, 3, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 198000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (162, 61, 1159, NULL, NULL, 3.00, 3, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 172500.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (163, 61, 1161, NULL, NULL, 3.00, 3, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 108000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (164, 61, 1164, NULL, NULL, 3.00, 3, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 139500.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (165, 61, 1163, NULL, NULL, 3.00, 3, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 289500.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (166, 61, 1162, NULL, NULL, 3.00, 3, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 162000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (167, 61, 1165, NULL, NULL, 3.00, 3, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 241500.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (168, 61, 1166, NULL, NULL, 3.00, 3, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 189000.00, '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `penjualan_detail` VALUES (169, 62, 1149, NULL, NULL, 3.00, 3, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 255000.00, '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `penjualan_detail` VALUES (170, 62, 1150, NULL, NULL, 3.00, 3, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 780000.00, '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `penjualan_detail` VALUES (171, 62, 1151, NULL, NULL, 3.00, 3, 146000.00, 0.00, NULL, 11.00, 0.00, 146000.00, 0, 438000.00, '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `penjualan_detail` VALUES (172, 63, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-19 15:50:07', '2022-04-19 15:50:07', NULL);
INSERT INTO `penjualan_detail` VALUES (173, 63, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-19 15:50:07', '2022-04-19 15:50:07', NULL);
INSERT INTO `penjualan_detail` VALUES (174, 64, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-19 15:55:00', '2022-04-19 15:55:00', NULL);
INSERT INTO `penjualan_detail` VALUES (175, 64, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-19 15:55:00', '2022-04-19 15:55:00', NULL);
INSERT INTO `penjualan_detail` VALUES (176, 65, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (177, 65, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (178, 65, 1153, NULL, NULL, 5.00, 5, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 295000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (179, 65, 1154, NULL, NULL, 5.00, 5, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 465000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (180, 65, 1156, NULL, NULL, 5.00, 5, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 330000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (181, 65, 1159, NULL, NULL, 5.00, 5, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 287500.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (182, 65, 1161, NULL, NULL, 5.00, 5, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 180000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (183, 65, 1164, NULL, NULL, 5.00, 5, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 232500.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (184, 65, 1163, NULL, NULL, 5.00, 5, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 482500.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (185, 65, 1162, NULL, NULL, 5.00, 5, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 270000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (186, 65, 1165, NULL, NULL, 5.00, 5, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 402500.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (187, 65, 1166, NULL, NULL, 5.00, 5, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 315000.00, '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `penjualan_detail` VALUES (188, 66, 1142, NULL, NULL, 2.00, 2, 140000.00, 0.00, NULL, 11.00, 0.00, 140000.00, 0, 280000.00, '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `penjualan_detail` VALUES (189, 66, 1144, NULL, NULL, 2.00, 2, 115000.00, 0.00, NULL, 11.00, 0.00, 115000.00, 0, 230000.00, '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `penjualan_detail` VALUES (190, 66, 1145, NULL, NULL, 2.00, 2, 135000.00, 0.00, NULL, 11.00, 0.00, 135000.00, 0, 270000.00, '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `penjualan_detail` VALUES (191, 66, 1143, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `penjualan_detail` VALUES (192, 66, 1146, NULL, NULL, 2.00, 2, 70000.00, 0.00, NULL, 11.00, 0.00, 70000.00, 0, 140000.00, '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `penjualan_detail` VALUES (193, 66, 1147, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `penjualan_detail` VALUES (194, 67, 1142, NULL, NULL, 2.00, 2, 140000.00, 0.00, NULL, 11.00, 0.00, 140000.00, 0, 280000.00, '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `penjualan_detail` VALUES (195, 67, 1144, NULL, NULL, 2.00, 2, 115000.00, 0.00, NULL, 11.00, 0.00, 115000.00, 0, 230000.00, '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `penjualan_detail` VALUES (196, 67, 1145, NULL, NULL, 2.00, 2, 135000.00, 0.00, NULL, 11.00, 0.00, 135000.00, 0, 270000.00, '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `penjualan_detail` VALUES (197, 67, 1143, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `penjualan_detail` VALUES (198, 67, 1146, NULL, NULL, 2.00, 2, 70000.00, 0.00, NULL, 11.00, 0.00, 70000.00, 0, 140000.00, '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `penjualan_detail` VALUES (199, 67, 1147, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `penjualan_detail` VALUES (200, 68, 1142, NULL, NULL, 2.00, 2, 140000.00, 0.00, NULL, 11.00, 0.00, 140000.00, 0, 280000.00, '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `penjualan_detail` VALUES (201, 68, 1144, NULL, NULL, 2.00, 2, 115000.00, 0.00, NULL, 11.00, 0.00, 115000.00, 0, 230000.00, '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `penjualan_detail` VALUES (202, 68, 1145, NULL, NULL, 2.00, 2, 135000.00, 0.00, NULL, 11.00, 0.00, 135000.00, 0, 270000.00, '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `penjualan_detail` VALUES (203, 68, 1143, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `penjualan_detail` VALUES (204, 68, 1146, NULL, NULL, 2.00, 2, 70000.00, 0.00, NULL, 11.00, 0.00, 70000.00, 0, 140000.00, '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `penjualan_detail` VALUES (205, 68, 1147, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `penjualan_detail` VALUES (206, 69, 1142, NULL, NULL, 2.00, 2, 140000.00, 0.00, NULL, 11.00, 0.00, 140000.00, 0, 280000.00, '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `penjualan_detail` VALUES (207, 69, 1144, NULL, NULL, 2.00, 2, 115000.00, 0.00, NULL, 11.00, 0.00, 115000.00, 0, 230000.00, '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `penjualan_detail` VALUES (208, 69, 1145, NULL, NULL, 2.00, 2, 135000.00, 0.00, NULL, 11.00, 0.00, 135000.00, 0, 270000.00, '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `penjualan_detail` VALUES (209, 69, 1143, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `penjualan_detail` VALUES (210, 69, 1146, NULL, NULL, 2.00, 2, 70000.00, 0.00, NULL, 11.00, 0.00, 70000.00, 0, 140000.00, '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `penjualan_detail` VALUES (211, 69, 1147, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `penjualan_detail` VALUES (212, 70, 1149, NULL, NULL, 5.00, 5, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 425000.00, '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `penjualan_detail` VALUES (213, 70, 1150, NULL, NULL, 5.00, 5, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 1300000.00, '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `penjualan_detail` VALUES (214, 70, 1151, NULL, NULL, 5.00, 5, 146000.00, 0.00, NULL, 11.00, 0.00, 146000.00, 0, 730000.00, '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `penjualan_detail` VALUES (215, 71, 1142, NULL, NULL, 2.00, 2, 140000.00, 0.00, NULL, 11.00, 0.00, 140000.00, 0, 280000.00, '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `penjualan_detail` VALUES (216, 71, 1144, NULL, NULL, 2.00, 2, 115000.00, 0.00, NULL, 11.00, 0.00, 115000.00, 0, 230000.00, '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `penjualan_detail` VALUES (217, 71, 1145, NULL, NULL, 2.00, 2, 135000.00, 0.00, NULL, 11.00, 0.00, 135000.00, 0, 270000.00, '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `penjualan_detail` VALUES (218, 71, 1143, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `penjualan_detail` VALUES (219, 71, 1146, NULL, NULL, 2.00, 2, 70000.00, 0.00, NULL, 11.00, 0.00, 70000.00, 0, 140000.00, '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `penjualan_detail` VALUES (220, 71, 1147, NULL, NULL, 2.00, 2, 95000.00, 0.00, NULL, 11.00, 0.00, 95000.00, 0, 190000.00, '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `penjualan_detail` VALUES (221, 72, 1162, NULL, NULL, 1.00, 1, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 54000.00, '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `penjualan_detail` VALUES (222, 72, 1163, NULL, NULL, 1.00, 1, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 96500.00, '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `penjualan_detail` VALUES (223, 72, 1164, NULL, NULL, 1.00, 1, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 46500.00, '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `penjualan_detail` VALUES (224, 72, 1165, NULL, NULL, 1.00, 1, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 80500.00, '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `penjualan_detail` VALUES (225, 72, 1166, NULL, NULL, 1.00, 1, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 63000.00, '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `penjualan_detail` VALUES (226, 73, 1167, NULL, NULL, 3.00, 3, 40000.00, 0.00, NULL, 11.00, 0.00, 40000.00, 0, 120000.00, '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `penjualan_detail` VALUES (227, 73, 1168, NULL, NULL, 3.00, 3, 71500.00, 0.00, NULL, 11.00, 0.00, 71500.00, 0, 214500.00, '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `penjualan_detail` VALUES (228, 73, 1170, NULL, NULL, 3.00, 3, 67500.00, 0.00, NULL, 11.00, 0.00, 67500.00, 0, 202500.00, '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `penjualan_detail` VALUES (229, 73, 1169, NULL, NULL, 3.00, 3, 66500.00, 0.00, NULL, 11.00, 0.00, 66500.00, 0, 199500.00, '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `penjualan_detail` VALUES (230, 73, 1171, NULL, NULL, 3.00, 3, 72000.00, 0.00, NULL, 11.00, 0.00, 72000.00, 0, 216000.00, '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `penjualan_detail` VALUES (231, 74, 1167, NULL, NULL, 3.00, 3, 40000.00, 0.00, NULL, 11.00, 0.00, 40000.00, 0, 120000.00, '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `penjualan_detail` VALUES (232, 74, 1168, NULL, NULL, 3.00, 3, 71500.00, 0.00, NULL, 11.00, 0.00, 71500.00, 0, 214500.00, '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `penjualan_detail` VALUES (233, 74, 1170, NULL, NULL, 3.00, 3, 67500.00, 0.00, NULL, 11.00, 0.00, 67500.00, 0, 202500.00, '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `penjualan_detail` VALUES (234, 74, 1169, NULL, NULL, 3.00, 3, 66500.00, 0.00, NULL, 11.00, 0.00, 66500.00, 0, 199500.00, '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `penjualan_detail` VALUES (235, 74, 1171, NULL, NULL, 3.00, 3, 72000.00, 0.00, NULL, 11.00, 0.00, 72000.00, 0, 216000.00, '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `penjualan_detail` VALUES (236, 75, 1199, NULL, NULL, 327.00, 327, 6500.00, 0.00, NULL, 11.00, 0.00, 6500.00, 0, 2125500.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (237, 75, 1274, NULL, NULL, 109.00, 109, 10000.00, 0.00, NULL, 11.00, 0.00, 10000.00, 0, 1090000.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (238, 75, 1277, NULL, NULL, 109.00, 109, 23000.00, 0.00, NULL, 11.00, 0.00, 23000.00, 0, 2507000.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (239, 75, 1275, NULL, NULL, 109.00, 109, 10000.00, 0.00, NULL, 11.00, 0.00, 10000.00, 0, 1090000.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (240, 75, 1276, NULL, NULL, 109.00, 109, 25000.00, 0.00, NULL, 11.00, 0.00, 25000.00, 0, 2725000.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (241, 75, 1281, NULL, NULL, 109.00, 109, 6000.00, 0.00, NULL, 11.00, 0.00, 6000.00, 0, 654000.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (242, 75, 1282, NULL, NULL, 109.00, 109, 6500.00, 0.00, NULL, 11.00, 0.00, 6500.00, 0, 708500.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (243, 75, 1198, NULL, NULL, 5.00, 5, 68500.00, 0.00, NULL, 11.00, 0.00, 68500.00, 0, 342500.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (244, 75, 1197, NULL, NULL, 5.00, 5, 61500.00, 0.00, NULL, 11.00, 0.00, 61500.00, 0, 307500.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (245, 75, 1283, NULL, NULL, 109.00, 109, 15000.00, 0.00, NULL, 11.00, 0.00, 15000.00, 0, 1635000.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (246, 75, 1230, NULL, NULL, 1.00, 1, 3750000.00, 0.00, NULL, 11.00, 0.00, 3750000.00, 0, 3750000.00, '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `penjualan_detail` VALUES (247, 76, 1269, NULL, NULL, 2.00, 2, 280000.00, 0.00, NULL, 11.00, 0.00, 280000.00, 0, 560000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (248, 76, 1270, NULL, NULL, 1.00, 1, 100000.00, 0.00, NULL, 11.00, 0.00, 100000.00, 0, 100000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (249, 76, 1271, NULL, NULL, 1.00, 1, 150000.00, 0.00, NULL, 11.00, 0.00, 150000.00, 0, 150000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (250, 76, 1272, NULL, NULL, 2.00, 2, 175000.00, 0.00, NULL, 11.00, 0.00, 175000.00, 0, 350000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (251, 76, 1198, NULL, NULL, 1.00, 1, 68500.00, 0.00, NULL, 11.00, 0.00, 68500.00, 0, 68500.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (252, 76, 1197, NULL, NULL, 1.00, 1, 61500.00, 0.00, NULL, 11.00, 0.00, 61500.00, 0, 61500.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (253, 76, 1284, NULL, NULL, 1.00, 1, 45000.00, 0.00, NULL, 11.00, 0.00, 45000.00, 0, 45000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (254, 76, 1285, NULL, NULL, 17.00, 17, 1100.00, 0.00, NULL, 11.00, 0.00, 1100.00, 0, 18700.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (255, 76, 1286, NULL, NULL, 2.00, 2, 25000.00, 0.00, NULL, 11.00, 0.00, 25000.00, 0, 50000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (256, 76, 1287, NULL, NULL, 1.00, 1, 72000.00, 0.00, NULL, 11.00, 0.00, 72000.00, 0, 72000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (257, 76, 1288, NULL, NULL, 9.00, 9, 13000.00, 0.00, NULL, 11.00, 0.00, 13000.00, 0, 117000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (258, 76, 1289, NULL, NULL, 1.00, 1, 155000.00, 0.00, NULL, 11.00, 0.00, 155000.00, 0, 155000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (259, 76, 1290, NULL, NULL, 1.00, 1, 100000.00, 0.00, NULL, 11.00, 0.00, 100000.00, 0, 100000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (260, 76, 1291, NULL, NULL, 5.00, 5, 25000.00, 0.00, NULL, 11.00, 0.00, 25000.00, 0, 125000.00, '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `penjualan_detail` VALUES (261, 77, 1199, NULL, NULL, 6.00, 6, 6500.00, 0.00, NULL, 11.00, 0.00, 6500.00, 0, 39000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (262, 77, 1200, NULL, NULL, 1.00, 1, 6000.00, 0.00, NULL, 11.00, 0.00, 6000.00, 0, 6000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (263, 77, 1277, NULL, NULL, 1.00, 1, 23000.00, 0.00, NULL, 11.00, 0.00, 23000.00, 0, 23000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (264, 77, 1278, NULL, NULL, 1.00, 1, 5000.00, 0.00, NULL, 11.00, 0.00, 5000.00, 0, 5000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (265, 77, 1279, NULL, NULL, 1.00, 1, 2000.00, 0.00, NULL, 11.00, 0.00, 2000.00, 0, 2000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (266, 77, 1280, NULL, NULL, 1.00, 1, 2000.00, 0.00, NULL, 11.00, 0.00, 2000.00, 0, 2000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (267, 77, 1275, NULL, NULL, 1.00, 1, 10000.00, 0.00, NULL, 11.00, 0.00, 10000.00, 0, 10000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (268, 77, 1276, NULL, NULL, 1.00, 1, 25000.00, 0.00, NULL, 11.00, 0.00, 25000.00, 0, 25000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (269, 77, 1281, NULL, NULL, 1.00, 1, 6000.00, 0.00, NULL, 11.00, 0.00, 6000.00, 0, 6000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (270, 77, 1282, NULL, NULL, 1.00, 1, 6500.00, 0.00, NULL, 11.00, 0.00, 6500.00, 0, 6500.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (271, 77, 1198, NULL, NULL, 1.00, 1, 68500.00, 0.00, NULL, 11.00, 0.00, 68500.00, 0, 68500.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (272, 77, 1197, NULL, NULL, 1.00, 1, 61500.00, 0.00, NULL, 11.00, 0.00, 61500.00, 0, 61500.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (273, 77, 1292, NULL, NULL, 1.00, 1, 35000.00, 0.00, NULL, 11.00, 0.00, 35000.00, 0, 35000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (274, 77, 1283, NULL, NULL, 1.00, 1, 15000.00, 0.00, NULL, 11.00, 0.00, 15000.00, 0, 15000.00, '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `penjualan_detail` VALUES (275, 78, 1149, NULL, NULL, 2.00, 2, 85000.00, 0.00, NULL, 11.00, 0.00, 85000.00, 0, 170000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (276, 78, 1150, NULL, NULL, 2.00, 2, 260000.00, 0.00, NULL, 11.00, 0.00, 260000.00, 0, 520000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (277, 78, 1153, NULL, NULL, 2.00, 2, 59000.00, 0.00, NULL, 11.00, 0.00, 59000.00, 0, 118000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (278, 78, 1154, NULL, NULL, 2.00, 2, 93000.00, 0.00, NULL, 11.00, 0.00, 93000.00, 0, 186000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (279, 78, 1156, NULL, NULL, 2.00, 2, 66000.00, 0.00, NULL, 11.00, 0.00, 66000.00, 0, 132000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (280, 78, 1159, NULL, NULL, 2.00, 2, 57500.00, 0.00, NULL, 11.00, 0.00, 57500.00, 0, 115000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (281, 78, 1161, NULL, NULL, 2.00, 2, 36000.00, 0.00, NULL, 11.00, 0.00, 36000.00, 0, 72000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (282, 78, 1162, NULL, NULL, 2.00, 2, 54000.00, 0.00, NULL, 11.00, 0.00, 54000.00, 0, 108000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (283, 78, 1163, NULL, NULL, 2.00, 2, 96500.00, 0.00, NULL, 11.00, 0.00, 96500.00, 0, 193000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (284, 78, 1164, NULL, NULL, 2.00, 2, 46500.00, 0.00, NULL, 11.00, 0.00, 46500.00, 0, 93000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (285, 78, 1165, NULL, NULL, 2.00, 2, 80500.00, 0.00, NULL, 11.00, 0.00, 80500.00, 0, 161000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
INSERT INTO `penjualan_detail` VALUES (286, 78, 1166, NULL, NULL, 2.00, 2, 63000.00, 0.00, NULL, 11.00, 0.00, 63000.00, 0, 126000.00, '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for piutang_lain
-- ----------------------------
DROP TABLE IF EXISTS `piutang_lain`;
CREATE TABLE `piutang_lain` (
  `id_piutang_lain` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) DEFAULT NULL,
  `ptl_no_faktur` varchar(25) DEFAULT NULL,
  `ptl_tanggal` date DEFAULT NULL,
  `ptl_tanggal_jatuh_tempo` date DEFAULT NULL,
  `ptl_total` decimal(20,2) DEFAULT NULL,
  `ptl_sisa` decimal(20,2) DEFAULT NULL,
  `ptl_status` enum('lunas','belum_lunas') DEFAULT NULL,
  `ptl_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_piutang_lain`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for piutang_lain_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `piutang_lain_pembayaran`;
CREATE TABLE `piutang_lain_pembayaran` (
  `id_piutang_lain_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_piutang_lain` int(11) DEFAULT NULL,
  `id_user` varchar(255) DEFAULT NULL,
  `plp_total_piutang` decimal(20,2) DEFAULT NULL,
  `plp_jumlah_bayar` decimal(20,2) DEFAULT NULL,
  `plp_sisa_bayar` decimal(20,2) DEFAULT NULL,
  `plp_tanggal_bayar` date DEFAULT NULL,
  `plp_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_piutang_lain_pembayaran`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for piutang_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `piutang_pelanggan`;
CREATE TABLE `piutang_pelanggan` (
  `id_piutang_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `id_penjualan` int(11) DEFAULT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `id_retur_barang` int(11) DEFAULT NULL,
  `ppl_no_faktur` varchar(25) DEFAULT NULL,
  `ppl_tanggal` date DEFAULT NULL,
  `ppl_jatuh_tempo` date DEFAULT NULL,
  `ppl_total` decimal(20,2) DEFAULT NULL,
  `ppl_sisa` decimal(20,2) DEFAULT NULL,
  `ppl_status` enum('lunas','belum_lunas','lunas_karena_retur','lunas_karena_penjualan_edit') DEFAULT NULL,
  `ppl_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_piutang_pelanggan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of piutang_pelanggan
-- ----------------------------
BEGIN;
INSERT INTO `piutang_pelanggan` VALUES (27, 45, 8, NULL, 'PPL/KDR/2022/04/001', '2022-04-11', '2022-05-14', 4985000.00, 4985000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/KDR/2022/04/001 dengan no faktur PJL/KDR/2022/04/001', '2022-04-14 15:06:01', '2022-04-14 15:06:01', NULL);
INSERT INTO `piutang_pelanggan` VALUES (28, 46, 9, NULL, 'PPL/PPN/2022/04/002', '2022-04-11', '2022-05-14', 1725000.00, 1725000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PPN/2022/04/002 dengan no faktur PJL/PPN/2022/04/002', '2022-04-14 15:14:18', '2022-04-14 15:14:18', NULL);
INSERT INTO `piutang_pelanggan` VALUES (29, 47, 10, NULL, 'PPL/PPN/2022/04/003', '2022-04-11', '2022-05-14', 1725000.00, 1725000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PPN/2022/04/003 dengan no faktur PJL/PPN/2022/04/003', '2022-04-14 15:18:51', '2022-04-14 15:18:51', NULL);
INSERT INTO `piutang_pelanggan` VALUES (30, 48, 11, NULL, 'PPL/PPN/2022/04/004', '2022-04-11', '2022-05-14', 1035000.00, 1035000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PPN/2022/04/004 dengan no faktur PJL/PPN/2022/04/004', '2022-04-14 15:21:23', '2022-04-14 15:21:23', NULL);
INSERT INTO `piutang_pelanggan` VALUES (31, 49, 12, NULL, 'PPL/PPN/2022/04/005', '2022-04-11', '2022-05-14', 430000.00, 430000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PPN/2022/04/005 dengan no faktur PJL/PPN/2022/04/005', '2022-04-14 15:23:41', '2022-04-14 15:23:41', NULL);
INSERT INTO `piutang_pelanggan` VALUES (32, 50, 13, NULL, 'PPL/PPN/2022/04/006', '2022-04-11', '2022-05-14', 690000.00, 690000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PPN/2022/04/006 dengan no faktur PJL/PPN/2022/04/006', '2022-04-14 15:27:15', '2022-04-14 15:27:15', NULL);
INSERT INTO `piutang_pelanggan` VALUES (33, 51, 14, NULL, 'PPL/PG/2022/04/007', '2022-04-09', '2022-05-14', 21291000.00, 21291000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PG/2022/04/007 dengan no faktur PJL/PG/2022/04/007', '2022-04-14 15:41:55', '2022-04-14 15:41:55', NULL);
INSERT INTO `piutang_pelanggan` VALUES (34, 52, 53, NULL, 'PPL/PYG/2022/04/008', '2022-01-12', '2022-05-31', 639000.00, 639000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PYG/2022/04/008 dengan no faktur PJL/PYG/2022/04/008', '2022-04-19 14:26:45', '2022-04-19 14:26:45', NULL);
INSERT INTO `piutang_pelanggan` VALUES (35, 53, 54, NULL, 'PPL/KDR/2022/04/008', '2022-04-08', '2022-05-31', 3950000.00, 3950000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/KDR/2022/04/008 dengan no faktur PJL/KDR/2022/04/008', '2022-04-19 14:35:54', '2022-04-19 14:35:54', NULL);
INSERT INTO `piutang_pelanggan` VALUES (36, 54, 55, NULL, 'PPL/SELTIM/2022/04/009', '2022-04-08', '2022-05-31', 2309000.00, 2309000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/SELTIM/2022/04/009 dengan no faktur PJL/SELTIM/2022/04/009', '2022-04-19 14:50:05', '2022-04-19 14:50:05', NULL);
INSERT INTO `piutang_pelanggan` VALUES (37, 55, 56, NULL, 'PPL/SLMG/2022/04/010', '2022-04-08', '2022-05-31', 4985000.00, 4985000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/SLMG/2022/04/010 dengan no faktur PJL/SLMG/2022/04/010', '2022-04-19 14:57:50', '2022-04-19 14:57:50', NULL);
INSERT INTO `piutang_pelanggan` VALUES (38, 56, 57, NULL, 'PPL/SELTIM/2022/04/011', '2022-04-08', '2022-05-31', 2991000.00, 2991000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/SELTIM/2022/04/011 dengan no faktur PJL/SELTIM/2022/04/011', '2022-04-19 15:07:03', '2022-04-19 15:07:03', NULL);
INSERT INTO `piutang_pelanggan` VALUES (39, 57, 58, NULL, 'PPL/SELTIM/2022/04/012', '2022-04-08', '2022-05-31', 345000.00, 345000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/SELTIM/2022/04/012 dengan no faktur PJL/SELTIM/2022/04/012', '2022-04-19 15:23:21', '2022-04-19 15:23:21', NULL);
INSERT INTO `piutang_pelanggan` VALUES (40, 58, 59, NULL, 'PPL/SLMG/2022/04/013', '2022-04-08', '2022-04-19', 1994000.00, 1994000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/SLMG/2022/04/013 dengan no faktur PJL/SLMG/2022/04/013', '2022-04-19 15:36:30', '2022-04-19 15:36:30', NULL);
INSERT INTO `piutang_pelanggan` VALUES (41, 59, 60, NULL, 'PPL/MRG/2022/04/014', '2022-04-07', '2022-05-31', 345000.00, 345000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/MRG/2022/04/014 dengan no faktur PJL/MRG/2022/04/014', '2022-04-19 15:38:44', '2022-04-19 15:38:44', NULL);
INSERT INTO `piutang_pelanggan` VALUES (42, 60, 61, NULL, 'PPL/MRG/2022/04/015', '2022-04-07', '2022-05-31', 2991000.00, 2991000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/MRG/2022/04/015 dengan no faktur PJL/MRG/2022/04/015', '2022-04-19 15:42:31', '2022-04-19 15:42:31', NULL);
INSERT INTO `piutang_pelanggan` VALUES (43, 61, 62, NULL, 'PPL/MRG/2022/04/016', '2022-04-07', '2022-05-31', 2991000.00, 2991000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/MRG/2022/04/016 dengan no faktur PJL/MRG/2022/04/016', '2022-04-19 15:46:02', '2022-04-19 15:46:02', NULL);
INSERT INTO `piutang_pelanggan` VALUES (44, 62, 63, NULL, 'PPL/PNBL/2022/04/017', '2022-04-07', '2022-05-31', 1473000.00, 1473000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNBL/2022/04/017 dengan no faktur PJL/PNBL/2022/04/017', '2022-04-19 15:48:07', '2022-04-19 15:48:07', NULL);
INSERT INTO `piutang_pelanggan` VALUES (45, 63, 64, NULL, 'PPL/PNBL/2022/04/018', '2022-04-07', '2022-05-31', 1725000.00, 1725000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNBL/2022/04/018 dengan no faktur PJL/PNBL/2022/04/018', '2022-04-19 15:50:07', '2022-04-19 15:50:07', NULL);
INSERT INTO `piutang_pelanggan` VALUES (46, 64, 65, NULL, 'PPL/PNBL/2022/04/019', '2022-04-07', '2022-05-31', 1725000.00, 1725000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNBL/2022/04/019 dengan no faktur PJL/PNBL/2022/04/019', '2022-04-19 15:55:00', '2022-04-19 15:55:00', NULL);
INSERT INTO `piutang_pelanggan` VALUES (47, 65, 66, NULL, 'PPL/TBN/2022/04/020', '2022-04-07', '2022-05-31', 4985000.00, 4985000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/TBN/2022/04/020 dengan no faktur PJL/TBN/2022/04/020', '2022-04-19 16:00:17', '2022-04-19 16:00:17', NULL);
INSERT INTO `piutang_pelanggan` VALUES (48, 66, 5, NULL, 'PPL/PNB/2022/04/021', '2022-04-07', '2022-05-31', 1300000.00, 1300000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNB/2022/04/021 dengan no faktur PJL/PNB/2022/04/021', '2022-04-19 16:23:00', '2022-04-19 16:23:00', NULL);
INSERT INTO `piutang_pelanggan` VALUES (49, 67, 67, NULL, 'PPL/PNBL/2022/04/022', '2022-04-07', '2022-05-31', 1300000.00, 1300000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNBL/2022/04/022 dengan no faktur PJL/PNBL/2022/04/022', '2022-04-19 16:35:17', '2022-04-19 16:35:17', NULL);
INSERT INTO `piutang_pelanggan` VALUES (50, 68, 68, NULL, 'PPL/PNBL/2022/04/023', '2022-04-07', '2022-05-31', 1300000.00, 1300000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNBL/2022/04/023 dengan no faktur PJL/PNBL/2022/04/023', '2022-04-19 16:38:30', '2022-04-19 16:38:30', NULL);
INSERT INTO `piutang_pelanggan` VALUES (51, 69, 69, NULL, 'PPL/PNBL/2022/04/024', '2022-04-07', '2022-05-31', 1300000.00, 1300000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNBL/2022/04/024 dengan no faktur PJL/PNBL/2022/04/024', '2022-04-19 16:40:59', '2022-04-19 16:40:59', NULL);
INSERT INTO `piutang_pelanggan` VALUES (52, 70, 70, NULL, 'PPL/NGR/2022/04/025', '2022-04-19', '2022-05-31', 2455000.00, 2455000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/NGR/2022/04/025 dengan no faktur PJL/NGR/2022/04/025', '2022-04-20 08:15:19', '2022-04-20 08:15:19', NULL);
INSERT INTO `piutang_pelanggan` VALUES (53, 71, 71, NULL, 'PPL/PNBL/2022/04/026', '2022-04-07', '2022-05-31', 1300000.00, 1300000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/PNBL/2022/04/026 dengan no faktur PJL/PNBL/2022/04/026', '2022-04-20 08:21:57', '2022-04-20 08:21:57', NULL);
INSERT INTO `piutang_pelanggan` VALUES (54, 72, 72, NULL, 'PPL/BTR/2022/04/027', '2022-04-21', '2022-05-31', 340500.00, 340500.00, 'belum_lunas', 'Piutang Pelanggan #PPL/BTR/2022/04/027 dengan no faktur PJL/BTR/2022/04/027', '2022-04-21 09:02:03', '2022-04-21 09:02:03', NULL);
INSERT INTO `piutang_pelanggan` VALUES (55, 73, 73, NULL, 'PPL/KDR/2022/04/028', '2022-04-21', '2022-05-31', 952500.00, 952500.00, 'belum_lunas', 'Piutang Pelanggan #PPL/KDR/2022/04/028 dengan no faktur PJL/KDR/2022/04/028', '2022-04-21 09:20:28', '2022-04-21 09:20:28', NULL);
INSERT INTO `piutang_pelanggan` VALUES (56, 74, 74, NULL, 'PPL/TBN/2022/04/029', '2022-04-21', '2022-05-31', 952500.00, 952500.00, 'belum_lunas', 'Piutang Pelanggan #PPL/TBN/2022/04/029 dengan no faktur PJL/TBN/2022/04/029', '2022-04-21 09:23:17', '2022-04-21 09:23:17', NULL);
INSERT INTO `piutang_pelanggan` VALUES (57, 75, 75, NULL, 'PPL/KAR/2022/04/030', '2022-04-22', '2022-05-31', 16935000.00, 16935000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/KAR/2022/04/030 dengan no faktur PJL/KAR/2022/04/030', '2022-04-22 09:01:26', '2022-04-22 09:01:26', NULL);
INSERT INTO `piutang_pelanggan` VALUES (58, 76, 76, NULL, 'PPL/KAR/2022/04/031', '2022-04-22', '2022-05-31', 1972700.00, 1972700.00, 'belum_lunas', 'Piutang Pelanggan #PPL/KAR/2022/04/031 dengan no faktur PJL/KAR/2022/04/031', '2022-04-22 09:04:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `piutang_pelanggan` VALUES (59, 77, 77, NULL, 'PPL/KAR/2022/04/032', '2022-04-22', '2022-05-31', 304500.00, 304500.00, 'belum_lunas', 'Piutang Pelanggan #PPL/KAR/2022/04/032 dengan no faktur PJL/KAR/2022/04/032', '2022-04-22 09:25:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `piutang_pelanggan` VALUES (60, 78, 133, NULL, 'PPL/BP/2022/04/033', '2022-04-26', '2022-05-31', 1994000.00, 1994000.00, 'belum_lunas', 'Piutang Pelanggan #PPL/BP/2022/04/033 dengan no faktur PJL/BP/2022/04/033', '2022-04-25 17:32:22', '2022-04-25 17:32:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for piutang_pelanggan_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `piutang_pelanggan_pembayaran`;
CREATE TABLE `piutang_pelanggan_pembayaran` (
  `id_piutang_pelanggan_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_piutang_pelanggan` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_retur_barang` int(11) DEFAULT NULL,
  `ppp_total_piutang` decimal(20,2) DEFAULT NULL,
  `ppp_jumlah_bayar` decimal(20,2) DEFAULT NULL,
  `ppp_sisa_bayar` decimal(20,2) DEFAULT NULL,
  `ppp_tanggal_bayar` date DEFAULT NULL,
  `ppp_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_piutang_pelanggan_pembayaran`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province` (
  `id_province` int(11) NOT NULL AUTO_INCREMENT,
  `province_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_province`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for retur_barang
-- ----------------------------
DROP TABLE IF EXISTS `retur_barang`;
CREATE TABLE `retur_barang` (
  `id_retur_barang` int(11) NOT NULL AUTO_INCREMENT,
  `id_penjualan` int(11) DEFAULT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `rbr_urutan` bigint(20) DEFAULT NULL,
  `rbr_no_faktur` varchar(100) DEFAULT NULL,
  `rbr_tanggal_retur` datetime DEFAULT NULL,
  `rbr_jenis_pembayaran` enum('cash','debit','transfer') DEFAULT NULL,
  `rbr_total` decimal(20,2) DEFAULT NULL,
  `rbr_biaya_tambahan` decimal(20,2) DEFAULT NULL,
  `rbr_potongan` decimal(20,2) DEFAULT NULL,
  `rbr_grand_total` decimal(20,2) DEFAULT NULL,
  `rbr_jumlah_bayar` decimal(20,2) DEFAULT NULL,
  `rbr_sisa_pembayaran` decimal(20,2) DEFAULT NULL,
  `rbr_jumlah_pengembalian_uang` decimal(20,0) DEFAULT NULL,
  `rbr_jumlah_pengembalian_uang_penjelasan` text DEFAULT NULL,
  `rbr_keterangan` text DEFAULT NULL,
  `rbr_tanggal_jatuh_tempo` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_retur_barang`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for retur_barang_detail
-- ----------------------------
DROP TABLE IF EXISTS `retur_barang_detail`;
CREATE TABLE `retur_barang_detail` (
  `id_retur_barang_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_retur_barang` int(11) DEFAULT NULL,
  `id_penjualan_detail` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `rbd_qty` decimal(20,2) DEFAULT NULL,
  `rbd_harga_jual` decimal(20,2) DEFAULT NULL,
  `rbd_potongan_harga_barang` decimal(20,0) DEFAULT NULL,
  `rbd_sub_total` decimal(20,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_retur_barang_detail`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL AUTO_INCREMENT,
  `stn_nama` varchar(255) DEFAULT NULL,
  `stn_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_satuan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of satuan
-- ----------------------------
BEGIN;
INSERT INTO `satuan` VALUES (19, 'Eks', 'Eksemplar', '2022-03-25 09:48:00', '2022-03-25 09:48:00', NULL);
INSERT INTO `satuan` VALUES (20, 'Rim', '-', '2022-03-25 09:48:32', '2022-03-25 09:48:32', NULL);
INSERT INTO `satuan` VALUES (21, 'Botol', '-', '2022-03-25 09:48:46', '2022-03-25 09:48:46', NULL);
INSERT INTO `satuan` VALUES (22, 'Unit', '-', '2022-03-25 09:52:01', '2022-03-25 09:52:01', NULL);
INSERT INTO `satuan` VALUES (23, 'Pack', '-', '2022-03-25 09:54:08', '2022-03-25 09:54:08', NULL);
INSERT INTO `satuan` VALUES (24, 'Pcs', '-', '2022-03-25 09:54:33', '2022-03-25 09:54:33', NULL);
INSERT INTO `satuan` VALUES (25, 'Dus', '-', '2022-03-25 09:54:52', '2022-03-25 09:55:21', '2022-03-25 09:55:21');
INSERT INTO `satuan` VALUES (26, 'Buah', '-', '2022-03-25 09:59:44', '2022-03-25 09:59:44', NULL);
INSERT INTO `satuan` VALUES (27, 'Tablet', '-', '2022-03-31 13:27:01', '2022-03-31 13:27:01', NULL);
INSERT INTO `satuan` VALUES (28, 'Tube', '-', '2022-03-31 13:39:46', '2022-03-31 13:39:46', NULL);
INSERT INTO `satuan` VALUES (29, 'Set', '-', '2022-03-31 16:39:34', '2022-03-31 16:39:34', NULL);
INSERT INTO `satuan` VALUES (30, 'Kotak', '-', '2022-04-02 09:18:55', '2022-04-02 09:18:55', NULL);
COMMIT;

-- ----------------------------
-- Table structure for stok_barang
-- ----------------------------
DROP TABLE IF EXISTS `stok_barang`;
CREATE TABLE `stok_barang` (
  `id_stok_barang` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_pembelian_detail` int(11) DEFAULT NULL,
  `id_penjualan` int(11) DEFAULT NULL,
  `id_penjualan_detail` int(11) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL,
  `sbr_kode_batch` varchar(255) DEFAULT NULL,
  `sbr_expired` date DEFAULT NULL,
  `sbr_qty` int(11) DEFAULT NULL,
  `sbr_harga_beli` decimal(20,2) DEFAULT NULL,
  `sbr_harga_jual` decimal(20,2) DEFAULT NULL,
  `sbr_status` enum('ready','return','restore_penjualan_edit') DEFAULT 'ready',
  `sbr_konsinyasi_status` enum('yes','no') DEFAULT 'yes',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_stok_barang`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1298 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of stok_barang
-- ----------------------------
BEGIN;
INSERT INTO `stok_barang` VALUES (1143, 1135, NULL, 394, 1112, NULL, NULL, 46, NULL, NULL, 20, 11500.00, NULL, 'ready', 'yes', '2022-03-30 14:38:23', '2022-03-30 14:53:43', NULL);
INSERT INTO `stok_barang` VALUES (1144, 1139, NULL, 395, 1113, NULL, NULL, 46, NULL, NULL, 12, 10500.00, NULL, 'ready', 'yes', '2022-03-30 14:48:40', '2022-03-30 14:53:14', NULL);
INSERT INTO `stok_barang` VALUES (1145, 1140, NULL, 395, 1114, NULL, NULL, 46, NULL, NULL, 12, 12000.00, NULL, 'ready', 'yes', '2022-03-30 14:48:40', '2022-03-30 14:53:14', NULL);
INSERT INTO `stok_barang` VALUES (1146, 1152, NULL, 396, 1115, NULL, NULL, 57, NULL, NULL, 5, 17000.00, NULL, 'ready', 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `stok_barang` VALUES (1147, 1155, NULL, 396, 1116, NULL, NULL, 57, NULL, NULL, 6, 24000.00, NULL, 'ready', 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `stok_barang` VALUES (1148, 1157, NULL, 396, 1117, NULL, NULL, 57, NULL, NULL, 4, 68000.00, NULL, 'ready', 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `stok_barang` VALUES (1149, 1158, NULL, 396, 1118, NULL, NULL, 57, NULL, NULL, 3, 150000.00, NULL, 'ready', 'yes', '2022-03-30 15:53:08', '2022-03-30 15:53:08', NULL);
INSERT INTO `stok_barang` VALUES (1150, 1172, NULL, 397, 1119, NULL, NULL, 51, NULL, NULL, 1, 37000.00, NULL, 'ready', 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `stok_barang` VALUES (1151, 1173, NULL, 397, 1120, NULL, NULL, 51, NULL, NULL, 1, 37000.00, NULL, 'ready', 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `stok_barang` VALUES (1152, 1174, NULL, 397, 1121, NULL, NULL, 51, NULL, NULL, 3, 36400.00, NULL, 'ready', 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `stok_barang` VALUES (1153, 1175, NULL, 397, 1122, NULL, NULL, 51, NULL, NULL, 1, 37000.00, NULL, 'ready', 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `stok_barang` VALUES (1154, 1176, NULL, 397, 1123, NULL, NULL, 51, NULL, NULL, 4, 10600.00, NULL, 'ready', 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `stok_barang` VALUES (1155, 1177, NULL, 397, 1124, NULL, NULL, 51, NULL, NULL, 1, 10600.00, NULL, 'ready', 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `stok_barang` VALUES (1156, 1178, NULL, 397, 1125, NULL, NULL, 51, NULL, NULL, 5, 20000.00, NULL, 'ready', 'yes', '2022-03-30 16:26:12', '2022-03-30 16:26:12', NULL);
INSERT INTO `stok_barang` VALUES (1157, 1179, NULL, 398, 1126, NULL, NULL, 58, NULL, NULL, 12, 132000.00, NULL, 'ready', 'yes', '2022-03-30 16:37:59', '2022-03-30 16:37:59', NULL);
INSERT INTO `stok_barang` VALUES (1158, 1180, NULL, 399, 1127, NULL, NULL, 59, NULL, NULL, 12, 28000.00, NULL, 'ready', 'yes', '2022-03-30 16:43:33', '2022-03-30 16:43:33', NULL);
INSERT INTO `stok_barang` VALUES (1159, 1182, NULL, 400, 1128, NULL, NULL, 51, NULL, NULL, 12, 9400.00, NULL, 'ready', 'yes', '2022-03-30 16:57:11', '2022-03-30 16:57:11', NULL);
INSERT INTO `stok_barang` VALUES (1160, 1181, NULL, 400, 1129, NULL, NULL, 51, NULL, NULL, 40, 9700.00, NULL, 'ready', 'yes', '2022-03-30 16:57:11', '2022-03-30 16:57:11', NULL);
INSERT INTO `stok_barang` VALUES (1161, 1181, NULL, 401, 1130, NULL, NULL, 51, NULL, NULL, 20, 9700.00, NULL, 'ready', 'yes', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `stok_barang` VALUES (1162, 1183, NULL, 401, 1131, NULL, NULL, 51, NULL, NULL, 10, 4700.00, NULL, 'ready', 'yes', '2022-03-30 17:00:27', '2022-03-30 17:00:27', NULL);
INSERT INTO `stok_barang` VALUES (1163, 1184, NULL, 402, 1132, NULL, NULL, 60, NULL, NULL, 400, 522.00, NULL, 'ready', 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `stok_barang` VALUES (1164, 1185, NULL, 402, 1133, NULL, NULL, 60, NULL, NULL, 150, 750.00, NULL, 'ready', 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `stok_barang` VALUES (1165, 1186, NULL, 402, 1134, NULL, NULL, 60, NULL, NULL, 100, 751.00, NULL, 'ready', 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `stok_barang` VALUES (1166, 1187, NULL, 402, 1135, NULL, NULL, 60, NULL, NULL, 3, 15380.00, NULL, 'ready', 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `stok_barang` VALUES (1167, 1188, NULL, 402, 1136, NULL, NULL, 60, NULL, NULL, 80, 375.00, NULL, 'ready', 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `stok_barang` VALUES (1168, 1189, NULL, 402, 1137, NULL, NULL, 60, NULL, NULL, 40, 27577.00, NULL, 'ready', 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `stok_barang` VALUES (1169, 1190, NULL, 402, 1138, NULL, NULL, 60, NULL, NULL, 30, 13582.00, NULL, 'ready', 'yes', '2022-03-31 13:53:15', '2022-03-31 13:53:15', NULL);
INSERT INTO `stok_barang` VALUES (1170, 1191, NULL, 403, 1139, NULL, NULL, 39, NULL, NULL, 5, 78000.00, NULL, 'ready', 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `stok_barang` VALUES (1171, 1192, NULL, 403, 1140, NULL, NULL, 39, NULL, NULL, 5, 78000.00, NULL, 'ready', 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `stok_barang` VALUES (1172, 1193, NULL, 403, 1141, NULL, NULL, 39, NULL, NULL, 5, 78000.00, NULL, 'ready', 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `stok_barang` VALUES (1173, 1194, NULL, 403, 1142, NULL, NULL, 39, NULL, NULL, 5, 78000.00, NULL, 'ready', 'yes', '2022-03-31 14:23:36', '2022-03-31 14:23:36', NULL);
INSERT INTO `stok_barang` VALUES (1174, 1195, NULL, 404, 1143, NULL, NULL, 50, NULL, NULL, 40, 2000.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `stok_barang` VALUES (1175, 1196, NULL, 404, 1144, NULL, NULL, 50, NULL, NULL, 57, 29700.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `stok_barang` VALUES (1176, 1197, NULL, 404, 1145, NULL, NULL, 50, NULL, NULL, 16, 39000.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1177, 1198, NULL, 404, 1146, NULL, NULL, 50, NULL, NULL, 16, 44200.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1178, 1199, NULL, 404, 1147, NULL, NULL, 50, NULL, NULL, 324, 3200.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1179, 1200, NULL, 404, 1148, NULL, NULL, 50, NULL, NULL, 59, 2850.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1180, 1201, NULL, 404, 1149, NULL, NULL, 50, NULL, NULL, 60, 2850.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `stok_barang` VALUES (1181, 1202, NULL, 404, 1150, NULL, NULL, 50, NULL, NULL, 47, 2100.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1182, 1203, NULL, 404, 1151, NULL, NULL, 50, NULL, NULL, 6, 13000.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `stok_barang` VALUES (1183, 1204, NULL, 404, 1152, NULL, NULL, 50, NULL, NULL, 12, 2100.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `stok_barang` VALUES (1184, 1205, NULL, 404, 1153, NULL, NULL, 50, NULL, NULL, 55, 2800.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-04-22 09:28:55', NULL);
INSERT INTO `stok_barang` VALUES (1185, 1206, NULL, 404, 1154, NULL, NULL, 50, NULL, NULL, 12, 10700.00, NULL, 'ready', 'yes', '2022-03-31 15:05:44', '2022-03-31 15:05:44', NULL);
INSERT INTO `stok_barang` VALUES (1186, 1208, NULL, 405, 1155, NULL, NULL, 52, NULL, NULL, 2, 35000.00, NULL, 'ready', 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `stok_barang` VALUES (1187, 1207, NULL, 405, 1156, NULL, NULL, 52, NULL, NULL, 1, 72000.00, NULL, 'ready', 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `stok_barang` VALUES (1188, 1209, NULL, 405, 1157, NULL, NULL, 52, NULL, NULL, 6, 10000.00, NULL, 'ready', 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `stok_barang` VALUES (1189, 1210, NULL, 405, 1158, NULL, NULL, 52, NULL, NULL, 10, 15000.00, NULL, 'ready', 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `stok_barang` VALUES (1190, 1211, NULL, 405, 1159, NULL, NULL, 52, NULL, NULL, 120, 2000.00, NULL, 'ready', 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `stok_barang` VALUES (1191, 1212, NULL, 405, 1160, NULL, NULL, 52, NULL, NULL, 2, 55000.00, NULL, 'ready', 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `stok_barang` VALUES (1192, 1213, NULL, 405, 1161, NULL, NULL, 52, NULL, NULL, 3, 15000.00, NULL, 'ready', 'yes', '2022-03-31 16:23:23', '2022-03-31 16:23:23', NULL);
INSERT INTO `stok_barang` VALUES (1193, 1214, NULL, 406, 1162, NULL, NULL, 53, NULL, NULL, 10, 315000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1194, 1215, NULL, 406, 1163, NULL, NULL, 53, NULL, NULL, 2, 315000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1195, 1216, NULL, 406, 1164, NULL, NULL, 53, NULL, NULL, 7, 315000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1196, 1217, NULL, 406, 1165, NULL, NULL, 53, NULL, NULL, 5, 315000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1197, 1218, NULL, 406, 1166, NULL, NULL, 53, NULL, NULL, 5, 315000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1198, 1219, NULL, 406, 1167, NULL, NULL, 53, NULL, NULL, 3, 150000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1199, 1220, NULL, 406, 1168, NULL, NULL, 53, NULL, NULL, 1, 150000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1200, 1221, NULL, 406, 1169, NULL, NULL, 53, NULL, NULL, 2, 150000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1201, 1222, NULL, 406, 1170, NULL, NULL, 53, NULL, NULL, 1, 460000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1202, 1223, NULL, 406, 1171, NULL, NULL, 53, NULL, NULL, 2, 500000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1203, 1224, NULL, 406, 1172, NULL, NULL, 53, NULL, NULL, 1, 225000.00, NULL, 'ready', 'yes', '2022-03-31 17:08:20', '2022-03-31 17:08:20', NULL);
INSERT INTO `stok_barang` VALUES (1204, 1225, NULL, 407, 1173, NULL, NULL, 52, NULL, NULL, 7, 60000.00, NULL, 'ready', 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `stok_barang` VALUES (1205, 1226, NULL, 407, 1174, NULL, NULL, 52, NULL, NULL, 10, 15000.00, NULL, 'ready', 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `stok_barang` VALUES (1206, 1227, NULL, 407, 1175, NULL, NULL, 52, NULL, NULL, 10, 10000.00, NULL, 'ready', 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `stok_barang` VALUES (1207, 1228, NULL, 407, 1176, NULL, NULL, 52, NULL, NULL, 9, 20000.00, NULL, 'ready', 'yes', '2022-04-02 08:58:12', '2022-04-02 08:58:12', NULL);
INSERT INTO `stok_barang` VALUES (1208, 1209, NULL, 408, 1177, NULL, NULL, 52, NULL, NULL, 2, 10000.00, NULL, 'ready', 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `stok_barang` VALUES (1209, 1210, NULL, 408, 1178, NULL, NULL, 52, NULL, NULL, 3, 20000.00, NULL, 'ready', 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `stok_barang` VALUES (1210, 1225, NULL, 408, 1179, NULL, NULL, 52, NULL, NULL, 6, 60000.00, NULL, 'ready', 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `stok_barang` VALUES (1211, 1213, NULL, 408, 1180, NULL, NULL, 52, NULL, NULL, 2, 15000.00, NULL, 'ready', 'yes', '2022-04-02 09:13:13', '2022-04-02 09:13:13', NULL);
INSERT INTO `stok_barang` VALUES (1212, 1229, NULL, 409, 1181, NULL, NULL, 52, NULL, NULL, 65, 16000.00, NULL, 'ready', 'yes', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `stok_barang` VALUES (1213, 1229, NULL, 409, 1182, NULL, NULL, 52, NULL, NULL, 5, 15000.00, NULL, 'ready', 'yes', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `stok_barang` VALUES (1214, 1229, NULL, 409, 1183, NULL, NULL, 52, NULL, NULL, 10, 14000.00, NULL, 'ready', 'yes', '2022-04-02 09:23:37', '2022-04-02 09:23:37', NULL);
INSERT INTO `stok_barang` VALUES (1215, 1229, NULL, 410, 1184, NULL, NULL, 52, NULL, NULL, 500, 14000.00, NULL, 'ready', 'yes', '2022-04-02 09:26:31', '2022-04-02 09:26:31', NULL);
INSERT INTO `stok_barang` VALUES (1216, 1229, NULL, 411, 1185, NULL, NULL, 52, NULL, NULL, 642, 14000.00, NULL, 'ready', 'yes', '2022-04-02 09:28:42', '2022-04-02 09:28:42', NULL);
INSERT INTO `stok_barang` VALUES (1217, 1230, NULL, 412, 1186, NULL, NULL, 39, NULL, NULL, 3, 2425000.00, NULL, 'ready', 'yes', '2022-04-02 09:34:07', '2022-04-22 09:01:26', NULL);
INSERT INTO `stok_barang` VALUES (1218, 1231, NULL, 413, 1187, NULL, NULL, 44, NULL, NULL, 7, 7700000.00, NULL, 'ready', 'yes', '2022-04-02 09:39:37', '2022-04-02 09:39:37', NULL);
INSERT INTO `stok_barang` VALUES (1219, 1214, NULL, 424, 1189, NULL, NULL, 53, NULL, NULL, 3, 315000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1220, 1215, NULL, 424, 1190, NULL, NULL, 53, NULL, NULL, 3, 315000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1221, 1216, NULL, 424, 1191, NULL, NULL, 53, NULL, NULL, 3, 315000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1222, 1217, NULL, 424, 1192, NULL, NULL, 53, NULL, NULL, 3, 315000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1223, 1218, NULL, 424, 1193, NULL, NULL, 53, NULL, NULL, 3, 315000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1224, 1235, NULL, 424, 1194, NULL, NULL, 53, NULL, NULL, 5, 325000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1225, 1236, NULL, 424, 1195, NULL, NULL, 53, NULL, NULL, 2, 325000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1226, 1219, NULL, 424, 1196, NULL, NULL, 53, NULL, NULL, 3, 150000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1227, 1220, NULL, 424, 1197, NULL, NULL, 53, NULL, NULL, 1, 150000.00, NULL, 'ready', 'yes', '2022-04-13 13:06:42', '2022-04-13 13:06:42', NULL);
INSERT INTO `stok_barang` VALUES (1228, 1238, NULL, 425, 1198, NULL, NULL, 61, NULL, NULL, 1, 2600000.00, NULL, 'ready', 'yes', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `stok_barang` VALUES (1229, 1239, NULL, 425, 1199, NULL, NULL, 61, NULL, NULL, 0, 1800000.00, NULL, 'ready', 'yes', '2022-04-13 15:23:02', '2022-04-13 15:49:32', NULL);
INSERT INTO `stok_barang` VALUES (1230, 1241, NULL, 425, 1200, NULL, NULL, 61, NULL, NULL, 0, 110000.00, NULL, 'ready', 'yes', '2022-04-13 15:23:02', '2022-04-13 15:47:57', NULL);
INSERT INTO `stok_barang` VALUES (1231, 1242, NULL, 425, 1201, NULL, NULL, 61, NULL, NULL, 1, 160000.00, NULL, 'ready', 'yes', '2022-04-13 15:23:02', '2022-04-13 15:28:05', NULL);
INSERT INTO `stok_barang` VALUES (1232, 1240, NULL, 425, 1202, NULL, NULL, 61, NULL, NULL, 1, 1550000.00, NULL, 'ready', 'yes', '2022-04-13 15:23:02', '2022-04-13 15:23:02', NULL);
INSERT INTO `stok_barang` VALUES (1233, 1150, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, NULL, NULL, 'ready', 'yes', '2022-04-14 13:51:36', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1234, 1167, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 94, NULL, NULL, 'ready', 'yes', '2022-04-14 13:51:59', '2022-04-21 09:23:17', NULL);
INSERT INTO `stok_barang` VALUES (1235, 1171, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 94, NULL, NULL, 'ready', 'yes', '2022-04-14 13:52:19', '2022-04-21 09:23:17', NULL);
INSERT INTO `stok_barang` VALUES (1236, 1170, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 94, NULL, NULL, 'ready', 'yes', '2022-04-14 13:52:36', '2022-04-21 09:23:17', NULL);
INSERT INTO `stok_barang` VALUES (1237, 1169, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 94, NULL, NULL, 'ready', 'yes', '2022-04-14 13:52:55', '2022-04-21 09:23:17', NULL);
INSERT INTO `stok_barang` VALUES (1238, 1168, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, 'ready', 'yes', '2022-04-14 13:53:11', '2022-04-21 09:23:17', NULL);
INSERT INTO `stok_barang` VALUES (1239, 1166, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, NULL, NULL, 'ready', 'yes', '2022-04-14 13:53:44', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1240, 1165, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, NULL, NULL, 'ready', 'yes', '2022-04-14 13:54:00', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1241, 1164, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, NULL, NULL, 'ready', 'yes', '2022-04-14 13:54:15', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1242, 1163, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, NULL, NULL, 'ready', 'yes', '2022-04-14 13:54:29', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1243, 1162, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, NULL, NULL, 'ready', 'yes', '2022-04-14 13:54:41', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1244, 1161, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, NULL, NULL, 'ready', 'yes', '2022-04-14 13:54:58', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1245, 1159, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, NULL, NULL, 'ready', 'yes', '2022-04-14 13:55:13', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1246, 1149, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 528, NULL, NULL, 'ready', 'yes', '2022-04-14 13:55:31', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1247, 1153, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, NULL, NULL, 'ready', 'yes', '2022-04-14 13:56:08', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1248, 1154, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 167, NULL, NULL, 'ready', 'yes', '2022-04-14 13:56:24', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1249, 1156, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 167, NULL, NULL, 'ready', 'yes', '2022-04-14 13:56:44', '2022-04-25 17:32:22', NULL);
INSERT INTO `stok_barang` VALUES (1250, 1145, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 590, NULL, NULL, 'ready', 'yes', '2022-04-14 14:59:26', '2022-04-20 08:21:57', NULL);
INSERT INTO `stok_barang` VALUES (1251, 1144, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 590, NULL, NULL, 'ready', 'yes', '2022-04-14 14:59:50', '2022-04-20 08:21:57', NULL);
INSERT INTO `stok_barang` VALUES (1252, 1143, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 590, NULL, NULL, 'ready', 'yes', '2022-04-14 15:00:03', '2022-04-20 08:21:57', NULL);
INSERT INTO `stok_barang` VALUES (1253, 1142, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 590, NULL, NULL, 'ready', 'yes', '2022-04-14 15:00:16', '2022-04-20 08:21:57', NULL);
INSERT INTO `stok_barang` VALUES (1254, 1243, NULL, 426, 1203, NULL, NULL, 62, NULL, NULL, 16, 70500.00, NULL, 'ready', 'yes', '2022-04-14 15:40:38', '2022-04-14 15:41:55', NULL);
INSERT INTO `stok_barang` VALUES (1255, 1255, NULL, 427, 1204, NULL, NULL, 62, NULL, NULL, 45, 55000.00, NULL, 'ready', 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `stok_barang` VALUES (1256, 1256, NULL, 427, 1205, NULL, NULL, 62, NULL, NULL, 37, 61500.00, NULL, 'ready', 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `stok_barang` VALUES (1257, 1257, NULL, 427, 1206, NULL, NULL, 62, NULL, NULL, 61, 58000.00, NULL, 'ready', 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `stok_barang` VALUES (1258, 1258, NULL, 427, 1207, NULL, NULL, 62, NULL, NULL, 63, 68500.00, NULL, 'ready', 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `stok_barang` VALUES (1259, 1259, NULL, 427, 1208, NULL, NULL, 62, NULL, NULL, 28, 58000.00, NULL, 'ready', 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `stok_barang` VALUES (1260, 1260, NULL, 427, 1209, NULL, NULL, 62, NULL, NULL, 156, 54500.00, NULL, 'ready', 'yes', '2022-04-19 12:58:27', '2022-04-19 12:58:27', NULL);
INSERT INTO `stok_barang` VALUES (1261, 1259, NULL, 428, 1210, NULL, NULL, 62, NULL, NULL, 42, 58000.00, NULL, 'ready', 'yes', '2022-04-19 13:00:15', '2022-04-19 13:00:15', NULL);
INSERT INTO `stok_barang` VALUES (1262, 1259, NULL, 429, 1211, NULL, NULL, 62, NULL, NULL, 72, 58000.00, NULL, 'ready', 'yes', '2022-04-19 13:01:52', '2022-04-19 13:01:52', NULL);
INSERT INTO `stok_barang` VALUES (1263, 1261, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-19 14:18:52', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1264, 1262, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-19 14:19:13', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1265, 1263, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-19 14:19:28', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1266, 1264, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-19 14:19:44', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1267, 1267, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-19 14:20:02', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1268, 1266, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-19 14:20:12', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1269, 1265, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-19 14:20:24', '2022-04-19 14:26:45', NULL);
INSERT INTO `stok_barang` VALUES (1270, 1151, NULL, 430, 1212, NULL, NULL, 62, NULL, NULL, 18, 117500.00, NULL, 'ready', 'yes', '2022-04-19 14:48:37', '2022-04-20 08:15:19', NULL);
INSERT INTO `stok_barang` VALUES (1271, 1146, NULL, 431, 1213, NULL, NULL, 62, NULL, NULL, 490, 63500.00, NULL, 'ready', 'yes', '2022-04-19 16:20:56', '2022-04-20 08:21:57', NULL);
INSERT INTO `stok_barang` VALUES (1272, 1147, NULL, 431, 1214, NULL, NULL, 62, NULL, NULL, 490, 88500.00, NULL, 'ready', 'yes', '2022-04-19 16:20:56', '2022-04-20 08:21:57', NULL);
INSERT INTO `stok_barang` VALUES (1273, 1292, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:46:41', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1274, 1291, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:46:52', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1275, 1290, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:47:04', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1276, 1289, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:47:13', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1277, 1288, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:47:22', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1278, 1287, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:47:29', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1279, 1286, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:47:38', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1280, 1285, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:47:45', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1281, 1284, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:47:53', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1282, 1283, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:49:22', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1283, 1282, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:49:37', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1284, 1281, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:49:48', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1285, 1280, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:50:04', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1286, 1279, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, NULL, NULL, 'ready', 'yes', '2022-04-22 08:50:28', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1287, 1278, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, NULL, NULL, 'ready', 'yes', '2022-04-22 08:50:50', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1288, 1274, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:51:12', '2022-04-22 09:01:26', NULL);
INSERT INTO `stok_barang` VALUES (1289, 1275, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:51:29', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1290, 1276, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:51:45', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1291, 1277, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, NULL, NULL, 'ready', 'yes', '2022-04-22 08:52:19', '2022-04-22 09:25:22', NULL);
INSERT INTO `stok_barang` VALUES (1292, 1272, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:52:40', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1293, 1272, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:52:40', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1294, 1271, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:52:54', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1295, 1270, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:53:10', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1296, 1269, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ready', 'yes', '2022-04-22 08:53:29', '2022-04-22 09:04:45', NULL);
INSERT INTO `stok_barang` VALUES (1297, 1275, NULL, 432, 1215, NULL, NULL, 50, NULL, NULL, 101, 5900.00, NULL, 'ready', 'yes', '2022-04-22 09:20:16', '2022-04-22 09:25:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for subdistrict
-- ----------------------------
DROP TABLE IF EXISTS `subdistrict`;
CREATE TABLE `subdistrict` (
  `id_subdistrict` int(11) NOT NULL AUTO_INCREMENT,
  `id_province` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `subdistrict_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_subdistrict`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6995 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL AUTO_INCREMENT,
  `spl_kode` varchar(20) DEFAULT NULL,
  `spl_nama` varchar(255) DEFAULT NULL,
  `spl_alamat` text DEFAULT NULL,
  `spl_phone` varchar(255) DEFAULT NULL,
  `spl_email` varchar(255) DEFAULT NULL,
  `spl_keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_supplier`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of supplier
-- ----------------------------
BEGIN;
INSERT INTO `supplier` VALUES (38, 'BC', 'Bagas Computer', 'Jl Teuku Umar No 255, Denpasar', '087754713543', '-', 'Tempat Service Laptop', '2022-03-25 11:32:37', '2022-03-25 11:32:37', NULL);
INSERT INTO `supplier` VALUES (39, 'MSC', 'Multi Sarana Computer', 'Jl Gunung Slamet No 11 Tegal Harum DPS Bali', '081339137357', '-', 'Beli Laptop, Tinta, Printer', '2022-03-25 11:36:32', '2022-03-25 11:36:32', NULL);
INSERT INTO `supplier` VALUES (40, 'SR', 'Sri Rama', 'Jl Pulau Roti Gg Banteng No 43X', '081338684190', 'ud.srirama@yahoo.com', 'Buku Bhs Bali Penerbit SRI RAMA', '2022-03-25 11:44:57', '2022-03-25 11:44:57', NULL);
INSERT INTO `supplier` VALUES (41, 'TS', 'UD Trisna Sari', 'Jl Taman Wedasari Selatan No 4-8 Padang Sambian Kaja', '087860036037', '-', 'Distributor Karet Talang, Seng Talang, Terpal, Tangki Air,dll', '2022-03-25 11:51:54', '2022-03-25 11:51:54', NULL);
INSERT INTO `supplier` VALUES (42, 'WS', 'Widhi Stationey', 'Jl Tukad Balian 1/8A. Perumnas Bukit Sanggulan Indah, Tabanan', '081906969672', '-', 'Belanja ATK', '2022-03-25 11:54:22', '2022-03-25 11:54:22', NULL);
INSERT INTO `supplier` VALUES (43, 'BM', 'CV. Bumi Mas Bali Prima', 'Jl Gajah Mada 67 Tabanan', '0361811285', '-', 'Elektronik & Furniture', '2022-03-25 11:55:44', '2022-03-25 15:30:28', NULL);
INSERT INTO `supplier` VALUES (44, 'FXB', 'FX-Bali', 'RTC Diponegoro Lt 2 Blok C 3B', '08993113889', '-', 'Elektronik', '2022-03-25 11:57:06', '2022-03-25 11:57:06', NULL);
INSERT INTO `supplier` VALUES (45, 'BCS', 'UD Bali Cipta Seni', 'Jl Wahidin No 6 Denpasar', '0361431632', '-', 'Art Materials Specialist', '2022-03-25 11:58:18', '2022-03-25 11:58:18', NULL);
INSERT INTO `supplier` VALUES (46, 'TI', 'Toko Indra', 'Jl Jadi Raya Sanggulan', '-', '-', '-', '2022-03-25 11:59:19', '2022-03-25 11:59:19', NULL);
INSERT INTO `supplier` VALUES (47, 'DT', 'Doudou Toys', 'Jl Gajah Mada No 3 Tabanan', '081337327291', '-', 'Grosir dan Eceran Mainan Anak', '2022-03-25 11:59:55', '2022-03-25 15:31:08', NULL);
INSERT INTO `supplier` VALUES (48, 'APP', 'PT Arta Pamelar Putra', '-', '085100908010', '-', 'Toko Tisu', '2022-03-25 12:01:38', '2022-03-25 12:01:38', NULL);
INSERT INTO `supplier` VALUES (49, 'TKA', 'Toko Kemasan ASLI', 'Jl Imam Bonjol No 34 Denpasar Kota', '08113851105', '-', 'Toko Botol', '2022-03-25 15:21:02', '2022-03-25 15:21:02', NULL);
INSERT INTO `supplier` VALUES (50, 'NM', 'Nadhi Mart', 'Jl Mahendradata No 100', '0895413372794`', '-', 'Toko ATK Grosir', '2022-03-25 15:21:44', '2022-03-25 15:21:44', NULL);
INSERT INTO `supplier` VALUES (51, 'CSA', 'PT Clandys Sejahtera Abadi', 'Jl Raya Buluh Indah RT.RW.Pemecutan Kaja Denpasar Utara', '-', '-', 'Toko ATK', '2022-03-25 15:29:17', '2022-03-25 15:29:17', NULL);
INSERT INTO `supplier` VALUES (52, 'BE', 'Bu Evi', 'Sanggulan', '-', '-', 'Toko Alat Kebersihan', '2022-03-25 15:34:12', '2022-03-25 15:34:12', NULL);
INSERT INTO `supplier` VALUES (53, 'BPN', 'CV Bima Peraga Nusantara', 'Gg Tower 4A Unggahan Banjaragung Mojokerto', '0321330850', '-', 'Toko Alat Peraga', '2022-03-25 15:35:19', '2022-03-25 15:35:19', NULL);
INSERT INTO `supplier` VALUES (54, 'POS', 'Kantor Pos', '-', '-', '-', 'Beli Materai', '2022-03-25 15:36:24', '2022-03-25 15:36:24', NULL);
INSERT INTO `supplier` VALUES (55, 'GJ', 'Garda Jaya', 'Jl Raya Br Robokan No 5 Padangasambian Kaja', '03617824449', 'gardajaya2000@gmail.com', 'Toko Alat Pemadam Kebakaran', '2022-03-25 15:38:19', '2022-03-25 15:38:19', NULL);
INSERT INTO `supplier` VALUES (56, 'MT', 'Mediatama', 'SOLO', '08122604014', '-', 'Buku Mediatama', '2022-03-25 15:44:10', '2022-03-25 15:44:10', NULL);
INSERT INTO `supplier` VALUES (57, 'RET', 'Ramah Edu toys', 'Bunderan Rt.01/11, Ds Kalangan,Kec.Pedan, Kab.Klaten, Jateng', '081280005570/085741917655', '-', 'web : mainanklaten.blogspot.com', '2022-03-30 15:46:31', '2022-03-30 15:46:31', NULL);
INSERT INTO `supplier` VALUES (58, 'POT', 'Pak Oles Tokcer', 'Jl.Nusa Kambangan No.7 Denpasar', '03614458555/4458487', '-', '-', '2022-03-30 16:36:10', '2022-03-30 16:36:10', NULL);
INSERT INTO `supplier` VALUES (59, 'AP', 'Ayu Parwati', '-', '-', '-', '-', '2022-03-30 16:39:37', '2022-03-30 16:39:37', NULL);
INSERT INTO `supplier` VALUES (60, 'AS', 'Apotekku Sanggulan', 'Jl.Tukad Balian No 4 Perumnas Sanggulan-Tabanan', '082339004624', '-', NULL, '2022-03-30 16:47:22', '2022-03-30 16:47:22', NULL);
INSERT INTO `supplier` VALUES (61, 'JF', 'JAYA FURNITURE', 'Jl Kiyai Ronggo Mulyo, Ujung Batu, Jepara 59416', '089661143146', '-', '-', '2022-04-13 10:56:29', '2022-04-13 10:56:29', NULL);
INSERT INTO `supplier` VALUES (62, 'YW', 'CV Yrama Widya', 'Margahayu-Bandung', '-', '-', '-', '2022-04-14 15:35:53', '2022-04-14 15:35:53', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `tb_karyawan`;
CREATE TABLE `tb_karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_karyawan` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `nama_karyawan` varchar(255) DEFAULT NULL,
  `alamat_karyawan` text CHARACTER SET latin1 DEFAULT NULL,
  `telp_karyawan` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `posisi_karyawan` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `email_karyawan` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `foto_karyawan` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_karyawan
-- ----------------------------
BEGIN;
INSERT INTO `tb_karyawan` VALUES (5, '2', 'Mahendra Wardana', 'Siibangkaja', '081934364063', 'Programmer', 'mahendra.adi.wardana@gmail.com', 'circle-cropped.png', '2019-02-08 07:38:08', '2021-06-02 21:24:35', NULL);
INSERT INTO `tb_karyawan` VALUES (126, NULL, 'Anggita Virginia Tanjaya', 'Perum Karaniya Graha D2 Sanggulan,Tabanan', '0895322687079', 'Admin', 'anggitavt@gmail.com', NULL, '2022-03-25 09:25:26', '2022-03-25 09:25:26', NULL);
INSERT INTO `tb_karyawan` VALUES (127, NULL, 'Ni Putu Sundari Angraini', 'jln raya padang luwih no 130', '081339501764', 'Koordinator Admin', 'putusun@yahoo.com', NULL, '2022-03-25 09:26:32', '2022-03-25 09:26:32', NULL);
INSERT INTO `tb_karyawan` VALUES (128, NULL, 'I Putu Agus Lesa', 'Tabanan', '-', 'Staff Gudang', 'lesa@gmail.com', NULL, '2022-04-25 14:52:19', '2022-04-25 14:52:19', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_karyawan` int(11) DEFAULT NULL,
  `id_user_role` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `inisial_karyawan` varchar(10) DEFAULT NULL,
  `id_lokasi` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
BEGIN;
INSERT INTO `tb_user` VALUES (23, 5, 2, 'mahendrawardana', '$2y$10$mGbiqqsuH7VMJYMG9HpEHOj6QjtWz316pF17vdQqASvfM2NwIwmn.', 'billing', '6', '2020-08-02 18:18:43', '2021-06-03 08:29:26', NULL);
INSERT INTO `tb_user` VALUES (35, 126, 2, 'anggita98', '$2y$10$biiZmDVWXnnsAA/eUGNSB.zvS1Cy.NIjyQLZhVxikNbrDr7779R6q', NULL, NULL, '2022-03-25 09:27:56', '2022-03-25 09:28:56', NULL);
INSERT INTO `tb_user` VALUES (36, 127, 2, 'putu', '$2y$10$w9saXYDc0qcRGj5PwtPmR.8xxpL.uwV1cMdMEWkebSDtcbioWBEyK', NULL, NULL, '2022-03-25 09:28:23', '2022-03-25 09:29:54', NULL);
INSERT INTO `tb_user` VALUES (37, 128, 22, 'lesa78', '$2y$10$J3Pi7lsXIljVp8VnQ/C6LuXWqjxbSNLNFPGX8PWUjnXd95TqNAqq2', NULL, NULL, '2022-04-25 14:53:57', '2022-04-25 14:53:57', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `role_keterangan` text DEFAULT NULL,
  `role_akses` text DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
BEGIN;
INSERT INTO `tb_user_role` VALUES (2, 'ADMINISTRATOR', NULL, '{\"dashboard\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"pembelian\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"penjualan\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"retur_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"penyesuaian_stok\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"kartu_stok\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"stok_alert\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"hutang_piutang\":{\"akses_menu\":true,\"hutang_lain_lain\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true},\"hutang_supplier\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true},\"piutang_lain_lain\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true},\"piutang_pelanggan\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"laporan\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"master_data\":{\"akses_menu\":true,\"satuan\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"jenis_barang\":{\"akses_menu\":true,\"list\":true,\"menu_akses\":true,\"create\":true,\"edit\":true,\"delete\":true},\"supplier\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"staf\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"pelanggan\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"role_user\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}}}', '2022-03-23 23:24:50', '2019-05-14 22:20:00', NULL);
INSERT INTO `tb_user_role` VALUES (22, 'STAFF GUDANG', '-', '{\"dashboard\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"pembelian\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"penjualan\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"retur_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"penyesuaian_stok\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"kartu_stok\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"stok_alert\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"hutang_piutang\":{\"akses_menu\":false,\"hutang_lain_lain\":{\"akses_menu\":false,\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false},\"hutang_supplier\":{\"akses_menu\":false,\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false},\"piutang_lain_lain\":{\"akses_menu\":false,\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false},\"piutang_pelanggan\":{\"akses_menu\":false,\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"laporan\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"master_data\":{\"akses_menu\":true,\"satuan\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"jenis_barang\":{\"akses_menu\":false,\"list\":false,\"menu_akses\":false,\"create\":false,\"edit\":false,\"delete\":false},\"supplier\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"staf\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"pelanggan\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"role_user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}}}', '2022-04-25 14:51:41', '2022-04-25 11:46:59', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
