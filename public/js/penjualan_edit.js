$(document).ready(function () {

    penjualan_select();

    kondisi_tombol_submit();
    bahan_modal();
    bahan_select();

    change_harga_jual();
    change_qty();
    change_potongan_harga_barang();
    change_ppn_nominal();

    change_biaya_tambahan();
    change_potongan();
    change_jumlah_bayar();

    change_harga_jual();
    change_qty();
    change_ppn_nominal();

});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');
var routePenjualanNoFaktur = $('#base-value').data('route-penjualan-no-faktur');

function bahan_modal() {
    $('.btn-modal-bahan').click(function () {
        var index = $(this).parents('tr').data('index');
        $('#base-value').data('index', index);
        $('#modal-bahan').modal('show');
    });
}

function bahan_select() {
    $('.table').on('click', '.btn-bahan-select', function () {
        var index = $('#base-value').data('index');
        var id_barang = $(this).parents('tr').data('id-barang');
        var kode_barang = $(this).parents('tr').data('kode-barang');
        var nama_barang = $(this).parents('tr').data('nama-barang');
        var harga_jual = $(this).parents('tr').data('harga-jual');
        var total_stok = $(this).parents('tr').data('total-stok');

        console.log(index);

        $('.table-bahan tr[data-index="' + index + '"] [name="id_barang[]"]').val(id_barang);
        $('.table-bahan tr[data-index="' + index + '"] .nama-barang').html(kode_barang + ' ' + nama_barang);
        $('.table-bahan tr[data-index="' + index + '"] [name="harga_jual[]"]').val(format_number(harga_jual));
        $('.table-bahan tr[data-index="' + index + '"] .stok-barang').html(format_number(total_stok));
        $('.table-bahan tr[data-index="' + index + '"] .btn-modal-stok-barang').prop('disabled', false);

        $('#modal-bahan').modal('hide');
    });
}

function penjualan_select() {
    $('#m_table_2').on('click', '.btn-pelanggan-select', function () {
        var id_pelanggan = $(this).parents('tr').data('id-pelanggan');
        var kode_pelanggan = $(this).parents('tr').data('plg-kode');
        var nama_pelanggan = $(this).parents('tr').data('plg-nama');
        var alamat_pelanggan = $(this).parents('tr').data('plg-alamat');
        var telp_pelanggan = $(this).parents('tr').data('plg-phone');

        $('[name="id_pelanggan"]').val(id_pelanggan);
        $('[name="plg_kode"]').val(kode_pelanggan);
        $('.nama_pelanggan').val("(" + kode_pelanggan + ") " + nama_pelanggan);
        $('.alamat_pelanggan').html(alamat_pelanggan);
        $('.telp_pelanggan').html(telp_pelanggan);
        $('#modal-pelanggan').modal('hide');

        // proses_no_faktur();
    });
}

function proses_no_faktur() {

    var kode_pelanggan = $('[name="plg_kode"]').val();
    var id_pelanggan = $('[name="id_pelanggan"]').val();

    var data_send = {
        kode_pelanggan: kode_pelanggan,
        id_pelanggan: id_pelanggan,
        _token: _token
    };

    $.ajax({
        beforeSend: function () {
            loading_start();
        },
        url: routePenjualanNoFaktur,
        type: "POST",
        data: data_send,
        success: function (no_faktur) {
            $('[name="no_faktur"]').val(no_faktur);
            $('.no_faktur').html(no_faktur);

            loading_finish();
        }
    });

}


function change_harga_jual() {
    $('[name="harga_jual[]"]').on('change keyup', function () {

        var index = $(this).parents('tr').data('index');

        proses_sub_total(index);

        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_ppn_nominal() {
    $('[name="ppn_nominal[]"]').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');
        proses_sub_total(index);
        proses_harga_net(index);
        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_qty() {
    $('[name="qty[]"]').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');

        proses_harga_net(index);
        proses_sub_total(index);

        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_potongan_harga_barang() {
    $('[name="potongan_harga_barang[]"]').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');

        proses_sub_total(index);

        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_biaya_tambahan() {
    $('[name="biaya_tambahan"]').on('change keyup', function () {
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_potongan() {
    $('[name="potongan"]').on('change keyup', function () {
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_jumlah_bayar() {
    $('[name="jumlah_bayar"]').on('change keyup', function () {
        proses_sisa_pembayaran();
    });
}


function proses_harga_net(index) {
    var harga_jual = $('.table-bahan tr[data-index="' + index + '"] [name="harga_jual[]"]').val();
    harga_jual = strtonumber(harga_jual);

    var ppn_nominal = $('.table-bahan tr[data-index="' + index + '"] [name="ppn_nominal[]"]').val();
    ppn_nominal = strtonumber(ppn_nominal);

    var harga_net = parseFloat(harga_jual) + parseFloat(ppn_nominal);
    harga_net = Math.ceil(harga_net);

    $('.table-bahan tr[data-index="' + index + '"] [name="harga_net[]"]').val(harga_net);
    $('.table-bahan tr[data-index="' + index + '"] .harga-net').html(format_number(harga_net));

}

function proses_sub_total(index) {
    var harga_net = $('.table-bahan tr[data-index="' + index + '"] [name="harga_jual[]"]').val();

    harga_net = strtonumber(harga_net);

    var qty = $('.table-bahan tr[data-index="' + index + '"] [name="qty[]"]').val();
    qty = strtonumber(qty);

    var potongan_harga_barang = $('.table-bahan tr[data-index="' + index + '"] [name="potongan_harga_barang[]"]').val();
    potongan_harga_barang = strtonumber(potongan_harga_barang);

    var sub_total = (parseFloat(harga_net) * parseFloat(qty)) - parseFloat(potongan_harga_barang);

    // console.log(harga_net+' - '+qty);

    sub_total = Math.ceil(sub_total);

    $('.table-bahan tr[data-index="' + index + '"] [name="sub_total[]"]').val(sub_total);
    $('.table-bahan tr[data-index="' + index + '"] .sub-total').html(format_number(sub_total));
}

function proses_total() {
    var total = 0;
    $('[name="sub_total[]"]').each(function () {
        var sub_total = $(this).val();
        total = parseFloat(total) + parseFloat(sub_total);
    });

    $('[name="total"]').val(total);
    $('.total').html(format_number(total));
}

function proses_grand_total() {
    var total = $('[name="total"]').val();
    var biaya_tambahan = $('[name="biaya_tambahan"]').val();
    biaya_tambahan = strtonumber(biaya_tambahan);

    var potongan = $('[name="potongan"]').val();
    potongan = strtonumber(potongan);

    var grand_total = parseFloat(total) + parseFloat(biaya_tambahan) - parseFloat(potongan);

    $('[name="grand_total"]').val(grand_total);
    $('.grand-total').html(format_number(grand_total));
}

function proses_sisa_pembayaran() {
    var grand_total = $('[name="grand_total"]').val();

    var jumlah_bayar = $('[name="jumlah_bayar"]').val();
    jumlah_bayar = strtonumber(jumlah_bayar);

    var sisa_pembayaran = parseFloat(grand_total) - parseFloat(jumlah_bayar);
    // console.log('proses_sisa_pembayaran', grand_total, jumlah_bayar, sisa_pembayaran);

    $('[name="sisa_pembayaran"]').val(sisa_pembayaran);
    $('.sisa-pembayaran').text(format_number(sisa_pembayaran));

    if (sisa_pembayaran > 0) {
        $('.div-jatuh-tempo').removeClass('m--hide');
    } else {
        $('.div-jatuh-tempo').addClass('m--hide');
    }

}

function kondisi_tombol_submit() {
    $('.btn-simpan').hide();
    $('.btn-selanjutnya').click(function () {
        $(this).hide();
        $('.btn-simpan').show();
    });
    $('.btn-modal-pembayaran-tutup').click(function () {
        $('.btn-simpan').hide();
        $('.btn-selanjutnya').show();
    });

    $('.btn-kembali').click(function (e) {
        e.preventDefault();
        if ($('#modal-pembayaran').hasClass('show')) {
            $('#modal-pembayaran').modal('hide');
            $('.btn-simpan').hide();
            $('.btn-selanjutnya').show();
        } else {
            var href = $(this).attr('href');
            window.location.href = href;
        }

        return false;
    });
}
