$(document).ready(function () {

    kondisi_tombol_submit();

    change_qty();
    change_potongan_harga_barang();

    change_biaya_tambahan();
    change_potongan();
    change_jumlah_bayar();

});

var base_url = $('#base-value').data('base-url');
var _token = $('#base-value').data('csrf-token');

var routeReturBarangNoFaktur = $('#base-value').data('route-retur-barang-no-faktur');
var routePenjualanStokBarang = $('#base-value').data('route-penjualan-stok-barang');
var routeReturPenjualanBarangList = $('#base-value').data('route-retur-penjualan-barang-list');


function change_qty() {
    $('[name="qty[]"]').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');

        proses_sub_total(index);
        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_potongan_harga_barang() {
    $('[name="potongan_harga_barang[]"]').on('change keyup', function () {
        var index = $(this).parents('tr').data('index');

        proses_sub_total(index);
        proses_total();
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_biaya_tambahan() {
    $('[name="biaya_tambahan"]').on('change keyup', function () {
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_potongan() {
    $('[name="potongan"]').on('change keyup', function () {
        proses_grand_total();
        proses_sisa_pembayaran();
    });
}

function change_jumlah_bayar() {
    $('[name="jumlah_bayar"]').on('change keyup', function () {
        proses_sisa_pembayaran();
    });
}

function proses_sub_total(index) {
    var harga_jual = $('.table-bahan tr[data-index="' + index + '"] [name="harga_jual[]"]').val();
    harga_jual = strtonumber(harga_jual);

    var qty = $('.table-bahan  tr[data-index="' + index + '"] [name="qty[]"]').val();
    qty = strtonumber(qty);

    var potongan_harga_barang = $('.table-bahan tr[data-index="' + index + '"] [name="potongan_harga_barang[]"]').val();
    potongan_harga_barang = strtonumber(potongan_harga_barang);

    //
    // console.log(harga_jual, qty, potongan_harga_barang);

    var sub_total = (parseFloat(harga_jual) * parseFloat(qty)) - parseFloat(potongan_harga_barang);
    sub_total = Math.ceil(sub_total);

    console.log(harga_jual, qty, potongan_harga_barang, sub_total);

    $('.table-bahan tr[data-index="' + index + '"] [name="sub_total[]"]').val(sub_total);
    $('.table-bahan tr[data-index="' + index + '"] .sub-total').html(format_number(sub_total));
}

function proses_total() {
    var total = 0;
    $('[name="sub_total[]"]').each(function () {
        var sub_total = $(this).val();
        total = parseFloat(total) + parseFloat(sub_total);
    });

    $('[name="total"]').val(total);
    $('.total').html(format_number(total));
}

function proses_grand_total() {
    var total = $('[name="total"]').val();
    var biaya_tambahan = $('[name="biaya_tambahan"]').val();
    biaya_tambahan = strtonumber(biaya_tambahan);

    var potongan = $('[name="potongan"]').val();
    potongan = strtonumber(potongan);

    var grand_total = parseFloat(total) + parseFloat(biaya_tambahan) - parseFloat(potongan);

    $('[name="grand_total"]').val(grand_total);
    $('.grand-total').html(format_number(grand_total));
}

function proses_sisa_pembayaran() {
    var grand_total = $('[name="grand_total"]').val();

    var jumlah_bayar = $('[name="jumlah_bayar"]').val();
    jumlah_bayar = strtonumber(jumlah_bayar);

    var sisa_pembayaran = parseFloat(grand_total) - parseFloat(jumlah_bayar);
    // console.log('proses_sisa_pembayaran', grand_total, jumlah_bayar, sisa_pembayaran);

    $('[name="sisa_pembayaran"]').val(sisa_pembayaran);
    $('.sisa-pembayaran').text(format_number(sisa_pembayaran));

}

function kondisi_tombol_submit() {
    $('.btn-simpan').hide();
    $('.btn-selanjutnya').click(function () {
        $(this).hide();
        $('.btn-simpan').show();
    });
    $('.btn-modal-pembayaran-tutup').click(function () {
        $('.btn-simpan').hide();
        $('.btn-selanjutnya').show();
    });

    $('.btn-kembali').click(function (e) {
        e.preventDefault();
        if ($('#modal-pembayaran').hasClass('show')) {
            $('#modal-pembayaran').modal('hide');
            $('.btn-simpan').hide();
            $('.btn-selanjutnya').show();
        } else {
            var href = $(this).attr('href');
            window.location.href = href;
        }

        return false;
    });
}
