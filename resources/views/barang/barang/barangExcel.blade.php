@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap Data Barang.xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">REKAP DATA BARANG</h2>
<br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th width="30">No</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Jenis</th>
        <th>Minimal Stok</th>
        <th>Total Stok</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_stok = 0;
    @endphp
    @foreach($data as $key => $row)
        @php
            $total_stok += intval($row->total_stok);
        @endphp
        <tr>
            <td align="center" class="string">{{ ++$key }}</td>
            <td class="string">{{ $row->brg_kode }}</td>
            <td class="string">{{ $row->brg_nama }}</td>
            <td class="string">{{ $row->jenis_barang->jbr_nama }}</td>
            <td class="string" align="center">{{ $row->brg_minimal_stok }}</td>
            <td class="string" align="right">{{ intval($row->total_stok) }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="5"><strong>TOTAL</strong></td>
        <td align="right">{{ intval($total_stok) }}</td>
    </tr>
    </tbody>
</table>



