<div class="m-portlet m-portlet--tab">
    <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">

        <input type="hidden" name="filter_type" value="filter">
        <input type="hidden" name="modal_show" value="{{ $modal_show }}">
        <input type="hidden" name="modal_nama" value="{{ $modal_nama }}">

        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="flaticon-search"></i>
                    </span>
                    <h4 class="m-portlet__head-text">
                        Filter Data
                    </h4>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row text-center">
                @if(in_array('date', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control m_datepicker_1_modal"
                               readonly
                               value="{{ $date_from }}"
                               placeholder="Select time"
                               name="date_from"/>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        <label class="form-control-label" style="padding-top: 10px">
                            sampai
                        </label>
                    </div>
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control m_datepicker_1_modal"
                               readonly
                               value="{{ $date_to }}"
                               placeholder="Select time"
                               name="date_to"/>
                    </div>
                @endif
                @if(in_array('kode_barang', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $kode_barang }}"
                               placeholder="Ketik Kode Barang ..."
                               name="kode_barang"/>
                    </div>
                @endif
                @if(in_array('kode_supplier', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $kode_supplier }}"
                                   placeholder="Ketik Kode Supplier ..."
                                   name="kode_supplier"/>
                        </div>
                @endif
                @if(in_array('nama_barang', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $nama_barang }}"
                               placeholder="Ketik Nama Barang ..."
                               name="nama_barang"/>
                    </div>
                @endif
                @if(in_array('nama_jenis_barang', $type))
                     <div class="col-lg-2 col-md-9 col-sm-12">
                         <input type='text'
                                class="form-control"
                                value="{{ $nama_jenis_barang }}"
                                placeholder="Ketik Nama Jenis Barang ..."
                                name="nama_jenis_barang"/>
                     </div>
                @endif
                @if(in_array('nama_satuan', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $nama_satuan }}"
                                   placeholder="Ketik Nama Satuan ..."
                                   name="nama_satuan"/>
                        </div>
                @endif
                @if(in_array('no_faktur_pembelian', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $no_faktur_pembelian }}"
                               placeholder="Ketik No Faktur Pembelian ..."
                               name="no_faktur_pembelian"/>
                    </div>
                @endif
                @if(in_array('no_faktur_penjualan', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $no_faktur_penjualan }}"
                               placeholder="Ketik No Faktur Penjualan ..."
                               name="no_faktur_penjualan"/>
                    </div>
                @endif
                @if(in_array('no_faktur_retur', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $no_faktur_retur }}"
                               placeholder="Ketik No Faktur Retur ..."
                               name="no_faktur_retur"/>
                    </div>
                @endif
                @if(in_array('no_faktur_supplier', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $no_faktur_supplier }}"
                               placeholder="Ketik No Faktur Supplier ..."
                               name="no_faktur_supplier"/>
                    </div>
                @endif
                @if(in_array('no_faktur_hutang', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $no_faktur_hutang }}"
                               placeholder="Ketik No Faktur Hutang ..."
                               name="no_faktur_hutang"/>
                    </div>
                @endif
                @if(in_array('username', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $username }}"
                                   placeholder="Ketik Username ..."
                                   name="username"/>
                        </div>
                @endif
                @if(in_array('no_faktur_piutang', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $no_faktur_piutang }}"
                               placeholder="Ketik No Faktur Piutang ..."
                               name="no_faktur_piutang"/>
                    </div>
                @endif
                @if(in_array('nama_supplier', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $nama_supplier }}"
                               placeholder="Ketik Nama Supplier ..."
                               name="nama_supplier"/>
                    </div>
                @endif
                @if(in_array('nama_staf', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $nama_staf }}"
                                   placeholder="Ketik Nama Staf ..."
                                   name="nama_staf"/>
                        </div>
                @endif
                @if(in_array('posisi', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $posisi }}"
                                   placeholder="Ketik Posisi ..."
                                   name="posisi"/>
                        </div>
                @endif
                @if(in_array('kode_pelanggan', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $kode_pelanggan }}"
                                   placeholder="Ketik Kode Pelanggan ..."
                                   name="kode_pelanggan"/>
                        </div>
                @endif
                @if(in_array('nama_pelanggan', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $nama_pelanggan }}"
                               placeholder="Ketik Nama Pelanggan ..."
                               name="nama_pelanggan"/>
                    </div>
                @endif
                @if(in_array('alamat_pelanggan', $type))
                    <div class="col-lg-2 col-md-9 col-sm-12">
                        <input type='text'
                               class="form-control"
                               value="{{ $alamat_pelanggan }}"
                               placeholder="Ketik Alamat Pelanggan ..."
                               name="alamat_pelanggan"/>
                    </div>
                @endif
                @if(in_array('nama_role', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $nama_role }}"
                                   placeholder="Ketik Role Name ..."
                                   name="nama_role"/>
                        </div>
                @endif
                @if(in_array('alamat', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $alamat }}"
                                   placeholder="Ketik Alamat ..."
                                   name="alamat"/>
                        </div>
                @endif
                @if(in_array('telepon', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $telepon }}"
                                   placeholder="Ketik Nomer Telepon ..."
                                   name="telepon"/>
                        </div>
                @endif
                @if(in_array('email', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $email }}"
                                   placeholder="Ketik Email ..."
                                   name="email"/>
                        </div>
                @endif
                @if(in_array('keterangan', $type))
                        <div class="col-lg-2 col-md-9 col-sm-12">
                            <input type='text'
                                   class="form-control"
                                   value="{{ $keterangan }}"
                                   placeholder="Ketik Keterangan ..."
                                   name="keterangan"/>
                        </div>
                @endif
            </div>
        </div>
        <div class="m-portlet__foot">
            <div class="row align-items-center">
                <div class="col-lg-12 m--align-center">

                    <button type="submit"
                            data-filter-type="filter"
                            class="form-send-filter-btn btn btn-accent m-btn--pill">
                        <i class="la la-search"></i> Filter Data
                    </button>
                    @if(in_array('date', $type))
                        <div class="m-btn-group btn-group">
                            <button type="button"
                                    class="btn btn-info  m-btn--pill dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-calendar"></i> Filter Waktu
                                <span class="sr-only">
                                    Toggle Dropdown
                                </span>
                            </button>
                            <div class="dropdown-menu">
                                <button
                                        type="submit"
                                        class="form-send-filter-btn dropdown-item"
                                        data-date-from="{{ date('d-m-Y') }}"
                                        data-date-to="{{ date('d-m-Y') }}"
                                        data-filter-type="filter">
                                    <i class="la la-search"></i> Data Hari Ini
                                </button>
                                <button type="submit"
                                        class="form-send-filter-btn dropdown-item"
                                        data-date-from="{{ date('01-m-Y') }}"
                                        data-date-to="{{ date('t-m-Y') }}"
                                        data-filter-type="filter">
                                    <i class="la la-search"></i> Data Bulan Ini
                                </button>
                                <button type="submit"
                                        class="form-send-filter-btn dropdown-item"
                                        data-date-from="{{ date('d-m-Y', strtotime($date_first)) }}"
                                        data-date-to="{{ date('t-m-Y') }}"
                                        data-filter-type="filter">
                                    <i class="la la-search"></i> Semua Data
                                </button>
                            </div>
                        </div>
                    @endif
                    @if(in_array('download', $type))
                        <div class="m-btn-group btn-group">
                            <button type="button"
                                    class="btn btn-primary  m-btn--pill dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> Tipe Download
                                <span class="sr-only">
                                    Toggle Dropdown
                                </span>
                            </button>
                            <div class="dropdown-menu">
                                <button type="submit"
                                        class="form-send-filter-btn dropdown-item"
                                        data-filter-type="download_pdf">
                                    <i class="la la-file-pdf-o"></i> Download ke PDF
                                </button>
                                <button type="submit"
                                        class="form-send-filter-btn dropdown-item"
                                        data-filter-type="download_excel">
                                    <i class="la la-file-excel-o"></i> Download ke Excel
                                </button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </form>
</div>