@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            {!! $date_filter !!}

            <ul class="nav nav-tabs" role="tablist" style="margin-bottom: -2px">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('hutangSupplierList') }}">
                        <h4>
                            <i class="la la-remove"></i>
                            Belum Lunas
                        </h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('hutangSupplierLunasList') }}">
                        <h4>
                            <i class="la la-check"></i>
                            Sudah Lunas
                        </h4>
                    </a>
                </li>
            </ul>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-bordered datatable-new"
                           data-url="{{ route('hutangSupplierLunasDataTable') }}"
                           data-column="{{ json_encode($datatable_column) }}"
                           data-data="{{ json_encode($table_data_post) }}">
                        <thead>
                        <tr>
                            <th width="20" class="text-center">No</th>
                            <th>Faktur Hutang</th>
                            <th>Faktur Pembelian</th>
                            <th>Nama Supplier</th>
                            <th>Tanggal Hutang</th>
                            <th>Jatuh Tempo</th>
                            <th>Waktu Lunas</th>
                            <th>Sisa</th>
                            <th>Keterangan</th>
                            <th>Menu</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
