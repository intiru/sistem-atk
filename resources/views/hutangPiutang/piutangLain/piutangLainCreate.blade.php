<form action="{{ route('piutangLainInsert') }}" method="post" class="m-form form-send" autocomplete="off">
    {{ csrf_field() }}

    <input type="hidden" name="ptl_no_faktur" value="{{ $ptl_no_faktur }}">

    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">
                                No Faktur
                            </label>
                            <div class="col-8 col-form-label">
                                {{ $ptl_no_faktur }}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Judul Hutang Lain Lain
                            </label>
                            <div class="col-8">
                                <textarea class="form-control m-input" name="ptl_keterangan"></textarea>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Jumlah Hutang
                            </label>
                            <div class="col-8">
                                <input class="form-control m-input input-numeral" type="text" name="ptl_total">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Tanggal Hutang
                            </label>
                            <div class="col-8">
                                <input class="form-control m-input m_datepicker_1_modal" name="ptl_tanggal" type="text" value="{{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Tanggal Jatuh Tempo
                            </label>
                            <div class="col-8">
                                <input class="form-control m-input m_datepicker_1_modal" name="ptl_tanggal_jatuh_tempo" type="text" value="{{ date('d-m-Y') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>