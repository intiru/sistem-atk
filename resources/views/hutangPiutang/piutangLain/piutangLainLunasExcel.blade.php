@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Piutang Lain Lunas ".$date_from." sampai ".$date_to.".xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">PIUTANG LAIN - LUNAS</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Faktur Piutang</th>
        <th>Tanggal Piutang</th>
        <th>Jatuh Tempo</th>
        <th>Total Piutang</th>
        <th>Sisa</th>
        <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_piutang = 0;
        $total_piutang_sisa = 0;
    @endphp
    @foreach($data_list as $row)
        @php
            $total_piutang += $row->ptl_total;
            $total_piutang_sisa += $row->ptl_sisa;
        @endphp
        <tr>
            <td class="string">{{ $no++ }}</td>
            <td class="string">{{ $row->ptl_no_faktur }}</td>
            <td class="string">{{ Main::format_date($row->ptl_tanggal) }}</td>
            <td class="string">{{ Main::format_date($row->ptl_tanggal_jatuh_tempo) }}</td>
            <td class="string" align="right">{{ $row->ptl_total }}</td>
            <td class="string" align="right">{{ $row->ptl_sisa }}</td>
            <td class="string">{{ $row->ptl_keterangan }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="4" align="center"><strong>TOTAL</strong></td>
        <td align="right"><strong>{{ intval($total_piutang) }}</strong></td>
        <td align="right"><strong>{{ intval($total_piutang_sisa) }}</strong></td>
        <td></td>
    </tr>
    </tbody>
</table>



