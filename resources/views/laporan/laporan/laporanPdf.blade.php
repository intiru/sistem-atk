<head>
    <title>LAPORAN {{ $date_from.' sampai '.$date_to }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">LAPORAN </h2>

<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<h4 align="center">Rangkuman</h4>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Profit</th>
        <th>Pembelian</th>
        <th>Penjualan</th>
        <th>Hutang</th>
        <th>Piutang</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>{{ Main::format_money($profit) }}</th>
        <th>{{ Main::format_money($pembelian) }}</th>
        <th>{{ Main::format_money($penjualan) }}</th>
        <th>{{ Main::format_money($hutang) }}</th>
        <th>{{ Main::format_money($piutang) }}</th>
    </tr>
    </tbody>
</table>
<br/>
<h4 align="center">Data Pembelian</h4>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Tanggal</th>
        <th>Nama Barang</th>
        <th>Harga Beli</th>
        <th>Qty</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_pembelian as $key => $row)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ Main::format_datetime($row->pbl_tanggal_order) }}</td>
            <td>{{ $row->brg_kode . ' ' . $row->brg_nama }}</td>
            <td class="number">{{ Main::format_money($row->pbd_harga_beli) }}</td>
            <td class="number">{{ Main::format_number($row->pbd_qty) }}</td>
            <td class="number">{{ Main::format_money($row->pbd_sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<br/>
<h4 align="center">Data Penjualan</h4>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Tanggal</th>
        <th>Nama Barang</th>
        <th>Harga Jual</th>
        <th>Qty</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_penjualan as $key => $row)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ Main::format_datetime($row->pjl_tanggal_penjualan) }}</td>
            <td>{{ $row->brg_kode . ' ' . $row->brg_nama }}</td>
            <td class="number">{{ Main::format_money($row->pjd_harga) }}</td>
            <td class="number">{{ Main::format_number($row->pjd_qty) }}</td>
            <td class="number">{{ Main::format_money($row->pjd_sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>

