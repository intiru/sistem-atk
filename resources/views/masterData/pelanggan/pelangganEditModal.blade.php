<form action="{{ route('pelangganUpdate', ['id'=>Main::encrypt($edit->id_pelanggan)]) }}" method="post" class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Pelanggan</label>
                            <input type="text" class="form-control m-input" name="plg_kode" value="{{ $edit->plg_kode }}" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Pelanggan</label>
                            <input type="text" class="form-control m-input" name="plg_nama" value="{{ $edit->plg_nama }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat</label>
                            <input type="text" class="form-control m-input" name="plg_alamat" value="{{ $edit->plg_alamat }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Telepon/WhatsApp</label>
                            <input type="text" class="form-control m-input" name="plg_telepon" value="{{ $edit->plg_telepon }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Email</label>
                            <input type="text" class="form-control m-input" name="plg_email" value="{{ $edit->plg_email }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Keterangan</label>
                            <textarea class="form-control" name="plg_keterangan">{{ $edit->plg_keterangan }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>