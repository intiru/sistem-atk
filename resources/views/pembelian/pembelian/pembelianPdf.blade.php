<head>
    <title>REKAP PEMBELIAN {{ $date_from.' sampai '.$date_to }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">REKAP PEMBELIAN</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<hr/>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>No Faktur Pembelian</th>
        <th>No Faktur Supplier</th>
        <th>Nama Supplier</th>
        <th>Tanggal Beli</th>
        <th>Jenis Pembayaran</th>
        <th>Qty</th>
        <th>Grand Total</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_qty = 0;
        $total_grand_total = 0;
    @endphp
    @foreach($data_pembelian as $key => $row)
        @php
            $total_qty += $row->total_qty;
            $total_grand_total += $row->pbl_grand_total;
        @endphp
        <tr>
            <td align="center">{{ ++$key }}</td>
            <td>{{ $row->pbl_no_faktur }}</td>
            <td>{{ $row->pbl_no_faktur_supplier }}</td>
            <td>{{ $row->spl_nama }}</td>
            <td>{{ $row->pbl_tanggal_order }}</td>
            <td>{{ $row->pbl_jenis_pembayaran }}</td>
            <td class="number">{{ Main::format_number($row->total_qty) }}</td>
            <td class="number">{{ Main::format_number($row->pbl_grand_total) }}</td>
        </tr>
    @endforeach
        <tr>
            <td colspan="6" align="center"><strong>TOTAL</strong></td>
            <td align="right"><strong>{{ Main::format_number($total_qty) }}</strong></td>
            <td align="right"><strong>{{ Main::format_number($total_grand_total) }}</strong></td>
        </tr>
    </tbody>
</table>



