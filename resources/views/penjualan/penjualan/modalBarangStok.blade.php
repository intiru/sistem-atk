<div class="modal" id="modal-barang-stok" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-info">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Stok Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable datatable-new-2">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Supplier</th>
                        <th>Satuan</th>
                        <th>Kode Batch</th>
                        <th>Expired</th>
                        <th>Konsinyasi</th>
                        <th>Qty</th>
                        <th>Harga Jual</th>
                        <th width="40">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>