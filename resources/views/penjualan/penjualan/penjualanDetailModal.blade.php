<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xxlg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Penjualan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body detail-info">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
                        <div class="col-9 col-form-label">
                            {{ $penjualan->pelanggan->plg_kode.' '.$penjualan->pelanggan->plg_nama }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Faktur Penjualan</label>
                        <div class="col-9 col-form-label">
                            {{ $penjualan->pjl_no_faktur }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Tanggal Penjualan</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date($penjualan->pjl_tanggal_penjualan) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jenis Pembayaran</label>
                        <div class="col-9 col-form-label">
                            {{ $penjualan->pjl_jenis_pembayaran }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                        <div class="col-9 col-form-label">
                            {{ $penjualan->pjl_keterangan }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Staff Pembuat Data</label>
                        <div class="col-9 col-form-label">
                            {{ $penjualan->user['karyawan']['nama_karyawan'] }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Staff Posting Data</label>
                        <div class="col-9 col-form-label">
                            {{ $penjualan->nama_karyawan }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Waktu Posting Data</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_datetime($penjualan->pjl_posting_waktu) }}
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
{{--                                <th>Satuan</th>--}}
{{--                                <th>Expired</th>--}}
                                <th>Harga Jual</th>
{{--                                <th>PPn (%)</th>--}}
{{--                                <th>PPn</th>--}}
{{--                                <th>Harga Net</th>--}}
                                <th>Qty</th>
                                <th>Potongan Harga</th>
                                <th>Sub Total</th>
{{--                                <th>Konsinyasi</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($penjualan->penjualan_detail as $key => $row)
                                <tr>
                                    <td>{{ ++$key }}.</td>
                                    <td>{{ $row->barang->brg_kode }}</td>
                                    <td>{{ $row->barang->brg_nama }}</td>
{{--                                    <td>{{ $row->stok_barang->satuan->stn_nama }}</td>--}}
{{--                                    <td>{{ Main::format_date_label($row->stok_barang->sbr_expired) }}</td>--}}
                                    <td>{{ Main::format_money($row->pjd_harga_jual) }}</td>
{{--                                    <td>{{ $row->pjd_ppn_persen }}</td>--}}
{{--                                    <td>{{ Main::format_money($row->pjd_ppn_nominal) }}</td>--}}
{{--                                    <td>{{ Main::format_money($row->pjd_harga_net) }}</td>--}}
                                    <td>{{ Main::format_number($row->pjd_qty) }}</td>
                                    <td>{{ Main::format_money($row->pjd_potongan_harga_barang) }}</td>
                                    <td>{{ Main::format_money($row->pjd_sub_total) }}</td>
{{--                                    <td>{!! Main::status($row->stok_barang->sbr_konsinyasi_status)  !!} </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br/>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Total</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($penjualan->pjl_total) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Biaya Tambahan</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($penjualan->pjl_biaya_tambahan) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Potongan</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($penjualan->pjl_potongan) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($penjualan->pjl_grand_total) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jumlah Bayar</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($penjualan->pjl_jumlah_bayar) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Sisa (Piutang)</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($penjualan->pjl_sisa_pembayaran) }}
                        </div>
                    </div>
                    @if($penjualan->pjl_sisa_pembayaran > 0)
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Jatuh Tempo</label>
                            <div class="col-9 col-form-label">
                                {{ Main::format_date_label($penjualan->pjl_jatuh_tempo) }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>