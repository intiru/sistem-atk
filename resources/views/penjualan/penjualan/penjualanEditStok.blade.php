@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('penjualan.penjualan.modalBarang')
    @include('penjualan.penjualan.modalBarangStok')
    @include('penjualan.penjualan.modalPelanggan')
    @include('penjualan.penjualan.trBarang')

    <form action="{{ route('penjualanUpdateStok', ['id_penjualan' => Main::encrypt($penjualan->id_penjualan)]) }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('penjualanList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="id_penjualan" value="{{ $penjualan->id_penjualan }}">
        <input type="hidden" name="urutan" value="{{ $penjualan->pjl_urutan }}">
        <input type="hidden" name="no_faktur" value="{{ $penjualan->pjl_no_faktur }}">
        <input type="hidden" name="id_pelanggan" value="{{ $penjualan->id_pelanggan }}">
        <input type="hidden" name="plg_kode" value="{{ $penjualan->pelanggan->plg_kode }}">
        <input type="hidden" name="total" value="{{ $penjualan->pjl_total }}">
        <input type="hidden" name="grand_total" value="{{ $penjualan->pjl_grand_total }}">
        <input type="hidden" name="sisa_pembayaran" value="{{ $penjualan->pjl_sisa_pembayaran }}">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-information"></i>
                            </span>
                                <h4 class="m-portlet__head-text">
                                    Informasi Penjualan
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Penjualan</label>
                                <div class="col-6">

                                    <input type="text"
                                           class="form-control m_datetimepicker"
                                           readonly=""
                                           name="tanggal"
                                           placeholder="Select date &amp; time"
                                           value="{{ date('d-m-Y H:i', strtotime($penjualan->pjl_tanggal_penjualan)) }}">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control nama_pelanggan"
                                               placeholder="{{ '( '.$penjualan->pelanggan->plg_kode.') '.$penjualan->pelanggan->plg_nama }}"
                                               disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-pelanggan">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nomer Telepon</label>
                                <div class="col-2 col-form-label telp_supplier">{{ $penjualan->pelanggan->plg_telepon }}</div>
                                <label for="example-text-input" class="col-2 col-form-label">Alamat</label>
                                <div class="col-4 col-form-label alamat_supplier">{{ $penjualan->pelanggan->plg_alamat }}</div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur Penjualan</label>
                                <div class="col-9 col-form-label no_faktur">{{ $penjualan->pjl_no_faktur }}</div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jenis Pembayaran</label>
                                <div class="col-6">
                                    <select name="pembayaran" class="form-control m-select2">
                                        <option value="cash" {{ $penjualan->pjl_jenis_pembayaran == 'cash' ? 'selected':'' }}>Cash</option>
                                        <option value="kredit" {{ $penjualan->pjl_jenis_pembayaran == 'kredit' ? 'selected':'' }}>Kredit</option>
                                        <option value="transfer" {{ $penjualan->pjl_jenis_pembayaran == 'transfer' ? 'selected':'' }}>Transfer</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan/Catatan</label>
                                <div class="col-6">
                                    <textarea class="form-control" name="pjl_keterangan">{{ $penjualan->pjl_keterangan }}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-cart"></i>
                                </span>
                                <h4 class="m-portlet__head-text">
                                    Data Penjualan
                                </h4>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-bahan-row-add btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Barang
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th align="center">Nama Barang</th>
                                <th>Harga Jual</th>
                                <th class="m--hide">PPN({{ $ppnPersen }}%)</th>
                                <th class="m--hide">Harga Net</th>
                                <th>Qty Stok</th>
                                <th>Qty Jual</th>
                                <th>Potongan Harga</th>
                                <th>Subtotal</th>
                                <th width="100">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($penjualan->penjualan_detail as $key => $row)
                                    @php
                                        $stok_saat_ini = \app\Models\mStokBarang
                                                            ::where('id_barang', $row->id_barang)
                                                            ->sum('sbr_qty');
                                        $penjualan_qty_barang_total = \app\Models\mPenjualanDetail
                                                            ::where([
                                                                'id_penjualan' => $penjualan->id_penjualan,
                                                                'id_barang' => $row->id_barang
                                                            ])
                                                            ->sum('pjd_qty');

                                        $stok_saat_ini_plus_terjual = $stok_saat_ini + $penjualan_qty_barang_total;
                                    @endphp
                                    <tr data-index="{{ $key }}">
                                        <td class="m--hide">
                                            <input type="hidden" name="id_penjualan_detail[]" value="{{ $row->id_penjualan_detail }}">
                                            <input type="hidden" name="id_barang[]" value="{{ $row->id_barang }}">
                                            <input type="hidden" name="id_stok_barang[]" value="{{ $row->id_stok_barang }}">
                                            <input type="hidden" name="harga_beli[]" value="{{ $row->pjd_harga_beli }}">
                                            <input type="hidden" name="harga_net[]" value="{{ $row->pjd_harga_net }}">
                                            <input type="hidden" name="sub_total[]" value="{{ $row->pjd_sub_total }}">
                                        </td>
                                        <td align=" center">
                                            <div class="nama-barang">{{ $row->barang->brg_kode.' '.$row->barang->brg_nama }}</div>
                                            <br />
                                            <button class="btn-modal-bahan btn btn-accent btn-sm m-btn--pill"
                                                    type="button">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </td>
                                        <td align="center" class="m--hide">
                                            <div class="nama-stok-barang"></div>
                                            <br />
                                            <button class="btn-modal-stok-barang btn btn-accent btn-sm m-btn--pill"
                                                    type="button" disabled>
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </td>
                                        <td>
                                            <input type="text" name="harga_jual[]" class="form-control m-input input-numeral" value="{{ Main::format_number($row->pjd_harga_jual) }}">
                                        </td>
                                        <td class="ppn-nominal m--hide">
                                            <input type="text" name="ppn_nominal[]" class="form-control m-input input-numeral" value="{{ Main::format_number($row->pjd_ppn_nominal) }}">
                                        </td>
                                        <td class="harga-net m--hide">
                                            <input type="text" name="harga_net[]" class="form-control m-input input-numeral" value="{{ Main::format_number($row->pjd_harga_net) }}">
                                        </td>
                                        <td>
                                            <div class="stok-barang">{{ Main::format_number($stok_saat_ini) }}</div>
                                        </td>
                                        <td>
                                            <input type="text" name="qty[]" class="form-control m-input input-numeral" value="{{ Main::format_number($row->pjd_qty) }}">
                                        </td>
                                        <td>
                                            <input type="text" name="potongan_harga_barang[]" class="form-control m-input input-numeral" value="{{ Main::format_number($row->pjd_potongan_harga_barang) }}">
                                        </td>
                                        <td class="sub-total">{{ Main::format_number($row->pjd_sub_total) }}</td>
                                        <td>
                                            <button type="button" class="btn-bahan-row-delete btn m-btn--pill btn-danger btn-sm"
                                                    data-confirm="false">
                                                <i class="la la-remove"></i> Hapus
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label total">{{ Main::format_number($penjualan->pjl_total) }}</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="biaya_tambahan" value="{{ Main::format_number($penjualan->pjl_biaya_tambahan) }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="potongan" value="{{ Main::format_number($penjualan->pjl_potongan) }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label grand-total"
                                             style="font-size: 20px; font-weight: bold">{{ Main::format_number($penjualan->pjl_grand_total) }}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Jumlah
                                            Bayar</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="jumlah_bayar" value="{{ Main::format_number($penjualan->pjl_jumlah_bayar) }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Sisa
                                            (Piutang)</label>
                                        <div class="col-8 col-form-label sisa-pembayaran"
                                             style="font-size: 20px; font-weight: bold">{{ Main::format_number($penjualan->pjl_sisa_pembayaran) }}
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row div-jatuh-tempo {{ $penjualan->pjl_sisa_pembayaran > 0 ? '':'m--hide' }}">
                                        <label for="example-text-input" class="col-4 col-form-label">Tanggal Jatuh
                                            Tempo</label>
                                        <div class="col-8">
                                            <input type="text"
                                                   name="tanggal_jatuh_tempo"
                                                   class="form-control m-input m_datepicker_top"
                                                   readonly=""
                                                   value="{{ date('d-m-Y', strtotime($penjualan->pjl_jatuh_tempo)) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Perbarui Penjualan</span>
                        </span>
            </button>

            <a href="{{ route('penjualanList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection