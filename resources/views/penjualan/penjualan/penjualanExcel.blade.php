@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap Penjualan ".$date_from." sampai ".$date_to.".xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">REKAP PENJUALAN</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>No Faktur Penjualan</th>
        <th>Tanggal Penjualan</th>
        <th>Jenis Pembayaran</th>
        <th>Qty</th>
        <th>Grand Total</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_qty = 0;
        $total_grand_total = 0;
    @endphp
    @foreach($data_list as $key => $row)
        @php
            $total_qty += $row->total_qty;
            $total_grand_total += $row->pjl_grand_total;
        @endphp
        <tr>
            <td align="center" class="string">{{ ++$key }}</td>
            <td class="string">{{ $row->pjl_no_faktur }}</td>
            <td class="string">{{ $row->pjl_tanggal_penjualan }}</td>
            <td class="string" align="center">{{ $row->pjl_jenis_pembayaran }}</td>
            <td class="string" align="right">{{ intval($row->total_qty) }}</td>
            <td class="string" align="right">{{ intval($row->pjl_grand_total) }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="4" align="center"><strong>TOTAL</strong></td>
        <td align="right"><strong>{{ intval($total_qty) }}</strong></td>
        <td align="right"><strong>{{ intval($total_grand_total) }}</strong></td>
    </tr>
    </tbody>
</table>



