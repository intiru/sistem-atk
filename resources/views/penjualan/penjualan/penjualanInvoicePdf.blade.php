<head>
    <title>Nota Retur Barang {{ $penjualan->pjl_no_faktur }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">INVOICE</h2>
<br/>
<br/>
<div class="div-2">
    <table>
        <tr>
            <td width="80"><strong>Kepada Yth</strong></td>
            <td>: {{ $penjualan->pelanggan->plg_nama }}</td>
        </tr>
        <tr>
            <td><strong>Alamat</strong></td>
            <td>: {{ $penjualan->pelanggan->plg_alamat }}</td>
        </tr>
    </table>
</div>
<div class="div-2">
    <table>
        <tr>
            <td width="120"><strong>No Faktur Penjualan</strong></td>
            <td>: {{ $penjualan->pjl_no_faktur }}</td>
        </tr>
        <tr>
            <td><strong>Tanggal Faktur Penjualan</strong></td>
            <td>: {{ Main::format_date_label($penjualan->pjl_tanggal_penjualan) }}</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Harga</th>
        <th>Qty</th>
        <th>Potongan Harga</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($penjualan->penjualan_detail as $key => $row)
        <tr>
            <td align="center">{{ ++$key }}</td>
            <td class="string">{{ $row->barang->brg_kode }}</td>
            <td class="string">{{ $row->barang->brg_nama }}</td>
            <td class="string" align="right">{{ Main::format_number($row->pjd_harga_jual) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->pjd_qty) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->pjd_potongan_harga_barang) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->pjd_sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="6">Total Harga</th>
        <th class="text-right">{{ Main::format_number($penjualan->pjl_grand_total) }}</th>
    </tr>
    </tfoot>
</table>
<br/>
<br/>
<div class="div-2">
    <h4 align="center">
        Hormat Kami,<br/>
    </h4>
    <br/>
    <br/>
    <br/>
    <br/>
    <h4 align="center">
        <u><strong>Bambang Adi Yuswanto, ST</strong></u><br />
        Marketing
    </h4>

</div>
<div class="div-2">
    <h4 align="center">
        Tanggal, {{ date('d F Y') }}<br/>
    </h4>
    <br/>
    <br/>
    <br/>
    <br/>
    <h4 align="center">
        <u><strong>{{ $penjualan->pelanggan->plg_nama }}</strong></u><br />
        Penerima Barang
    </h4>
</div>





