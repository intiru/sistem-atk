@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="alert alert-info" role="alert">
                <strong>
                    Perhatian !!
                </strong><br/>
                <p>Ada beberapa cara untuk menggunakan Menu Penjualan, sebagai berikut:</p>
                <ol>
                    <li>Data Penjualan Barang Baru dapat disimpan sementara tanpa mempengaruhi stok, hanya dengan saat
                        membuat Penjualan
                        tekan Tombol <strong>Simpan Data Dulu</strong><br/>
                        Sehingga, dapat beraktifitas ke Menu yang lainnya.
                    </li>
                    <li>Data Penjualan Barang Baru yang di <strong>Simpan Sementara</strong> Dapat dilakukan proses
                        keluar <strong>Stok Barang</strong>, <br/>
                        dengan Menu <strong>Posting Penjualan</strong> di Setiap baris data Penjualan
                    </li>
                    <li>Data Penjualan Barang Baru dapat langsung disimpan dan masuk ke dalam stok dengan Klik Menu
                        <strong>Simpan & Posting Penjualan</strong></li>
                    <li>Edit Data Penjualan dilakukan dengan 2 cara, yaitu:
                        <ol>
                            <li>Saat Penjualan Barang Baru di <strong>Simpan Sementara</strong>, maka dapat memperbarui
                                keseluruhan data, mulai dari <strong>Data Barang, Data Pelanggan serta Data
                                    Pembayaran</strong></li>
                            <li>Saat Penjualan Barang Baru di <strong>Simpan & Posting Penjualan</strong>, maka hanya dapat memperbarui data <strong>Harga Barang dan Data Pembayaran</strong></li>
                        </ol>
                    </li>
                    <li>Hapus Data Penjualan Barang akan memproses ke <strong>Penambahan Stok Barang</strong></li>
                </ol>
            </div>

            {!! $date_filter !!}

            <div class="m-portlet m-portlet--mobile akses-list">

                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-list"></i>
                            </span>
                            <h4 class="m-portlet__head-text">
                                Data Penjualan
                            </h4>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <a href="{{ route('penjualanCreate') }}"
                           class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Penjualan</span>
                        </span>
                        </a>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <table class="table table-bordered datatable-new"
                           data-url="{{ route('penjualanDataTable') }}"
                           data-column="{{ json_encode($datatable_column) }}"
                           data-data="{{ json_encode($table_data_post) }}">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>No Faktur Penjualan</th>
                            <th>Nama Pelanggan</th>
                            <th>Tanggal Penjualan</th>
                            <th>Qty</th>
                            <th>Grand Total</th>
                            <th>Status Data</th>
                            <th width="100">Menu</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
