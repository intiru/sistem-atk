<head>
    <title>REKAP PENJUALAN {{ $date_from.' - '.$date_to }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">REKAP PENJUALAN</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>No Faktur Penjualan</th>
        <th>Tanggal Penjualan</th>
        <th>Jenis Pembayaran</th>
        <th>Qty</th>
        <th>Grand Total</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_qty = 0;
        $total_grand_total = 0;
    @endphp
    @foreach($data_list as $key => $row)
        @php
            $total_qty += $row->total_qty;
            $total_grand_total += $row->pjl_grand_total;
        @endphp
        <tr>
            <td align="center">{{ ++$key }}</td>
            <td class="string">{{ $row->pjl_no_faktur }}</td>
            <td class="string">{{ Main::format_datetime($row->pjl_tanggal_penjualan) }}</td>
            <td class="string" align="center">{{ $row->pjl_jenis_pembayaran }}</td>
            <td class="string" align="right">{{ Main::format_number($row->total_qty) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->pjl_grand_total) }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="4" align="center"><strong>TOTAL</strong></td>
        <td align="right"><strong>{{ Main::format_number($total_qty) }}</strong></td>
        <td align="right"><strong>{{ Main::format_number($total_grand_total) }}</strong></td>
    </tr>
    </tbody>
</table>



