<table class="bahan-row m--hide">
    <tr>
        <td class="m--hide">
            <input type="hidden" name="id_barang[]">
            <input type="hidden" name="id_stok_barang[]">
            <input type="hidden" name="harga_beli[]" value="0">
            <input type="hidden" name="harga_net[]" value="0">
            <input type="hidden" name="sub_total[]" value="0">
        </td>
        <td align=" center">
            <div class="nama-barang"></div>
            <br />
            <button class="btn-modal-bahan btn btn-accent btn-sm m-btn--pill"
                    type="button">
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td align="center" class="m--hide">
            <div class="nama-stok-barang"></div>
            <br />
            <button class="btn-modal-stok-barang btn btn-accent btn-sm m-btn--pill"
                    type="button" disabled>
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td>
            <input type="text" name="harga_jual[]" class="form-control m-input input-numeral" value="0">
        </td>
        <td class="ppn-nominal m--hide">
            <input type="text" name="ppn_nominal[]" class="form-control m-input input-numeral" value="0">
        </td>
        <td class="harga-net m--hide">
            <input type="text" name="harga_net[]" class="form-control m-input input-numeral" value="0">
        </td>
        <td>
            <div class="stok-barang"></div>
        </td>
        <td>
            <input type="text" name="qty[]" class="form-control m-input input-numeral" value="0">
        </td>
        <td>
            <input type="text" name="potongan_harga_barang[]" class="form-control m-input input-numeral" value="0">
        </td>
        <td class="sub-total">0</td>
        <td>
            <button type="button" class="btn-bahan-row-delete btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
</table>