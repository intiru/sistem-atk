<style type="text/css">
    #swal2-content {
        text-align: left;
    }
</style>

<form action="{{ route('penyesuaianStokUpdate') }}"
      method="post"
      style="width: 100%"
      data-redirect="{{ route('penyesuaianStokList') }}"
      data-alert-show="true"
      data-alert-field-message="true"
      class="form-send">

    <input type="hidden" name="id_barang" value="{{ $id_barang }}">
    <input type="hidden" name="tipe" value="{{ $check_stok == 0 ? 'create':'update' }}">
    {{ csrf_field() }}

    <div class="modal" id="modal-general" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header btn-info">
                    <h5 class="modal-title m--font-light" id="exampleModalLabel">Stok Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped- table-bordered table-hover table-checkable datatable-new-2">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Satuan</th>
                            <th>Supplier</th>
                            <th>Stok Sekarang</th>
                            <th>Stok Penyesuaian</th>
                            <th>Keterangan Penyesuaian Stok</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($stok_barang as $key => $row)
                            <tr>
                                <td class="m--hide">
                                    <input type="hidden" name="nama_barang[]"
                                           value="{{ $row->barang->brg_kode.' '.$row->barang->brg_nama }}">
                                    <input type="hidden" name="id_stok_barang[]" value="{{ $row->id_stok_barang }}">
                                    <input type="hidden" name="hps_qty_awal[]" value="{{ $row->sbr_qty }}">
                                </td>
                                <td>{{ ++$key }}</td>
                                <td>{{ $row->barang->brg_kode }}</td>
                                <td>{{ $row->barang->brg_nama }}</td>
                                <td>{{ $row->barang->satuan['stn_nama'] }}</td>
                                <td>{{ $row->supplier['spl_kode'].' '.$row->supplier['spl_nama'] }}</td>
                                <td align="right">{{ Main::format_number($row->sbr_qty) }}</td>
                                <td><input type="text" class="input-numeral form-control" name="hps_qty_akhir[]"
                                           value="{{ $row->sbr_qty }}"></td>
                                <td><textarea class="form-control" name="hps_keterangan[]"></textarea></td>
                            </tr>
                        @endforeach
                        @if($check_stok == 0)
                            <tr>
                                <td class="m--hide">
                                    <input type="hidden" name="nama_barang[]"
                                           value="{{ $barang->brg_kode.' '.$barang->brg_nama }}">
                                    <input type="hidden" name="id_stok_barang[]" value="0">
                                    <input type="hidden" name="hps_qty_awal[]" value="0">
                                </td>
                                <td>1.</td>
                                <td>{{ $barang->brg_kode }}</td>
                                <td>{{ $barang->brg_nama }}</td>
                                <td>{{ $barang->satuan['stn_nama'] }}</td>
                                <td>-</td>
                                <td align="right">0</td>
                                <td><input type="text" class="input-numeral form-control" name="hps_qty_akhir[]"
                                           value="0"></td>
                                <td><textarea class="form-control" name="hps_keterangan[]"></textarea></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">

                    <button type="submit"
                            class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-check"></i>
                                <span>Proses Penyesuaian Stok</span>
                            </span>
                    </button>
                    <button type="button"
                            class="btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            data-dismiss="modal">
                            <span>
                                <i class="la la-angle-double-left"></i>
                                <span>Kembali</span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>