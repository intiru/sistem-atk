<div class="modal" id="modal-penjualan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header btn-success">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Penjualan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                {!! $date_filter !!}

                <div class="m-portlet m-portlet--tab">
                    <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">

                        <input type="hidden" name="filter_type" value="filter">

                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon">
                                        <i class="flaticon-cart"></i>
                                    </span>
                                    <h4 class="m-portlet__head-text">
                                        Data Penjualan
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row text-center">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <table class="table table-bordered datatable-new"
                                           data-url="{{ route('returBarangPenjualanDataTable') }}"
                                           data-column="{{ json_encode($datatable_column) }}"
                                           data-data="{{ json_encode($table_data_post) }}"
                                           data-params-get="{{ json_encode(request()->all()) }}">
                                        <thead>
                                        <tr>
                                            <th width="30">No</th>
                                            <th>No Faktur Penjualan</th>
                                            <th>Nama Pelanggan</th>
                                            <th>Tanggal Penjualan</th>
                                            <th>Jenis Pembayaran</th>
                                            <th>Qty</th>
                                            <th>Grand Total</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer text-center">
                <center>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <i class="flaticon-close"></i> Tutup Pop Up Penjualan
                    </button>
                </center>
            </div>
        </div>
    </div>
</div>