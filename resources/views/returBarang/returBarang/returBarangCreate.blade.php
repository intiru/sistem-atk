@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/retur_barang.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('returBarang.returBarang.modalPenjualan')

    <form action="{{ route('returBarangInsert') }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('returBarangList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          data-confirm-status="true"
          data-confirm-message="Data akan masuk ke stok barang, jika terjadi kesalahan input jumlah barang, silahkan perbaiki di <strong>Menu Penyesuaian Stok</strong>"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="id_penjualan" value="{{ $id_penjualan }}">
        <input type="hidden" name="id_pelanggan" value="{{ $penjualan['id_pelanggan'] }}">
        <input type="hidden" name="urutan" value="">
        <input type="hidden" name="no_faktur" value="{{ $rbr_no_faktur }}">
        <input type="hidden" name="plg_kode" value="{{ $penjualan['plg_kode'] }}">
        <input type="hidden" name="total" value="0">
        <input type="hidden" name="grand_total" value="0">
        <input type="hidden" name="sisa_pembayaran" value="0">

        <div class="m-grid__item m-grid__item--fluid m-wrapper retur-barang-page">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-information"></i>
                            </span>
                                <h4 class="m-portlet__head-text">
                                    Informasi Retur Barang
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Retur Barang</label>
                                <div class="col-6">

                                    <input type="text"
                                           class="form-control m_datetimepicker"
                                           readonly=""
                                           name="tanggal"
                                           placeholder="Select date &amp; time"
                                           value="{{ date('d-m-Y H:i') }}">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur Penjualan</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control pjl-no-faktur"
                                               placeholder="{{ $penjualan['pjl_no_faktur'] }}"
                                               disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-penjualan">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nama Pelanggan</label>
                                <div class="col-2">
                                    <div class="input-group col-form-label plg-nama">{{ $penjualan['plg_nama'] }}</div>
                                </div>
                                <label for="example-text-input" class="col-2 col-form-label">Alamat</label>
                                <div class="col-4">
                                    <div class="input-group col-form-label plg-alamat">{{ $penjualan['plg_alamat'] }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur Retur Barang</label>
                                <div class="col-9 col-form-label rbr-no-faktur">{{ $rbr_no_faktur }}</div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jenis Pembayaran</label>
                                <div class="col-6">
                                    <select name="jenis_pembayaran" class="form-control m-select2">
                                        <option value="cash">Cash</option>
                                        <option value="kredit">Kredit</option>
                                        <option value="transfer">Transfer</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan/Catatan</label>
                                <div class="col-6">
                                    <textarea class="form-control" name="keterangan"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile retur-barang-penjualan">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-cart"></i>
                                </span>
                                <h4 class="m-portlet__head-text">
                                    Data Penjualan Sebelumnya
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Harga Jual</th>
                                <th>Qty Penjualan Awal</th>
                                <th>Qty Penjualan Sekarang</th>
                                <th>Qty Retur</th>
                                <th>Potongan Harga</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($penjualan_detail as $key => $row)
                                <tr data-index="{{ $key }}">
                                    <td class="m--hide">
                                        <input type="hidden" name="id_penjualan_detail[]" value="{{ $row->id_penjualan_detail }}">
                                        <input type="hidden" name="harga_jual[]" value="{{ $row->pjd_harga_jual }}">
                                        <input type="hidden" name="qty_jual[]" value="{{ $row->pjd_qty_sisa }}">
                                        <input type="hidden" name="id_barang[]" value="{{ $row->id_barang }}">
                                        <input type="hidden" name="sub_total[]" value="{{ $row->pjd_sub_total }}">
                                    </td>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->barang->brg_kode }}</td>
                                    <td>{{ $row->barang->brg_nama }}</td>
                                    <td>{{ Main::format_number($row->pjd_harga_jual) }}</td>
                                    <td>{{ Main::format_number($row->pjd_qty) }}</td>
                                    <td>{{ Main::format_number($row->pjd_qty_sisa) }}</td>
                                    <td class="qty">
                                        <input type="text" name="qty[]" class="form-control m-input input-numeral" value="0">
                                    </td>
                                    <td class="potongan">
                                        <input type="text" name="potongan_harga_barang[]" class="form-control m-input input-numeral" value="0">
                                    </td>
                                    <td class="sub-total">0</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label total">0</div>
                                    </div>
                                    <div class="form-group m-form__group row m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">
                                            Biaya Tambahan
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="biaya_tambahan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="potongan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label grand-total"  style="font-size: 20px; font-weight: bold">
                                            0
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group m-form__group row m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">
                                            Jumlah Bayar
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="jumlah_bayar" value="0">
                                        </div>
                                    </div>
{{--                                    <div class="form-group m-form__group row">--}}
{{--                                        <label for="example-text-input" class="col-4 col-form-label">--}}
{{--                                            Sisa Hutang</label>--}}
{{--                                        <div class="col-8 col-form-label sisa-pembayaran"--}}
{{--                                             style="font-size: 20px; font-weight: bold">--}}
{{--                                            0--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Cetak Retur Barang</span>
                        </span>
            </button>

            <a href="{{ route('returBarangList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection