<head>
    <title>REKAP RETUR BARANG {{ $date_from.' - '.$date_to }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">REKAP RETUR BARANG</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>No Faktur Penjualan</th>
        <th>No Faktur Retur</th>
        <th>Nama Pelanggan</th>
        <th>Tanggal Retur Barang</th>
        <th>Jenis Pembayaran</th>
        <th>Total QTY Retur</th>
        <th>Grand Total</th>
        <th>Jumlah Pengembalian Uang</th>
        <th>Alasan Pengembalian Uang</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_qty = 0;
        $total_grand_total = 0;
        $total_pengembalian_uang = 0;
    @endphp
    @foreach($data_list as $key => $row)
        @php
            $total_qty += $row->total_qty;
            $total_grand_total += $row->rbr_grand_total;
            $total_pengembalian_uang += $row->rbr_jumlah_pengembalian_uang;
        @endphp
        <tr>
            <td align="center" class="string">{{ ++$key }}</td>
            <td class="string">{{ $row->pjl_no_faktur }}</td>
            <td class="string">{{ $row->rbr_no_faktur }}</td>
            <td class="string">{{ $row->plg_nama }}</td>
            <td class="string" align="center">{{ Main::format_datetime($row->rbr_tanggal_retur) }}</td>
            <td class="string" align="center">{{ $row->rbr_jenis_pembayaran }}</td>
            <td class="string" align="right">{{ Main::format_number($row->total_qty) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->rbr_grand_total) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->rbr_jumlah_pengembalian_uang) }}</td>
            <td class="string" align="left">{{ $row->rbr_jumlah_pengembalian_uang_penjelasan }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="6" align="center"><strong>TOTAL</strong></td>
        <td align="right"><strong>{{ Main::format_number($total_qty) }}</strong></td>
        <td align="right"><strong>{{ Main::format_number($total_grand_total) }}</strong></td>
        <td align="right"><strong>{{ Main::format_number($total_pengembalian_uang) }}</strong></td>
        <td></td>
    </tr>
    </tbody>
</table>
</body>



