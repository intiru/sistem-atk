<table class="table-bahan table table-striped table-bordered table-hover table-checkable">
    <thead>
    <tr>
        <th>No</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Harga Jual</th>
        <th>Qty</th>
        <th>Potongan Harga</th>
        <th>Subtotal</th>
    </tr>
    </thead>
    <tbody>
    @foreach($penjualan_detail as $row)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $row->brg_kode }}</td>
            <td>{{ $row->brg_nama }}</td>
            <td>{{ Main::format_number($row->pjd_harga_jual) }}</td>
            <td>
                <input type="text" name="qty[]" class="form-control m-input input-numeral" value="{{ Main::format_number($row->pjd_qty) }}">
            </td>
            <td>
                <input type="text" name="qty[]" class="form-control m-input input-numeral" value="{{ Main::format_number($row->pjd_potongan_harga_barang) }}">
            </td>
            <td>{{ Main::format_number($row->pjd_sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
