<head>
    <title>Nota Retur Barang {{ $retur_barang->rbr_no_faktur }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">NOTA RETUR BARANG</h2>
<br/>
<br/>
<div class="div-2">
    <table>
        <tr>
            <td width="80"><strong>Nama Pelanggan</strong></td>
            <td>: {{ $retur_barang->pelanggan->plg_nama }}</td>
        </tr>
        <tr>
            <td><strong>Alamat Pelanggan</strong></td>
            <td>: {{ $retur_barang->pelanggan->plg_alamat }}</td>
        </tr>
    </table>
</div>
<div class="div-2">
    <table>
        <tr>
            <td width="100"><strong>No Faktur Retur</strong></td>
            <td>: {{ $retur_barang->rbr_no_faktur }}</td>
        </tr>
        <tr>
            <td><strong>Tanggal Faktur Retur</strong></td>
            <td>: {{ $retur_barang->rbr_tanggal_retur }}</td>
        </tr>
    </table>
</div>
<div class="clearfix"></div>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Qty Retur</th>
        <th>Harga</th>
        <th>Potongan Harga</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($retur_barang->retur_barang_detail as $key => $row)
        <tr>
            <td align="center">{{ ++$key }}</td>
            <td class="string">{{ $row->barang->brg_kode }}</td>
            <td class="string">{{ $row->barang->brg_nama }}</td>
            <td class="string" align="right">{{ Main::format_number($row->rbd_qty) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->rbd_harga_jual) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->rbd_potongan_harga_barang) }}</td>
            <td class="string" align="right">{{ Main::format_number($row->rbd_sub_total) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="6">Total Harga</th>
        <th class="text-right">{{ Main::format_number($retur_barang->rbr_grand_total) }}</th>
    </tr>
    </tfoot>
</table>
<br />
<br />
<div class="div-2">
    <h4 align="center">
        Tanggal, {{ date('d F Y') }}<br />
        Pelanggan
    </h4>
    <br />
    <br />
    <br />
    <br />
    <h4 align="center">({{ $retur_barang->pelanggan->plg_nama }})</h4>
</div>
<div class="div-2">
    <h4 align="center">
        <span style="color: white">tanggal</span><br />
        Penyedia
    </h4>
    <br />
    <br />
    <br />
    <br />
    <h4 align="center">(Bambang Adi Yuswanto, ST)</h4>
</div>





